﻿CREATE TABLE [pacs].[DicomDictionary] (
    [InternalDicomDictionaryID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [Tag]                       NVARCHAR (10)  NOT NULL,
    [Keyword]                   NVARCHAR (256) NOT NULL,
    [VR]                        NVARCHAR (2)   NOT NULL,
    [VM]                        NVARCHAR (64)  NOT NULL,
    [IsActive]                  BIT            CONSTRAINT [DF_DicomDictionary_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]                   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_DicomDictionary] PRIMARY KEY CLUSTERED ([InternalDicomDictionaryID] ASC),
    CONSTRAINT [U_DicomDictionary_Tag] UNIQUE NONCLUSTERED ([Tag] ASC)
);

