﻿CREATE TABLE [RedGateLocal].[DeploymentMetadata] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (MAX) NOT NULL,
    [Type]            VARCHAR (50)   NOT NULL,
    [Action]          VARCHAR (50)   NOT NULL,
    [By]              NVARCHAR (128) DEFAULT (original_login()) NOT NULL,
    [As]              NVARCHAR (128) DEFAULT (suser_sname()) NOT NULL,
    [CompletedDate]   DATETIME       DEFAULT (getdate()) NOT NULL,
    [With]            NVARCHAR (128) DEFAULT (app_name()) NOT NULL,
    [BlockId]         VARCHAR (50)   NOT NULL,
    [InsertedSerial]  BINARY (8)     DEFAULT (@@dbts+(1)) NOT NULL,
    [UpdatedSerial]   ROWVERSION     NOT NULL,
    [MetadataVersion] VARCHAR (50)   NOT NULL,
    [Hash]            NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This table records deployments with migration scripts. Learn more: http://rd.gt/230GBP3', @level0type = N'SCHEMA', @level0name = N'RedGateLocal', @level1type = N'TABLE', @level1name = N'DeploymentMetadata';

