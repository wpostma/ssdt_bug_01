﻿
CREATE PROCEDURE [dicom].[SP_ConvertDicomDateTimeToDateTime]   
   	    @ItemDate nvarchar(8),
		@ItemTime nvarchar(6), 
		@TimezoneOffsetFromUTC nvarchar(10), 
		@OutputDateTime  datetime2(7) OUTPUT,
		@ValidDicomValue bit OUTPUT
AS
BEGIN

	DECLARE @InputDateTimeStr	 nvarchar(32);
	DECLARE @ModifiedTimeStr     nvarchar(32);
	DECLARE @InputDateTime	     datetime2(7);
	DECLARE @ContinueExecution   bit = 1;
    DECLARE @SignCharacter nvarchar(1) = NULL; 
	DECLARE @ValidUTC bit = NULL; 
	DECLARE @TimezoneOffsetOutput nvarchar(10) = NULL;
	
	SET NOCOUNT ON;

	SET @ValidDicomValue = 0;
	SET @ContinueExecution = 1;
	SET @OutputDateTime = '';

	IF ((@ItemDate = '') AND (@ItemTime = ''))
	BEGIN
		SET @ContinueExecution  = 0;
	END

	IF (@ContinueExecution = 1)
    BEGIN
		IF ((LEN(@ItemDate) <> 8) OR (LEN(@ItemTime) > 6))
		BEGIN
			SET @ContinueExecution  = 0;
		END
	END 

	IF (@ContinueExecution = 1)
	BEGIN
		IF(LEN(@ItemTime) = 6)
		BEGIN
			SET @ModifiedTimeStr = CONCAT(SUBSTRING(@ItemTime, 1, 2), ':', SUBSTRING(@ItemTime, 3, 2), ':', SUBSTRING(@ItemTime, 5, 2)); 
			SET @InputDateTimeStr =  CONCAT(@ItemDate, ' ', @ModifiedTimeStr);
			SET @InputDateTime = CAST(@InputDateTimeStr AS datetime2);
		END
		ELSE IF(LEN(@ItemTime) = 4)
		BEGIN
			SET @ModifiedTimeStr = CONCAT(SUBSTRING(@ItemTime, 1, 2), ':', SUBSTRING(@ItemTime, 3, 2)); 
			SET @InputDateTimeStr =  CONCAT(@ItemDate, ' ', @ModifiedTimeStr);
			SET @InputDateTime = CAST(@InputDateTimeStr AS datetime2);
		END
		ELSE 
		BEGIN
			SET @InputDateTime = CAST(@ItemDate AS datetime2);
		END
    
	    SET @ValidDicomValue = 1;
			    
		SET @SignCharacter = SUBSTRING(@TimezoneOffsetFromUTC , 1, 1);
	    IF (@SignCharacter = '+')
		BEGIN
		   SET @TimezoneOffsetOutput = CONCAT('-', SUBSTRING(@TimezoneOffsetFromUTC, 2, LEN(@TimezoneOffsetFromUTC) -1));
		END
	    ELSE IF (@SignCharacter = '-')
		BEGIN
		   SET @TimezoneOffsetOutput = CONCAT('+', SUBSTRING(@TimezoneOffsetFromUTC, 2, LEN(@TimezoneOffsetFromUTC) -1));
		END

	    SET @OutputDateTime =  SWITCHOFFSET( @InputDateTime, @TimezoneOffsetOutput);

     END 

END
