﻿

CREATE PROCEDURE [dicom].[SP_DicomEntry_Put]   
   		@ExtJson nvarchar(max),
		@StudyInstanceUID nvarchar(64),
		@SeriesInstanceUID nvarchar(64),
		@SopInstanceUID nvarchar(64),
		@StudyIsLocked bit OUTPUT,
		@LocalFileID nvarchar(260) OUTPUT,
		@InternalInstanceID bigint OUTPUT,
		@OverwriteInstanceRequired bit OUTPUT,
		@ErrorCode int OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE  @InternalStudyID	bigint = NULL;
	DECLARE  @InternalPatientID	bigint = NULL;
	DECLARE  @InternalVisitID	bigint = NULL;
	DECLARE  @InternalSeriesID  bigint = NULL;
	DECLARE  @IsPatientLocked bit = 0;
	DECLARE  @IsStudyLocked bit = 0; 
	DECLARE  @PatientInternalAssigningAuthorityID bigint = NULL;
	DECLARE  @PatientDBDataIsNewer bit  = 0;
	DECLARE  @StudyDBDataIsNewer   bit = 0;	
	DECLARE	 @OverWriteInstance bit = 0;
	DECLARE	 @InstanceExists bit = 0;
				
	DECLARE  @ErrorNumber   int;
	DECLARE  @ErrorMessage  nvarchar(max);
	DECLARE  @ErrorSeverity int;
	DECLARE  @ErrorState    int;

	DECLARE  @SuccessMessage  nvarchar(max);
	DECLARE  @StatusMessage nvarchar(1024);
	DECLARE  @DicomKeyword  nvarchar(256) = NULL;	
	DECLARE  @DicomValue  nvarchar(1024) = NULL;
	DECLARE  @ValidDicomValue bit = 0;
	DECLARE  @InstanceErrorCode  int = 0;
	DECLARE  @ContinueExecution bit = 0;	
	DECLARE  @PatientJson nvarchar(max) = NULL;


	SET	@ErrorCode = 42752; -- Out of Resources
	SET @StudyIsLocked = 1;
	SET @OverwriteInstanceRequired = 1;
	
	SET @ContinueExecution = 1;	

	SET @SuccessMessage = CONCAT('Store Procedure <SP_DicomEntry_Put> Start Insert DICOM Object: ', SUBSTRING(@ExtJson, 1, 500));
	INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
    
	-- Continue Operations if we have valid @ExtJSon
	IF  (ISJSON(@ExtJson) > 0)
    BEGIN
       	BEGIN TRY
	   		SET @StatusMessage = 'SP_Instance_Exists_Or_Overwritten ';	

			EXEC  [dicom].[SP_Instance_Exists_Or_Overwritten]  
				  	@ExtJson, @SopInstanceUID, @OverWriteInstance OUTPUT,
					@InstanceExists OUTPUT,	@ErrorCode  OUTPUT;   
            IF ((@OverWriteInstance = 0) AND (@InstanceExists = 1))
			BEGIN
				SET @OverwriteInstanceRequired = 0;
				SET @StudyIsLocked = 0;

				SET @SuccessMessage = 'Store Procedure <SP_DicomEntry_Put> was finished. Overwrite Instance is not Required ';
				INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
			END
			ELSE -- Continue Operation 
			BEGIN  
   				SET @StatusMessage =  'SP_Patient_Put Store Procedure ';		
				
                IF (@ContinueExecution = 1)  
				BEGIN						   		        
					EXEC [dicom].[SP_Patient_Put]   
   						@StudyInstanceUID, 
						@ExtJson,
						@InternalStudyID OUTPUT,
						@InternalPatientID OUTPUT,
						@IsPatientLocked OUTPUT,
						@PatientInternalAssigningAuthorityID OUTPUT,
						@PatientDBDataIsNewer OUTPUT,
						@PatientJson OUTPUT;
						
					IF ((@InternalPatientID IS NULL) OR (@IsPatientLocked = 1))
					BEGIN
						SET @ContinueExecution = 0;	
						IF (@IsPatientLocked = 1)
						BEGIN		
                 			SET	@ErrorCode = 0; 
						    SET @ErrorMessage = CONCAT(' Study: ',  @StudyInstanceUID,  ' is Locked ');
						END
						ELSE IF (@InternalPatientID IS NULL)
						BEGIN
							SET	@ErrorCode = 49752;
						    SET @ErrorMessage = CONCAT(' InternalPatientID of Study: ',  @StudyInstanceUID,  ' is NULL');
						END 

						SET @OverwriteInstanceRequired = 0;
						INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','ERROR MESSAGE ', @ErrorMessage, default);
					END  

	               	IF (@ContinueExecution = 1)  
                    BEGIN					
					    SET @StatusMessage =  'SP_Patient_Allergies_Put Store Procedure ';	
				
						EXEC [dicom].[SP_Patient_Allergies_Put]  
     						@InternalPatientID,
	 						@ExtJson,
							@PatientDBDataIsNewer,
							@PatientJson;

					    SET @StatusMessage =  'SP_Visit_Put Store Procedure ';
						EXEC [dicom].[SP_Visit_Put]   
   							@InternalPatientID,
							@PatientInternalAssigningAuthorityID,
							@PatientDBDataIsNewer,
							@ExtJson, 
		                    @InternalVisitID OUTPUT;
             						    
						IF (@InternalVisitID IS NULL)
						BEGIN
						    SET @ContinueExecution = 0;	
							SET	@ErrorCode = 49752;
							SET @OverwriteInstanceRequired = 0;
							SET @ErrorMessage = CONCAT('InternalVisitID of InternalPatientID: ', @InternalPatientID, ' is NULL');
							INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
								 VALUES('STOW-RS','ERROR MESSAGE ', @ErrorMessage, default);
						END 
                    END

					IF (@ContinueExecution = 1)  
                    BEGIN
		                SET @StatusMessage =  'SP_Study_Put Store Procedure '; 	
						EXEC [dicom].[SP_Study_Put]  
								@InternalPatientID, 
								@InternalVisitID, 
   								@ExtJson,
								@StudyInstanceUID, 
								@InternalStudyID  OUTPUT,
								@IsStudyLocked   OUTPUT,
								@StudyDBDataIsNewer OUTPUT;

						IF ((@InternalStudyID IS NULL) OR (@IsStudyLocked = 1))
						BEGIN
							SET @ContinueExecution = 0;	
							IF (@IsStudyLocked = 1)
							BEGIN		              					     
								SET	@ErrorCode = 0; 
								SET @ErrorMessage = CONCAT(' Study: ',  @StudyInstanceUID,  ' is Locked ');
							END
							ELSE IF (@InternalStudyID IS NULL)
							BEGIN
								SET	@ErrorCode = 49752;
								SET @ErrorMessage = CONCAT(' InternalStudyID of InternalPatientID: ',  @InternalPatientID,  ' is NULL');
							END 

							SET @OverwriteInstanceRequired = 0;
							INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','ERROR MESSAGE ', @ErrorMessage, default);

						END  
                      END

					  IF (@ContinueExecution = 1)  
					  BEGIN

					      SET @StudyIsLocked = 0;	   

					      SET @StatusMessage =  'SP_Study_Procedure_Codes_Put ';

					      EXEC [dicom].[SP_Study_Procedure_Codes_Put]  
     							@InternalStudyID,@ExtJson, 	@StudyDBDataIsNewer;

                          SET @StatusMessage =  'SP_Study_Diagnosis_Codes_Put ';
                           
						  EXEC  [dicom].[SP_Study_Diagnosis_Codes_Put]  
     							@InternalStudyID, @ExtJson,   @StudyDBDataIsNewer;

						   SET @StatusMessage =  'SP_Series_Put Store Procedure ';
						 

						   EXEC [dicom].[SP_Series_Put]  
							    @InternalStudyID, @ExtJson, @SeriesInstanceUID, @InternalSeriesID OUTPUT;

                           IF (@InternalSeriesID IS NULL)
						   BEGIN
								SET @ContinueExecution = 0;	
								SET	@ErrorCode = 49752;
						   END 
                      END
					   
					  IF (@ContinueExecution = 1)  
					  BEGIN
						   SET @StatusMessage =  'SP_Instance_Put Store Procedure ';
						      
						   EXEC [dicom].[SP_Instance_Put]  
								@InternalPatientID,
								@InternalStudyID,
								@InternalSeriesID,	  
								@ExtJson,
								@SopInstanceUID,
								@LocalFileID OUTPUT,
								@InternalInstanceID OUTPUT,
							    @InstanceErrorCode OUTPUT;

                            SET @ErrorCode = @InstanceErrorCode;
                      END
    			 END
				 ELSE 
				 BEGIN
				    SET @ErrorMessage = CONCAT('Invalid value of StudyInstanceUID: ',  @DicomValue); 
					INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','ERROR MESSAGE ', @ErrorMessage, default);                   
                    SET	@ErrorCode = 49152; -- cannot understand certain Data Elements.
				 END
		    END 
    	 END TRY
		 BEGIN CATCH
			SELECT 
				@ErrorMessage  = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState    = ERROR_STATE();
					   	    
			 -- Insert Error Message to Log File
		     SET  @StatusMessage = CONCAT('Error in: ', @StatusMessage, ' is Occurred. ');
			 SET  @ErrorMessage =  CONCAT( @StatusMessage,  ' Error Message is: ', @ErrorMessage,
			    	' .Error Severity is: ', @ErrorSeverity, ' .Error State is: ', @ErrorState); 

			 INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','ERROR MESSAGE ',  @ErrorMessage, default);

			-- Rethrow Exception 					    
		      THROW 50001, @ErrorMessage, @ErrorState;  
									
		  END CATCH
	END
	ELSE
	BEGIN
		INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			   VALUES('STOW-RS','ERROR MESSAGE - Not Valid JSON string', @ExtJson , default);	
	END;
END  

