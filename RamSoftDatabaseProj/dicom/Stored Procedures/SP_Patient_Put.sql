﻿CREATE PROCEDURE [dicom].[SP_Patient_Put]   
			@StudyInstanceUID  nvarchar(64), 
			@ExtJson nvarchar(max), -- DICOM JSON
			@InternalStudyID bigint OUTPUT,
			@InternalPatientID bigint OUTPUT,
			@IsLocked bit OUTPUT,
			@PatientInternalAssigningAuthorityID bigint OUTPUT,
			@PatientDBDataIsNewer bit OUTPUT,
			@ExtJsonPatientOutput nvarchar(max) OUTPUT
	AS
	BEGIN
		
		-- Batch submitted through debugger: SQLStoreProcedure_SP_Patient_Put_August_31_Errors.sql|7|0|D:\SQL_2016_Queries\SQLStoreProcedure_SP_Patient_Put_August_31_Errors.sql
		-- Batch submitted through debugger: SQLQuery2.sql|7|0|C:\Users\vkharam\AppData\Local\Temp\~vsBF26.sql
		-- Batch submitted through debugger: SQLStoreProcedure_SP_Patient_Put_14.sql|7|0|D:\SQL_2016_Queries\SQLStoreProcedure_SP_Patient_Put_14.sql
		-- Batch submitted through debugger: SQLQuery8.sql|7|0|C:\Users\vkharam\AppData\Local\Temp\~vsD412.sql

		-- Batch submitted through debugger: SQLStoreProcedure_SP_Patient_Put.sql|7|0|D:\SQL_2016_Queries\SQLStoreProcedure_SP_Patient_Put.sql

		SET NOCOUNT ON;
		
		DECLARE	 @CurValue nvarchar(max);

		DECLARE  @IsActive bit = 0;
		
		DECLARE  @SequenceKey nvarchar(1024) = NULL;
		DECLARE  @SequenceKeyValue nvarchar(1024) = NULL;

		DECLARE  @TempValue nvarchar(1024) = NULL;

		DECLARE  @PatientTelephoneNumbers nvarchar(4000) = NULL;
		
		DECLARE  @PatientID nvarchar(64) = NULL;
		DECLARE  @PatientName nvarchar(64) = NULL;
		DECLARE  @UpdatedPatientName nvarchar(64) = NULL;
		DECLARE  @IssuerOfPatientID nvarchar(64) = NULL;
		DECLARE  @PatientSSN nvarchar(64) = NULL;
		DECLARE	 @BirthDate nvarchar(10);
		
		DECLARE	 @JsonAddress nvarchar(4000) = NULL;
		DECLARE	 @PatientsAddress nvarchar(64) = NULL;
		DECLARE  @PatientsLine nvarchar(64) = NULL;
		DECLARE  @PatientsCityName nvarchar(64) = NULL;
		DECLARE  @PatientsStateCode nvarchar(64) = NULL;
		DECLARE  @PatientsZipCode nvarchar(64) = NULL;

		DECLARE  @ContactPointJson nvarchar (4000) = NULL;
		DECLARE  @InternalAssigningAuthorityID bigint = NULL;

		DECLARE  @UniversalEntityID nvarchar (max) = NULL;
		DECLARE  @UniversalEntityIDTypeCode nvarchar (64) = NULL;
		DECLARE  @UniversalEntityIDTypeID int = NULL;
		DECLARE  @IssuerOfPatientIDQualifiersSequenceValue nvarchar(1024) = NULL;
		
		DECLARE  @LastupdateUserID bigint = NULL;
		DECLARE  @SexID int = NULL;
		DECLARE  @State nvarchar (64) = NULL;
		DECLARE  @CountryCode char (2) = NULL;
		DECLARE  @StateID int = NULL;
		DECLARE  @CountryID int = NULL;
		
		DECLARE  @AccountNumber nvarchar(64) = NULL; 
		DECLARE  @Language nvarchar(512) = NULL;
		DECLARE  @Occupation nvarchar(64) = NULL;
		DECLARE  @MotherMaidenName nvarchar(64) = NULL;
		DECLARE  @Ethnicity nvarchar(512) = NULL; 
		DECLARE  @Race nvarchar(512) = NULL; 
		DECLARE  @SmokingStatus nvarchar(64) = NULL;  
		DECLARE  @Notes nvarchar(max) = NULL;
			
		DECLARE  @ExtJsonPatient nvarchar(max) = NULL;

		DECLARE  @DicomKeyword nvarchar(256) = NULL;				
		DECLARE  @DicomTagKey nvarchar(10) = NULL;	
		DECLARE  @ValidDicomValue bit = 0;	
		DECLARE  @DicomValue  nvarchar(max) = NULL;
		DECLARE  @HL7Updated bit = 0;
		DECLARE  @PatientInfoNewer bit = 0;  -- Dicom Entry is newer
		DECLARE  @PatientLastUpdatedUTC datetime2(7);
		DECLARE  @TempBoolean bit = 0;;
		DECLARE  @TempIntegerConfig int = 0;
		DECLARE  @SuccessMessage  nvarchar(max);

		DECLARE  @ErrorNumber   int;
		DECLARE  @ErrorMessage  nvarchar(max);
		DECLARE  @ErrorSeverity int;
		DECLARE  @ErrorState    int;
		
		-- Initial Settings for Output Variables 
		SET @InternalStudyID = NULL;
		SET	@InternalPatientID = NULL;
		SET @IsLocked = 1;
				
		IF  (ISJSON(@ExtJson) > 0)
		BEGIN 
			DECLARE @LocalPatientID int = NULL;
			DECLARE @LocalStudyID int = NULL;
			DECLARE @LocalStatusValue smallint = NULL;
			DECLARE @LocalAssigningAuthorityID bigint = NULL;
			DECLARE @LockedStudyStatus smallint;
							
			DECLARE @ProcessedIDTab TABLE (id bigint);

			SET @SuccessMessage = 'Store Procedure <SP_Patient_Put>  Start';
			INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
									VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);

			/* Fetch Data from Input ExtJson String */
					  
			EXEC [dicom].[SP_GetDicomValueFromJson]   
						 'PatientID', @ExtJson,  @DicomValue  OUTPUT,  @ValidDicomValue  OUTPUT;

			IF (@ValidDicomValue = 1)
			BEGIN
				SET @PatientID = UPPER(@DicomValue);  
			END
														
			/* If PatientID is blank trigger of [PHI].[PATIENT] table is generating PatientID */

			-- 'IssuerOfPatientID'
			EXEC [dicom].[SP_GetDicomValueFromJson]   
						'IssuerOfPatientID', @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;																		
			IF (@ValidDicomValue = 1)
			BEGIN
				SET @IssuerOfPatientID = UPPER(@DicomValue);   
			END

			/* IssuerOfPatientIDQualifiersSequence */
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
							  'IssuerOfPatientIDQualifiersSequence', @ExtJson,  @IssuerOfPatientIDQualifiersSequenceValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)  
			BEGIN			
				EXEC  [dicom].[SP_GetDicomValueFromJson]
						 'UniversalEntityID',  @IssuerOfPatientIDQualifiersSequenceValue,  @UniversalEntityID  OUTPUT,   @ValidDicomValue  OUTPUT;
					  
				IF (@UniversalEntityID <> '')
				BEGIN
					SET @UniversalEntityID  = UPPER(@UniversalEntityID); 
						  -- Extract Conditional Field  'UniversalEntityIDType'
					EXEC  [dicom].[SP_GetDicomValueFromJson]
							  'UniversalEntityIDType',  @IssuerOfPatientIDQualifiersSequenceValue, @UniversalEntityIDTypeCode  OUTPUT,  @ValidDicomValue  OUTPUT;
						
					 SELECT @UniversalEntityIDTypeID = UniversalEntityIDTypeID  FROM [code].[UniversalEntityIDType] 
						   WHERE UniversalEntityIDTypeCode = @UniversalEntityIDTypeCode;
						   -- NULL Value is Permitted in  dbo.AssigningAuthority for UniversalEntityIDTypeCode
				END 
			END 
					
			-- First Priority is @UniversalEntityID
			-- Second Priority is (@IssuerOfPatientID

			/* The DICOM Standard Required that The authority identified by @UniversalEntityID shall be the same as the
				  Issuer of Patient ID (0010,0021), if present. If Issuer is Present But Not Identical to 
				  UniversalEntityID the behavoiur is not clear. 
				  The Name of AssigningAuthority is mandatory in [dbo].[AssigningAuthority] table */
					 
			IF (@UniversalEntityID IS NOT NULL)
			BEGIN
				SELECT @InternalAssigningAuthorityID = InternalAssigningAuthorityID FROM [dbo].[AssigningAuthority]
								   WHERE UniversalEntityID = @UniversalEntityID; 
				IF (@InternalAssigningAuthorityID IS NULL) 
				BEGIN 
					IF (@IssuerOfPatientID IS NOT NULL)
					BEGIN
						SELECT @InternalAssigningAuthorityID = InternalAssigningAuthorityID FROM [dbo].[AssigningAuthority]
							   WHERE [Name] =  @IssuerOfPatientID;
					END
				END
						 
				IF (@InternalAssigningAuthorityID IS NULL) 
				BEGIN
					IF (@IssuerOfPatientID IS NULL)
					BEGIN
						SET @IssuerOfPatientID = SUBSTRING(@UniversalEntityID, 1,64); 
					END
							-- Insert new Entry in AssigningAuthority
					INSERT INTO dbo.AssigningAuthority([Name], UniversalEntityID, UniversalEntityIDTypeID, IsActive)
								   OUTPUT inserted.InternalAssigningAuthorityID INTO @ProcessedIDTab
							VALUES (@IssuerOfPatientID, @UniversalEntityID,  @UniversalEntityIDTypeID, 1);

					SET @InternalAssigningAuthorityID = (SELECT TOP 1 id FROM @ProcessedIDTab);
							-- We will reuse @ProcessedIDTab for Inserting Patient
					DELETE FROM  @ProcessedIDTab;
				 END
			 END 
			 ELSE IF (@IssuerOfPatientID  IS NOT NULL) 
			 BEGIN
				SELECT @InternalAssigningAuthorityID = InternalAssigningAuthorityID FROM [dbo].[AssigningAuthority]
								   WHERE [Name] =  @IssuerOfPatientID;
				IF (@InternalAssigningAuthorityID IS NULL) 
				BEGIN
				-- Insert new Entry in AssigningAuthority
					INSERT INTO dbo.AssigningAuthority([Name], IsActive)
								   OUTPUT inserted.InternalAssigningAuthorityID INTO @ProcessedIDTab
					VALUES (@IssuerOfPatientID, 1);

					SET @InternalAssigningAuthorityID = (SELECT TOP 1 id FROM @ProcessedIDTab);
					DELETE FROM  @ProcessedIDTab;
				END 
			END
			ELSE -- InternalAssigningAuthorityID is Mandatory 
			BEGIN
				SELECT @InternalAssigningAuthorityID = InternalAssigningAuthorityID FROM dbo.AssigningAuthority
					WHERE Name = [dbo].GetSystemConfigStr('Default Issuer of PatientID');
			END
					
			-- Set Output Parameter of Store Procedure
			SET @PatientInternalAssigningAuthorityID = @InternalAssigningAuthorityID;
												
			--  PatientName  
			EXEC [dicom].[SP_GetDicomPersonNameFromJson]   
				'PatientName', @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)
			BEGIN
				SET @PatientName = @DicomValue; 
				SET @TempIntegerConfig = [dbo].GetSystemConfigIntValue('Allow Case Sensitive Person Name');
				IF ((@TempIntegerConfig IS NULL) OR  (@TempIntegerConfig = 0))
				BEGIN
					SET @PatientName = UPPER(@PatientName);
		
					EXEC  [dicom].[SP_UpdatePersonName]   
									 @PatientName,  @UpdatedPatientName OUTPUT, @TempBoolean OUTPUT;    
					IF (@TempBoolean = 1)
					BEGIN
						SET @PatientName = @UpdatedPatientName;
					END
				END
			END
					
			--  PatientSex  
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
						   'PatientSex', @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)  
			BEGIN
				SELECT @SexID = (SELECT SexID FROM [code].[Sex] WHERE SexCode = @DicomValue);
			END		 
			
			-- Other Patient ID - SSN  
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
							'OtherPatientIDs', @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1) 
			BEGIN
			   -- Extract First Element similar to version 6.X 
				SELECT @PatientSSN  = [value] FROM OPENJSON(@DicomValue) WHERE [key] = 0;
			END		

			-- Patient BirthDate 
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
							'PatientBirthDate', @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)  
			BEGIN
				SET @BirthDate = @DicomValue;
			END		
		
			/* Patient Address.  We will try to parse the values. 
					   If we can't parse the value   
					   We will be add this address to ExtJson field as line information */
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
							  'PatientAddress', @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1) 
			BEGIN
				SET @PatientsAddress = @DicomValue;
				DECLARE @CityIndex int = 0;
				DECLARE @StateIndex int = 0;
				DECLARE @ZipCodeIndex int = 0;

				SET	 @JsonAddress = '{ }'; -- Initialize as Json text

				SELECT @CityIndex = (SELECT CHARINDEX('^', @PatientsAddress));  
				IF (@CityIndex = 0)
				BEGIN
					SELECT @PatientsLine = @PatientsAddress;  
							SET @JsonAddress  = JSON_MODIFY(@JsonAddress,'$.line',  @PatientsLine);		 
				END
				ELSE
				BEGIN
					SELECT @PatientsLine =  SUBSTRING(@PatientsAddress, 1, (@CityIndex - 1));						  
					SET @JsonAddress  = JSON_MODIFY(@JsonAddress,'$.line',  @PatientsLine);	
								   
					SELECT @StateIndex = (SELECT CHARINDEX('^', @PatientsAddress, @CityIndex + 1));  
													
					SELECT @ZipCodeIndex = (SELECT CHARINDEX('^', @PatientsAddress, @StateIndex + 1)); 
									
					SELECT @PatientsCityName = SUBSTRING(@PatientsAddress, @CityIndex + 1, (@StateIndex - @CityIndex - 1));
					IF 	(@PatientsCityName IS NOT NULL AND  @PatientsCityName <> '')
					BEGIN
						SET @JsonAddress  = JSON_MODIFY(@JsonAddress,'$.city',  @PatientsCityName);	
					END

					SELECT @PatientsStateCode = SUBSTRING(@PatientsAddress, @StateIndex + 1, (@ZipCodeIndex - @StateIndex -1));
					IF 	( @PatientsStateCode IS NOT NULL AND @PatientsStateCode <> '')								
					BEGIN
						SET @JsonAddress  = JSON_MODIFY(@JsonAddress,'$.state', @PatientsStateCode);	
					END
								
					SELECT @PatientsZipCode = SUBSTRING(@PatientsAddress, @ZipCodeIndex + 1, (LEN(@PatientsAddress) - @ZipCodeIndex));
					IF 	(@PatientsZipCode IS NOT NULL AND @PatientsZipCode <> '')		
					BEGIN
						SET @JsonAddress  = JSON_MODIFY(@JsonAddress,'$.postalCode', @PatientsZipCode);	
					END																		
				 END		
			END 		
											
			/* Country of Residence */
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
							'CountryOfResidence', @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;

			IF ( @ValidDicomValue = 1)
			BEGIN   
				SET @CountryCode = @DicomValue; 
				SELECT @CountryID = CountryID from dbo.Country where CountryCode = @CountryCode;
				IF (@CountryID IS NULL) OR (@CountryID <= 0)
				BEGIN
					SELECT @CountryID = CountryID from dbo.Country where CountryName = @CountryCode;
				END

				IF (@JsonAddress IS NOT NULL)
				BEGIN
					SET @JsonAddress  = JSON_MODIFY(@JsonAddress,'$.country', @CountryCode);	
				END
			END 

			IF ((@StateID IS NOT NULL) AND  (@CountryID IS NOT NULL))
			BEGIN
				SELECT @StateID = StateID from [code].[State] where [State] = @State and CountryID = @CountryID;
				IF (@StateID IS NULL) OR (@StateID <= 0)
				BEGIN
					SELECT @StateID = StateID from [code].[State] where StateCode = @State and CountryID = @CountryID;
				END
			END

			/* PatientMotherBirthName */
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
					'PatientMotherBirthName', @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1) 
			BEGIN
				SET @MotherMaidenName = @DicomValue;
			END		
																			
			/* EthnicGroup */
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
						'EthnicGroup', @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)  
			BEGIN
				SET @Ethnicity = @DicomValue;
			END		
				
			/* Occupation  */
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
						'Occupation', @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)  
			BEGIN
				SET @Occupation = @DicomValue;
			END		
				
			/* SmokingStatus  */
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
					'SmokingStatus', @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)  
			BEGIN
				SET @SmokingStatus = @DicomValue;
			END	       	
				
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
				'Race', @ExtJson, @DicomValue  OUTPUT, @ValidDicomValue  OUTPUT;

			IF (@ValidDicomValue = 1)  
			BEGIN
				SET @Race = @DicomValue;
			END		
								
			/* PatientNotes */
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
				'PatientComments', @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;

			IF (@ValidDicomValue = 1)  
			BEGIN
				SET @Notes = @DicomValue;
			END		
						
			/* PatientPrimaryLanguageCodeSequence */
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
						'PatientPrimaryLanguageCodeSequence', @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)  
			BEGIN														
				/* PatientPrimaryLanguageCodeSequence has only one level of hierarchy */					 
				SELECT @SequenceKey = @DicomValue;  
																		 
				SELECT @CurValue = [value] FROM OPENJSON(@SequenceKey);

				SELECT @SequenceKeyValue = JSON_VALUE(@CurValue, '$.Value[0]'); 
								
				-- Set Primary Language --
				IF (@SequenceKeyValue IS NOT NULL)
				BEGIN
					SET  @Language = @SequenceKeyValue;	
				END
			END 
							
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
						'PatientInsurancePlanCodeSequence', @ExtJson,  @DicomValue  OUTPUT, @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)  
			BEGIN					
				/* PatientPrimaryLanguageCodeSequence has only one level of hierarchy */					 
				SELECT @SequenceKey = @DicomValue;  
																		 
				SELECT @CurValue = [value] FROM OPENJSON(@SequenceKey);

				SELECT @SequenceKeyValue = JSON_VALUE(@CurValue, '$.Value[0]'); 
								
				-- AccountNumber  --
				IF (@SequenceKeyValue IS NOT NULL)
				BEGIN
					SET  @AccountNumber = @SequenceKeyValue;
				END
			END 
						 
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
					'PatientTelephoneNumbers',  @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1) 
			BEGIN					
				-- Fetching Full Array 
				-- The Array can be inserted to ExtJson string
				SET @PatientTelephoneNumbers = @DicomValue;
			END  

			/* 
				We Don't have  DeceasedDate, MaritalStatus, Guarantor Information in DICOM
				Those values from RIS and FHIR 
			*/
				
			SET @LockedStudyStatus =  [dbo].GetSystemConfigIntValue('Prevent Modification Status');
				
			-- Check Config Value 
			SELECT @TempValue = StatusName  FROM [code].[Status] WHERE StatusValue = @LockedStudyStatus;
					
			IF (@TempValue IS NULL) -- Error Situation
			BEGIN 
				SELECT  @LockedStudyStatus = StatusValue FROM [code].[Status] WHERE StatusName = 'SIGNED';  
						
				SELECT @ErrorMessage = CONCAT('Invalid Config Entry: ', 'Prevent Modification Status');
				INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
					VALUES('STOW-RS','ERROR MESSAGE ', @ErrorMessage , default);	
			END 

			DECLARE @PerformLinkPatients bit = 0;
			DECLARE @PerformInsertPatientID bit = 0;
			DECLARE @OnlyPatientWithoutStudyExists bit = 0;

			SELECT @LocalPatientID  = InternalPatientID,  @LocalStudyID = InternalStudyID,
				@LocalStatusValue = StatusValue,  @LocalAssigningAuthorityID  = auth.InternalAssigningAuthorityID
				FROM [PHI].[Study] st JOIN [dbo].[AssigningAuthority] auth
				ON  st.InternalAssigningAuthorityID = auth.InternalAssigningAuthorityID
				WHERE StudyUID = @StudyInstanceUID;

			-- Only Patient Without Study can exists. PatientID can't be NULL or Empty 
			IF ((@LocalPatientID IS NULL) AND (@PatientID <> ''))  
			BEGIN
				SELECT TOP 1 @LocalPatientID  = InternalPatientID,  @LocalAssigningAuthorityID  = auth.InternalAssigningAuthorityID
				FROM [PHI].[Patient] pt JOIN [dbo].[AssigningAuthority] auth
				ON  pt.InternalAssigningAuthorityID = auth.InternalAssigningAuthorityID
				WHERE PatientID = @PatientID ORDER BY pt.InternalAssigningAuthorityID DESC;

				IF (@LocalPatientID  > 0)
				BEGIN
						SET @OnlyPatientWithoutStudyExists = 1;
				END   
			END 
				
			IF (@LocalPatientID IS NULL)  
			BEGIN
					SET @PerformInsertPatientID = 1;
					SET @IsLocked = 0; 
			END
			ELSE IF ( @OnlyPatientWithoutStudyExists = 1 AND @LocalPatientID  > 0) -- InternalPatientID Exists
			BEGIN
				IF  (@LocalAssigningAuthorityID <> @InternalAssigningAuthorityID)
				BEGIN
					SET @PerformLinkPatients = 1;
					SET @PerformInsertPatientID = 1; -- Insert New Patient
					SET @IsLocked = 0;
				END
				ELSE
				BEGIN
					SET  @PerformInsertPatientID = 0; -- Perform Update 
					SET  @IsLocked = 0; 
				END
			END
			ELSE IF (@OnlyPatientWithoutStudyExists = 0 AND @LocalPatientID  > 0) -- InternalPatientID  and Study Exists
			BEGIN
				SET	@InternalStudyID  = @LocalStudyID;
				-- Check Issuer of PatientID  
				-- If Existing Issuer of PatientID is different from Issuer of Patient ID in Received Object
				-- We should Create New Patient and Link the Patients 
				IF  (@LocalAssigningAuthorityID <> @InternalAssigningAuthorityID)
				BEGIN
					SET @PerformLinkPatients = 1;
					SET @PerformInsertPatientID = 1;
					SET @IsLocked = 0;
				END
				ELSE -- Perform Update Patient if Study is not locked 
				BEGIN
					IF  (@LocalStatusValue  < @LockedStudyStatus)
					BEGIN
						-- Update Patient
						SET @IsLocked = 0;
					END
					ELSE
					BEGIN
						SET @IsLocked = 1; -- Don't Update Patient
					END
				END 
			END
								
			-- Set LastUpdateUser as 'DICOM'			  		
			SELECT @LastupdateUserID = InternalUserID from [dbo].[User] WHERE UserName = 'DICOM'; 
							
			-- Construct JSON Object for Patient's ExtJson field;
			IF (@PerformInsertPatientID = 1)
			BEGIN
				SET @ExtJsonPatient = '{ }'; -- Initialize @ExtJsonPatient
			END 
			ELSE IF (@IsLocked = 0)
			BEGIN
				-- Build   @ExtJsonPatient from Existing Patient 
				SELECT @HL7Updated = Hl7updated,  @PatientLastUpdatedUTC = LastUpdateUTC, @ExtJsonPatient = ExtJson FROM [PHI].[Patient]
					WHERE InternalPatientID = @LocalPatientID;
						
				IF (@HL7Updated = 1)
				BEGIN
					SET @PatientInfoNewer = 1;			
				END 
				ELSE
				BEGIN   
					SET @PatientInfoNewer =  [dbo].GetSystemConfigBool('Rely Upon DB Information When Processing Dicom Object');   
					IF (@PatientInfoNewer = 1) 
					BEGIN
						SELECT @SuccessMessage = 'Configuration Setting <Rely Upon DB Information When Processing Dicom Object> is True';
									  
						INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
								VALUES('STOW-RS','INFO MESSAGE ', @SuccessMessage , default);
					END 
					ELSE
					BEGIN
						EXEC [dicom].[SP_IsDBDataNewer]
							@ExtJson,  @PatientLastUpdatedUTC, @PatientInfoNewer OUTPUT;
					END			                 
				END;
			END 
					
			-- Set Output Parameter 
			SET @PatientDBDataIsNewer  = @PatientInfoNewer;
							  
			IF(@PatientInfoNewer = 0) 
			BEGIN

				SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, '$.address', JSON_QUERY(@JsonAddress));
						   
				/* Add ContactPoint and Phone OtherNumbers */ 
				IF (@PatientTelephoneNumbers IS NOT NULL)
				BEGIN
					DECLARE @CountOfPhoneNumers int = 0;
					DECLARE @PhoneHomeNumber nvarchar(1024);
					DECLARE @PhoneWorkNumber nvarchar(1024);
					DECLARE @PhoneMobileNumber nvarchar(1024);
					DECLARE @OtherContactNumbers nvarchar(1024);

					SET  @OtherContactNumbers =  '{ }'; 
					SET  @PhoneHomeNumber =      '{ }'; 
					SET  @PhoneMobileNumber =    '{ }'; 
					SET  @PhoneWorkNumber =      '{ }'; 

					DECLARE Json_Cursor CURSOR LOCAL FOR SELECT [value] FROM OPENJSON(@PatientTelephoneNumbers);

					OPEN Json_Cursor;
					FETCH NEXT  FROM Json_Cursor INTO @CurValue;
					SET @CountOfPhoneNumers  = @CountOfPhoneNumers + 1;

					WHILE (@@FETCH_STATUS = 0) 
					BEGIN
						IF (@CountOfPhoneNumers = 1)
						BEGIN
							-- Set Home Phone
							SET @PhoneHomeNumber = JSON_MODIFY(@PhoneHomeNumber, '$.system', 'phone');
							SET @PhoneHomeNumber = JSON_MODIFY(@PhoneHomeNumber, '$.value', @CurValue);
							SET @PhoneHomeNumber = JSON_MODIFY(@PhoneHomeNumber, '$.use', 'home');
			
							SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, 'append $.contactPoint', JSON_QUERY(@PhoneHomeNumber));	
						END  
						ELSE IF (@CountOfPhoneNumers = 2)
						BEGIN
				
							-- Set Work Phone 
							SET @PhoneWorkNumber = JSON_MODIFY(@PhoneWorkNumber, '$.system', 'phone');
							SET @PhoneWorkNumber = JSON_MODIFY(@PhoneWorkNumber, '$.value', @CurValue);
							SET @PhoneWorkNumber = JSON_MODIFY(@PhoneWorkNumber, '$.use', 'work');
			
							SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, 'append $.contactPoint', JSON_QUERY(@PhoneWorkNumber));
						END
						ELSE IF (@CountOfPhoneNumers = 3)
						BEGIN
						-- Set Mobile Phone 
							SET @PhoneMobileNumber  = JSON_MODIFY(@PhoneMobileNumber, '$.system', 'phone');
							SET @PhoneMobileNumber  = JSON_MODIFY(@PhoneMobileNumber, '$.value', @CurValue);
							SET @PhoneMobileNumber  = JSON_MODIFY(@PhoneMobileNumber, '$.use', 'mobile');

							SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, 'append $.contactPoint', JSON_QUERY(@PhoneMobileNumber));
				
						END 
						ELSE IF (@CountOfPhoneNumers = 4)
						BEGIN
							-- In Design "otherNumber" is Single Value 
							-- We don't want break Existion Possibilities by adding array.  
							-- MS SQL does not accepting Syntax as 
							--- SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, @CountVariable,  @CurValue);
							--- Limitation for 3 Other Phone Number can be good itteration

							SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, '$.otherNumber',  @CurValue);
						END 

						ELSE IF (@CountOfPhoneNumers = 5)
						BEGIN
							-- In Design "otherNumber" is Single Value 
							-- We don't want break Existion Possibilities by adding array.  
							SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, '$.otherNumber1',  @CurValue);
						END 

						ELSE IF (@CountOfPhoneNumers = 6)
						BEGIN
							SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, '$.otherNumber2',  @CurValue);
							BREAK;
						END 

						FETCH NEXT FROM Json_Cursor INTO @CurValue;
						SET @CountOfPhoneNumers  = @CountOfPhoneNumers + 1;
					END 

					CLOSE Json_Cursor; 
					DEALLOCATE Json_Cursor; 

				END

				IF (@Language IS NOT NULL)
				BEGIN
					SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, '$.language', @Language);
				END

				/* We Don't have Employment Status in DICOM. The field $.occupation' will be useful */ 
				IF (@Occupation IS NOT NULL)
				BEGIN
					SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, '$.occupation', @Occupation);
				END
						   
				IF (@MotherMaidenName IS NOT NULL)
				BEGIN
					SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, '$.motherMaidenName', @MotherMaidenName);
				END
							   
				IF (@Ethnicity IS NOT NULL)
				BEGIN
					SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, '$.ethnicity', @Ethnicity);
				END
							   
				IF (@Race IS NOT NULL)
				BEGIN
					SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, '$.race', @Race);
				END
							   
				IF (@SmokingStatus IS NOT NULL)
				BEGIN
					SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, '$.smokingStatus', @SmokingStatus);
				END
								   
				IF (@Notes IS NOT NULL)
				BEGIN
					SET @ExtJsonPatient = JSON_MODIFY(@ExtJsonPatient, '$.notes', @Notes);
				END
			END   	
			
			SET @ExtJsonPatientOutput = @ExtJsonPatient; 
									
			IF (@PerformInsertPatientID = 1)
			BEGIN
				INSERT INTO [PHI].[PATIENT]
				(
						InternalAssigningAuthorityID,
						LastupdateUserID,
						PatientID,
						PatientName,
						BirthDate,
						SexID,
						StateID,
						CountryID,
						Accountnumber,
						Ssn,
						ExtJson
				)
				OUTPUT inserted.InternalPatientID INTO @ProcessedIDTab
					VALUES
					(
						@InternalAssigningAuthorityID,
						@LastupdateUserID,
						@PatientID, 
						@PatientName,
						@BirthDate, 
						@SexID,
						@StateID,
						@CountryID,
						@Accountnumber,
						@PatientSSN,
						@ExtJsonPatient);
								
						SET @InternalPatientID = (SELECT TOP 1 id FROM @ProcessedIDTab);		

						SELECT @SuccessMessage = CONCAT('Successfully Inserted PatienID: ', @PatientID,
							'  With IssuerOfPatientID: ', @IssuerOfPatientID);
						  
						INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
							VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);

						/* Perform Link Patients if required */	
						IF (@PerformLinkPatients = 1)	
						BEGIN  
							DECLARE @LinkID int = 0; 
							DECLARE @LinkageTypeID int = 0; 

							SELECT @LinkageTypeID = LinkageTypeID FROM [code].[LinkageType]    
									WHERE LinkageTypeName = 'Alternate Record';  
										
							SELECT @LinkID = LinkID  FROM [dbo].[PatientLinked]
								WHERE InternalPatientID = @LocalPatientID;
							IF (@LinkID IS NOT NULL AND @LinkID <> 0)
							BEGIN
								-- Add New Patient to Existing LinkID
								INSERT INTO  [dbo].[PatientLinked]
								(
									LinkID, InternalPatientID, LinkageTypeID
								)
								VALUES (@LinkID, @InternalPatientID, @LinkageTypeID);
							END	  
							ELSE 
							BEGIN
								-- Create new LinkID

								SELECT @LinkID =  MAX(LinkID)  FROM [dbo].[PatientLinked];
								SET @LinkID =  @LinkID + 1;
																
								INSERT INTO  [dbo].[PatientLinked]
								(
									LinkID, InternalPatientID, LinkageTypeID
								)
								VALUES (@LinkID, @LocalPatientID, @LinkageTypeID);

								INSERT INTO  [dbo].[PatientLinked]
								(
									LinkID, InternalPatientID, LinkageTypeID
								)
								VALUES (@LinkID, @InternalPatientID, @LinkageTypeID);
							END

							SELECT @SuccessMessage = CONCAT('Successfully Linked PatienID: ', @LocalPatientID,
									'  And ', @InternalPatientID);
							INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
								VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
						END 
					END
					ELSE IF (@PerformInsertPatientID = 0 AND @IsLocked = 0)
					BEGIN
						IF (@PatientInfoNewer = 1) -- Don't Update Patient's Fields Because DataBase is never
						BEGIN
							SET @PatientID =  NULL;
							SET @PatientName = NULL;
							SET @BirthDate = NULL;
							SET @SexID = NULL;
							SET @StateID = NULL; 
							SET @CountryID = NULL;  
							SET @Accountnumber = NULL;       
							SET @PatientSSN = NULL;
							SET @ExtJson = NULL;
							SET @InternalAssigningAuthorityID  = NULL;
						 END

						 UPDATE [PHI].[PATIENT] SET
							InternalAssigningAuthorityID = COALESCE( @InternalAssigningAuthorityID,InternalAssigningAuthorityID),
							LastupdateUserID = @LastupdateUserID, 
							PatientID = COALESCE(@PatientID, PatientID),
							PatientName = COALESCE(@PatientName, PatientName),
							BirthDate = COALESCE(@BirthDate, BirthDate),  
							SexID = COALESCE(@SexID, SexID), 
							StateID = COALESCE(@StateID, StateID),  
							CountryID = COALESCE(@CountryID, CountryID), 
							Accountnumber = COALESCE(@Accountnumber, Accountnumber),      
							Ssn = COALESCE(@PatientSSN, @PatientSSN),
							ExtJson = COALESCE(@ExtJsonPatient, ExtJson)
								WHERE InternalPatientID = @LocalPatientID; 

						   SET @InternalPatientID = @LocalPatientID;
											
						   SELECT @SuccessMessage = CONCAT('Successfully Update PatienID: ', @LocalPatientID,
									'  With IssuerOfPatientID: ', @IssuerOfPatientID);
						  
						   INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
									VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
					 END	
		END
		ELSE
		BEGIN
			INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
				   VALUES('STOW-RS','ERROR MESSAGE - Not Valid JSON string', @ExtJson , default);	
		END;

	END
