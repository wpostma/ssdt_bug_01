﻿
CREATE PROCEDURE [dicom].[SP_Study_Put]  
        @InternalPatientID bigint, 
		@InternalVisitID bigint, 
   		@ExtJson nvarchar(max), -- DICOM JSON
		@StudyInstanceUID nvarchar(64),
		@InternalStudyID bigint OUTPUT,
		@IsLocked bit OUTPUT,
		@StudyDBDataIsNewerOutput bit OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE  @IsActive bit = 0;
	DECLARE	 @CurValue nvarchar(max);
	DECLARE  @StudyID nvarchar(64) = NULL;		
	DECLARE  @PriorityValue smallint = NULL;
	DECLARE  @StatusValue smallint = NULL;
	DECLARE  @StudyDescription nvarchar(max) = NULL;
	DECLARE  @RequestedProcedureID nvarchar(64);
	DECLARE  @AccessionNumber  nvarchar(64) = NULL;
	DECLARE  @Modality nvarchar(16) = NULL;
	DECLARE  @ModalitiesInStudy nvarchar(1024) = NULL; 
	DECLARE  @Laterality nvarchar(16) = NULL;		
	DECLARE  @BodyPart nvarchar(16) = NULL;
	DECLARE  @FacilityID bigint = NULL;
	DECLARE  @ReasonForReferral nvarchar(max) = NULL;
	DECLARE  @ClinicalNotes nvarchar(max) = NULL;
	DECLARE  @StudyComments nvarchar(max) = NULL;
	DECLARE	 @Notes nvarchar(max) = NULL;

	DECLARE  @LastUpdateUserID bigint = NULL;
	
	DECLARE  @ExtJsonStudy nvarchar (max) = NULL;		
	DECLARE  @TimeStamp datetime2(7) = NULL ;
	DECLARE  @InstanceAvailabilityID int = NULL;
	DECLARE  @BodyPartID bigint = NULL;
	DECLARE  @IsBilled bit = 0;
	DECLARE  @IsPosted bit = 0;
	DECLARE  @HL7Updated bit = 0;
	DECLARE  @StudyDBDataIsNewer bit = 0;  -- Dicom Entry is newer
		
	DECLARE  @UniversalEntityID nvarchar (max) = NULL;
	DECLARE  @UniversalEntityIDTypeCode nvarchar (64) = NULL;
	DECLARE  @UniversalEntityIDTypeID int = NULL;
	DECLARE  @IssuerOfAdmissionIDSequenceValue nvarchar(1024) = NULL;
	DECLARE  @LocalNameSpaceEntityCode nvarchar(64) = NULL;
	DECLARE  @InternalAssigningAuthorityID  bigint = NULL;
	
	DECLARE  @OrderFillerUniversalEntityID nvarchar (max) = NULL;
	DECLARE  @OrderFillerUniversalEntityIDTypeCode nvarchar (64) = NULL;
	DECLARE  @OrderFillerUniversalEntityIDTypeID int = NULL;
	DECLARE  @OrderFillerSequenceValue nvarchar(1024) = NULL;
	DECLARE  @OrderFillerLocalNameSpaceEntityCode nvarchar(64) = NULL;
    DECLARE  @OrderFillerOrderNumber nvarchar(64) = NULL;  
	
	DECLARE  @OrderPlacerUniversalEntityID nvarchar (max) = NULL;
	DECLARE  @OrderPlacerUniversalEntityIDTypeCode nvarchar (64) = NULL;
	DECLARE  @OrderPlacerUniversalEntityIDTypeID int = NULL;
	DECLARE  @OrderPlacerSequenceValue nvarchar(1024) = NULL;
	DECLARE  @OrderPlacerLocalNameSpaceEntityCode nvarchar(64) = NULL;
    DECLARE  @OrderPlacerOrderNumber nvarchar(64) = NULL;  

	DECLARE  @PatientTransportArrangements nvarchar(64) = NULL;	
	DECLARE  @TempCode nvarchar(256) = NULL;
	DECLARE  @DicomKeyword nvarchar(256) = NULL;				
    DECLARE  @ContinueExecution bit = 0;	
	DECLARE  @ValidDicomValue bit = 0;	
	DECLARE  @DicomValue  nvarchar(max) = NULL;
	DECLARE  @SuccessMessage  nvarchar(max);
	DECLARE  @ErrorMessage  nvarchar(max);
	DECLARE  @LocalStudyID bigint = NULL;
    DECLARE  @LocalStatusValue smallint = NULL;
	DECLARE  @LocalAssigningAuthorityName nvarchar(64) = NULL;
	DECLARE  @LockedStudyStatus smallint;
	DECLARE  @StudyDateDicom nvarchar(8) = NULL;
	DECLARE  @StudyTimeDicom nvarchar(6) = NULL;
	DECLARE  @StudyDateTimeUTC datetime2(7) = NULL;
	DECLARE  @TimeZoneOffsetFromUTC nvarchar(20) = NULL; 
	DECLARE  @OutputDateTime  datetime2(7);
	DECLARE  @StudyDateTimeDB datetimeoffset(7) = NULL;
	DECLARE  @PerformInsertStudy bit = 0;
	DECLARE  @StudyLastUpdatedUTC datetime2(7);

	-- Initial Settings for Output Variables 
	SET   @InternalStudyID = NULL;
	SET   @IsLocked = 1;
	SET   @StudyDBDataIsNewerOutput = 0;
			
	-- Continue Operations if we have valid @ExtJSon
	IF  (ISJSON(@ExtJson) > 0)
	BEGIN 
		/* Fetch Data from Input ExtJson String */
			

        SET @SuccessMessage = 'Store Procedure <SP_Study_Put>  Start';
		INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);

		SET @ContinueExecution = 1;	

		DECLARE @ProcessedIDTab TABLE (id bigint);
			
		SELECT @LastupdateUserID = InternalUserID from [dbo].[User] WHERE UserName = 'DICOM'; 
		
		SET @LockedStudyStatus = [dbo].GetSystemConfigIntValue('Prevent Modification Status');
		-- Check Config Value 
		SELECT @TempCode = StatusName  FROM [code].[Status] WHERE StatusValue = @LockedStudyStatus;
		IF (@TempCode IS NULL) -- Error Situation
		BEGIN 
			SELECT  @LockedStudyStatus = StatusValue FROM [code].[Status] WHERE StatusName = 'SIGNED';  
					
			SELECT @ErrorMessage = CONCAT('Invalid Config Entry: ', 'Prevent Modification Status');
			INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
				VALUES('STOW-RS','ERROR MESSAGE ', @ErrorMessage , default);	
		END 

		SELECT  @LocalStudyID = InternalStudyID,
				@LocalStatusValue = StatusValue,  @LocalAssigningAuthorityName  = auth.Name
				FROM [PHI].[Study] st JOIN [dbo].[AssigningAuthority] auth
				ON  st.InternalAssigningAuthorityID = auth.InternalAssigningAuthorityID
				WHERE StudyUID = @StudyInstanceUID AND InternalPatientID = @InternalPatientID;

        /*  Check if Study is Looked */
        IF ((@LocalStudyID IS NOT NULL) AND  (@LocalStatusValue  > @LockedStudyStatus))  
		BEGIN 
			SET @IsLocked = 1; -- Study Is Locked. Exit  
        END 
		ELSE
		BEGIN
			SET @IsLocked = 0; -- Study Is Not Locked  
				/* We will get the LastUpdateUserID from Session */   
			SELECT @LastupdateUserID = InternalUserID from [dbo].[User] WHERE UserName = 'DICOM'; 
					
			--  AccessionNumber
			SET @DicomKeyword = 'AccessionNumber';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1) 
			BEGIN
				SET @AccessionNumber = @DicomValue;  
			END  		     

			IF (@LocalStudyID IS NULL)  
			BEGIN
				    SET @PerformInsertStudy = 1;
					DECLARE @ConflictAccessionNumber nvarchar(64) = NULL;
					/* Resolve Conflict of Accession Number */ 

				IF ( @AccessionNumber IS NOT NULL)
				BEGIN
					SELECT TOP 1 @ConflictAccessionNumber  = AccessionNumber FROM [PHI].[Study] 
							WHERE AccessionNumber = @AccessionNumber; 
					IF ( @ConflictAccessionNumber IS NOT NULL)
					BEGIN
						SET @AccessionNumber = NULL;
					END
				END
			END

			-- Construct JSON Object for Study's ExtJson field;
			IF (@PerformInsertStudy = 1)
			BEGIN
	 			SET @ExtJsonStudy = '{ }'; -- Initialize @ExtJsonStudy
			END 
			ELSE 
			BEGIN
			    -- Build   @ExtJsonStudy from Existing Study 
				SELECT @HL7Updated = IsHl7Updated,  @StudyLastUpdatedUTC = LastUpdateUTC, 
					    @ExtJsonStudy = ExtJson FROM [PHI].[Study]
					WHERE InternalStudyID = @LocalStudyID;
                    
				IF (@HL7Updated = 1)
				BEGIN
					SET @StudyDBDataIsNewer = 1;	
					SET @StudyDBDataIsNewerOutput = 1;		
				END 
				ELSE
				BEGIN   
					SET @StudyDBDataIsNewer = [dbo].GetSystemConfigBool('Rely Upon DB Information When Processing Dicom Object');   
					IF (@StudyDBDataIsNewer = 1) 
					BEGIN
						SELECT @SuccessMessage = 'Configuration Setting <Rely Upon DB Information When Processing Dicom Object> is True';			  
						INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
							VALUES('STOW-RS','INFO MESSAGE ', @SuccessMessage , default);
					END 
					ELSE
					BEGIN
						EXEC [dicom].[SP_IsDBDataNewer]
							@ExtJson, @StudyLastUpdatedUTC, @StudyDBDataIsNewer OUTPUT;
					END			                 
				END;
			END  

			/* Parsing the Values from DICOM JSON string if  @StudyDBDataIsNewer is false
				If  @StudyDBDataIsNewer is true we are using Database Value */

			IF (@StudyDBDataIsNewer = 0)
			BEGIN
				--   StudyID
				SET @DicomKeyword = 'StudyID';
				EXEC [dicom].[SP_GetDicomValueFromJson]   
   						@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
				IF (@ValidDicomValue = 1)
				BEGIN
					SET @StudyID = @DicomValue;  
				END  		    
			
					-- Set Authority 
					/* IssuerOfAccessionNumberSequence */
				EXEC  [dicom].[SP_GetDicomValueFromJson]   
   							'IssuerOfAccessionNumberSequence', @ExtJson, @IssuerOfAdmissionIDSequenceValue  OUTPUT,
								@ValidDicomValue  OUTPUT;
				IF (@ValidDicomValue = 1)  
				BEGIN			
					EXEC  [dicom].[SP_GetDicomValueFromJson]
								'UniversalEntityID',  @IssuerOfAdmissionIDSequenceValue,  @UniversalEntityID  OUTPUT,   @ValidDicomValue  OUTPUT;
                  
	 			IF (@UniversalEntityID <> '')
				BEGIN
					SET @UniversalEntityID  = UPPER(@UniversalEntityID); 
							-- Extract Conditional Field  'UniversalEntityIDType'
					EXEC  [dicom].[SP_GetDicomValueFromJson]
								'UniversalEntityIDType',  @IssuerOfAdmissionIDSequenceValue, @UniversalEntityIDTypeCode  OUTPUT,  @ValidDicomValue  OUTPUT;
                    
						SELECT @UniversalEntityIDTypeID = UniversalEntityIDTypeID  FROM [code].[UniversalEntityIDType] 
							WHERE UniversalEntityIDTypeCode = @UniversalEntityIDTypeCode;
							-- NULL Value is Permitted in  dbo.AssigningAuthority for UniversalEntityIDTypeCode

					EXEC  [dicom].[SP_GetDicomValueFromJson]
								'LocalNamespaceEntityID',  @IssuerOfAdmissionIDSequenceValue,
									@LocalNameSpaceEntityCode  OUTPUT,  @ValidDicomValue  OUTPUT;

					IF (@LocalNameSpaceEntityCode IS NOT NULL)
					BEGIN
						SET @LocalNameSpaceEntityCode = UPPER(@LocalNameSpaceEntityCode); 
					END
				END 
		        				 
				IF (@UniversalEntityID IS NOT NULL)
				BEGIN
					SELECT @InternalAssigningAuthorityID = InternalAssigningAuthorityID FROM [dbo].[AssigningAuthority]
										WHERE UniversalEntityID = @UniversalEntityID; 
					IF (@InternalAssigningAuthorityID IS NULL) 
					BEGIN 
						IF (@LocalNameSpaceEntityCode IS NOT NULL)
						BEGIN
							SELECT @InternalAssigningAuthorityID = InternalAssigningAuthorityID FROM [dbo].[AssigningAuthority]
									WHERE [Name] = @LocalNameSpaceEntityCode;
						END
					END
					 
					IF (@InternalAssigningAuthorityID IS NULL) 
					BEGIN
						IF (@LocalNameSpaceEntityCode IS NULL)
						BEGIN
							SET @LocalNameSpaceEntityCode = SUBSTRING(@UniversalEntityID, 1,64); 
						END
								-- Insert new Entry in AssigningAuthority
	  					INSERT INTO dbo.AssigningAuthority([Name], UniversalEntityID, UniversalEntityIDTypeID, IsActive)
										OUTPUT inserted.InternalAssigningAuthorityID INTO @ProcessedIDTab
				       			VALUES (@LocalNameSpaceEntityCode, @UniversalEntityID,  @UniversalEntityIDTypeID, 1);

						SET @InternalAssigningAuthorityID = (SELECT TOP 1 id FROM @ProcessedIDTab);
								-- We will reuse @ProcessedIDTab for Inserting Patient
						DELETE FROM  @ProcessedIDTab;
						END
					END 
					ELSE IF (@LocalNameSpaceEntityCode IS NOT NULL) 
					BEGIN
					SELECT @InternalAssigningAuthorityID = InternalAssigningAuthorityID FROM [dbo].[AssigningAuthority]
										WHERE [Name] = @LocalNameSpaceEntityCode;
					IF (@InternalAssigningAuthorityID IS NULL) 
					BEGIN
					-- Insert new Entry in AssigningAuthority
	  					INSERT INTO dbo.AssigningAuthority([Name], IsActive)
										OUTPUT inserted.InternalAssigningAuthorityID INTO @ProcessedIDTab
						VALUES (@LocalNameSpaceEntityCode, 1);

						SET @InternalAssigningAuthorityID = (SELECT TOP 1 id FROM @ProcessedIDTab);
						DELETE FROM  @ProcessedIDTab;
					END 
				END
			END
			-- If @InternalAssigningAuthorityID IS NULL Study Trigger is inserting Patient InternalAssigningAuthorityID
		
			/* We will set the value of ModalitiesInStudy in ExtJson
					This field in FHIR Standard of Study. Modality Field is related to Series */
			 
			SET @DicomKeyword = 'ModalitiesInStudy';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1) 
			BEGIN
				SET @ModalitiesInStudy = @DicomValue;  
			END  		   
         
			SET @DicomKeyword = 'Modality';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1) 
			BEGIN
				SET @Modality = @DicomValue;  
			END  		     

			SET @DicomKeyword = 'Laterality';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)
			BEGIN
				SET @Laterality = @DicomValue;  
			END  		     

			-- BodyPart 
			SET @DicomKeyword = 'BodyPartExamined';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)
			BEGIN
				SET  @BodyPart = @DicomValue;  
				SELECT @BodyPartID = InternalBodyPartID  FROM [code].[BodyPart] WHERE Value =  @BodyPart;
			END  		
			
			-- RequestedProcedureID 
			SET @DicomKeyword = 'RequestedProcedureID';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)
			BEGIN
				SET @RequestedProcedureID =  @DicomValue;   
			END  		    

			-- Priority. First Using 'RequestedProcedurePriority'; 
			SET @DicomKeyword = 'RequestedProcedurePriority';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)
			BEGIN
				/* Check Received Priority */
				SELECT @PriorityValue = PriorityValue FROM  [code].[Priority] WHERE [Name] = @DicomValue;   
			END  		     
		
			IF  (@PriorityValue IS NULL)
			BEGIN
				-- -- We are using RETIRED_StudyPriorityID as in  Versios 6.X
				SET @DicomKeyword = 'RETIRED_StudyPriorityID';
				EXEC [dicom].[SP_GetDicomValueFromJson]   
   						@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT, @ValidDicomValue  OUTPUT;

				IF (@ValidDicomValue = 1)
				BEGIN
					/* Check Received Priority */
					SELECT @PriorityValue = PriorityValue FROM  [code].[Priority] WHERE [Name] = @DicomValue;   
					IF  (@PriorityValue IS NULL)
					BEGIN
						-- We not will be fail if Priority hasn't Standard Value
						-- Priority is mandatoty field in our System  					      
						SET @PriorityValue = [dbo].GetSystemConfigIntValue('Default Priority Value For New Study');
					END
				END  		     
				ELSE
				BEGIN
					SET @PriorityValue = [dbo].GetSystemConfigIntValue('Default Priority Value For New Study');
				END
			END

			-- Study Status 
			-- We are managing 'RETIRED_StudyStatusID' in Versios 6.X
			SET @DicomKeyword = 'RETIRED_StudyStatusID';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)  
			BEGIN
				/* Check Received Study Status */
				SELECT @StatusValue = StatusValue FROM  [code].[Status] WHERE  StatusName = @DicomValue;   
				IF  (@StatusValue IS NULL)
				BEGIN
					-- We not will be fail if StudyStatus hasn't Standard Value
					-- Study Status is mandatory value in our System
					SET @StatusValue = [dbo].GetSystemConfigIntValue('Default Status Order For New Study');
				END
			END  		     
			ELSE
			BEGIN
				SET @StatusValue = [dbo].GetSystemConfigIntValue('Default Status Order For New Study');
			END
	
			-- PatientTransportArrangements
			SET @DicomKeyword = 'PatientTransportArrangements';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)
			BEGIN
				-- We will be use @PatientTransportArrangements in ExtJson string
				SET  @PatientTransportArrangements = @DicomValue; 
			END  				
					
			-- Study Will be Saved in Disk Space, In this time Set Availability as ONLINE 
			SELECT @InstanceAvailabilityID = InstanceAvailabilityID FROM [code].[InstanceAvailability]
				WHERE InstanceAvailabilityCode = 'ONLINE';
																	
			SET @DicomKeyword = 'RETIRED_StudyComments';
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
   				@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,  @ValidDicomValue  OUTPUT;
      		IF (@ValidDicomValue = 1)  
			BEGIN
				SET @StudyComments = @DicomValue;
			END		
		        														
			SET @DicomKeyword = 'AdditionalPatientHistory';
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
   				@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,  @ValidDicomValue  OUTPUT;
      		IF (@ValidDicomValue = 1)  
			BEGIN
				SET @ClinicalNotes = @DicomValue;
			END		 	
			     														
			SET @DicomKeyword = 'RETIRED_ReasonForStudy';
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
   				@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,  @ValidDicomValue  OUTPUT;
      		IF (@ValidDicomValue = 1)  
			BEGIN
				SET @ReasonForReferral = @DicomValue;
			END		 				
																			
			SET @DicomKeyword = 'StudyDescription';
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
   				@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,  @ValidDicomValue  OUTPUT;
      		IF (@ValidDicomValue = 1)  
			BEGIN
				SET @StudyDescription = @DicomValue;
			END		 

			-- DICOM StudyDate  
			SET @DicomKeyword = 'StudyDate';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1) 
			BEGIN
				SET @StudyDateDicom = @DicomValue;  
			END 
				
			-- DICOM StudyTime 
			SET @DicomKeyword = 'StudyTime';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1) 
			BEGIN
				SET @StudyTimeDicom = @DicomValue;  
			END 
				
			EXEC  [dicom].[SP_GetTimeZoneOffset] @ExtJson, @TimeZoneOffsetFromUTC OUTPUT;
			IF (@StudyDateDicom IS NOT NULL)
			BEGIN
				EXEC  [dicom].[SP_ConvertDicomDateTimeToDateTime]   
   					@StudyDateDicom, @StudyTimeDicom, @TimeZoneOffsetFromUTC, 
						@OutputDateTime OUTPUT, @ValidDicomValue OUTPUT;
			END  
			IF (@ValidDicomValue = 1)
			BEGIN
				SET @StudyDateTimeUTC = @OutputDateTime;
				SET @StudyDateTimeDB = SWITCHOFFSET(@OutputDateTime, @TimeZoneOffsetFromUTC);
			END

			-- Institution Name as Facility Name 
			SET @DicomKeyword = 'InstitutionName';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1) 
			BEGIN
				SELECT @FacilityID = InternalOrganizationID FROM  [dbo].[Organization] WHERE OrganizationName = @DicomValue;  
			END 
 	
			IF ((@StudyComments IS NOT NULL) OR (@ClinicalNotes IS NOT NULL)
					OR (@ReasonForReferral IS NOT NULL))
            BEGIN  
			  	SET	 @Notes = '{ }'; -- Initialize as Json text
				IF (@ClinicalNotes IS NOT NULL)
				BEGIN
					SET  @Notes  = JSON_MODIFY(@Notes,'$.clinical',  @ClinicalNotes);	
				END

				IF (@StudyComments IS NOT NULL)
				BEGIN
					SET  @Notes  = JSON_MODIFY(@Notes,'$.comments',  @StudyComments);	
				END
						
				IF (@ReasonForReferral IS NOT NULL)
				BEGIN
					SET  @Notes  = JSON_MODIFY(@Notes,'$.history',  @ReasonForReferral);	
				END

				SET  @ExtJsonStudy = JSON_MODIFY(@ExtJsonStudy, '$.notes', JSON_QUERY(@Notes));

			END

			IF (@RequestedProcedureID IS NOT NULL) 
			BEGIN
				SET @ExtJsonStudy = JSON_MODIFY(@ExtJsonStudy, '$.RequestedProcedureID', @RequestedProcedureID); 
			END
				
			IF (@PatientTransportArrangements IS NOT NULL) 
			BEGIN
				SET @ExtJsonStudy = JSON_MODIFY(@ExtJsonStudy, '$.PatientTransportArrangements', @PatientTransportArrangements); 
			END

			IF (@ModalitiesInStudy IS NOT NULL)
			BEGIN
				DECLARE Json_Cursor CURSOR LOCAL FOR SELECT [value] FROM OPENJSON(@ModalitiesInStudy);
					
				OPEN Json_Cursor;
				FETCH NEXT  FROM Json_Cursor INTO @CurValue;

				WHILE (@@FETCH_STATUS = 0) 
				BEGIN
					SET  @ExtJsonStudy = JSON_MODIFY(@ExtJsonStudy, 'append $.ModalitiesInStudy', @CurValue);	
					FETCH NEXT  FROM Json_Cursor INTO @CurValue;
					END
                 
				    CLOSE Json_Cursor; 
					DEALLOCATE Json_Cursor; 
				END

				-- Add Order Filler to ExtJsonField Field
		    EXEC  [dicom].[SP_GetDicomValueFromJson]   
   				'FillerOrderNumberImagingServiceRequest', @ExtJson, @DicomValue  OUTPUT,
					     		@ValidDicomValue  OUTPUT; 
            SET @OrderFillerOrderNumber = NULL;
            IF (@ValidDicomValue = 1)  
			BEGIN	 
                SET @OrderFillerOrderNumber = UPPER(@DicomValue);  
		    END

			EXEC  [dicom].[SP_GetDicomValueFromJson]   
   				'OrderFillerIdentifierSequence', @ExtJson, @DicomValue  OUTPUT,
					     		@ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)  
			BEGIN
				SET @OrderFillerSequenceValue = @DicomValue;		
				EXEC  [dicom].[SP_GetDicomValueFromJson]
							'UniversalEntityID',  @OrderFillerSequenceValue,  @OrderFillerUniversalEntityID  OUTPUT,   @ValidDicomValue  OUTPUT;
                  
	 			IF ((@OrderFillerUniversalEntityID IS NOT NULL) AND (@OrderFillerUniversalEntityID <> ''))
				BEGIN
					SET @OrderFillerUniversalEntityID  = UPPER(@OrderFillerUniversalEntityID); 
							-- Extract Conditional Field  'UniversalEntityIDType'
					EXEC  [dicom].[SP_GetDicomValueFromJson]
								'UniversalEntityIDType',  @OrderFillerSequenceValue, @OrderFillerUniversalEntityIDTypeCode  OUTPUT,  @ValidDicomValue  OUTPUT;
                    
					EXEC  [dicom].[SP_GetDicomValueFromJson]
								'LocalNamespaceEntityID',  @OrderFillerSequenceValue,
									@OrderFillerLocalNameSpaceEntityCode OUTPUT,  @ValidDicomValue  OUTPUT;

					IF (@OrderFillerLocalNameSpaceEntityCode IS NOT NULL)
					BEGIN
						SET @OrderFillerLocalNameSpaceEntityCode = UPPER(@OrderFillerLocalNameSpaceEntityCode); 
					END
				END 
		    END					

			DECLARE @ExtJsonOrderFiller nvarchar(max) = NULL;
			IF ((@OrderFillerOrderNumber IS NOT NULL) OR (@OrderFillerSequenceValue IS NOT NULL))
		    BEGIN  
			  	SET	 @ExtJsonOrderFiller = '{ }'; -- Initialize as Json text
				IF (@OrderFillerOrderNumber IS NOT NULL)
				BEGIN
					SET @ExtJsonOrderFiller = JSON_MODIFY(@ExtJsonOrderFiller,'$.OrderNumber', @OrderFillerOrderNumber);
				END 
															
				IF (@OrderFillerUniversalEntityID IS NOT NULL)
				BEGIN
					SET @ExtJsonOrderFiller = JSON_MODIFY(@ExtJsonOrderFiller,'$.UniversalEntityID', @OrderFillerUniversalEntityID);
				END

                IF (@OrderFillerUniversalEntityIDTypeCode IS NOT NULL)
				BEGIN
					SET @ExtJsonOrderFiller = JSON_MODIFY(@ExtJsonOrderFiller, '$.UniversalEntityIDType', @OrderFillerUniversalEntityIDTypeCode);
				END
					
				IF (@OrderFillerLocalNameSpaceEntityCode IS NOT NULL)
				BEGIN
					SET @ExtJsonOrderFiller = JSON_MODIFY(@ExtJsonOrderFiller,'$.LocalNamespaceEntityID', @OrderFillerLocalNameSpaceEntityCode);
				END

				SET  @ExtJsonStudy = JSON_MODIFY(@ExtJsonStudy, '$.OrderFiller', JSON_QUERY(@ExtJsonOrderFiller));

			END
		  			
		    -- Add Order Placer
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
   				'PlacerOrderNumberImagingServiceRequest', @ExtJson, @DicomValue  OUTPUT, @ValidDicomValue  OUTPUT; 
            IF (@ValidDicomValue = 1)  
			BEGIN	 
                SET @OrderPlacerOrderNumber = UPPER(@DicomValue);  
		    END
        
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
   				'OrderPlacerIdentifierSequence', @ExtJson, @DicomValue  OUTPUT, @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)  
			BEGIN	
				SET @OrderPlacerSequenceValue = @DicomValue; 		
				EXEC  [dicom].[SP_GetDicomValueFromJson]
							'UniversalEntityID',  @OrderPlacerSequenceValue,  @OrderPlacerUniversalEntityID  OUTPUT,   @ValidDicomValue  OUTPUT;
                  
	 			IF ((@OrderPlacerUniversalEntityID IS NOT NULL)  AND (@OrderPlacerUniversalEntityID <> ''))
				BEGIN
					SET @OrderPlacerUniversalEntityID  = UPPER(@OrderPlacerUniversalEntityID); 
							-- Extract Conditional Field  'UniversalEntityIDType'
					EXEC  [dicom].[SP_GetDicomValueFromJson]
								'UniversalEntityIDType',  @OrderPlacerSequenceValue, @OrderPlacerUniversalEntityIDTypeCode  OUTPUT,  @ValidDicomValue  OUTPUT;
                    
					EXEC  [dicom].[SP_GetDicomValueFromJson]
								'LocalNamespaceEntityID',  @OrderPlacerSequenceValue,
									@OrderPlacerLocalNameSpaceEntityCode OUTPUT,  @ValidDicomValue  OUTPUT;

					IF (@OrderPlacerLocalNameSpaceEntityCode IS NOT NULL)
					BEGIN
						SET @OrderPlacerLocalNameSpaceEntityCode = UPPER(@OrderPlacerLocalNameSpaceEntityCode); 
					END
				END 
		    END					

			DECLARE @ExtJsonOrderPlacer nvarchar(max) = NULL;
			IF ((@OrderPlacerOrderNumber IS NOT NULL) OR (@OrderPlacerSequenceValue IS NOT NULL))
		    BEGIN  
			  	SET	 @ExtJsonOrderPlacer = '{ }'; -- Initialize as Json text
				IF (@OrderPlacerOrderNumber IS NOT NULL)
				BEGIN
					SET @ExtJsonOrderPlacer = JSON_MODIFY(@ExtJsonOrderPlacer,'$.OrderNumber', @OrderPlacerOrderNumber);
				END 
																				
				IF (@OrderPlacerUniversalEntityID IS NOT NULL)
				BEGIN
					SET @ExtJsonOrderPlacer = JSON_MODIFY(@ExtJsonOrderPlacer,'$.UniversalEntityID', @OrderPlacerUniversalEntityID);
				END
					
				IF (@OrderPlacerUniversalEntityIDTypeCode IS NOT NULL)
				BEGIN
					SET @ExtJsonOrderPlacer = JSON_MODIFY(@ExtJsonOrderPlacer, '$.UniversalEntityIDType', @OrderPlacerUniversalEntityIDTypeCode);
				END
		
				IF (@OrderPlacerLocalNameSpaceEntityCode IS NOT NULL)
				BEGIN
					SET @ExtJsonOrderPlacer = JSON_MODIFY(@ExtJsonOrderPlacer,'$.LocalNamespaceEntityID', @OrderPlacerLocalNameSpaceEntityCode);
				END

				SET  @ExtJsonStudy = JSON_MODIFY(@ExtJsonStudy, '$.OrderPlacer', JSON_QUERY(@ExtJsonOrderPlacer));

			END
			
		END  /* End if @StudyDBDataIsNewer is False */
					              				
		-- Perform Inset or Update 	
        IF (@PerformInsertStudy = 1)
		BEGIN
			SET @IsActive = 1;
			INSERT INTO [PHI].[Study]
			(
				[IsActive],
				[InternalAssigningAuthorityID],
				[ModalityCode],
				[LateralityCode],
				[InternalBodyPartID],
				[PriorityValue],
				[StatusValue],
				[LastUpdateUserID],
				[StudyDateTime],
				[StudyDateTimeUTC],
				[Description],
				[StudyID],					  
				[IsHL7Updated],					  
				[IsBilled],
				[IsPosted],
				[ExtJson],
				[StudyUID],
				[AccessionNumber],
				[InternalPatientID],
				[ImagingFacilityID],
				[InstanceAvailabilityID],
				[PrimaryEncounterID]
			)
			OUTPUT inserted.InternalStudyID INTO @ProcessedIDTab
			VALUES (
				@IsActive,
				@InternalAssigningAuthorityID,
				@Modality,
				@Laterality,
				@BodyPartID,
				@PriorityValue,
				@StatusValue,
				@LastUpdateUserID,
				@StudyDateTimeDB,
				@StudyDateTimeUTC,
				@StudyDescription,
				@StudyID,
				@HL7Updated,
				@IsBilled,
				@IsPosted,
				@ExtJsonStudy,
				@StudyInstanceUID,
				@AccessionNumber,
				@InternalPatientID,
				@FacilityID,
				@InstanceAvailabilityID,
				@InternalVisitID
			);
				
			SET @InternalStudyID = (SELECT TOP 1 id FROM @ProcessedIDTab);

			SELECT @SuccessMessage = CONCAT('Successfully Inserted InternalStudy: ', @InternalStudyID,
					    ' For OnternalPatientID:n', @InternalPatientID);
					  
			INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			            VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
        END
		ELSE  -- Update Study
		BEGIN
			IF (@StudyDBDataIsNewer = 1)
			BEGIN
				SET @InternalAssigningAuthorityID = NULL;
				SET @Modality = NULL;
				SET @Laterality = NULL;
				SET @BodyPartID = NULL;
				SET @PriorityValue = NULL;
				SET @StatusValue = NULL;
				SET @StudyDateTimeDB  = NULL;
				SET @StudyDateTimeUTC = NULL;
				SET @StudyDescription = NULL;
				SET @StudyID = NULL;
				SET @ExtJsonStudy = NULL;
				SET @AccessionNumber = NULL;
				SET @FacilityID = NULL;
				SET @InstanceAvailabilityID = NULL;
			END

			UPDATE [PHI].[STUDY] SET
				InternalAssigningAuthorityID = COALESCE( @InternalAssigningAuthorityID, InternalAssigningAuthorityID),
				LastupdateUserID = @LastupdateUserID, 
				ModalityCode = COALESCE(@Modality, ModalityCode), 
				LateralityCode = COALESCE(@Laterality, LateralityCode),
				InternalBodyPartID = COALESCE(@BodyPartID, InternalBodyPartID),
				PriorityValue = COALESCE(@PriorityValue, PriorityValue),
				StatusValue = COALESCE(@StatusValue, StatusValue),
				StudyDateTime = COALESCE(@StudyDateTimeDB, StudyDateTime),
				StudyDateTimeUTC = COALESCE(@StudyDateTimeUTC, StudyDateTimeUTC),
				[Description] = COALESCE(@StudyDescription, [Description]),
				StudyID = COALESCE(@StudyID, StudyID ),
				AccessionNumber = COALESCE(@AccessionNumber, AccessionNumber), 
				ImagingFacilityID = COALESCE(@FacilityID, ImagingFacilityID), 
				InstanceAvailabilityID = COALESCE(@InstanceAvailabilityID, InstanceAvailabilityID),  
				ExtJson = COALESCE(@ExtJsonStudy, ExtJson)
					WHERE  InternalStudyID = @LocalStudyID; 

				SET @InternalStudyID = @LocalStudyID;
					                    
				SELECT @SuccessMessage = CONCAT('Successfully Update InternalStudyID: ', @InternalStudyID,
					    '  Of InternalPatient: ', @InternalPatientID);  
				INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			            VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
		END
	END								
	END
	ELSE
	BEGIN
		INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			   VALUES('STOW-RS','ERROR MESSAGE - Not Valid JSON string', @ExtJson , default);	
	END;

END







