﻿
CREATE  PROCEDURE [dicom].[SP_Study_Diagnosis_Codes_Put]  
     	@InternalStudyID bigint,
	 	@ExtJson nvarchar(max),
	    @StudyDBDataIsNewer bit
AS
BEGIN
		SET NOCOUNT ON;
	
	DECLARE  @IsActive bit = 0;
	DECLARE  @ExtJsonStudyDiagnosisMap nvarchar (max) = NULL;		
	DECLARE  @StudyDiagnosisCodeSequence nvarchar (max) = NULL;	
	DECLARE  @StudyDiagnosisCodes nvarchar (max) = NULL;		
	DECLARE  @StudyDiagnosisCodeCurrentSequence nvarchar (max) = NULL;	
	DECLARE  @DiagnosisCode nvarchar (16) = NULL; /* CodeValue */
	DECLARE  @DiagnosisDescription  nvarchar (64) = NULL; /* CodeMeaning */
	DECLARE  @CodingSchemeDesignator nvarchar (16) = NULL; /* CodingSchemeDesignator */
	
	DECLARE  @DiagnosisCodeTag nvarchar(16) = '00080100';
	DECLARE  @CodingSchemeDesignatorTag nvarchar (16) = '00080102';
	DECLARE  @DiagnosisDescriptionTag  nvarchar(64) = '00080104';
	
	DECLARE  @CurrentValue nvarchar(max) = NULL;
	
	DECLARE  @DicomKeyword nvarchar(256) = NULL;				
    DECLARE  @ValidDicomValue bit = 0;	
	DECLARE  @DicomValue  nvarchar(max) = NULL;
	DECLARE  @SuccessMessage  nvarchar(max) = '';
	
   	DECLARE  @PerformInsertStudyDiagnosisCodes bit = 0;
	DECLARE  @InternalDiagnosisCodeID  int = NULL;
	DECLARE  @ContinueOperation bit = 0;
	DECLARE  @LocalInternalStudyDiagnosisMapID bigint = NULL;
	
    SET @SuccessMessage = 'Store Diagnosis <SP_Study_Diagnosis_Codes_Put>  Start';
	INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
		
	SET @DicomKeyword = 'AdmittingDiagnosesCodeSequence';
	EXEC [dicom].[SP_GetDicomValueFromJson]   
   			@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
	IF (@ValidDicomValue = 1) 
	BEGIN
		SET @StudyDiagnosisCodeSequence = @DicomValue;
		SET @ContinueOperation = 1;
	END 

    IF (@ContinueOperation = 1)
	BEGIN	
		SELECT TOP 1 @StudyDiagnosisCodes = [value] FROM OPENJSON(@StudyDiagnosisCodeSequence)
		     WHERE [value] <> 'SQ';

        IF (@StudyDBDataIsNewer = 0)
		BEGIN
			DELETE FROM [dbo].[StudyDiagnosisMap] WHERE  InternalStudyID = @InternalStudyID;
		END 

		DECLARE Json_Cursor  CURSOR LOCAL FOR SELECT [value] FROM OPENJSON(@StudyDiagnosisCodes); 	
		
		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @StudyDiagnosisCodeCurrentSequence;	   

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
			SELECT @CurrentValue = [Value] FROM OPENJSON(@StudyDiagnosisCodeCurrentSequence) 
			   WHERE [key] =  @DiagnosisCodeTag;
			SET @DiagnosisCode =  JSON_VALUE(@CurrentValue, '$.Value[0]');

			SELECT @CurrentValue = [Value] FROM OPENJSON(@StudyDiagnosisCodeCurrentSequence) 
				WHERE [key] = @CodingSchemeDesignatorTag;
			SET @CodingSchemeDesignator =  JSON_VALUE(@CurrentValue, '$.Value[0]');
	
			SELECT @CurrentValue = [Value] FROM OPENJSON(@StudyDiagnosisCodeCurrentSequence) 
				WHERE [key] =  @DiagnosisDescriptionTag;
			SET @DiagnosisDescription =  JSON_VALUE(@CurrentValue, '$.Value[0]');
		
			SELECT @InternalDiagnosisCodeID = InternalDiagnosisCodeID  FROM [code].[Diagnosis]
				  WHERE DiagnosisCode =  @DiagnosisCode;
				
			IF (@InternalDiagnosisCodeID IS NOT NULL)
			BEGIN
				 -- Insert of Update [dbo].[StudyDiagnosisMap]
				 IF (@StudyDBDataIsNewer = 0)
				 BEGIN
					SET @PerformInsertStudyDiagnosisCodes = 1;
				 END
				 ELSE
				 BEGIN	 	
					SELECT   @LocalInternalStudyDiagnosisMapID = EntryID,
				      		 @ExtJsonStudyDiagnosisMap = ExtJson   FROM [dbo].[StudyDiagnosisMap] WHERE
							 InternalStudyID = @InternalStudyID AND 
								 InternalDiagnosisCodeID = @InternalDiagnosisCodeID;
					IF (@LocalInternalStudyDiagnosisMapID IS NULL)  
					BEGIN
						SET @PerformInsertStudyDiagnosisCodes = 1;
					END	
		        END  

				IF (@ExtJsonStudyDiagnosisMap IS NULL)
				BEGIN
					SET	 @ExtJsonStudyDiagnosisMap = '{ }'; -- Initialize as Json text
				END

				SET  @ExtJsonStudyDiagnosisMap = JSON_MODIFY( @ExtJsonStudyDiagnosisMap,'$.CodingSchemeDesignator', @CodingSchemeDesignator);	
                
				IF (@PerformInsertStudyDiagnosisCodes = 1)
				BEGIN
						INSERT INTO [dbo].[StudyDiagnosisMap]
						(
							[InternalStudyID],
							[InternalDiagnosisCodeID],
							[ExtJson]
						)
						VALUES (
								@InternalStudyID,
								@InternalDiagnosisCodeID,
								@ExtJsonStudyDiagnosisMap
						);
					END
					ELSE  
					BEGIN			   
						UPDATE [dbo].[StudyDiagnosisMap] SET
							ExtJson =  COALESCE(@ExtJsonStudyDiagnosisMap, ExtJson) 
								WHERE  EntryID = @LocalInternalStudyDiagnosisMapID;  
					END

					SELECT @SuccessMessage = CONCAT('Successfully Update StudyDiagnosisMAP For InternalStudyID: ', 
							@InternalStudyID, ' And InternalDiagnosisCodeID: ', @InternalDiagnosisCodeID);                    	  
					INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
								VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
			END

			FETCH NEXT FROM Json_Cursor INTO @StudyDiagnosisCodeCurrentSequence;

        END

		CLOSE Json_Cursor;
		DEALLOCATE Json_Cursor; 

	END

	SET @SuccessMessage = 'Store Diagnosis <SP_Study_Diagnosis_Codes_Put>  Finish';
	INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);

END







