﻿
CREATE PROCEDURE [dicom].[SP_Series_Put]  
        @InternalStudyID bigint, 
		@ExtJson nvarchar(max), -- DICOM JSON
		@SeriesInstanceUID nvarchar(64),
		@InternalSeriesID bigint OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE  @IsActive bit = 0;
	DECLARE  @SeriesDescription nvarchar(max) = NULL;
	DECLARE  @Modality nvarchar(16) = NULL;
	DECLARE  @Laterality nvarchar(16) = NULL;		
	DECLARE  @BodyPart nvarchar(16) = NULL;
	DECLARE  @BodyPartID bigint = NULL;
	DECLARE  @SeriesNumber int = NULL;
	DECLARE  @ExtJsonSeries nvarchar (max) = NULL;		
	DECLARE  @InstanceAvailabilityID int = NULL;
	DECLARE  @SeriesDBDataIsNewer bit = 0;  -- Dicom Entry is newer
	DECLARE  @DicomKeyword nvarchar(256) = NULL;				
    DECLARE  @ContinueExecution bit = 0;	
	DECLARE  @ValidDicomValue bit = 0;	
	DECLARE  @DicomValue  nvarchar(max) = NULL;
	DECLARE  @SuccessMessage  nvarchar(max);
	DECLARE  @ErrorMessage  nvarchar(max);
	DECLARE  @LocalSeriesID bigint = NULL;
   	DECLARE  @PerformInsertSeries bit = 0;
	DECLARE  @SeriesLastUpdatedUTC datetime2(7);
	DECLARE  @LastupdateUserID bigint = NULL;  
					
	-- Continue Operations if we have valid @JsonInput
	IF  (ISJSON(@ExtJson) > 0)
	BEGIN 
		/* Fetch Data from ExtJson String */
        SET @SuccessMessage = 'Store Procedure <SP_Series_Put>  Start';
		INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
	
		DECLARE @ProcessedIDTab TABLE (id bigint);
					
		SELECT  @LocalSeriesID = InternalSeriesID,	@ExtJsonSeries = ExtJson,
			        @SeriesLastUpdatedUTC = LastUpdateUTC
				FROM [PHI].[Series]	WHERE SeriesUID = @SeriesInstanceUID
					AND InternalStudyID = @InternalStudyID;

        IF (@LocalSeriesID IS NULL)  
		BEGIN
			SET @PerformInsertSeries = 1;
			SET @SeriesDBDataIsNewer = 0;
			SET @ExtJsonSeries = '{ }'; -- Initialize @ExtJsonSeries
		END				
		ELSE -- Check @SeriesDBDataIsNewer 
		BEGIN   
			SET @SeriesDBDataIsNewer = [dbo].GetSystemConfigBool('Rely Upon DB Information When Processing Dicom Object');   
			IF (@SeriesDBDataIsNewer = 1) 
			BEGIN
				SELECT @SuccessMessage = 'Configuration Setting <Rely Upon DB Information When Processing Dicom Object> is True';			  
				INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
					VALUES('STOW-RS','INFO MESSAGE ', @SuccessMessage , default);
			END 
			ELSE
			BEGIN
				EXEC [dicom].[SP_IsDBDataNewer]
					@ExtJson, @SeriesLastUpdatedUTC, @SeriesDBDataIsNewer OUTPUT;
			END			                 
		END
		
		/* Parsing the Values from DICOM JSON string if  @SeriesDBDataIsNewer is false
			If  @SeriesDBDataIsNewer is true we are using Database Value */

		IF (@SeriesDBDataIsNewer = 0)
		BEGIN
			--   SeresNumber
			SET @DicomKeyword = 'SeriesNumber';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)
			BEGIN
				SET @SeriesNumber = CAST(@DicomValue AS int);
			END  		    
						         
			SET @DicomKeyword = 'Modality';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1) 
			BEGIN
				SET @Modality = @DicomValue;  
			END 
			ELSE
			BEGIN
				SET @Modality = 'OT'; -- Mandatory Value for Series Table 
			END  		     

			SET @DicomKeyword = 'Laterality';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)
			BEGIN
				SET @Laterality = @DicomValue;  
			END  		     
								
			-- BodyPart 
			SET @DicomKeyword = 'BodyPartExamined';
			EXEC [dicom].[SP_GetDicomValueFromJson]   
   					@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)
			BEGIN
				SET  @BodyPart = @DicomValue;  
				SELECT @BodyPartID = InternalBodyPartID  FROM [code].[BodyPart] WHERE Value =  @BodyPart;
			END  		
				
																			
			SET @DicomKeyword = 'SeriesDescription';
			EXEC  [dicom].[SP_GetDicomValueFromJson]   
   				@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,  @ValidDicomValue  OUTPUT;
      		IF (@ValidDicomValue = 1)  
			BEGIN
				SET @SeriesDescription = @DicomValue;
			END

			-- Construct JsonObject 
			IF (@SeriesDescription IS NOT NULL) 
			BEGIN
				SET @ExtJsonSeries = JSON_MODIFY(@ExtJsonSeries, '$.Description', @SeriesDescription); 
			END
							
			-- Series Will be Saved in Disk Space, In this time Set Availability as ONLINE 
			SELECT @InstanceAvailabilityID = InstanceAvailabilityID FROM [code].[InstanceAvailability]
				WHERE InstanceAvailabilityCode = 'ONLINE';
   
            SET @ExtJsonSeries = JSON_MODIFY(@ExtJsonSeries, '$.InstanceAvailability', @InstanceAvailabilityID);  
						 
		END  /* End if @SeriesDBDataIsNewer is False */
			
		SET  @SeriesLastUpdatedUTC = SYSUTCDATETIME();
				
		SELECT @LastupdateUserID = InternalUserID from [dbo].[User] WHERE UserName = 'DICOM'; 	
					              				
		-- Perform Inset or Update 	
        IF (@PerformInsertSeries = 1)
		BEGIN
			SET @IsActive = 1;
			INSERT INTO [PHI].[Series]
			(
				[IsActive],
				[LastUpdateUTC],
				[InternalStudyID],
				[ModalityCode],
				[LateralityCode],
				[InternalBodyPartID],
				[SeriesUID],
				[SeriesNumber],
                [ExtJson],
				[LastupdateUserID]  
			)
			OUTPUT inserted.InternalSeriesID INTO @ProcessedIDTab
			VALUES (
				@IsActive,
				@SeriesLastUpdatedUTC,
				@InternalStudyID,
				@Modality,
				@Laterality,
				@BodyPartID,
				@SeriesInstanceUID,
				@SeriesNumber,
				@ExtJsonSeries,
				@LastupdateUserID
			);
				
			SET @InternalSeriesID = (SELECT TOP 1 id FROM @ProcessedIDTab);

			SELECT @SuccessMessage = CONCAT('Successfully Inserted InternalSeriesID: ', @InternalSeriesID,
				        ' For InternalStudyID: ',  @InternalStudyID);
						  
			INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			            VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);

        END
		ELSE  -- Update Series
		BEGIN
			IF (@SeriesDBDataIsNewer = 1)
			BEGIN
				SET @Modality = NULL;
				SET @Laterality = NULL;
				SET @BodyPartID = NULL;
				SET @SeriesNumber = NULL;
				SET @ExtJsonSeries = NULL;
			END

			UPDATE [PHI].[SERIES] SET
				ModalityCode = COALESCE(@Modality, ModalityCode), 
				LateralityCode = COALESCE(@Laterality, LateralityCode),
				InternalBodyPartID = COALESCE(@BodyPartID, InternalBodyPartID),
				SeriesNumber =  COALESCE(@SeriesNumber, SeriesNumber),  
				LastupdateUserID = COALESCE(@LastupdateUserID, LastupdateUserID),  
				ExtJson = COALESCE(@ExtJsonSeries, ExtJson)
					WHERE InternalSeriesID = @LocalSeriesID; 

				SET @InternalSeriesID = @LocalSeriesID;
					                    
				SELECT @SuccessMessage = CONCAT('Successfully Update InternalSeriesID: ', @InternalSeriesID,
				        ' For InternalStudyID: ',  @InternalStudyID);

				INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			            VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
		 END
	END								
	ELSE
	BEGIN
		INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			   VALUES('STOW-RS','ERROR MESSAGE - Not Valid JSON string', @ExtJson , default);	
	END;

END







