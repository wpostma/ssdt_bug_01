﻿

CREATE PROCEDURE [dicom].[SP_MakeObjectFileID]
 @InternalPatientID bigint,
 @InternalStudyID   bigint,
 @InternalSeriesID  bigint,
 @ObjectNumber int,
 @FileID nvarchar(256) OUTPUT
 
AS
BEGIN
	DECLARE  @PatientIDPath  CHAR(8);
	DECLARE  @StudyIDPath    CHAR(8);
	DECLARE  @SeriesIDPath     CHAR(8);

	EXEC [dicom].[SP_Get_Base26Value] @InternalPatientID, 8,  @PatientIDPath OUTPUT;
    EXEC [dicom].[SP_Get_Base26Value] @InternalStudyID, 8,  @StudyIDPath  OUTPUT;
	EXEC [dicom].[SP_Get_Base26Value] @InternalSeriesID, 8,  @SeriesIDPath  OUTPUT;
   
	SELECT @FileID =  CONCAT('\', @PatientIDPath,  '\', @StudyIDPath, '\',
	       @SeriesIDPath, '\', CAST(@ObjectNumber  AS VARCHAR(16)));

END

