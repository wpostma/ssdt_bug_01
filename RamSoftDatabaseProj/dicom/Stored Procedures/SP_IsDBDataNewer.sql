﻿
CREATE PROCEDURE [dicom].[SP_IsDBDataNewer]
 @JsonExt nvarchar(max),
 @DBDateTime datetime2(7),
 @IsNewer bit OUTPUT 
 
AS
BEGIN
		DECLARE  @OutputDateTime    datetime2(7) = NULL;
		DECLARE  @InputDateTimeStr  nvarchar(30) = NULL;
		DECLARE  @ModifiedTimeStr   nvarchar(10) = NULL;
		DECLARE  @ContinueExecution   bit = 1;
	    DECLARE  @DicomKeyword nvarchar(256);
        DECLARE  @ValidDicomValue bit = 0;	
        DECLARE  @DicomValue  nvarchar(64) = NULL;
        DECLARE  @TempValue  nvarchar(64) = NULL;
        DECLARE  @DicomObjectInstanceDateTime  nvarchar(64) = NULL;
        DECLARE  @DicomObjectInstanceDate  nvarchar(8) = NULL;
        DECLARE  @DicomObjectInstanceTime  nvarchar(6) = NULL;
        DECLARE  @TimezoneOffsetFromUTCDicom   nvarchar(16) = NULL;
        DECLARE  @TimezoneOffsetFromUTC   nvarchar(20) = NULL;
        DECLARE  @DataBaseNewer  bit = 0;
        DECLARE  @DataBaseDateTimeDifference   int = 0;
        DECLARE  @SignCharacter nvarchar(1) = NULL;
        DECLARE  @ValidUTC bit = NULL; 

		SET NOCOUNT ON;
        
        SET @DicomKeyword = 'InstanceCreationDate';
        EXEC [dicom].[SP_GetDicomValueFromJson]  @DicomKeyword, @JsonExt,  @DicomValue  OUTPUT, 
		     @ValidDicomValue  OUTPUT;
		IF  (@ValidDicomValue = 1)
		BEGIN
			SET @DicomObjectInstanceDate = @DicomValue;
			SET @DicomKeyword = 'InstanceCreationTime';
			EXEC  [dicom].[SP_GetDicomValueFromJson]  @DicomKeyword, @JsonExt,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF  (@ValidDicomValue = 1)
			BEGIN
				SET @DicomObjectInstanceTime = @DicomValue;		
			END
		END
		
		IF (@DicomObjectInstanceDate IS NULL)  -- We Will Parse ContentDate and ContentTime 
		BEGIN
			SET @DicomKeyword = 'ContentDate';
			EXEC [dicom].[SP_GetDicomValueFromJson]  @DicomKeyword, @JsonExt,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)
			BEGIN
				SET @DicomObjectInstanceDate = @DicomValue;
				SET @DicomKeyword = 'ContentTime';
				EXEC [dicom].[SP_GetDicomValueFromJson]  @DicomKeyword, @JsonExt,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
				IF  (@ValidDicomValue = 1)
				BEGIN
					SET @DicomObjectInstanceTime = @DicomValue;	
				END
			END
		END

		IF (@DicomObjectInstanceDate IS NULL) 
		BEGIN
			SET @DicomKeyword = 'AcquisitionDateTime';
			EXEC [dicom].[SP_GetDicomValueFromJson]  @DicomKeyword, @JsonExt,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF  (@ValidDicomValue = 1)
			BEGIN
				SET @DicomObjectInstanceDate = SUBSTRING(@DicomValue, 1, 8);
				SET @DicomObjectInstanceTime = SUBSTRING(@DicomValue, 9, 6);
			END
			ELSE
			BEGIN
				SET @DicomKeyword = 'AcquisitionDate';
				EXEC  [dicom].[SP_GetDicomValueFromJson]  @DicomKeyword, @JsonExt,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
				IF  (@ValidDicomValue = 1)
			    BEGIN
					SET @DicomObjectInstanceDate = @DicomValue;
					SET @DicomKeyword = 'AcquisitionTime';
					EXEC  [PACS].[dicom].[SP_GetDicomValueFromJson]  @DicomKeyword, @JsonExt,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
					IF  (@ValidDicomValue = 1)
					BEGIN
						SET @DicomObjectInstanceTime = @DicomValue;	
					END
				END
			END
		END
		
		IF (@DicomObjectInstanceDate IS NULL) 
		BEGIN
			SET @ContinueExecution = 0;
			SET @DataBaseNewer = 1; -- Database values will be used
		END
				
		IF (@ContinueExecution  = 1) 
		BEGIN
			SET @DicomKeyword = 'TimezoneOffsetFromUTC';
 			EXEC  [dicom].[SP_GetDicomValueFromJson]  @DicomKeyword, @JsonExt,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
			IF (@ValidDicomValue = 1)
			BEGIN
				SET @TimezoneOffsetFromUTCDicom = @DicomValue;
				SET @SignCharacter = SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 1);

				SET @ValidUTC =  ISNUMERIC(@TimezoneOffsetFromUTCDicom); 
				IF (@ValidUTC = 1)  
				BEGIN
					IF ((@SignCharacter = '+') OR (@SignCharacter = '-'))  
					BEGIN
						IF (LEN (@TimezoneOffsetFromUTCDicom) = 2)  /*  '+1', '-1' */ 
						BEGIN
							SET @TimezoneOffsetFromUTC = CONCAT(SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 1), '0',
								SUBSTRING(@TimezoneOffsetFromUTCDicom, 2, 1), ':', '00');  
						END
						ELSE IF (LEN (@TimezoneOffsetFromUTCDicom) = 3)  /* '+02','-02' */
						BEGIN
							SET @TimezoneOffsetFromUTC = CONCAT(SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 1),
								SUBSTRING(@TimezoneOffsetFromUTCDicom, 2, 2), ':', '00');  
						END
						ELSE IF (LEN (@TimezoneOffsetFromUTCDicom) = 4) -- '+040', '-040', 
						BEGIN
							SET @TimezoneOffsetFromUTC = CONCAT(SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 1), SUBSTRING(@TimezoneOffsetFromUTCDicom, 2, 2), 
								 ':', SUBSTRING(@TimezoneOffsetFromUTCDicom, 4, 1),  '0');  
						END
						ELSE IF (LEN (@TimezoneOffsetFromUTCDicom) = 5)  /* '+0500', '-0500' */ 
						BEGIN
							SET @TimezoneOffsetFromUTC = CONCAT(SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 1), SUBSTRING(@TimezoneOffsetFromUTCDicom, 2, 2), 
								':', SUBSTRING(@TimezoneOffsetFromUTCDicom, 4, 2));  
						END
					END 	
					ELSE 
					BEGIN
						IF (LEN (@TimezoneOffsetFromUTCDicom) = 1)  /*  '1' */ 
						BEGIN
							SET @TimezoneOffsetFromUTC = CONCAT('+0', SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 1), ':', '00');  
						END
						ELSE IF (LEN (@TimezoneOffsetFromUTCDicom) = 2)  /* '02' */
						BEGIN
							SET @TimezoneOffsetFromUTC = CONCAT('+', SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 2), ':', '00');  
						END
						ELSE IF (LEN (@TimezoneOffsetFromUTCDicom) = 3) /* '040' */ 
						BEGIN
							SET @TimezoneOffsetFromUTC = CONCAT('+', SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 2), 
								 ':', SUBSTRING(@TimezoneOffsetFromUTCDicom, 3, 1),  '0');  
						END
						ELSE IF (LEN (@TimezoneOffsetFromUTCDicom) = 4)  /* '0500' */ 
						BEGIN
							SET @TimezoneOffsetFromUTC = CONCAT('+', SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 2), 
								':', SUBSTRING(@TimezoneOffsetFromUTCDicom, 3, 2));  
						END
					END
				END	  
		    END 

	        IF (@TimezoneOffsetFromUTC IS NULL)
	        BEGIN
				SET @TimezoneOffsetFromUTC = DATENAME(tz, SYSDATETIMEOFFSET());
			END
	 
			EXEC [dicom].[SP_ConvertDicomDateTimeToDateTime]   
   				@DicomObjectInstanceDate, @DicomObjectInstanceTime,	@TimezoneOffsetFromUTC, 
				@OutputDateTime OUTPUT,
				@ValidDicomValue OUTPUT;
	
	        SET  @DataBaseDateTimeDifference = DATEDIFF(second, @OutputDateTime, @DBDateTime);
	
		    IF (@DataBaseDateTimeDifference > 0)
		    BEGIN
				SET @DataBaseNewer = 1;
			END     
		 END

		 SET @IsNewer = @DataBaseNewer;
END
