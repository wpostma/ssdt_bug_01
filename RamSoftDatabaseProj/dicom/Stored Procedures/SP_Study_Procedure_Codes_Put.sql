﻿
CREATE PROCEDURE [dicom].[SP_Study_Procedure_Codes_Put]  
     	@InternalStudyID bigint,
	 	@ExtJson nvarchar(max),
	    @StudyDBDataIsNewer bit
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE  @IsActive bit = 0;
	DECLARE  @ExtJsonStudyProcedureMap nvarchar (max) = NULL;		
	DECLARE  @StudyProcedureCodeSequence nvarchar (max) = NULL;	
	DECLARE  @StudyProcedureCodes nvarchar (max) = NULL;		
	DECLARE  @StudyProcedureCodeCurrentSequence nvarchar (max) = NULL;	
	DECLARE  @ProcedureCode nvarchar (16) = NULL; /* CodeValue */
	DECLARE  @ProcedureDescription  nvarchar (64) = NULL; /* CodeMeaning */
	DECLARE  @CodingSchemeDesignator nvarchar (16) = NULL; /* CodingSchemeDesignator */
	
	DECLARE  @ProcedureCodeTag nvarchar(16) = '00080100';
	DECLARE  @CodingSchemeDesignatorTag nvarchar (16) = '00080102';
	DECLARE  @ProcedureDescriptionTag  nvarchar(64) = '00080104';
	
	DECLARE  @CurrentValue nvarchar(max) = NULL;
	
	DECLARE  @DicomKeyword nvarchar(256) = NULL;				
    DECLARE  @ValidDicomValue bit = 0;	
	DECLARE  @DicomValue  nvarchar(max) = NULL;
	DECLARE  @SuccessMessage  nvarchar(max) = '';
	
   	DECLARE  @PerformInsertStudyProcedureCodes bit = 0;
	DECLARE  @InternalProcedureCodeID  int = NULL;
	DECLARE  @ContinueOperation bit = 0;
	DECLARE  @LocalInternalStudyProcedureMapID bigint = NULL;

    SET @SuccessMessage = 'Store Procedure <SP_Study_Procedure_Codes_Put>  Start';
	INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
		
	SET @DicomKeyword = 'ProcedureCodeSequence';
	EXEC [dicom].[SP_GetDicomValueFromJson]   
   			@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
	IF (@ValidDicomValue = 1) 
	BEGIN
		SET @StudyProcedureCodeSequence = @DicomValue;
		SET @ContinueOperation = 1;

		IF (@StudyDBDataIsNewer = 0)
		BEGIN
			DELETE FROM [dbo].[StudyProcedureMap] WHERE  InternalStudyID = @InternalStudyID;
		END 
	END 

    IF (@ContinueOperation = 1)
	BEGIN	
		SELECT TOP 1  @StudyProcedureCodes = [value] FROM OPENJSON(@StudyProcedureCodeSequence) 
		   WHERE [value] <> 'SQ';

		DECLARE Json_Cursor  CURSOR LOCAL FOR SELECT [value] FROM OPENJSON(@StudyProcedureCodes); 	
		
		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @StudyProcedureCodeCurrentSequence;	   

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
			SELECT @CurrentValue = [Value] FROM OPENJSON(@StudyProcedureCodeCurrentSequence) 
			   WHERE [key] =  @ProcedureCodeTag;
			SET @ProcedureCode =  JSON_VALUE(@CurrentValue, '$.Value[0]');

			SELECT @CurrentValue = [Value] FROM OPENJSON(@StudyProcedureCodeCurrentSequence) 
				WHERE [key] = @CodingSchemeDesignatorTag;
			SET @CodingSchemeDesignator =  JSON_VALUE(@CurrentValue, '$.Value[0]');
	
			SELECT @CurrentValue = [Value] FROM OPENJSON(@StudyProcedureCodeCurrentSequence) 
				WHERE [key] =  @ProcedureDescriptionTag;
			SET @ProcedureDescription =  JSON_VALUE(@CurrentValue, '$.Value[0]');
		
			SELECT @InternalProcedureCodeID = InternalProcedureCodeID  FROM [code].[ProcedureCode]
				  WHERE ProcedureCode =  @ProcedureCode;
				
			IF (@InternalProcedureCodeID IS NOT NULL)
			BEGIN
				 -- Insert of Update [dbo].[StudyProcedureMap]
				 IF (@StudyDBDataIsNewer = 0)
				 BEGIN
					SET @PerformInsertStudyProcedureCodes = 1;
				 END
				 ELSE
				 BEGIN	 	
					SELECT   @LocalInternalStudyProcedureMapID = InternalStudyProcedureMapID,
				      		 @ExtJsonStudyProcedureMap = ExtJson   FROM [dbo].[StudyProcedureMap] WHERE
							 InternalStudyID = @InternalStudyID AND 
								 InternalProcedureCodeID = @InternalProcedureCodeID;
					IF (@LocalInternalStudyProcedureMapID IS NULL)  
					BEGIN
						SET @PerformInsertStudyProcedureCodes = 1;
					END	
		        END  

				IF (@ExtJsonStudyProcedureMap IS NULL)
				BEGIN
					SET	 @ExtJsonStudyProcedureMap = '{ }'; -- Initialize as Json text
				END

				SET  @ExtJsonStudyProcedureMap = JSON_MODIFY( @ExtJsonStudyProcedureMap,'$.CodingSchemeDesignator', @CodingSchemeDesignator);	
                
				IF (@PerformInsertStudyProcedureCodes = 1)
				BEGIN
						INSERT INTO [dbo].[StudyProcedureMap]
						(
							[InternalStudyID],
							[InternalProcedureCodeID],
							[ExtJson]
						)
						VALUES (
								@InternalStudyID,
								@InternalProcedureCodeID,
								@ExtJsonStudyProcedureMap
						);
					END
					ELSE  
					BEGIN			   
						UPDATE [dbo].[StudyProcedureMap] SET
							ExtJson =  COALESCE(@ExtJsonStudyProcedureMap, ExtJson) 
								WHERE  InternalStudyProcedureMapID = @LocalInternalStudyProcedureMapID;  
					END

					SELECT @SuccessMessage = CONCAT('Successfully Update StudyProcedureMAP For InternalStudyID: ', 
							@InternalStudyID, ' And InternalProcedureCodeID: ', @InternalProcedureCodeID);                    	  
					INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
								VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
			END

			FETCH NEXT FROM Json_Cursor INTO @StudyProcedureCodeCurrentSequence;

        END

		CLOSE Json_Cursor;
		DEALLOCATE Json_Cursor; 

	END

	SET @SuccessMessage = 'Store Procedure <SP_Study_Procedure_Codes_Put>  Finish';
	INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);

END







