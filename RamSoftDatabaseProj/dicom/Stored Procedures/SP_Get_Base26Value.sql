﻿
CREATE PROCEDURE [dicom].[SP_Get_Base26Value]
 @IntValue bigint,
 @IntBaseLength bigint,
 @Base26Value CHAR(16) OUTPUT
 

AS
BEGIN
	DECLARE @I BigInt;
	DECLARE @TEMPCHAR CHAR(1);
	DECLARE @ErrorMessage nvarchar(256);
	DECLARE @ErrorState int = 4; -- Common Error

    SET @I = 0;
	SET @Base26Value = '';

	IF (@IntBaseLength <= 0)
	BEGIN
		SET  @ErrorMessage =  CONCAT('Incorrect Parameter  @IntBaseLength: ', @IntBaseLength,  'For the Store Procedure [dicom].[SP_Get_Base26Value]'); 
		INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','ERROR MESSAGE ',  @ErrorMessage, default);
		-- Trow Exception 					    
        THROW 50001, @ErrorMessage, @ErrorState;  
	END 

    WHILE (@I < @IntBaseLength)
    BEGIN
		SET  @TEMPCHAR = CHAR((@IntValue % 26) + 65);
        SET  @Base26Value = CONCAT(@TEMPCHAR, @Base26Value);
        SET  @IntValue =  @IntValue /26;
        SET  @I = @I + 1;
    END
END
