﻿
CREATE PROCEDURE [dicom].[SP_GetTimeZoneOffset]
 @JsonExt nvarchar(max),
 @TimeZoneOffset nvarchar(20) OUTPUT 
 
AS
BEGIN
		DECLARE  @DicomKeyword nvarchar(256);
        DECLARE  @ValidDicomValue bit = 0;	
        DECLARE  @DicomValue  nvarchar(64) = NULL;
        DECLARE  @TempValue  nvarchar(64) = NULL;
        DECLARE  @TimezoneOffsetFromUTCDicom   nvarchar(16) = NULL;
        DECLARE  @TimezoneOffsetFromUTC   nvarchar(20) = NULL;
        DECLARE  @SignCharacter nvarchar(1) = NULL;
        DECLARE  @ValidUTC bit = NULL; 

		SET NOCOUNT ON;

       	SET @DicomKeyword = 'TimezoneOffsetFromUTC';
 		EXEC  [dicom].[SP_GetDicomValueFromJson]  @DicomKeyword, @JsonExt,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
		IF (@ValidDicomValue = 1)
		BEGIN
			SET @TimezoneOffsetFromUTCDicom = @DicomValue;
			SET @SignCharacter = SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 1);

			SET @ValidUTC =  ISNUMERIC(@TimezoneOffsetFromUTCDicom); 
			IF (@ValidUTC = 1)  
			BEGIN
				IF ((@SignCharacter = '+') OR (@SignCharacter = '-'))  
				BEGIN
					IF (LEN (@TimezoneOffsetFromUTCDicom) = 2)  /*  '+1', '-1' */ 
					BEGIN
						SET @TimezoneOffsetFromUTC = CONCAT(SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 1), '0',
							SUBSTRING(@TimezoneOffsetFromUTCDicom, 2, 1), ':', '00');  
					END
					ELSE IF (LEN (@TimezoneOffsetFromUTCDicom) = 3)  /* '+02','-02' */
					BEGIN
						SET @TimezoneOffsetFromUTC = CONCAT(SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 1),
							SUBSTRING(@TimezoneOffsetFromUTCDicom, 2, 2), ':', '00');  
					END
					ELSE IF (LEN (@TimezoneOffsetFromUTCDicom) = 4) -- '+040', '-040', 
					BEGIN
						SET @TimezoneOffsetFromUTC = CONCAT(SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 1), SUBSTRING(@TimezoneOffsetFromUTCDicom, 2, 2), 
								':', SUBSTRING(@TimezoneOffsetFromUTCDicom, 4, 1),  '0');  
					END
					ELSE IF (LEN (@TimezoneOffsetFromUTCDicom) = 5)  /* '+0500', '-0500' */ 
					BEGIN
						SET @TimezoneOffsetFromUTC = CONCAT(SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 1), SUBSTRING(@TimezoneOffsetFromUTCDicom, 2, 2), 
							':', SUBSTRING(@TimezoneOffsetFromUTCDicom, 4, 2));  
					END
				END 	
				ELSE 
				BEGIN
					IF (LEN (@TimezoneOffsetFromUTCDicom) = 1)  /*  '1' */ 
					BEGIN
						SET @TimezoneOffsetFromUTC = CONCAT('+0', SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 1), ':', '00');  
					END
					ELSE IF (LEN (@TimezoneOffsetFromUTCDicom) = 2)  /* '02' */
					BEGIN
						SET @TimezoneOffsetFromUTC = CONCAT('+', SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 2), ':', '00');  
					END
					ELSE IF (LEN (@TimezoneOffsetFromUTCDicom) = 3) /* '040' */ 
					BEGIN
						SET @TimezoneOffsetFromUTC = CONCAT('+', SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 2), 
								':', SUBSTRING(@TimezoneOffsetFromUTCDicom, 3, 1),  '0');  
					END
					ELSE IF (LEN (@TimezoneOffsetFromUTCDicom) = 4)  /* '0500' */ 
					BEGIN
						SET @TimezoneOffsetFromUTC = CONCAT('+', SUBSTRING(@TimezoneOffsetFromUTCDicom, 1, 2), 
							':', SUBSTRING(@TimezoneOffsetFromUTCDicom, 3, 2));  
					END
				END
			END	  
		END 

	    IF (@TimezoneOffsetFromUTC IS NULL)
	    BEGIN
			SET @TimezoneOffsetFromUTC = DATENAME(tz, SYSDATETIMEOFFSET());
		END
	
	    SET	 @TimeZoneOffset = @TimezoneOffsetFromUTC;
END
