﻿
CREATE PROCEDURE [dicom].[SP_Instance_Exists_Or_Overwritten]  
       	@ExtJson nvarchar(max),
		@SopInstanceUID nvarchar(64),
		@OverWriteInstance bit OUTPUT,
		@InstanceExists bit OUTPUT,
		@ErrorCode int OUTPUT  
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE  @LocalFileExists bit = 0;
	DECLARE  @LocalOverWriteInstance bit = 0;
	DECLARE  @InstanceDBDataIsNewer bit = 0;  -- Dicom Entry is newer
	DECLARE  @DicomKeyword nvarchar(256) = NULL;				
    DECLARE  @ContinueExecution bit = 0;	
	DECLARE  @ValidDicomValue bit = 0;	
	DECLARE  @DicomValue  nvarchar(max) = NULL;
	DECLARE  @SuccessMessage  nvarchar(max) = '';
	DECLARE  @ErrorMessage  nvarchar(max) = '';
	DECLARE  @LocalInstanceID bigint = NULL;
   	DECLARE  @PerformInsertInstance bit = 0;
	DECLARE  @LastRevisedDate  datetime2(7) = NULL;
	DECLARE  @LocalLastRevisedDate  datetime2(7);
    DECLARE  @LocalVolumeID  bigint = NULL;
	
	SET @ErrorCode = 0;  
	SET @OverWriteInstance = 1;
	SET @InstanceExists = 0;
	
	/* Fetch Data from ExtJson String */
    SET @SuccessMessage = '<SP_Instance_Exists_Or_Overwritten> Start';
	INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			            VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
   	
	SELECT   @LocalInstanceID = InternalInstanceID, @LocalLastRevisedDate = LastUpdateUTC,
				@LocalFileExists = FileExists, @LocalVolumeID  = StorageVolumeID 
				FROM [PHI].[Instance] WHERE SOPInstanceUID = @SOPInstanceUID
	IF ( @LocalInstanceID IS NULL)  
	BEGIN
		SET @InstanceExists = 0;   
	END	
	ELSE IF ((@LocalVolumeID IS NOT NULL) AND (@LocalFileExists = 1)) 
	BEGIN
		SET @InstanceExists = 1; 
		SET @InstanceDBDataIsNewer = [dbo].GetSystemConfigBool('Rely Upon DB Information When Processing Dicom Object');   
		IF (@InstanceDBDataIsNewer = 1) 
		BEGIN
			SELECT @SuccessMessage = 'Configuration Setting <Rely Upon DB Information When Processing Dicom Object> is True';			  
			INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
				VALUES('STOW-RS','INFO MESSAGE ', @SuccessMessage , default);
			SET @LocalOverWriteInstance = 0;
		END 
		ELSE
		BEGIN
			EXEC [dicom].[SP_IsDBDataNewer]
				@ExtJson, @LocalLastRevisedDate, @InstanceDBDataIsNewer OUTPUT;

            -- Add Routing Logique and Licence Options - TBD
			IF (@InstanceDBDataIsNewer = 1)
			BEGIN
				SET @OverWriteInstance = 0;
			END 
			ELSE 
			BEGIN
				SET @OverWriteInstance = 1;
			END 
		END			                 
		
		SELECT @SuccessMessage = CONCAT('<SP_Instance_Exists_Or_Overwritten>. Check SopInstanceID: ', @SopInstanceUID,
		     ' Instance Exists is: ', @InstanceExists,  ' Overwrite Instance is: ',  @OverWriteInstance);
		INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
						VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
	END	

END








