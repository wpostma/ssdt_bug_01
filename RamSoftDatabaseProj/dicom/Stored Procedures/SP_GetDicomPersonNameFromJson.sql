﻿
CREATE PROCEDURE [dicom].[SP_GetDicomPersonNameFromJson]   
   		@DicomKeyword  nvarchar(64), 
		@ExtJson nvarchar(max), -- DICOM JSON
		@DicomValue nvarchar(max) OUTPUT,
		@ValidDicomValue bit OUTPUT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE  @DicomTagKey nvarchar(10);
	DECLARE  @DicomValueArray nvarchar(max);
    DECLARE  @AlphabeticIndex int = 0;
	DECLARE  @AlphabeticIndexShift int = 0;
	DECLARE  @IdeoGraphicIndex int = 0;
	DECLARE  @DicomNameValueArray nvarchar(max) = NULL;
			
	SET @DicomTagKey = (SELECT Tag from [pacs].[DicomDictionary] WHERE Keyword =  @DicomKeyword);
    IF ((@DicomTagKey IS NOT NULL) AND (@DicomTagKey <> ''))
	BEGIN
	  	 SELECT  @DicomValueArray = (SELECT [value] FROM OPENJSON(@ExtJson) where [key] =  @DicomTagKey); 

		 SELECT  @DicomNameValueArray = JSON_QUERY(@DicomValueArray, '$.Value');
		
		 IF ((@DicomNameValueArray IS NOT NULL) AND (@DicomNameValueArray <> ''))
		 BEGIN

		    SET  @ValidDicomValue = 1;
	   
			SELECT @AlphabeticIndex = (SELECT CHARINDEX('Alphabetic', @DicomNameValueArray));  
			SET @AlphabeticIndexShift = @AlphabeticIndex +  LEN('Alphabetic') + 3; 
			SELECT @IdeoGraphicIndex = (SELECT CHARINDEX('Ideographic', @DicomNameValueArray));  

			IF (@IdeoGraphicIndex = 0) -- Ideographic Name is not present 
			BEGIN
				SET @DicomValue = SUBSTRING(@DicomNameValueArray,  @AlphabeticIndexShift, 
					    LEN(@DicomNameValueArray) -  @AlphabeticIndexShift - 2);
			END
			ELSE  -- Ideographic Name is present. We are extracting only Alphabetic Name 
			BEGIN
				SET @DicomValue =  SUBSTRING(@DicomNameValueArray, @AlphabeticIndexShift,
				   (@IdeoGraphicIndex  - @AlphabeticIndexShift - 5)); 
			END

			IF (@DicomValue <> '')
		    BEGIN
				SET @ValidDicomValue = 1;
            END 
	     END   
  	END
	ELSE
	BEGIN
	     DECLARE  @ErrorMessage nvarchar(256);
		 SELECT   @ErrorMessage  = CONCAT('Invalid Value for DICOMDictionary Keyword:  ', @DicomKeyword);  
	     INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			   VALUES('STOW-RS','ERROR MESSAGE ', @ErrorMessage , default);	

		 SET @ValidDicomValue = 0;
		 SET @DicomValue = '';
	END
END


