﻿-- Batch submitted through debugger: SQLQuery15.sql|7|0|C:\Users\vkharam\AppData\Local\Temp\~vsEDC8.sql


CREATE PROCEDURE [dicom].[SP_Visit_Put]   
   		@InternalPatientID bigint,
		@PatientInternalAssigningAuthorityID bigint,
        @PatientDBDataIsNewer bit,
		@ExtJson nvarchar(max), -- DICOM JSON
		@InternalVisitID bigint OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON;
	
 	DECLARE  @SequenceKeyValue nvarchar(1024);

	DECLARE  @TempValue nvarchar(1024);

	DECLARE  @TempCode nvarchar(64) = NULL;
	
	DECLARE  @ExtJsonVisit nvarchar(max);
    DECLARE  @AdmissionID nvarchar(64) = NULL;
	DECLARE  @RouteOfAdmission nvarchar(64) = NULL; -- Patient Class 
	DECLARE  @EncounterClassID int = NULL;
	DECLARE  @AdmissionDateDicom nvarchar(8) = NULL;
	DECLARE  @AdmissionTimeDicom nvarchar(6) = NULL;
	DECLARE  @AdmissionDateTimeDicom nvarchar(20) = NULL;

	DECLARE  @AdmissionDateTimeDB  datetime2(7) = NULL;
    DECLARE  @VisitComments nvarchar(1024) = NULL;
	DECLARE  @DicomKeyword nvarchar(256) = NULL;				
	DECLARE  @ValidDicomValue bit = 0;	
	DECLARE  @DicomValue  nvarchar(max) = NULL;
	DECLARE  @SuccessMessage  nvarchar(max) = NULL;
	DECLARE  @ErrorMessage  nvarchar(max) = NULL;
	DECLARE  @TimeZoneOffsetFromUTC nvarchar(20) = NULL; 
	DECLARE  @OutputDateTime  datetime2(7);
	DECLARE  @CurrentPatientLocation nvarchar(64) = NULL;
	DECLARE  @PregnancyStatus nvarchar(64) = NULL; /* Used for Ambulatory Status */
	DECLARE  @AmbulatoryStatusID int = NULL; 

	DECLARE  @UniversalEntityID nvarchar (max) = NULL;
	DECLARE  @UniversalEntityIDTypeCode nvarchar (64) = NULL;
	DECLARE  @UniversalEntityIDTypeID int = NULL;
	DECLARE  @IssuerOfAdmissionIDSequenceValue nvarchar(1024) = NULL;
	DECLARE  @LocalNameSpaceEntityCode nvarchar(64) = NULL;
	DECLARE  @InternalAssigningAuthorityID  bigint = NULL;
	DECLARE  @LocalInternalPatientID bigint = NULL;
	DECLARE  @LocalInternalVisitID bigint = NULL;
	DECLARE  @PerformInsertVisitID bit = 0;		   
				
	-- Continue Operations if we have valid @ExtJSon
	IF  (ISJSON(@ExtJson) > 0)
	BEGIN 
	    
		SET @SuccessMessage = 'Store Procedure <SP_Visit_Put>  Start';
		INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);

		DECLARE @ProcessedIDTab TABLE (id bigint);
		  
		SET @DicomKeyword = 'AdmissionID';
		EXEC [dicom].[SP_GetDicomValueFromJson]   
   		        @DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
        IF (@ValidDicomValue = 1) 
		BEGIN
			SET  @AdmissionID = @DicomValue;  
		END 
		ELSE
		BEGIN
		     /* In this case Encounter Insert / Update Trigger update the Value 
			    using RamSoft Prefix */
			SET  @AdmissionID = '-1';  
		END
				  
		/* IssuerOfAdmissionIDSequence */
		EXEC  [dicom].[SP_GetDicomValueFromJson]   
   				  'IssuerOfAdmissionIDSequence', @ExtJson, @IssuerOfAdmissionIDSequenceValue  OUTPUT,
				       @ValidDicomValue  OUTPUT;
		IF (@ValidDicomValue = 1)  
		BEGIN			
			EXEC  [dicom].[SP_GetDicomValueFromJson]
                     'UniversalEntityID',  @IssuerOfAdmissionIDSequenceValue,  @UniversalEntityID  OUTPUT,   @ValidDicomValue  OUTPUT;
                  
	 	    IF (@UniversalEntityID <> '')
			BEGIN
				SET @UniversalEntityID  = UPPER(@UniversalEntityID); 
				      -- Extract Conditional Field  'UniversalEntityIDType'
				EXEC  [dicom].[SP_GetDicomValueFromJson]
                          'UniversalEntityIDType',  @IssuerOfAdmissionIDSequenceValue, @UniversalEntityIDTypeCode  OUTPUT,  @ValidDicomValue  OUTPUT;
                    
				 SELECT @UniversalEntityIDTypeID = UniversalEntityIDTypeID  FROM [code].[UniversalEntityIDType] 
				       WHERE UniversalEntityIDTypeCode = @UniversalEntityIDTypeCode;
					   -- NULL Value is Permitted in  dbo.AssigningAuthority for UniversalEntityIDTypeCode

                EXEC  [dicom].[SP_GetDicomValueFromJson]
                          'LocalNamespaceEntityID',  @IssuerOfAdmissionIDSequenceValue,
						       @LocalNameSpaceEntityCode  OUTPUT,  @ValidDicomValue  OUTPUT;

                IF (@LocalNameSpaceEntityCode IS NOT NULL)
				BEGIN
					SET @LocalNameSpaceEntityCode = UPPER(@LocalNameSpaceEntityCode); 
				END
		    END 
		        				 
			IF (@UniversalEntityID IS NOT NULL)
			BEGIN
				SELECT @InternalAssigningAuthorityID = InternalAssigningAuthorityID FROM [dbo].[AssigningAuthority]
								   WHERE UniversalEntityID = @UniversalEntityID; 
				IF (@InternalAssigningAuthorityID IS NULL) 
				BEGIN 
					IF (@LocalNameSpaceEntityCode IS NOT NULL)
					BEGIN
						SELECT @InternalAssigningAuthorityID = InternalAssigningAuthorityID FROM [dbo].[AssigningAuthority]
							   WHERE [Name] = @LocalNameSpaceEntityCode;
					END
				END
					 
				IF (@InternalAssigningAuthorityID IS NULL) 
				BEGIN
					IF (@LocalNameSpaceEntityCode IS NULL)
					BEGIN
						SET @LocalNameSpaceEntityCode = SUBSTRING(@UniversalEntityID, 1,64); 
					END
							-- Insert new Entry in AssigningAuthority
	  				INSERT INTO dbo.AssigningAuthority([Name], UniversalEntityID, UniversalEntityIDTypeID, IsActive)
								   OUTPUT inserted.InternalAssigningAuthorityID INTO @ProcessedIDTab
				       		VALUES (@LocalNameSpaceEntityCode, @UniversalEntityID,  @UniversalEntityIDTypeID, 1);

					SET @InternalAssigningAuthorityID = (SELECT TOP 1 id FROM @ProcessedIDTab);
							-- We will reuse @ProcessedIDTab for Inserting Patient
					DELETE FROM  @ProcessedIDTab;
				 END
			 END 
			 ELSE IF (@LocalNameSpaceEntityCode IS NOT NULL) 
			 BEGIN
				SELECT @InternalAssigningAuthorityID = InternalAssigningAuthorityID FROM [dbo].[AssigningAuthority]
								   WHERE [Name] = @LocalNameSpaceEntityCode;
				IF (@InternalAssigningAuthorityID IS NULL) 
				BEGIN
				-- Insert new Entry in AssigningAuthority
	  				INSERT INTO dbo.AssigningAuthority([Name], IsActive)
								   OUTPUT inserted.InternalAssigningAuthorityID INTO @ProcessedIDTab
					VALUES (@LocalNameSpaceEntityCode, 1);

					SET @InternalAssigningAuthorityID = (SELECT TOP 1 id FROM @ProcessedIDTab);
					DELETE FROM  @ProcessedIDTab;
				END 
			END
			ELSE -- InternalAssigningAuthorityID is Mandatory 
			BEGIN
				SET @InternalAssigningAuthorityID = @PatientInternalAssigningAuthorityID;
			END
		END
		ELSE 
		BEGIN
			SET @InternalAssigningAuthorityID = @PatientInternalAssigningAuthorityID;
		END
	
		-- RouteOfAdmission 
		SET @DicomKeyword = 'RouteOfAdmissions';
		EXEC [dicom].[SP_GetDicomValueFromJson]   
   		        @DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
		IF (@ValidDicomValue = 1) 
		BEGIN
			SET  @RouteOfAdmission = @DicomValue;  
			SELECT @EncounterClassID = EncounterClassID FROM [code].[EncounterClass]
			WHERE EncounterClassCode = @RouteOfAdmission;
			IF (@EncounterClassID IS NULL OR @EncounterClassID = 0)
			BEGIN
				-- Set Default Value
				SELECT @EncounterClassID = EncounterClassID FROM [code].[EncounterClass]
					WHERE EncounterClassCode = [dbo].GetSystemConfigStr('Default Patient Class Code');
			END     
		END 
		
		SET @DicomKeyword = 'CurrentPatientLocation';
		EXEC [dicom].[SP_GetDicomValueFromJson]   
   		        @DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;

        IF (@ValidDicomValue = 1)
		BEGIN
			SET  @CurrentPatientLocation = @DicomValue;  
		END 

		-- Visit Comment 
		SET @DicomKeyword = 'VisitComments';
		EXEC [dicom].[SP_GetDicomValueFromJson]   
   		        @DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;

        IF (@ValidDicomValue = 1)
		BEGIN
			SET @VisitComments = @DicomValue;  
		END 

		-- DICOM Visit Date - Admitting Date  
		SET @DicomKeyword = 'AdmittingDate';
		EXEC [dicom].[SP_GetDicomValueFromJson]   
   		        @DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
        IF (@ValidDicomValue = 1) 
		BEGIN
			SET @AdmissionDateDicom = @DicomValue;  
		END 
				
		-- DICOM Visit Time - Admitting Date  
		SET @DicomKeyword = 'AdmittingTime';
		EXEC [dicom].[SP_GetDicomValueFromJson]   
   		        @DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
        IF (@ValidDicomValue = 1) 
		BEGIN
			SET @AdmissionTimeDicom = @DicomValue;  
		END 
				
        EXEC  [dicom].[SP_GetTimeZoneOffset] @ExtJson, @TimeZoneOffsetFromUTC OUTPUT;
		IF (@AdmissionDateDicom IS NOT NULL)
		BEGIN
			EXEC  [dicom].[SP_ConvertDicomDateTimeToDateTime]   
   			    @AdmissionDateDicom, @AdmissionTimeDicom, @TimeZoneOffsetFromUTC, 
					@OutputDateTime OUTPUT, @ValidDicomValue OUTPUT;
	    END  
		IF (@ValidDicomValue = 1)
		BEGIN
			SET @AdmissionDateTimeDB  = @OutputDateTime;
		END
	
	    
		-- PregnancyStatus - used for AmbulatoryStatus if Present
		SET @DicomKeyword = 'PregnancyStatus';
		EXEC [dicom].[SP_GetDicomValueFromJson]   
   		        @DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
        IF (@ValidDicomValue = 1) 
		BEGIN
			SET @PregnancyStatus = @DicomValue;  
			SELECT @AmbulatoryStatusID = AmbulatoryStatusID  FROM [code].[AmbulatoryStatus] 
			       WHERE  AmbulatoryStatusCode  =  @PregnancyStatus;
		END 


		SELECT @LocalInternalVisitID  = InternalEncounterID
			FROM [PHI].[Encounter] WHERE InternalPatientID = @InternalPatientID AND AdmissionID = @AdmissionID
				ORDER BY  InternalEncounterID DESC; 

		IF (@LocalInternalVisitID  IS NULL)
		BEGIN
			SET @PerformInsertVisitID = 1;
		END   

		IF (@PerformInsertVisitID = 1)
		BEGIN
	 		SET @ExtJsonVisit = '{ }'; -- Initialize @ExtJsonPatient
		END 
		ELSE 
		BEGIN
			-- Build   @ExtJsonPatient from Existing Patient 
			SELECT  @ExtJsonVisit = ExtJson FROM [PHI].[Encounter]
				WHERE InternalPatientID = @InternalPatientID AND AdmissionID = @AdmissionID;
        END       

		IF((@PatientDBDataIsNewer = 0) OR (@PerformInsertVisitID = 1))
		BEGIN
			-- Update JSON Field
			 IF (@CurrentPatientLocation IS NOT NULL)
			 BEGIN
				SET @ExtJsonVisit = JSON_MODIFY(@ExtJsonVisit, '$.patientLocation', @CurrentPatientLocation);
			 END		

			 IF (@VisitComments IS NOT NULL)
			 BEGIN
				SET @ExtJsonVisit = JSON_MODIFY(@ExtJsonVisit, '$.description', @VisitComments);
			 END		
		 END				
		   				
		 -- Perform Insert or Uodate 	
         IF(@PerformInsertVisitID = 1)
		 BEGIN
	         INSERT INTO [PHI].[Encounter]
			 (
				InternalPatientID,
				AdmissionID,
				AdmissionDateTime,
				AdmissionAssigningAuthorityID,
				EncounterClassID,
				AmbulatoryStatusID,
				ExtJson
			 )
				OUTPUT inserted.InternalEncounterID  INTO @ProcessedIDTab
			 VALUES
			 (
				@InternalPatientID,
				@AdmissionID,
				@AdmissionDateTimeDB,
				@InternalAssigningAuthorityID,
				@EncounterClassID,
				@AmbulatoryStatusID,
				@ExtJsonVisit
			 );
												
             SET @InternalVisitID = (SELECT TOP 1 id FROM @ProcessedIDTab);		
   	         
			 SELECT @SuccessMessage = CONCAT('Successfully Inserted VisitID: ', @AdmissionID,
			           '  for InternalPatientID IssuerOfPatientID: ', @InternalPatientID);
		
		     INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
		 END
		 ELSE IF ((@PerformInsertVisitID = 0) AND (@LocalInternalVisitID IS NOT NULL))
       	 BEGIN
			IF (@PatientDBDataIsNewer = 1) -- Don't Update Visit's Fields Because DataBase is never
			BEGIN
				SET @AdmissionID = NULL;
				SET @AdmissionDateTimeDB = NULL;
				SET @EncounterClassID = NULL;
				SET @InternalAssigningAuthorityID = NULL;
				SET @AmbulatoryStatusID = NULL;
				SET @ExtJsonVisit = NULL;
		    END

			 UPDATE [PHI].[Encounter] SET
				AdmissionAssigningAuthorityID = COALESCE( @InternalAssigningAuthorityID, AdmissionAssigningAuthorityID),
				AdmissionID = COALESCE(@AdmissionID, AdmissionID), 	
				AdmissionDateTime  = COALESCE(@AdmissionDateTimeDB, AdmissionDateTime), 	
				EncounterClassID = COALESCE(@EncounterClassID, EncounterClassID), 
				AmbulatoryStatusID = COALESCE(@AmbulatoryStatusID, AmbulatoryStatusID),
				ExtJson = COALESCE(@ExtJsonVisit, ExtJson)
					WHERE InternalEncounterID = @LocalInternalVisitID;

				SET @InternalVisitID  = @LocalInternalVisitID;;
											  
				SELECT @SuccessMessage = CONCAT('SP_Visit_Put ', 'Successfully Updated VisitID: ', @AdmissionID,
							'  for InternalPatientID: ', @InternalPatientID); 
				INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
		  END	
	END
	ELSE
	BEGIN
		INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			   VALUES('STOW-RS','ERROR MESSAGE - Not Valid JSON string', @ExtJson , default);	
	END;
	

END







