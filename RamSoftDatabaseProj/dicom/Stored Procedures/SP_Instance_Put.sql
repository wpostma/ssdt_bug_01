﻿-- Batch submitted through debugger: SQLQuery12.sql|7|0|C:\Users\vkharam\AppData\Local\Temp\~vsA26A.sql

CREATE PROCEDURE [dicom].[SP_Instance_Put]  
        @InternalPatientID bigint, 
		@InternalStudyID bigint,
	    @InternalSeriesID bigint,	  
		@ExtJson nvarchar(max),
		@SopInstanceUID nvarchar(64),
		@LocalFileID nvarchar(260) OUTPUT,
		@InternalInstanceID bigint OUTPUT,
		@ErrorCode int OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE  @IsActive bit = 0;
	DECLARE  @FileExists bit = 0;
	DECLARE  @IsReport bit = 0;
	DECLARE  @InternalLocalFileID nvarchar(260) = NULL;
	DECLARE  @StorageVolumeID bigint = NULL;
	DECLARE  @Modality nvarchar(16) = NULL;
	DECLARE  @SOPClassUID  nvarchar(64) = NULL;
	DECLARE  @InternalSOPClassID bigint = NULL;
	DECLARE  @DicomContentDate nvarchar(8) = NULL;
	DECLARE  @DicomContentTime nvarchar(6) = NULL;
	DECLARE  @TimeZoneOffsetFromUTC nvarchar(20) = NULL; 
	DECLARE  @ObjectNumber int = NULL;
	DECLARE  @ExtJsonInstance nvarchar (max) = NULL;		
	DECLARE  @InstanceDBDataIsNewer bit = 0;  -- Dicom Entry is newer
	DECLARE  @DicomKeyword nvarchar(256) = NULL;				
    DECLARE  @ContinueExecution bit = 0;	
	DECLARE  @ValidDicomValue bit = 0;	
	DECLARE  @DicomValue  nvarchar(max) = NULL;
	DECLARE  @SuccessMessage  nvarchar(max) = '';
	DECLARE  @LocalInstanceID bigint = NULL;
   	DECLARE  @PerformInsertInstance bit = 0;
	DECLARE  @OutputDateTime  datetime2(7);
	DECLARE  @LastRevisedDate  datetime2(7);
	DECLARE  @LocalLastRevisedDate  datetime2(7);
	DECLARE  @ContentDateTimeDB datetimeoffset(7) = NULL;
	DECLARE  @LastUpdateUserID  bigint = NULL;
	DECLARE  @ErrorNumber   int;
	DECLARE  @ErrorMessage  nvarchar(max);
	DECLARE  @ErrorSeverity int;
	DECLARE  @ErrorState    int;

			
	SET @ErrorCode = 0;  
						
	-- Continue Operations if we have valid @JsonInput
	IF  (ISJSON(@ExtJson) > 0)
	BEGIN 

		/* Fetch Data from ExtJson String */
        SET @SuccessMessage = 'Store Procedure <SP_Instance_Put>  Start';
		INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);

		SET @ContinueExecution = 1;	

		DECLARE @ProcessedIDTab TABLE (id bigint);
	
		SET @DicomKeyword = 'SOPClassUID';
		EXEC [dicom].[SP_GetDicomValueFromJson]   
   				@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
		IF (@ValidDicomValue = 1) 
		BEGIN
			SET @SOPClassUID = @DicomValue;
			SELECT  @InternalSOPClassID = InternalSOPClassID  FROM [code].[SOPClass] WHERE SOPClassUID = @DicomValue;  
		END 
		
		IF ((@SOPClassUID IS NULL) OR (@InternalSOPClassID IS NULL))
		BEGIN
			SET @ContinueExecution = 0;	

			IF (@SOPClassUID IS NULL)
			BEGIN
				SET @ErrorMessage = 'SOPClassUID is missing in DICOM Object ';
				SET @ErrorCode = 49152; 
			END

			IF (@InternalSOPClassID IS NULL)
			BEGIN
				SET @ErrorMessage = CONCAT('SOPClassUID: ', @SOPClassUID, ' Is not Supported ');
				SET @ErrorCode = 290; 
			END

			INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                VALUES('STOW-RS','ERROR MESSAGE ', @ErrorMessage , default);
    	END
		
					
		IF (@ContinueExecution = 1)	
		BEGIN
			
			SELECT   @LocalInstanceID = InternalInstanceID, @LocalLastRevisedDate = LastUpdateUTC,
			            @InternalLocalFileID = LocalFileID
						FROM [PHI].[Instance] WHERE SOPInstanceUID = @SOPInstanceUID
						AND InternalSeriesID = @InternalSeriesID AND InternalPatientID = @InternalPatientID; 

            -- Update DICOM Json for Received Instance
			SET @ExtJsonInstance = @ExtJson;

			SELECT @LastupdateUserID = InternalUserID from [dbo].[User] WHERE UserName = 'DICOM'; 

        	IF ( @LocalInstanceID IS NULL)  
			BEGIN
				SET @PerformInsertInstance = 1;
				SET @InstanceDBDataIsNewer = 0;
			END	
		    ELSE -- Check @InstanceDBDataIsNewer 
			BEGIN   
				SET @InstanceDBDataIsNewer = [dbo].GetSystemConfigBool('Rely Upon DB Information When Processing Dicom Object');   
				IF (@InstanceDBDataIsNewer = 1) 
				BEGIN
					SELECT @SuccessMessage = 'Configuration Setting <Rely Upon DB Information When Processing Dicom Object> is True';			  
					INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
						VALUES('STOW-RS','INFO MESSAGE ', @SuccessMessage , default);
				END 
				ELSE
				BEGIN
					EXEC [dicom].[SP_IsDBDataNewer]
						@ExtJson, @LocalLastRevisedDate, @InstanceDBDataIsNewer OUTPUT;
				END			                 
			END

		    IF (@InstanceDBDataIsNewer = 0)
			BEGIN						
				SET @DicomKeyword = 'InstanceNumber';
				EXEC [dicom].[SP_GetDicomValueFromJson]   
   						@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
				IF (@ValidDicomValue = 1)
				BEGIN
					SET @ObjectNumber = TRY_CAST(@DicomValue AS int);
					IF (@ObjectNumber IS NULL)
					BEGIN
						SET @ObjectNumber = 0; 
					END
				END  		    
				ELSE
				BEGIN
					SET @ObjectNumber = 0; -- This Value is used for Calculation of LocalFileID
				END
										 
				-- Content Date  
				SET @DicomKeyword = 'ContentDate';
				EXEC [dicom].[SP_GetDicomValueFromJson]   
   						@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
				IF (@ValidDicomValue = 1) 
				BEGIN
					SET  @DicomContentDate = @DicomValue;  
				END 
				
				-- Content Time 
				SET @DicomKeyword = 'ContentTime';
				EXEC [dicom].[SP_GetDicomValueFromJson]   
   						@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
				IF (@ValidDicomValue = 1) 
				BEGIN
					SET  @DicomContentTime = @DicomValue;  
				END 

				IF ((@DicomContentDate IS NULL) OR (@DicomContentTime IS NULL))
				BEGIN
					-- InstanceCreationDate  
					SET @DicomKeyword = 'InstanceCreationDate';
					EXEC [dicom].[SP_GetDicomValueFromJson]   
   							@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
					IF (@ValidDicomValue = 1) 
					BEGIN
						SET  @DicomContentDate = @DicomValue;  
					END 

					--  'InstanceCreationTime';
					SET @DicomKeyword = 'InstanceCreationTime';
					EXEC [dicom].[SP_GetDicomValueFromJson]   
   							@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
					IF (@ValidDicomValue = 1) 
					BEGIN
						SET  @DicomContentTime = @DicomValue;  
					END 
				END 

				EXEC  [dicom].[SP_GetTimeZoneOffset] @ExtJson, @TimeZoneOffsetFromUTC OUTPUT;
				IF (@DicomContentDate IS NOT NULL)
				BEGIN
					EXEC  [dicom].[SP_ConvertDicomDateTimeToDateTime]   
   							@DicomContentDate, @DicomContentDate, @TimeZoneOffsetFromUTC, 
								@OutputDateTime OUTPUT, @ValidDicomValue OUTPUT;
				END  

				IF (@ValidDicomValue = 1)
				BEGIN
					SET @LastRevisedDate = @OutputDateTime;
					SET @ContentDateTimeDB = SWITCHOFFSET(@OutputDateTime, @TimeZoneOffsetFromUTC);
				END
									         
				SET @DicomKeyword = 'Modality';
				EXEC [dicom].[SP_GetDicomValueFromJson]   
   						@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
				IF (@ValidDicomValue = 1) 
				BEGIN
					SET @Modality = @DicomValue;  
				END 

				IF ((@Modality = 'SR') OR (@SOPClassUID = '1.2.840.10008.5.1.4.1.1.104.1'))
				BEGIN 
					SET @IsReport = 1;
				END 

				EXEC [dicom].[SP_MakeObjectFileID]
						@InternalPatientID,  @InternalStudyID, @InternalSeriesID,
						@ObjectNumber,  @InternalLocalFileID OUTPUT;

			END
			              				
			-- Perform Inset or Update 	
            IF (@PerformInsertInstance = 1)
			BEGIN
			    DECLARE @LoopInsert int = 1;
			    SET @IsActive = 1;
				WHILE @LoopInsert = 1
				BEGIN
					  BEGIN TRY 
							INSERT INTO [PHI].[Instance]
							(
								[InternalSeriesID],
								[InternalPatientID],
								[InternalSOPClassID],
								[ContentDateTime],
								[ObjectNumber],
								[SOPInstanceUID],
								[StorageVolumeID],
								[LocalFileID],
								[FileExists],
								[IsReport],
								[IsActive],
								[DicomJson],
								[LastUpdateUTC],
								[LastupdateUserID]
							)
							OUTPUT inserted.InternalInstanceID INTO @ProcessedIDTab
							VALUES (
								 @InternalSeriesID,
								 @InternalPatientID,
								 @InternalSOPClassID,
								 @ContentDateTimeDB,
								 @ObjectNumber,
								 @SOPInstanceUID,
								 @StorageVolumeID,
								 @InternalLocalFileID,
								 @FileExists,
								 @IsReport,
								 @IsActive,
								 @ExtJsonInstance,
								 @LastRevisedDate,
								 @LastupdateUserID
							);
				
							SET @InternalInstanceID = (SELECT TOP 1 id FROM @ProcessedIDTab);
                 				
							SET @LocalFileID = @InternalLocalFileID; 
							
							 SET @LoopInsert = 0;		     

							SELECT @SuccessMessage = CONCAT('Successfully Inserted InternalInstanceID: ', @InternalInstanceID,
									 ' For InternalSeriesID: ', @InternalSeriesID);
						  
						INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
									VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
				     END TRY
				BEGIN  CATCH
				    DECLARE @ExceptionErrorCode  int = NULL;

					SELECT 	@ErrorMessage  = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(),
						@ErrorState  = ERROR_STATE(),  @ExceptionErrorCode = ERROR_NUMBER();
																		   	    
					-- Insert Error Message to Log File
					SET  @ErrorMessage =  CONCAT(' Error Message is: ', @ErrorMessage,
			    	' .Error Severity is: ', @ErrorSeverity, ' Error State is: ', @ErrorState, ' Error Code is: ', @ExceptionErrorCode); 

					INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                    VALUES('STOW-RS','ERROR MESSAGE ',  @ErrorMessage, default);

                    IF  (@ExceptionErrorCode = 2601) -- Duplicate Key
					BEGIN
					     -- Resolve Duplicate and Insert Instance Again 	
					   	DECLARE @DuplicateObjectNumber int  = NULL;
						DECLARE @ResolveConflictObjectNumber int = NULL; 
						SELECT  @DuplicateObjectNumber = MAX(ObjectNumber) FROM  [PHI].[INSTANCE] WHERE InternalSeriesID = @InternalSeriesID; 
						SET     @ResolveConflictObjectNumber = @DuplicateObjectNumber + 1;

						SET @ObjectNumber = @ResolveConflictObjectNumber;

						EXEC [dicom].[SP_MakeObjectFileID]
							@InternalPatientID,  @InternalStudyID, @InternalSeriesID,
							   @ObjectNumber,  @InternalLocalFileID OUTPUT;
					END 
					ELSE
					BEGIN 
						-- Rethrow Exception 
						 SET @LoopInsert = 0;					    
						THROW 50001, @ErrorMessage, @ErrorState;  

					END 
				END CATCH 
			  END		
            END
			ELSE  -- Update  Instance
			BEGIN			   
				 IF (@InstanceDBDataIsNewer = 1)
				 BEGIN
					SET @ContentDateTimeDB = NULL;
					SET @ObjectNumber = NULL;
					SET @ExtJsonInstance = NULL;
				 END
				
				 SET @LocalFileID = @InternalLocalFileID; 

				 UPDATE [PHI].[Instance] SET
						ContentDateTime = COALESCE(@ContentDateTimeDB, ContentDateTime), 
						ObjectNumber = COALESCE(@ObjectNumber, ObjectNumber), 
					    DicomJson = COALESCE(@ExtJsonInstance, DicomJson),
						LastupdateUserID = @LastupdateUserID
					       WHERE  InternalInstanceID = @LocalInstanceID; 

				  SET @InternalInstanceID = @LocalInstanceID; 
					
				  SELECT @SuccessMessage = CONCAT('Successfully Update InternalInstanceID: ', @InternalInstanceID,
				         ' For InternalSeriesID: ', @InternalSeriesID);                    
				  
				   INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
			END
		END  
	END								
	ELSE
	BEGIN
		INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			   VALUES('STOW-RS','ERROR MESSAGE - Not Valid JSON string', @ExtJson , default);	

        -- Common Error Code Cannot understand
        SET @ErrorCode = 49153; 
	END;

END







