﻿

-- Update VolumeID and FileExists for Specific for Specific @InternalInstanceID

CREATE PROCEDURE [dicom].[SP_Update_Instance]  
       	@InternalInstanceID bigint,
		@VolumeID bigint
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE  @SuccessMessage  nvarchar(max) = '';
			
    -- Set VolumeID
    UPDATE [PHI].[Instance] SET StorageVolumeID = @VolumeID, FileExists = 1
			WHERE  InternalInstanceID = @InternalInstanceID; 
				
    SELECT @SuccessMessage = CONCAT('Successfully Update InternalInstanceID: ', @InternalInstanceID,
			' with VolumeIFD:  ', @VolumeID, ' And FileExists equal to 1');  						  
	  
    INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
END






