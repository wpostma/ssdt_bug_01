﻿
CREATE PROCEDURE [dicom].[SP_GetDicomValueFromJson]   
   		@DicomKeyword  nvarchar(64), 
		@ExtJson nvarchar(max), -- DICOM JSON
		@DicomValue nvarchar(max) OUTPUT,
		@ValidDicomValue bit OUTPUT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE  @DicomTagKey nvarchar(10) = NULL
	DECLARE  @DicomValueArray nvarchar(max) = NULL;
	DECLARE  @DicomVM nvarchar(64) = NULL;
	DECLARE  @DicomVR nvarchar(4) = NULL;
	DECLARE  @DicomCheckSQ nvarchar(max) = NULL;
	
	SET @ValidDicomValue = 0;
	SET @DicomValue = NULL;


	SELECT  @DicomTagKey =  Tag,  @DicomVM = VM, @DicomVR = VR from [pacs].[DicomDictionary] WHERE Keyword =  @DicomKeyword;
    IF (@DicomTagKey IS NOT NULL)
	BEGIN
	   	 -- We have lowercase from DICOM JSON string
		 -- We should convert to Upper Value in C++ Code
		 SET @DicomTagKey =  LOWER(@DicomTagKey);

		 SELECT @DicomValueArray = (SELECT [value] FROM OPENJSON(@ExtJson) where [key] =  @DicomTagKey); 

		 IF (@DicomValueArray IS NOT NULL)
         BEGIN
			 IF ((@DicomVM = '1') AND (@DicomVR <> 'SQ'))
			 BEGIN
				SELECT @DicomValue = JSON_VALUE(@DicomValueArray, '$.Value[0]'); 
				IF (@DicomValue IS NULL)  
				BEGIN
					SET @DicomValue = '';
				END

				IF (@DicomValue <> '')
				BEGIN
					SET @ValidDicomValue = 1;
				END
			 END
			 ELSE IF (@DicomVR = 'SQ')
			 BEGIN
			    -- We are checking if we have more than one sequence
				-- If we have only one sequence we can extract the value
			    SET @DicomCheckSQ = JSON_QUERY(@DicomValueArray,'$.Value[1]'); 
				
				IF ( @DicomCheckSQ IS NULL)
				BEGIN
					SET @DicomValue =  JSON_QUERY(@DicomValueArray,'$.Value[0]'); 
					IF ((@DicomValue IS NULL) OR  (@DicomValue = '{}'))  
					BEGIN
						SET @DicomValue = '';
					END
					IF (@DicomValue <> '')
					BEGIN
						SET @ValidDicomValue = 1;
					END 
				END 
				ELSE
				BEGIN
					SET @DicomValue = @DicomValueArray;
					IF (@DicomValue IS NULL)  
					BEGIN
						SET @DicomValue = '';
					END
					IF (@DicomValue <> '')
					BEGIN
						SET @ValidDicomValue = 1;
					END 
				END
			 END
			 ELSE  -- Multiple VM
			 BEGIN
		   		SELECT  @DicomValue = [value] FROM OPENJSON(@DicomValueArray) where [key] ='Value';	
				IF (@DicomValue IS NULL)  
				BEGIN
					SET @DicomValue = '';
				END
				IF (@DicomValue <> '')
				BEGIN
					SET @ValidDicomValue = 1;
				END 
			 END
          END
  	END
	ELSE
	BEGIN
	     DECLARE  @ErrorMessage nvarchar(256);
		 SELECT   @ErrorMessage  = CONCAT('Invalid Value for DICOMDictionary Keyword:  ', @DicomKeyword);  
	     INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			   VALUES('STOW-RS','ERROR MESSAGE ', @ErrorMessage , default);	

		 SET @ValidDicomValue = 0;
		 SET @DicomValue = '';
	END

END
