﻿
CREATE PROCEDURE [dicom].[SP_UpdatePersonName]   
 		@InputPersonName nvarchar(64), 
		@UpdatedPersonalName nvarchar(64) OUTPUT,
		@ValidDicomValue bit OUTPUT
AS
BEGIN

	DECLARE  @OutputNames nvarchar(64) = NULL;
	DECLARE  @Family      nvarchar(64) = NULL;
	DECLARE  @Given       nvarchar(64) = NULL;
	DECLARE	 @Middle      nvarchar(64) = NULL;
	DECLARE  @Prefix      nvarchar(64) = NULL;
	DECLARE	 @Suffix      nvarchar(64) = NULL;
	DECLARE	 @DelimiterCharacter    nvarchar(2) = '^';

	SET @ValidDicomValue = 0;
	
	/* The data in DataBase for Personal Name is stored in FHIR Format 
	   FHIR Format is similar to DICOM Format */
	IF ((@InputPersonName IS NOT NULL) AND (@InputPersonName <> ''))
	BEGIN
		SET @ValidDicomValue = 1;

	    EXEC [dicom].[SP_ParsePersonName]   
 			@InputPersonName, @Family OUTPUT, @Given OUTPUT, 
			   @Middle OUTPUT, @Prefix OUTPUT, @Suffix OUTPUT;

		SET @OutputNames = COALESCE(@Family, NULL,'');

		IF (@Given IS NOT NULL)
		BEGIN
			SET @OutputNames = CONCAT(@OutputNames,  @DelimiterCharacter,  @Given);
		END 
	    ELSE 	
		BEGIN
			SET @OutputNames = CONCAT(@OutputNames,  @DelimiterCharacter);
		END 
	
		IF (@Middle IS NOT NULL)
		BEGIN
			SET @OutputNames = CONCAT(@OutputNames,  @DelimiterCharacter,  @Middle);
		END 
		ELSE
		BEGIN
			SET @OutputNames = CONCAT(@OutputNames,  @DelimiterCharacter);
		END 

		IF (@Prefix IS NOT NULL)
		BEGIN
			SET @OutputNames = CONCAT(@OutputNames,  @DelimiterCharacter,  @Prefix);
		END 
	    ELSE 
		BEGIN
			SET @OutputNames = CONCAT(@OutputNames,  @DelimiterCharacter);
		END 	
							
		IF (@Suffix IS NOT NULL)
		BEGIN
			SET @OutputNames = CONCAT(@OutputNames,  @DelimiterCharacter,  @Suffix);
		END 
		ELSE
		BEGIN
			SET @OutputNames = CONCAT(@OutputNames,  @DelimiterCharacter);
		END
			
        IF (@OutputNames <> '')
		BEGIN
			SET @UpdatedPersonalName = SUBSTRING(@OutputNames, 1,64);
		END
		
     END 
END

