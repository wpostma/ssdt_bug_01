﻿
CREATE PROCEDURE [dicom].[SP_Patient_Allergies_Put]  
     	@InternalPatientID bigint,
	 	@ExtJson nvarchar(max),
	    @PatientDBDataIsNewer bit,
	    @PatientJson nvarchar(max)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE  @ContinueOperation bit = 1;
	DECLARE  @ExtJsonAllergiesSequence nvarchar (max) = NULL;
	DECLARE  @ExtJsonCurrentAllergyItems nvarchar (max) = '{ }';
	DECLARE  @CurrentValue nvarchar(max) = NULL;
	DECLARE  @ExtJsonSubstance nvarchar(max) = '{ }';
    DECLARE  @Substance nvarchar (4000) = NULL;
	DECLARE  @Criticality nvarchar (128) = NULL;
    DECLARE  @Description nvarchar (128) = NULL;			
	
	DECLARE  @DicomKeyword nvarchar(256) = NULL;				
    DECLARE  @ValidDicomValue bit = 0;	
	DECLARE  @DicomValue  nvarchar(max) = NULL;
	DECLARE  @SuccessMessage  nvarchar(max) = '';
	DECLARE  @AllergyItemCounter int = 0;	
		
    SET @SuccessMessage = 'Store Procedure <SP_Patient_Allergies_Put>  Start';
	INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
	 IF (@PatientDBDataIsNewer = 1)
	 BEGIN
	      SET  @ContinueOperation = 0;
		  SET  @SuccessMessage = 'Store Procedure <SP_Patient_Allergies_Put> Finished because @PatientDBDataIsNewer is true  ';
		  INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
	 END

	 IF (@ContinueOperation = 1)
	 BEGIN
		SET @DicomKeyword = 'Allergies';
		EXEC [dicom].[SP_GetDicomValueFromJson]   
   				@DicomKeyword, @ExtJson,  @DicomValue  OUTPUT,   @ValidDicomValue  OUTPUT;
		IF (@ValidDicomValue = 1) 
		BEGIN
			SET  @ExtJsonAllergiesSequence  = UPPER(@DicomValue);  
		END 
		ELSE
		BEGIN
			  SET @ContinueOperation = 0;
		      SET  @SuccessMessage = 'Store Procedure <SP_Patient_Allergies_Put> Finished because Allergies Sequence is empty ';
		      INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			     VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);
		END
     END 

    IF (@ContinueOperation = 1)
	BEGIN	
	   	DECLARE Json_Cursor  CURSOR LOCAL FOR SELECT [value] FROM OPENJSON(@ExtJsonAllergiesSequence); 	
		
		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurrentValue;  

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
			SET @AllergyItemCounter = @AllergyItemCounter + 1;

			IF (@AllergyItemCounter = 1) -- Fetch Substance 
			BEGIN
				SET @Substance = @CurrentValue;
			END
			ELSE IF (@AllergyItemCounter = 2) -- Fetch Description
			BEGIN
				SET @Description = @CurrentValue;
			END
			ELSE IF (@AllergyItemCounter = 3) -- Fetch Criticality
			BEGIN
				
				  -- Convert to FHIR Criticality 

				  IF (@CurrentValue = 'SV')
				  BEGIN
					  SELECT @Criticality = AllergyCriticalityName FROM [code].[AllergyCriticality] WHERE AllergyCriticalityCode = 'CRITH';
				  END
				  ELSE IF (@CurrentValue = 'MO')
				  BEGIN
					   SELECT @Criticality = AllergyCriticalityName FROM [code].[AllergyCriticality] WHERE AllergyCriticalityCode = 'CRITL';
				  END
				  ELSE 
				  BEGIN
					   SELECT @Criticality = AllergyCriticalityName FROM [code].[AllergyCriticality] WHERE AllergyCriticalityCode = 'CRITU';
				  END
				  
				  SET  @ExtJsonSubstance =  JSON_MODIFY(@ExtJsonSubstance, '$.display', @Substance);

				  SET  @ExtJsonCurrentAllergyItems =  JSON_MODIFY(@ExtJsonCurrentAllergyItems, '$.coding',  JSON_QUERY(@ExtJsonSubstance));
					
				  SET  @ExtJsonCurrentAllergyItems =  JSON_MODIFY(@ExtJsonCurrentAllergyItems, '$.note', @Description);

				  SET  @ExtJsonCurrentAllergyItems =  JSON_MODIFY(@ExtJsonCurrentAllergyItems, '$.criticality', @Criticality);

				  SET  @PatientJson = JSON_MODIFY(@PatientJson, 'append $.allergies', JSON_QUERY(@ExtJsonCurrentAllergyItems));

				  SET @SuccessMessage = CONCAT('Store Procedure <SP_Patient_Allergies_Put>. Add Allergy: ', @ExtJsonCurrentAllergyItems,
				       ' To InternalPatientID: ', @InternalPatientID);
	              INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);

				  -- Reset Counter and  @ExtJsonCurrentAllergyItems  to Fetch Next Allergy Items
				  SET  @ExtJsonCurrentAllergyItems = '{ }';
				  SET  @ExtJsonSubstance  = '{ }';
				  SET  @AllergyItemCounter = 0; 

			END
			
			FETCH NEXT FROM Json_Cursor INTO @CurrentValue;

        END

		CLOSE Json_Cursor;
		DEALLOCATE Json_Cursor; 

	    UPDATE [PHI].[PATIENT] SET ExtJson = @PatientJson  WHERE InternalPatientID = @InternalPatientID; 

	END

	SET @SuccessMessage = 'Store Procedure <SP_Patient_Allergies_Put>  Finish';
	INSERT INTO [log].[DicomRestFulServices](ServiceName, Item, [Description], LogTime)
			                VALUES('STOW-RS','SUCCESS MESSAGE ', @SuccessMessage , default);

END







