﻿
CREATE PROCEDURE [dicom].[SP_ParsePersonName]   
 		@InputName nvarchar(64), 
		@FamilyName nvarchar(64) OUTPUT,
		@GivenName nvarchar(64) OUTPUT,
		@MiddleName nvarchar(64) OUTPUT,
		@Prefix nvarchar(64) OUTPUT,
		@Suffix nvarchar(64) OUTPUT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE  @DelimiterGivenIndex int = 0;
	DECLARE	 @DelimiterMiddleIndex int = 0;
	DECLARE  @DelimiterPrefixIndex int = 0;
	DECLARE	 @DelimiterSuffixIndex int = 0;
		
	DECLARE	 @NumberOfParsedComponents int = 1;
	DECLARE  @ContinueOperation bit = 1;
	DECLARE  @ReplaceDelimiter bit = 0;

	DECLARE  @PreliminaryFamilyName nvarchar(64) = NULL;
	DECLARE  @PreliminaryGivenName nvarchar(64) = NULL;
	DECLARE  @PreliminaryMiddleName nvarchar(64) = NULL;
	DECLARE	 @PreliminaryPrefix nvarchar(64) = NULL;
	DECLARE	 @PreliminarySuffix nvarchar(64) = NULL;
		
    DECLARE  @Delimiter nvarchar(2) = '^';

	-- Set Output Values 
	SET @FamilyName = NULL;
	SET	@GivenName = NULL;
	SET	@MiddleName = NULL;
	SET	@Prefix = NULL;
	SET	@Suffix = NULL;

	/* The data in DataBase for Personal Name is stored in FHIR Format 
	   FHIR Format is similar to DICOM Format */
	IF (@InputName IS NOT NULL)  
	BEGIN
		SELECT @DelimiterGivenIndex = (SELECT CHARINDEX(@Delimiter, @InputName));  
		IF (@DelimiterGivenIndex = 0)
		BEGIN
		    SET @ReplaceDelimiter = 1;
		END
	    ELSE
		BEGIN
			SET @PreliminaryFamilyName = dbo.TRIM(SUBSTRING(@InputName, 1, @DelimiterGivenIndex - 1));
			SET @NumberOfParsedComponents = @NumberOfParsedComponents  +  1; 
		END

		IF (@PreliminaryFamilyName  = '')
		BEGIN
			SET @PreliminaryFamilyName  = NULL;
		END
	
        IF ((@ContinueOperation = 1) AND (@ReplaceDelimiter = 0))
		BEGIN
			SELECT @DelimiterMiddleIndex = (SELECT CHARINDEX(@Delimiter, @InputName, @DelimiterGivenIndex + 1));  
			IF (@DelimiterMiddleIndex = 0)
			BEGIN
				SET @ContinueOperation = 0;
				SET @ReplaceDelimiter = 1;
				SET @PreliminaryGivenName = dbo.TRIM(SUBSTRING(@InputName, @DelimiterGivenIndex +1 , LEN(@InputName) -  @DelimiterGivenIndex)); 
			END
			ELSE
			BEGIN
				SET @PreliminaryGivenName = dbo.TRIM(SUBSTRING(@InputName, @DelimiterGivenIndex +1, @DelimiterMiddleIndex - @DelimiterGivenIndex -1)); 
				SET @NumberOfParsedComponents = @NumberOfParsedComponents + 1; 
			END
		END 

		IF (@PreliminaryGivenName = '')
		BEGIN
			SET @PreliminaryGivenName = NULL;
		END

		IF ((@ContinueOperation = 1) AND (@ReplaceDelimiter = 0))
		BEGIN
			SELECT @DelimiterPrefixIndex = (SELECT CHARINDEX(@Delimiter, @InputName, @DelimiterMiddleIndex + 1));  
			IF (@DelimiterPrefixIndex = 0)
			BEGIN
				SET @ContinueOperation = 0;
				SET @ReplaceDelimiter = 1;
				SET @PreliminaryMiddleName = dbo.TRIM(SUBSTRING(@InputName, @DelimiterMiddleIndex +1 , LEN(@InputName) -  @DelimiterMiddleIndex)); 
			END
			ELSE
			BEGIN
				SET @PreliminaryMiddleName = dbo.TRIM(SUBSTRING(@InputName, @DelimiterMiddleIndex + 1, @DelimiterPrefixIndex - @DelimiterMiddleIndex -1)); 
				SET @NumberOfParsedComponents = @NumberOfParsedComponents + 1; 
			END
		END

		IF (@PreliminaryMiddleName = '')
		BEGIN
			SET @PreliminaryMiddleName  = NULL;
		END
			
		IF ((@ContinueOperation = 1) AND (@ReplaceDelimiter = 0))
		BEGIN
			SELECT @DelimiterSuffixIndex = (SELECT CHARINDEX(@Delimiter, @InputName, @DelimiterPrefixIndex + 1));  
			IF (@DelimiterSuffixIndex = 0)
			BEGIN
				SET @ContinueOperation = 0;
				SET @ReplaceDelimiter = 1;
				SET @PreliminaryPrefix = dbo.TRIM(SUBSTRING(@InputName, @DelimiterPrefixIndex +1 , LEN(@InputName) - @DelimiterPrefixIndex)); 
			END
			ELSE
			BEGIN
				SET @PreliminaryPrefix = dbo.TRIM(SUBSTRING(@InputName, @DelimiterPrefixIndex +1 , @DelimiterSuffixIndex - @DelimiterPrefixIndex -1)); 
				SET @PreliminarySuffix = dbo.TRIM(SUBSTRING(@InputName, @DelimiterSuffixIndex +1 , LEN(@InputName) -  @DelimiterSuffixIndex)); 
				IF (@PreliminarySuffix = '')
				BEGIN
					SET @PreliminarySuffix = NULL;
				END

				SET @NumberOfParsedComponents = @NumberOfParsedComponents  +  1; 
			END
		END

		IF (@PreliminaryPrefix = '')
		BEGIN
			SET @PreliminaryPrefix = NULL;
		END

		IF (@NumberOfParsedComponents = 5)
		BEGIN
			SET @FamilyName = @PreliminaryFamilyName;
			IF (@PreliminaryGivenName IS NOT NULL)
			BEGIN
				SET @GivenName = @PreliminaryGivenName;
			END

			IF (@PreliminaryMiddleName IS NOT NULL)
			BEGIN
				SET @MiddleName = @PreliminaryMiddleName;
			END
			
			IF (@PreliminaryPrefix IS NOT NULL)
			BEGIN
				SET @Prefix = @PreliminaryPrefix;  
			END

			IF (@PreliminarySuffix IS NOT NULL)
			BEGIN
				SET @Suffix =  @PreliminarySuffix;
			END
		END 
		ELSE 
		BEGIN -- Continue with Parsing by Comma Delimiter
			/* Reset Indexes */
			SET  @DelimiterGivenIndex = 0;
			SET  @DelimiterMiddleIndex = 0;
			SET  @DelimiterPrefixIndex = 0;
			SET  @DelimiterSuffixIndex = 0;

			SET @Delimiter = ',';

			SET @ContinueOperation = 1;

			/* Analysing Number of Parsed Components */
			IF (@NumberOfParsedComponents = 1) -- We don't have Carret Delimiter 
			BEGIN
				SELECT @DelimiterGivenIndex = (SELECT CHARINDEX(@Delimiter, @InputName));  
				IF (@DelimiterGivenIndex = 0)
				BEGIN
					SET  @FamilyName = dbo.TRIM(@InputName); 
					SET  @ContinueOperation = 0;
				END
				ELSE
				BEGIN
					SET @FamilyName = dbo.TRIM(SUBSTRING(@InputName, 1, @DelimiterGivenIndex - 1));
                    IF (@FamilyName = '')
					BEGIN
						SET @FamilyName = NULL;
					END 
				END

			    IF (@FamilyName = '')
				BEGIN
					SET @FamilyName = NULL;
				END 
	
				IF (@ContinueOperation = 1)
				BEGIN
					SELECT @DelimiterMiddleIndex = (SELECT CHARINDEX(@Delimiter, @InputName, @DelimiterGivenIndex + 1));  
					IF (@DelimiterMiddleIndex = 0)
					BEGIN
						SET @ContinueOperation = 0;
						SET @GivenName = dbo.TRIM(SUBSTRING(@InputName, @DelimiterGivenIndex + 1 , LEN(@InputName) -  @DelimiterGivenIndex)); 
					END
					ELSE
					BEGIN
						SET @GivenName = dbo.TRIM(SUBSTRING(@InputName, @DelimiterGivenIndex + 1, @DelimiterMiddleIndex - @DelimiterGivenIndex -1)); 
					END
				END 
			
			    IF (@GivenName = '')
				BEGIN
					SET @GivenName = NULL;
				END 
				
				IF (@ContinueOperation = 1)
				BEGIN
					SELECT @DelimiterPrefixIndex = (SELECT CHARINDEX(@Delimiter, @InputName, @DelimiterMiddleIndex + 1));  
					IF (@DelimiterPrefixIndex = 0)
					BEGIN
						SET @ContinueOperation = 0;
						SET @MiddleName = dbo.TRIM(SUBSTRING(@InputName, @DelimiterMiddleIndex + 1 , LEN(@InputName) -  @DelimiterMiddleIndex)); 
					END
					ELSE
					BEGIN
						SET @MiddleName = dbo.TRIM(SUBSTRING(@InputName, @DelimiterMiddleIndex + 1, @DelimiterPrefixIndex - @DelimiterMiddleIndex -1)); 
					END
				END
			
			    IF (@MiddleName = '')
				BEGIN
					SET @MiddleName = NULL;
				END 
		
				IF (@ContinueOperation = 1)
				BEGIN
					SELECT @DelimiterSuffixIndex = (SELECT CHARINDEX(@Delimiter, @InputName, @DelimiterPrefixIndex + 1));  
					IF (@DelimiterSuffixIndex = 0)
					BEGIN
						SET @ContinueOperation = 0;
						SET @Prefix = dbo.TRIM(SUBSTRING(@InputName, @DelimiterPrefixIndex +1 , LEN(@InputName) - @DelimiterPrefixIndex)); 
					END
					ELSE
					BEGIN
						SET @Prefix = dbo.TRIM(SUBSTRING(@InputName, @DelimiterPrefixIndex +1 , @DelimiterSuffixIndex - @DelimiterPrefixIndex -1)); 
						SET @Suffix = dbo.TRIM(SUBSTRING(@InputName, @DelimiterSuffixIndex +1 , LEN(@InputName) -  @DelimiterSuffixIndex)); 
					END
				END

				IF (@Prefix = '')
				BEGIN
					SET @Prefix = NULL;
				END 

				IF (@Suffix = '')
				BEGIN
					SET  @Suffix = NULL;
				END 
								
				-- Final Stop Parsing
				SET @NumberOfParsedComponents = 5;

			END -- End Case if Number of Components equal to 1
			
			SET @ContinueOperation = 1;
		    /* We will Update Number of Components if it is Possible */
			IF (@NumberOfParsedComponents = 2)  
			BEGIN
			    IF ( @PreliminaryFamilyName IS NOT NULL)
				BEGIN
					SELECT @DelimiterGivenIndex = (SELECT CHARINDEX(@Delimiter, @PreliminaryFamilyName));  
					IF (@DelimiterGivenIndex <> 0)
					BEGIN
						SET  @NumberOfParsedComponents = @NumberOfParsedComponents  +  1;  
						SET  @PreliminaryMiddleName = dbo.Trim(@PreliminaryGivenName);
						SET  @PreliminaryGivenName =  dbo.Trim(SUBSTRING(@PreliminaryFamilyName, @DelimiterGivenIndex + 1 , LEN(@PreliminaryFamilyName) - @DelimiterGivenIndex)); 
						SET  @PreliminaryFamilyName = dbo.Trim(SUBSTRING(@PreliminaryFamilyName, 1, @DelimiterGivenIndex - 1));
					END
				END
						    
			    IF ((@NumberOfParsedComponents = 2) AND (@PreliminaryGivenName IS NOT NULL)) -- We didn't change number of parsed components
				BEGIN
					SELECT @DelimiterMiddleIndex = (SELECT CHARINDEX(@Delimiter, @PreliminaryGivenName));  
					IF (@DelimiterMiddleIndex <>  0)
					BEGIN
					    SET  @NumberOfParsedComponents = @NumberOfParsedComponents  +  1; 
						-- Preliminary Family Name wasn't changed
						SET  @PreliminaryMiddleName = dbo.TRIM(SUBSTRING(@PreliminaryGivenName, @DelimiterMiddleIndex + 1 , LEN(@PreliminaryGivenName) - @DelimiterMiddleIndex)); 
						SET  @PreliminaryGivenName = dbo.TRIM(SUBSTRING(@PreliminaryGivenName, 1, @DelimiterMiddleIndex - 1));
					END
				END
			END -- End Case if Number of Components equal to 2

			SET @ContinueOperation = 1;
		    /* We will Update Number of Components if it is Possible */
			IF (@NumberOfParsedComponents = 3)  
			BEGIN
			   IF ( @PreliminaryFamilyName IS NOT NULL)
			    BEGIN
					SELECT @DelimiterGivenIndex = (SELECT CHARINDEX(@Delimiter, @PreliminaryFamilyName));  
					IF (@DelimiterGivenIndex <> 0)
					BEGIN
						SET  @NumberOfParsedComponents = @NumberOfParsedComponents  +  1;  
						SET  @PreliminaryPrefix =     dbo.TRIM(@PreliminaryMiddleName);
						SET  @PreliminaryMiddleName = dbo.Trim(@PreliminaryGivenName);
						SET  @PreliminaryGivenName =  dbo.Trim(SUBSTRING(@PreliminaryFamilyName, @DelimiterGivenIndex + 1 , LEN(@PreliminaryFamilyName) - @DelimiterGivenIndex)); 
						SET  @PreliminaryFamilyName = dbo.Trim(SUBSTRING(@PreliminaryFamilyName, 1, @DelimiterGivenIndex - 1));
					END
				END
						    
			    IF ((@NumberOfParsedComponents = 3) AND  (@PreliminaryGivenName IS NOT NULL)) -- We didn't change number of parsed components
				BEGIN
					SELECT @DelimiterMiddleIndex = (SELECT CHARINDEX(@Delimiter, @PreliminaryGivenName));  
					IF (@DelimiterMiddleIndex <>  0)
					BEGIN
					    SET  @NumberOfParsedComponents = @NumberOfParsedComponents  +  1; 
						-- Preliminary Family Name wasn't changed
						SET  @PreliminaryPrefix =  dbo.TRIM(@PreliminaryMiddleName);
						SET  @PreliminaryMiddleName = dbo.TRIM(SUBSTRING(@PreliminaryGivenName, @DelimiterMiddleIndex + 1 , LEN(@PreliminaryGivenName) - @DelimiterMiddleIndex)); 
						SET  @PreliminaryGivenName =  dbo.TRIM(SUBSTRING(@PreliminaryGivenName, 1, @DelimiterMiddleIndex - 1));
					END
				END
				
				IF ((@NumberOfParsedComponents = 3) AND (@PreliminaryMiddleName IS NOT NULL)) -- We didn't change number of parsed components
				BEGIN
					SELECT @DelimiterPrefixIndex = (SELECT CHARINDEX(@Delimiter, @PreliminaryMiddleName));  
					IF (@DelimiterPrefixIndex <> 0)
				    BEGIN 
					    SET  @NumberOfParsedComponents = @NumberOfParsedComponents  +  1;
						SET  @PreliminaryPrefix = dbo.TRIM(SUBSTRING(@PreliminaryMiddleName, @DelimiterPrefixIndex + 1 , LEN(@PreliminaryMiddleName) - @DelimiterPrefixIndex)); 
						SET  @PreliminaryMiddleName = dbo.TRIM(SUBSTRING(@PreliminaryMiddleName, 1, @DelimiterPrefixIndex - 1));
					END
				END 
			END -- End Case if Number of Components equal to 3
	
			SET @ContinueOperation = 1;
			IF (@NumberOfParsedComponents = 4) -- Family Name was parsed by Carret Delimiter 
			BEGIN
			    IF (@PreliminaryFamilyName IS NOT NULL)
				BEGIN
					SELECT @DelimiterGivenIndex = (SELECT CHARINDEX(@Delimiter, @PreliminaryFamilyName));  
					IF (@DelimiterGivenIndex = 0)
					BEGIN
						SET  @FamilyName = @PreliminaryFamilyName; 
					END
					ELSE
					BEGIN
						SET  @FamilyName = dbo.TRIM(SUBSTRING(@PreliminaryFamilyName, 1, @DelimiterGivenIndex - 1));
						SET  @GivenName =  dbo.TRIM(SUBSTRING(@PreliminaryFamilyName, @DelimiterGivenIndex + 1 , LEN(@PreliminaryFamilyName) - @DelimiterGivenIndex)); 
						SET  @NumberOfParsedComponents = @NumberOfParsedComponents  +  1; 
						SET  @MiddleName = @PreliminaryGivenName;
						SET  @Prefix  = @PreliminaryMiddleName;
						SET  @Suffix  = @PreliminaryPrefix;
						SET  @ContinueOperation = 0; -- Stop Parsing        
					END
				END
				
			    IF ((@ContinueOperation = 1) AND (@PreliminaryGivenName IS NOT NULL)) -- We still don't have required number of components
				BEGIN
					SELECT @DelimiterMiddleIndex = (SELECT CHARINDEX(@Delimiter, @PreliminaryGivenName));  
					IF (@DelimiterMiddleIndex = 0)
					BEGIN
						SET  @GivenName = @PreliminaryGivenName; 
					END
					ELSE
					BEGIN
						SET  @GivenName = dbo.TRIM(SUBSTRING(@PreliminaryGivenName, 1, @DelimiterMiddleIndex - 1));
						SET  @MiddleName = dbo.TRIM(SUBSTRING(@PreliminaryGivenName, @DelimiterMiddleIndex + 1 , LEN(@PreliminaryGivenName) - @DelimiterMiddleIndex)); 
						SET  @NumberOfParsedComponents = @NumberOfParsedComponents  +  1; 
						SET  @Prefix  = @PreliminaryMiddleName;
						SET  @Suffix  = @PreliminaryPrefix;
						SET  @ContinueOperation = 0; -- Stop Parsing    
					END
				END

				IF ((@ContinueOperation = 1) AND (@PreliminaryPrefix IS NOT NULL)) -- We still don't have required number of components
				BEGIN
					SELECT @DelimiterPrefixIndex = (SELECT CHARINDEX(@Delimiter, @PreliminaryMiddleName));  
					IF (@DelimiterPrefixIndex = 0)
					BEGIN
						SET @MiddleName = @PreliminaryMiddleName; 
					END
					ELSE
					BEGIN
						SET  @MiddleName = dbo.TRIM(SUBSTRING(@PreliminaryMiddleName, 1,  @DelimiterPrefixIndex- 1));
						SET  @Prefix = dbo.TRIM(SUBSTRING(@PreliminaryMiddleName,  @DelimiterPrefixIndex + 1 , LEN(@PreliminaryMiddleName) - @DelimiterPrefixIndex)); 
						SET  @NumberOfParsedComponents = @NumberOfParsedComponents  +  1; 
						SET  @Suffix  = @PreliminaryPrefix;
						SET  @ContinueOperation = 0; -- Stop Parsing    
					END
				END
								
				IF ((@ContinueOperation = 1) AND (@PreliminaryPrefix IS NOT NULL)) -- We still don't have required number of components
				BEGIN
					SELECT @DelimiterSuffixIndex = (SELECT CHARINDEX(@Delimiter, @PreliminaryPrefix));  
					IF (@DelimiterSuffixIndex = 0)
					BEGIN
						SET @Prefix = @PreliminaryPrefix; 
					END
					ELSE
					BEGIN
						SET  @Prefix = dbo.TRIM(SUBSTRING(@PreliminaryPrefix, 1, @DelimiterSuffixIndex - 1));
						SET  @Suffix = dbo.TRIM(SUBSTRING(@PreliminaryPrefix, @DelimiterSuffixIndex + 1 , LEN(@PreliminaryPrefix) - @DelimiterSuffixIndex)); 
						SET  @NumberOfParsedComponents = @NumberOfParsedComponents  +  1; 
						SET  @ContinueOperation = 0; -- Stop Parsing    
					END
				END
			END -- End Case if Number of Components equal to 4
		END
		
		/* If we still don't have required number of components
			  we will be assign to output values preliminary results */
		IF (@NumberOfParsedComponents <> 5) 
		BEGIN
			SET  @FamilyName = @PreliminaryFamilyName; 
			IF (@PreliminaryGivenName IS NOT NULL)
			BEGIN
				SET  @GivenName =  @PreliminaryGivenName; 
			END 
			
			IF (@PreliminaryMiddleName IS NOT NULL)
			BEGIN
				SET  @MiddleName = @PreliminaryMiddleName;
			END

			IF (@PreliminaryPrefix IS NOT NULL)
			BEGIN
				SET  @Prefix = @PreliminaryPrefix;
			END
			IF ( @PreliminarySuffix IS NOT NULL)
			BEGIN
				SET  @Suffix = @PreliminarySuffix;
			END
		END

	END 
END


