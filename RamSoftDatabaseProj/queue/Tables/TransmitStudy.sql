﻿CREATE TABLE [queue].[TransmitStudy] (
    [EntryID]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [OperationStr]      NVARCHAR (16) NOT NULL,
    [InternalStationID] BIGINT        NOT NULL,
    [InternalStudyID]   BIGINT        NOT NULL,
    [InternalUserID]    BIGINT        NULL,
    [PriorityValue]     SMALLINT      CONSTRAINT [DF_TransmitStudy_PriorityValue] DEFAULT ((10)) NOT NULL,
    [StatusValue]       SMALLINT      CONSTRAINT [DF_TransmitStudy_StatusValue] DEFAULT ((0)) NOT NULL,
    [NumFailures]       SMALLINT      CONSTRAINT [DF_TransmitStudy_NumFailures] DEFAULT ((0)) NOT NULL,
    [NumWarnings]       SMALLINT      CONSTRAINT [DF_TransmitStudy_NumWarnings] DEFAULT ((0)) NOT NULL,
    [TimeStarted]       DATETIME2 (7) NULL,
    [TimeAdded]         DATETIME2 (7) CONSTRAINT [DF_TransmitStudy_TimeAdded] DEFAULT (sysutcdatetime()) NOT NULL,
    [TimeFailed]        DATETIME2 (7) NULL,
    [ThreadID]          INT           NULL,
    CONSTRAINT [PK_TransmitStudy] PRIMARY KEY CLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_TransmitStudy_Station] FOREIGN KEY ([InternalStationID]) REFERENCES [dbo].[Station] ([InternalStationID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_TransmitStudy_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_TransmitStudy_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE CASCADE ON UPDATE CASCADE
);

