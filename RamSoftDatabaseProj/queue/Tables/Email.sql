﻿CREATE TABLE [queue].[Email] (
    [InternalEmailID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [PriorityValue]   INT            NULL,
    [EmailType]       INT            NULL,
    [StatusValue]     INT            NULL,
    [FailuresValue]   INT            CONSTRAINT [DF_Email_FailuresValue] DEFAULT ((0)) NOT NULL,
    [ExtJson]         NVARCHAR (MAX) NULL,
    CONSTRAINT [EMAILQUEUE_PK_EMAILQUEUE] PRIMARY KEY NONCLUSTERED ([InternalEmailID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [EMAILQUEUE_I_EMAILQUEUE_EMAILID_DESC]
    ON [queue].[Email]([InternalEmailID] ASC);


GO
CREATE NONCLUSTERED INDEX [EMAILQUEUE_I_EMAILQUEUE_EMAILTYPE]
    ON [queue].[Email]([EmailType] ASC);


GO
CREATE NONCLUSTERED INDEX [EMAILQUEUE_I_EMAILQUEUE_STATUS]
    ON [queue].[Email]([StatusValue] ASC);

