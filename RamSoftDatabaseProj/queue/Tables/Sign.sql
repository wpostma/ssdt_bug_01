﻿CREATE TABLE [queue].[Sign] (
    [EntryID]             BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [InternalUserID]      BIGINT        NOT NULL,
    [InternalStudyID]     BIGINT        NULL,
    [InternalInstanceID]  BIGINT        NULL,
    [PriorityValue]       SMALLINT      NOT NULL,
    [StatusValue]         SMALLINT      CONSTRAINT [DF_Sign_StatusValue] DEFAULT ((0)) NOT NULL,
    [NumFailures]         SMALLINT      CONSTRAINT [DF_Sign_NumFailures] DEFAULT ((0)) NOT NULL,
    [NumWarnings]         SMALLINT      CONSTRAINT [DF_Sign_NumWarnings] DEFAULT ((0)) NOT NULL,
    [TimeStarted]         DATETIME2 (7) NULL,
    [TimeAdded]           DATETIME2 (7) CONSTRAINT [DF_Sign_TimeAdded] DEFAULT (sysutcdatetime()) NOT NULL,
    [TimeFailed]          DATETIME2 (7) NULL,
    [IsNeedToSign]        BIT           CONSTRAINT [DF__SIGNQUEUE__NEEDT__79D2FC8C] DEFAULT ((0)) NOT NULL,
    [IsNeedToSetDateRead] BIT           CONSTRAINT [DF__SIGNQUEUE__NEEDT__7AC720C5] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_SignQueueEntryID] PRIMARY KEY CLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_Sign_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_SignQueue_InternalStudyID] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [SIGNQUEUE_I_SIGNQUEUE_SOP]
    ON [queue].[Sign]([InternalInstanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [SIGNQUEUE_I_SIGNQUEUE_STATUS]
    ON [queue].[Sign]([StatusValue] ASC);


GO
CREATE NONCLUSTERED INDEX [SIGNQUEUE_I_SIGNQUEUE_TIMEADDED]
    ON [queue].[Sign]([TimeAdded] ASC);

