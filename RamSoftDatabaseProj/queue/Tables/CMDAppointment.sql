﻿CREATE TABLE [queue].[CMDAppointment] (
    [InternalStudyID] BIGINT NOT NULL,
    [ErrorCount]      INT    CONSTRAINT [DF_CMDAppointment_ErrorCount] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_CMDAppointment] PRIMARY KEY CLUSTERED ([InternalStudyID] ASC),
    CONSTRAINT [FK_CMDAppointment_CMDAppointment] FOREIGN KEY ([InternalStudyID]) REFERENCES [queue].[CMDAppointment] ([InternalStudyID])
);

