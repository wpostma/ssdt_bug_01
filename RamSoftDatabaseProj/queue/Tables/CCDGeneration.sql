﻿CREATE TABLE [queue].[CCDGeneration] (
    [InternalID]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [InternalPatientID] BIGINT        NOT NULL,
    [InternalVisitID]   BIGINT        NULL,
    [InternalUserID]    BIGINT        NULL,
    [DateTime]          DATETIME2 (7) CONSTRAINT [DF_CCDGeneration_DateTime] DEFAULT (sysutcdatetime()) NOT NULL,
    CONSTRAINT [PK_CCDGeneration] PRIMARY KEY CLUSTERED ([InternalID] ASC)
);

