﻿CREATE TABLE [queue].[CachePush] (
    [EntryID]         BIGINT        IDENTITY (1, 1) NOT NULL,
    [TimeStamp]       DATETIME2 (7) CONSTRAINT [DF_CachePush_TimeStamp] DEFAULT (sysutcdatetime()) NOT NULL,
    [InternalUserID]  BIGINT        NULL,
    [InternalStudyID] BIGINT        NOT NULL,
    [DestClientID]    BIGINT        NULL,
    [DestUserID]      BIGINT        NULL,
    [PriorityValue]   SMALLINT      NOT NULL,
    CONSTRAINT [PK_CachePush] PRIMARY KEY CLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_CachePush_Client] FOREIGN KEY ([DestClientID]) REFERENCES [dbo].[Client] ([InternalClientID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_CachePush_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_CachePush_User] FOREIGN KEY ([DestUserID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_CachePush_User1] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_CachePush_Study_Dest]
    ON [queue].[CachePush]([InternalStudyID] ASC, [DestClientID] ASC, [DestUserID] ASC);

