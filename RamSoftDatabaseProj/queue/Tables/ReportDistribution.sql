﻿CREATE TABLE [queue].[ReportDistribution] (
    [InternalID]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalInstanceID]     BIGINT         NOT NULL,
    [DistributedByUserID]    BIGINT         NULL,
    [RecipientName]          NVARCHAR (64)  NULL,
    [RecipientNumber]        NVARCHAR (64)  NULL,
    [RecipientEmail]         NVARCHAR (64)  NULL,
    [EmailReportMethod]      INT            NULL,
    [RecipientType]          INT            NULL,
    [Attn]                   NVARCHAR (64)  NULL,
    [RecipientCountryCode]   NVARCHAR (64)  NULL,
    [RecipientDirectAddress] NVARCHAR (128) NULL,
    CONSTRAINT [DISTRIBUTIONTRIGGERQUEUE_INTEG_1551] PRIMARY KEY NONCLUSTERED ([InternalID] ASC),
    CONSTRAINT [FK_ReportDistribution_Instance] FOREIGN KEY ([InternalInstanceID]) REFERENCES [PHI].[Instance] ([InternalInstanceID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ReportDistribution_User] FOREIGN KEY ([DistributedByUserID]) REFERENCES [dbo].[User] ([InternalUserID]) NOT FOR REPLICATION
);


GO
ALTER TABLE [queue].[ReportDistribution] NOCHECK CONSTRAINT [FK_ReportDistribution_User];

