﻿CREATE TABLE [queue].[MPPSTransmit] (
    [InternalStationID] BIGINT        NOT NULL,
    [InternalStudyID]   BIGINT        NOT NULL,
    [PriorityValue]     SMALLINT      NOT NULL,
    [NumFailures]       SMALLINT      NOT NULL,
    [StatusValue]       SMALLINT      NOT NULL,
    [NumWarnings]       SMALLINT      NULL,
    [TimeStarted]       DATETIME2 (7) NULL,
    [TimeAdded]         DATETIME2 (7) CONSTRAINT [DF_MPPSTransmit_TimeAdded] DEFAULT (sysutcdatetime()) NOT NULL,
    [TimeFailed]        DATETIME2 (7) NULL,
    [ThreadID]          INT           NULL,
    CONSTRAINT [MPPSTRANSMITQUEUE_PK_MPPSTRANSMITQUEUE] PRIMARY KEY NONCLUSTERED ([InternalStationID] ASC, [InternalStudyID] ASC),
    CONSTRAINT [FK_MPPSTransmit_Station] FOREIGN KEY ([InternalStationID]) REFERENCES [dbo].[Station] ([InternalStationID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_MPPSTransmit_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE
);

