﻿CREATE TABLE [queue].[Request] (
    [EntryID]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalUserID]    BIGINT         NULL,
    [InternalStationID] BIGINT         NOT NULL,
    [StudyInstanceUID]  NVARCHAR (64)  NOT NULL,
    [PriorityValue]     SMALLINT       CONSTRAINT [DF__REQUESTLI__PRIOR__18227982] DEFAULT ((10)) NOT NULL,
    [NumFailures]       SMALLINT       CONSTRAINT [DF_Request_NumFailures] DEFAULT ((0)) NOT NULL,
    [StatusValue]       SMALLINT       CONSTRAINT [DF_Request_StatusValue] DEFAULT ((0)) NOT NULL,
    [TimeAdded]         DATETIME2 (7)  NOT NULL,
    [TimeFailed]        DATETIME2 (7)  NULL,
    [ThreadID]          INT            NULL,
    [NumWarnings]       SMALLINT       CONSTRAINT [DF_Request_NumWarnings] DEFAULT ((0)) NOT NULL,
    [TimeStarted]       DATETIME2 (7)  NULL,
    [IsAutoQuery]       BIT            CONSTRAINT [DF__REQUESTLI__AUTOQ__19169DBB] DEFAULT ((0)) NOT NULL,
    [ExtJson]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Queue_Request] PRIMARY KEY CLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_Request_Station] FOREIGN KEY ([InternalStationID]) REFERENCES [dbo].[Station] ([InternalStationID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Request_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Request_Station_StudyInstanceUID]
    ON [queue].[Request]([InternalStationID] ASC, [StudyInstanceUID] ASC);

