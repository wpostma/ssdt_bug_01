﻿CREATE TABLE [queue].[Fax] (
    [DISTRIBUTIONID]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalSeriesObjectsID] BIGINT         NOT NULL,
    [RECIPIENTNAME]           NVARCHAR (64)  NULL,
    [RECIPIENTNUMBER]         NVARCHAR (64)  NULL,
    [RECIPIENTKIND]           INT            NOT NULL,
    [USERNAME]                NVARCHAR (64)  NOT NULL,
    [STATUS]                  INT            NOT NULL,
    [DETAILSTATUS]            NVARCHAR (128) NULL,
    [PRIORITY]                INT            NOT NULL,
    [NUMFAILURES]             INT            NOT NULL,
    [TIMEFAILED]              DATETIME2 (7)  NULL,
    [TRANSACTIONID]           NVARCHAR (64)  NULL,
    [AUTODISTRIBUTED]         BIT            CONSTRAINT [DF__FAXPOSTQU__AUTOD__39E294A9] DEFAULT ((0)) NOT NULL,
    [FACILITYNAME]            NVARCHAR (64)  NULL,
    [ATTEN]                   NVARCHAR (64)  NULL,
    [TEMPFAXFILE]             NVARCHAR (255) NULL,
    [LASTACCESSTIMESTAMP]     DATETIME2 (7)  CONSTRAINT [DF_QFax_LASTACCESSTIMESTAMP] DEFAULT (sysutcdatetime()) NOT NULL,
    [RECIPIENTCOUNTRYCODE]    NVARCHAR (64)  NULL,
    CONSTRAINT [PK_QFax] PRIMARY KEY CLUSTERED ([DISTRIBUTIONID] ASC),
    CONSTRAINT [FK_QFax_InternalInstanceID] FOREIGN KEY ([InternalSeriesObjectsID]) REFERENCES [PHI].[Instance] ([InternalInstanceID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_QFaxStatus]
    ON [queue].[Fax]([STATUS] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_QFaxLastAccessTimestamp]
    ON [queue].[Fax]([LASTACCESSTIMESTAMP] ASC);

