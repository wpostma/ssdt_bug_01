﻿CREATE TABLE [queue].[Transmit] (
    [InternalStationID]  BIGINT   NOT NULL,
    [InternalInstanceID] BIGINT   NOT NULL,
    [StatusValue]        SMALLINT CONSTRAINT [DF_Transmit_StatusValue] DEFAULT ((0)) NOT NULL,
    [ThreadID]           INT      NULL,
    CONSTRAINT [PK_Transmit] PRIMARY KEY CLUSTERED ([InternalStationID] ASC, [InternalInstanceID] ASC),
    CONSTRAINT [FK_Transmit_Instance] FOREIGN KEY ([InternalStationID]) REFERENCES [PHI].[Instance] ([InternalInstanceID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Transmit_Station] FOREIGN KEY ([InternalStationID]) REFERENCES [dbo].[Station] ([InternalStationID]) ON DELETE CASCADE ON UPDATE CASCADE
);

