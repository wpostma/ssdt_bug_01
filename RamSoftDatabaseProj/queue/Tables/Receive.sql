﻿CREATE TABLE [queue].[Receive] (
    [EntryID]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [InternalStationID] BIGINT        NOT NULL,
    [InternalStudyID]   BIGINT        NOT NULL,
    [TimeAdded]         DATETIME2 (7) CONSTRAINT [DF_Receive_TimeAdded] DEFAULT (sysutcdatetime()) NOT NULL,
    [TimeUpdated]       DATETIME2 (7) CONSTRAINT [DF_Receive_TimeUpdated] DEFAULT (sysutcdatetime()) NOT NULL,
    [NumObjects]        INT           NOT NULL,
    CONSTRAINT [PK_Receive] PRIMARY KEY CLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_Receive_Station] FOREIGN KEY ([InternalStationID]) REFERENCES [dbo].[Station] ([InternalStationID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Receive_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE
);

