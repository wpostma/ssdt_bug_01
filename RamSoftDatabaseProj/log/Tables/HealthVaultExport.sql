﻿CREATE TABLE [log].[HealthVaultExport] (
    [EntryID]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [TimeStamp]         DATETIME2 (7)  CONSTRAINT [DF_HealthVaultExport_TimeStamp] DEFAULT (sysutcdatetime()) NULL,
    [InternalPatientID] BIGINT         NOT NULL,
    [ExtJson]           NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_HealthVaultExport] PRIMARY KEY CLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_HealthVaultExport_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID])
);

