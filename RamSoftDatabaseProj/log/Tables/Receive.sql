﻿CREATE TABLE [log].[Receive] (
    [EntryID]           BIGINT        NOT NULL,
    [InternalStationID] BIGINT        NOT NULL,
    [InternalStudyID]   BIGINT        NOT NULL,
    [TimeStarted]       DATETIME2 (7) NOT NULL,
    [TimeFinished]      DATETIME2 (7) NOT NULL,
    [NumObjects]        INT           NOT NULL,
    CONSTRAINT [PK_Log_Receive] PRIMARY KEY CLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_Receive_Station] FOREIGN KEY ([InternalStationID]) REFERENCES [dbo].[Station] ([InternalStationID]),
    CONSTRAINT [FK_Receive_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE
);

