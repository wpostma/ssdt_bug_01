﻿CREATE TABLE [log].[MPPSTransmit] (
    [EntryID]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [TimeStamp]         DATETIME2 (7) CONSTRAINT [DF_MPPSTransmit_TimeStamp] DEFAULT (sysutcdatetime()) NOT NULL,
    [TimeQueued]        DATETIME2 (7) NOT NULL,
    [InternalStationID] BIGINT        NOT NULL,
    [InternalStudyID]   BIGINT        NULL,
    CONSTRAINT [MPPSTRANSMITLOG_PK_MPPSTRANSMITLOG] PRIMARY KEY NONCLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_MPPSTransmit_Station] FOREIGN KEY ([InternalStationID]) REFERENCES [dbo].[Station] ([InternalStationID]),
    CONSTRAINT [FK_MPPSTransmit_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE
);

