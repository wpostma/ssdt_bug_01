﻿CREATE TABLE [log].[PatientMerge] (
    [InternalPatientID]    BIGINT        NOT NULL,
    [InternalUserID]       BIGINT        NOT NULL,
    [TimeStamp]            DATETIME2 (7) CONSTRAINT [DF_PatientMerge_TimeStamp] DEFAULT (sysutcdatetime()) NOT NULL,
    [OldIssuerOfPatientID] NVARCHAR (64) NOT NULL,
    [OldPatientID]         NVARCHAR (64) NOT NULL,
    CONSTRAINT [PATIENTMERGELOG_PK_PATIENTMERGELOG] PRIMARY KEY NONCLUSTERED ([InternalPatientID] ASC),
    CONSTRAINT [FK_PatientMerge_Patient1] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_PatientMerge_User1] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID])
);

