﻿CREATE TABLE [log].[DicomPrint] (
    [EntryID]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [InternalUserID]    BIGINT        NOT NULL,
    [InternalStationID] BIGINT        NOT NULL,
    [TimeStarted]       DATETIME2 (7) NOT NULL,
    [TimeFinished]      DATETIME2 (7) CONSTRAINT [DF_DicomPrint_TimeFinished] DEFAULT (sysutcdatetime()) NOT NULL,
    [NumPages]          SMALLINT      NOT NULL,
    CONSTRAINT [PRINTLOG_INTEG_1593] PRIMARY KEY NONCLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_DicomPrint_Station] FOREIGN KEY ([InternalStationID]) REFERENCES [dbo].[Station] ([InternalStationID]),
    CONSTRAINT [FK_DicomPrint_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID])
);


GO
CREATE NONCLUSTERED INDEX [PRINTLOG_PRINTLOGENTRYTIMEANDENTRYID]
    ON [log].[DicomPrint]([TimeFinished] ASC, [EntryID] ASC);


GO
CREATE NONCLUSTERED INDEX [PRINTLOG_PRINTLOG_TIMEFINISHEDDESC]
    ON [log].[DicomPrint]([TimeFinished] ASC);

