﻿CREATE TABLE [log].[UserConfigs] (
    [Username] VARCHAR (20)   NOT NULL,
    [Configs]  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK__UserConf__536C85E5779284D7] PRIMARY KEY CLUSTERED ([Username] ASC)
);

