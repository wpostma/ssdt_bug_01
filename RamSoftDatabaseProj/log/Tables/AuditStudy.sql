﻿CREATE TABLE [log].[AuditStudy] (
    [EntryID]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [TimeStamp]       DATETIME2 (7)  CONSTRAINT [DF_AuditStudy_TimeStamp] DEFAULT (sysutcdatetime()) NOT NULL,
    [InternalStudyID] BIGINT         NOT NULL,
    [InternalUserID]  BIGINT         NULL,
    [ExtJson]         NVARCHAR (MAX) NULL,
    CONSTRAINT [STUDYACCESS_INTEG_1630] PRIMARY KEY NONCLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_AuditStudy_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_AuditStudy_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE SET NULL ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [STUDYACCESS_I_STUDYACCESS_ENTRYIDDESC]
    ON [log].[AuditStudy]([EntryID] ASC);


GO
CREATE NONCLUSTERED INDEX [STUDYACCESS_I_STUDYACCESS_ACCESSTIMEUTC]
    ON [log].[AuditStudy]([TimeStamp] ASC);


GO
CREATE NONCLUSTERED INDEX [STUDYACCESS_STUDYACCESSSTUDY]
    ON [log].[AuditStudy]([InternalStudyID] ASC);


GO
CREATE NONCLUSTERED INDEX [STUDYACCESS_STUDYACCESSUSER]
    ON [log].[AuditStudy]([InternalUserID] ASC);

