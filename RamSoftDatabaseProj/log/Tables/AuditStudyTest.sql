﻿CREATE TABLE [log].[AuditStudyTest] (
    [EntryID]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [TimeStamp]       DATETIME2 (7)  NOT NULL,
    [InternalStudyID] BIGINT         NOT NULL,
    [InternalUserID]  BIGINT         NULL,
    [ExtJson]         NVARCHAR (MAX) NULL,
    CONSTRAINT [PK__AuditStu__F57BD2D652F4515C] PRIMARY KEY NONCLUSTERED ([EntryID] ASC)
);

