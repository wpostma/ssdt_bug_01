﻿CREATE TABLE [log].[System] (
    [EntryID]        BIGINT         NOT NULL,
    [InternalUserID] BIGINT         NULL,
    [TimeStamp]      DATETIME2 (7)  CONSTRAINT [DF_System_TimeStamp] DEFAULT (sysutcdatetime()) NOT NULL,
    [ExtJson]        NVARCHAR (MAX) NULL,
    CONSTRAINT [SYSTEMLOG_INTEG_1637] PRIMARY KEY NONCLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_System_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE SET NULL ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [SYSTEMLOG_SYSTEMLOGDATETIME]
    ON [log].[System]([TimeStamp] ASC);

