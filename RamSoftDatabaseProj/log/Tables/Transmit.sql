﻿CREATE TABLE [log].[Transmit] (
    [EntryID]            BIGINT        IDENTITY (1, 1) NOT NULL,
    [TimeStarted]        DATETIME2 (7) NOT NULL,
    [TimeFinished]       DATETIME2 (7) CONSTRAINT [DF_Transmit_TimeFinished] DEFAULT (sysutcdatetime()) NOT NULL,
    [InternalInstanceID] BIGINT        NULL,
    [InternalStudyID]    BIGINT        NULL,
    [InternalUserID]     BIGINT        NULL,
    [NumObjects]         INT           NOT NULL,
    CONSTRAINT [TRANSMITLOG_INTEG_1640] PRIMARY KEY NONCLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_Transmit_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Transmit_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID])
);


GO
CREATE NONCLUSTERED INDEX [TRANSMITLOG_TRANSMITLOGENTRYTIMEANDENTRYID]
    ON [log].[Transmit]([TimeFinished] ASC, [EntryID] ASC);


GO
CREATE NONCLUSTERED INDEX [TRANSMITLOG_TRANSMITLOG_TIMEFINISHEDDESC]
    ON [log].[Transmit]([TimeFinished] ASC);

