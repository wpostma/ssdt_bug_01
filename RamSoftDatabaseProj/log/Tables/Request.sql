﻿CREATE TABLE [log].[Request] (
    [EntryID]           BIGINT        NOT NULL,
    [InternalStationID] BIGINT        NOT NULL,
    [InternalStudyID]   BIGINT        NOT NULL,
    [InternalUserID]    BIGINT        NULL,
    [TimeStarted]       DATETIME2 (7) NOT NULL,
    [TimeFinished]      DATETIME2 (7) NOT NULL,
    [IsAutoQuery]       BIT           CONSTRAINT [DF__REQUESTLO__AUTOQ__5CC1BC92] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_Request] PRIMARY KEY CLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_Request_Station] FOREIGN KEY ([InternalStationID]) REFERENCES [dbo].[Station] ([InternalStationID]),
    CONSTRAINT [FK_Request_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Request_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Request_TimeStarted]
    ON [log].[Request]([TimeStarted] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Request_TimeFinished]
    ON [log].[Request]([TimeFinished] ASC);

