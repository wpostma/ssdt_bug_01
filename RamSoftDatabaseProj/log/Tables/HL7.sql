﻿CREATE TABLE [log].[HL7] (
    [EntryID]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [TimeStamp]         DATETIME2 (7)  CONSTRAINT [DF_HL7_TimeStamp] DEFAULT (sysutcdatetime()) NOT NULL,
    [InternalPatientID] BIGINT         NULL,
    [InternalStudyID]   BIGINT         NULL,
    [ExtJson]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_HL7] PRIMARY KEY CLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_HL7_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]),
    CONSTRAINT [FK_HL7_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE
);

