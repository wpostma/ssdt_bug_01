﻿CREATE TABLE [log].[MU] (
    [EntryID]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [TimeStamp] DATETIME2 (7)  CONSTRAINT [DF_MU_TimeStamp] DEFAULT (sysutcdatetime()) NOT NULL,
    [ExtJson]   NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_MU] PRIMARY KEY CLUSTERED ([EntryID] ASC)
);

