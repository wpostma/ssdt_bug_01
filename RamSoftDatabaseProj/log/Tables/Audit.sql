﻿CREATE TABLE [log].[Audit] (
    [EntryID]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [TimeStamp]      DATETIME2 (7)  CONSTRAINT [DF_Audit_ActivityDateTime] DEFAULT (sysutcdatetime()) NOT NULL,
    [InternalUserID] BIGINT         NULL,
    [ExtJson]        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_Audit_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID])
);


GO
CREATE NONCLUSTERED INDEX [ACTIVITYAUDIT_I_ACTIVITYAUDIT_DATETIME]
    ON [log].[Audit]([TimeStamp] ASC);

