﻿CREATE TABLE [log].[HealthVaultVDT] (
    [EntryID]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalPatientID] BIGINT         NOT NULL,
    [TimeStamp]         DATETIME2 (7)  CONSTRAINT [DF_HealthVaultVDT_TimeStamp] DEFAULT (sysutcdatetime()) NOT NULL,
    [ExtJson]           NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [HEALTHVAULTVDT_PK_HEALTHVAULTVDT] PRIMARY KEY NONCLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_HealthVaultVDT_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID])
);

