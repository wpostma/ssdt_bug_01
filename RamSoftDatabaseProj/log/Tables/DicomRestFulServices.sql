﻿CREATE TABLE [log].[DicomRestFulServices] (
    [EntryID]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [ServiceName]   NVARCHAR (64)  NOT NULL,
    [Item]          NVARCHAR (MAX) NOT NULL,
    [Description]   NVARCHAR (MAX) NULL,
    [LogInternalID] INT            NULL,
    [LogTime]       DATETIME2 (7)  CONSTRAINT [DF_DicomRestFulServices_LogTime] DEFAULT (sysutcdatetime()) NULL,
    [ExtJson]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_DicomRestFulServicesLogging] PRIMARY KEY CLUSTERED ([EntryID] ASC)
);

