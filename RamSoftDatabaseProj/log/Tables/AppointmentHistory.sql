﻿CREATE TABLE [log].[AppointmentHistory] (
    [InternalAppointmentLogID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalStudyID]          BIGINT         NOT NULL,
    [AccessTime]               DATETIME2 (7)  NOT NULL,
    [ExtJson]                  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_AppointmentLog] PRIMARY KEY CLUSTERED ([InternalAppointmentLogID] ASC),
    CONSTRAINT [CK_AppointmentHistory_ExtJson] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_AppointmentHistory_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE
);

