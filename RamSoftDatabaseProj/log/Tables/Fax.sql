﻿CREATE TABLE [log].[Fax] (
    [InternalFaxID]      BIGINT        IDENTITY (1, 1) NOT NULL,
    [InternalInstanceID] BIGINT        NOT NULL,
    [TimeCompleted]      DATETIME2 (7) CONSTRAINT [DF_Fax_TimeCompleted] DEFAULT (sysutcdatetime()) NOT NULL,
    [RecipientName]      NVARCHAR (64) NOT NULL,
    [RecipientNumber]    NVARCHAR (64) NOT NULL,
    CONSTRAINT [FAXLOG_INTEG_1553] PRIMARY KEY NONCLUSTERED ([InternalFaxID] ASC),
    CONSTRAINT [FK_Fax_Instance] FOREIGN KEY ([InternalInstanceID]) REFERENCES [PHI].[Instance] ([InternalInstanceID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [FAXLOG_I_FAXLOG_TIME_DISTID_DESC]
    ON [log].[Fax]([TimeCompleted] ASC, [InternalInstanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [FAXLOG_I_FAXLOG_TIME]
    ON [log].[Fax]([TimeCompleted] ASC);

