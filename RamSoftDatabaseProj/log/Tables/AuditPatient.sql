﻿CREATE TABLE [log].[AuditPatient] (
    [EntryID]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [TimeStamp]         DATETIME2 (7)  CONSTRAINT [DF_AuditPatient_TimeStamp] DEFAULT (sysutcdatetime()) NOT NULL,
    [InternalPatientID] BIGINT         NOT NULL,
    [InternalUserID]    BIGINT         NULL,
    [ExtJson]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PATIENTACCESS_INTEG_1584] PRIMARY KEY NONCLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_AuditPatient_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_AuditPatient_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE SET NULL ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [PATIENTACCESS_I_PATIENTACCESS_ENTRYIDDESC]
    ON [log].[AuditPatient]([EntryID] ASC);


GO
CREATE NONCLUSTERED INDEX [PATIENTACCESS_PATIENTACCESSTIME]
    ON [log].[AuditPatient]([TimeStamp] ASC);

