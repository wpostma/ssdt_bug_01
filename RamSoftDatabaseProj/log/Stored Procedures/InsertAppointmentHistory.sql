﻿CREATE PROCEDURE [log].[InsertAppointmentHistory] 
(	
	@InternalStudyID bigint,
	@ExtJson nvarchar(max)
)
AS
BEGIN	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	

	IF (@InternalStudyID IS NOT NULL)
	BEGIN
		INSERT INTO log.AppointmentHistory (
			[InternalStudyID],
			[AccessTime],
			[ExtJson]
		)
		VALUES(
			@InternalStudyID,
			sysutcdatetime(),
			@ExtJson
		);
	END	
END
