﻿ALTER ROLE [db_owner] ADD MEMBER [wpostma];


GO
ALTER ROLE [db_owner] ADD MEMBER [vijay];


GO
ALTER ROLE [db_owner] ADD MEMBER [RAMSOFT\tinhuynh];


GO
ALTER ROLE [db_owner] ADD MEMBER [RAMSOFT\vnguyen];


GO
ALTER ROLE [db_accessadmin] ADD MEMBER [vijay];


GO
ALTER ROLE [db_securityadmin] ADD MEMBER [wpostma];


GO
ALTER ROLE [db_securityadmin] ADD MEMBER [vijay];


GO
ALTER ROLE [db_securityadmin] ADD MEMBER [RAMSOFT\tinhuynh];


GO
ALTER ROLE [db_securityadmin] ADD MEMBER [RAMSOFT\vnguyen];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [wpostma];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [vijay];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [RAMSOFT\tinhuynh];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [RAMSOFT\vnguyen];


GO
ALTER ROLE [db_backupoperator] ADD MEMBER [wpostma];


GO
ALTER ROLE [db_backupoperator] ADD MEMBER [vijay];


GO
ALTER ROLE [db_backupoperator] ADD MEMBER [RAMSOFT\tinhuynh];


GO
ALTER ROLE [db_datareader] ADD MEMBER [wpostma];


GO
ALTER ROLE [db_datareader] ADD MEMBER [vijay];


GO
ALTER ROLE [db_datareader] ADD MEMBER [RAMSOFT\tinhuynh];


GO
ALTER ROLE [db_datareader] ADD MEMBER [RAMSOFT\vnguyen];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [wpostma];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [vijay];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [RAMSOFT\tinhuynh];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [RAMSOFT\vnguyen];

