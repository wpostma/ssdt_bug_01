﻿CREATE TABLE [etl].[ImportHistory] (
    [Id]                         INT            IDENTITY (1, 1) NOT NULL,
    [Timestamp]                  DATETIME2 (7)  CONSTRAINT [DF_IMPORTHISTORY_TIMESTAMP] DEFAULT (getdate()) NOT NULL,
    [IncomingForeignKeyInt]      INT            NULL,
    [IncomingForeignKeyOther]    NVARCHAR (80)  NULL,
    [IncomingTableName]          NVARCHAR (80)  NULL,
    [DestinationTableName]       NVARCHAR (80)  NULL,
    [IncomingFieldName]          NVARCHAR (80)  NULL,
    [DestinationFieldName]       NVARCHAR (80)  NULL,
    [DestinationForeignKeyInt]   NVARCHAR (80)  NULL,
    [DestinationForeignKeyOther] NVARCHAR (80)  NULL,
    [ExtJSON]                    NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

