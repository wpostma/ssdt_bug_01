﻿CREATE TABLE [mem].[FindStudy] (
    [SessionGUID]       UNIQUEIDENTIFIER NOT NULL,
    [InternalStationID] BIGINT           NOT NULL,
    [StudyInstanceUID]  NVARCHAR (64)    NOT NULL,
    [PatientID]         NVARCHAR (64)    NOT NULL,
    [IssuerOfPatientID] NVARCHAR (64)    NOT NULL,
    [PatientName]       NVARCHAR (64)    NULL,
    [StudyDateTime]     DATETIME2 (7)    NULL,
    [StudyID]           NVARCHAR (16)    NULL,
    [AccessionNumber]   NVARCHAR (16)    NULL,
    [Modalities]        NVARCHAR (64)    NULL,
    [StudyDescription]  NVARCHAR (64)    NULL,
    [Status]            NVARCHAR (16)    NULL,
    [InstitutionName]   NVARCHAR (64)    NULL,
    [PriorityStr]       NVARCHAR (64)    NULL,
    [Priority]          SMALLINT         NULL,
    [UTCOffset]         SMALLINT         NULL,
    CONSTRAINT [FindStudy_primaryKey] PRIMARY KEY NONCLUSTERED HASH ([SessionGUID], [InternalStationID], [StudyInstanceUID]) WITH (BUCKET_COUNT = 1024)
)
WITH (DURABILITY = SCHEMA_ONLY, MEMORY_OPTIMIZED = ON);

