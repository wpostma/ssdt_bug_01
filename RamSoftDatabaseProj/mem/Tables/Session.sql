﻿CREATE TABLE [mem].[Session] (
    [SessionGUID]       UNIQUEIDENTIFIER CONSTRAINT [DF_Session_SessionGUID] DEFAULT (newid()) NOT NULL,
    [InternalUserID]    BIGINT           NOT NULL,
    [InternalClientID]  BIGINT           NOT NULL,
    [StartedDateTime]   DATETIME2 (7)    CONSTRAINT [DF_Session_StartedDateTime] DEFAULT (sysutcdatetime()) NOT NULL,
    [LastAliveDateTime] DATETIME2 (7)    CONSTRAINT [DF_Session_LastAliveDateTime] DEFAULT (sysutcdatetime()) NOT NULL,
    CONSTRAINT [Session_primaryKey] PRIMARY KEY NONCLUSTERED HASH ([SessionGUID]) WITH (BUCKET_COUNT = 512)
)
WITH (DURABILITY = SCHEMA_ONLY, MEMORY_OPTIMIZED = ON);

