﻿CREATE TABLE [mem].[TransmitStudyList] (
    [InternalTransmitStudyID] BIGINT        IDENTITY (1, 1) NOT NULL,
    [InternalStationID]       BIGINT        NOT NULL,
    [InternalStudyID]         BIGINT        NOT NULL,
    [InternalUserID]          BIGINT        NULL,
    [PriorityValue]           SMALLINT      CONSTRAINT [DF__TRANSMITS__PRIOR__384F51F2] DEFAULT ((10)) NOT NULL,
    [NumFailures]             SMALLINT      NOT NULL,
    [Status]                  SMALLINT      NOT NULL,
    [NumWarnings]             SMALLINT      NOT NULL,
    [TimeAdded]               DATETIME2 (7) CONSTRAINT [DF_TRANSMITSTUDYLIST_TimeAdded] DEFAULT (sysutcdatetime()) NOT NULL,
    [TimeStarted]             DATETIME2 (7) NULL,
    [TimeFailed]              DATETIME2 (7) NULL,
    [ThreadID]                INT           NULL,
    CONSTRAINT [TransmitStudyList_primaryKey] PRIMARY KEY NONCLUSTERED ([InternalTransmitStudyID] ASC)
)
WITH (MEMORY_OPTIMIZED = ON);

