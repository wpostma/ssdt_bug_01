﻿
CREATE PROCEDURE [rls].[Set_Session_Context] @UserID bigint
AS
BEGIN
    -- wiki: https://bugs.ramsoft.com/projects/ramsoft-scrum/wiki/DB_Session_Context_and_Row-Level_Security?parent=Backend_API

    IF (@UserID > 0)
	BEGIN
		DECLARE @RoleExtJson nvarchar(max) = null;
		DECLARE @GroupExtJson nvarchar(max) = null;
		DECLARE @UserHasFacilityMap bit = 0;

		SELECT @RoleExtJson = r.ExtJson,
			   @GroupExtJson = g.ExtJson,
			   @UserHasFacilityMap	= (SELECT TOP 1 1 FROM dbo.UserFacilityMap ufm WHERE ufm.InternalUserID = u.InternalUserID)
			FROM dbo.[User] u 
			JOIN dbo.[Group] g ON g.InternalGroupID = u.InternalGroupID
			JOIN dbo.[Role] r ON u.InternalRoleID = r.InternalRoleID 
			WHERE U.InternalUserID = @UserID;

		DECLARE @IsSyswideAdmin bit = (CASE WHEN JSON_VALUE(@RoleExtJson, '$.isSyswideAdmin') = 'true' THEN 1 ELSE 0 END);
		DECLARE @IsAdmin bit = @IsSyswideAdmin | (CASE WHEN JSON_VALUE(@RoleExtJson, '$.isAdmin') = 'true' THEN 1 ELSE 0 END);
		DECLARE @GroupAccessType int = (CASE WHEN TRY_CONVERT(int, JSON_VALUE(@GroupExtJson, '$.accessType')) IS NULL THEN 0 ELSE CONVERT(int, JSON_VALUE(@GroupExtJson, '$.accessType')) END);
		
		-- If an user has no assigned facilities then this means the user can access all facilities.
		DECLARE @CanAccessAllFacilities bit = @IsSyswideAdmin | (CASE WHEN @UserHasFacilityMap = 1 THEN 0 ELSE 1 END);

		-- If a group has no assigned issuers then this means the group can access all issuers.
		DECLARE @CanAccessAllIssuers bit = (CASE WHEN (@GroupAccessType = 5) OR (JSON_VALUE(@GroupExtJson, '$.issuers[0].id') IS NULL) THEN 1 ELSE 0 END);
		DECLARE @GroupIssuerJson nvarchar(2048) = JSON_QUERY(@GroupExtJson, '$.issuers');

		EXEC sp_set_session_context @key = N'UserID', @value=@UserID;
		EXEC sp_set_session_context @key = N'IsSyswideAdmin', @value=@IsSyswideAdmin;
		EXEC sp_set_session_context @key = N'IsAdmin', @value=@IsAdmin;
		EXEC sp_set_session_context @key = N'GroupAccessType', @value=@GroupAccessType;
		EXEC sp_set_session_context @key = N'GroupIssuerJson', @value=@GroupIssuerJson;
		EXEC sp_set_session_context @key = N'CanAccessAllFacilities', @value=@CanAccessAllFacilities;
		EXEC sp_set_session_context @key = N'CanAccessAllIssuers', @value=@CanAccessAllIssuers;
	END
END
