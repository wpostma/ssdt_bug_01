﻿CREATE TYPE [dbo].[HumanName]
    FROM NVARCHAR (MAX) NOT NULL;


GO
EXECUTE sp_addextendedproperty @name = N'family', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TYPE', @level1name = N'HumanName';


GO
EXECUTE sp_addextendedproperty @name = N'given', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TYPE', @level1name = N'HumanName';


GO
EXECUTE sp_addextendedproperty @name = N'period', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TYPE', @level1name = N'HumanName';


GO
EXECUTE sp_addextendedproperty @name = N'prefix', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TYPE', @level1name = N'HumanName';


GO
EXECUTE sp_addextendedproperty @name = N'suffix', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TYPE', @level1name = N'HumanName';


GO
EXECUTE sp_addextendedproperty @name = N'text', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TYPE', @level1name = N'HumanName';


GO
EXECUTE sp_addextendedproperty @name = N'use', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TYPE', @level1name = N'HumanName';

