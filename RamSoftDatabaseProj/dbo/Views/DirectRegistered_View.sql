﻿CREATE VIEW [dbo].[DirectRegistered_View] AS
SELECT d.InternalDirectAddressID,
       d.DirectAddress,
	   d.InternalUserID,
	   u.UserName,
	   u.[Name],
	   d.InternalFacilityID,
	   o.OrganizationName AS FacilityName,
	   d.IsActive,
	   d.LastUpdateUTC,
	   d.ExtJson
FROM dbo.DirectRegistered d
JOIN dbo.Organization o ON o.InternalOrganizationID = d.InternalFacilityID
LEFT JOIN dbo.[User] u ON u.InternalUserID = d.InternalUserID
