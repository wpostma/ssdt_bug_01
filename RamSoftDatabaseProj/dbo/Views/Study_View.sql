﻿
CREATE view [dbo].[Study_View]
	AS
	SELECT        sd.InternalStudyID,
				  sd.ImagingFacilityID,
				  sd.LateralityCode,
				  sd.StatusValue,
				  sd.StudyUID,
				  sd.AccessionNumber,
				  sd.StudyDateTime,
				  sd.StudyID,
				  sd.PriorityValue,
				  sd.InternalDepartmentID,
				  sd.ModalityCode, 
				  sd.InternalBodyPartID,
				  sd.InternalPatientID,
				  sd.InternalStudyTypeID,
				  sd.Description,
				  pa.InternalAssigningAuthorityID,
				  pa.PatientID,
				  pa.PatientName,
				  pa.Ssn,
				  og.OrganizationName, 				  				   
				  st.StatusName,
				  ro.InternalRoomID,
				  ro.Name as RoomName,
				  (SELECT        TOP (1) JSON_VALUE(VALUE, '$.value') AS Expr1
				    FROM            OPENJSON(JSON_VALUE(pa.ExtJson, '$.contactPoint'))
				    WHERE        (JSON_VALUE(VALUE, '$.use') = 'mobile') AND (JSON_VALUE(VALUE, '$.system') = 'phone')) AS CellPhone,				  
				  CONVERT(date, pa.BirthDate) AS BirthDate,
				  bp.Value AS BodyPartValue,
				  pr.Name AS PriorityName,
				  aa.Name AS AssigningAuthorityName			  				  				  

	FROM          PHI.Study sd LEFT OUTER JOIN
				  code.Priority pr ON sd.PriorityValue = pr.PriorityValue LEFT OUTER JOIN
				  code.BodyPart bp ON sd.InternalBodyPartID = bp.InternalBodyPartID LEFT OUTER JOIN
				  PHI.Patient pa ON sd.InternalPatientID = pa.InternalPatientID LEFT OUTER JOIN
				  dbo.Organization og ON sd.ImagingFacilityID = og.InternalOrganizationID LEFT OUTER JOIN
				  dbo.Room ro ON sd.InternalRoomID = ro.InternalRoomID LEFT OUTER JOIN
				  code.Status st ON sd.StatusValue = st.StatusValue LEFT OUTER JOIN				  
				  dbo.AssigningAuthority aa ON pa.InternalAssigningAuthorityID = aa.InternalAssigningAuthorityID


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[52] 4[12] 2[32] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ImagingServiceRequest (PHI)"
            Begin Extent = 
               Top = 294
               Left = 38
               Bottom = 424
               Right = 301
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Study (PHI)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 274
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Priority (code)"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BodyPart (code)"
            Begin Extent = 
               Top = 138
               Left = 496
               Bottom = 268
               Right = 684
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Patient (PHI)"
            Begin Extent = 
               Top = 6
               Left = 312
               Bottom = 136
               Right = 502
            End
            DisplayFlags = 280
            TopColumn = 17
         End
         Begin Table = "Facility"
            Begin Extent = 
               Top = 6
               Left = 540
               Bottom = 136
               Right = 729
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Status (code)"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 20', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'Study_View';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'8
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Visit (PHI)"
            Begin Extent = 
               Top = 138
               Left = 246
               Bottom = 268
               Right = 458
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "AssigningAuthority"
            Begin Extent = 
               Top = 294
               Left = 339
               Bottom = 424
               Right = 581
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'Study_View';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'Study_View';

