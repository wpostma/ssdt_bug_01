﻿CREATE VIEW [dbo].[ScheduledModality_View]
AS
SELECT * FROM code.Modality
WHERE IsSchedule = 1
