﻿CREATE VIEW [dbo].[ReservedTime_View]
	AS
	SELECT  A.InternalReservedTimeID, A.InternalStudyID, A.LastUpdateUserID, A.LastUpdateDateTime, A.Claimed, A.StartDate, A.EndDate, A.StartTime, A.EndTime, A.Recurrence, A.IsActive, A.IsAppointmentAllowed, 
				A.ExtJson AS ReservedExtJSON, A.Display, S.AccessionNumber, S.InternalStudyID AS Expr1, S.PriorityValue, PR.Name AS PriorityName, S.StatusValue, ST.StatusName, 
				S.LateralityCode, S.ModalityCode, S.InternalBodyPartID, B.Value, S.Description AS StudyDescription, S.StudyID, S.StudyDateTime, S.ExtJson AS StudyExtJSON, P.PatientName, P.ExtJson AS PatientExtJSON, 
				CONVERT(DATE, P.BirthDate) AS BirthDate, JSON_QUERY(P.ExtJson, '$.Address') AS Address, RT.InternalReservedTimeResourceID, D.InternalFacilityID, RS.InternalRoomID, RO.Name AS RoomName, 
				D.InternalDepartmentID, D.Department, F.OrganizationName AS FacilityName, F.IsImaging, RT.InternalResourceID, P.InternalPatientID, P.PatientID
	FROM    dbo.ReservedTime AS A LEFT OUTER JOIN
				PHI.Study AS S ON A.InternalStudyID = S.InternalStudyID LEFT OUTER JOIN
				code.Priority AS PR ON PR.PriorityValue = S.PriorityValue LEFT OUTER JOIN
				code.Status AS ST ON ST.StatusValue = S.StatusValue LEFT OUTER JOIN
				code.BodyPart AS B ON B.InternalBodyPartID = S.InternalBodyPartID LEFT OUTER JOIN                         
				PHI.Patient AS P ON P.InternalPatientID = S.InternalPatientID LEFT OUTER JOIN
				dbo.ReservedTimeResource AS RT ON A.InternalReservedTimeID = RT.InternalReservedTimeID LEFT OUTER JOIN
				dbo.Resource RS JOIN (dbo.Room ro JOIN (dbo.Department D JOIN	dbo.Organization F ON D.InternalFacilityID = F.InternalOrganizationID) 
				ON ro.InternalDepartmentID = D.InternalDepartmentID) ON RS.InternalRoomID = ro.InternalRoomID 
				ON RT.InternalResourceID = RS.InternalResourceID

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[47] 4[25] 2[22] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -672
      End
      Begin Tables = 
         Begin Table = "A"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 252
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "S"
            Begin Extent = 
               Top = 6
               Left = 290
               Bottom = 136
               Right = 553
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "PR"
            Begin Extent = 
               Top = 252
               Left = 38
               Bottom = 382
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ST"
            Begin Extent = 
               Top = 270
               Left = 246
               Bottom = 400
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "B"
            Begin Extent = 
               Top = 270
               Left = 454
               Bottom = 400
               Right = 642
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "I"
            Begin Extent = 
               Top = 21
               Left = 20
               Bottom = 151
               Right = 283
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "P"
            Begin Extent = 
               Top = 6
               Left = 892
               Bottom = 136
               Right = 1134
            End
            DisplayFlags = 280
            TopColumn = 5
         En', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ReservedTime_View';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'd
         Begin Table = "RT"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 300
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RS"
            Begin Extent = 
               Top = 138
               Left = 338
               Bottom = 268
               Right = 540
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RO"
            Begin Extent = 
               Top = 138
               Left = 578
               Bottom = 268
               Right = 781
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "D"
            Begin Extent = 
               Top = 58
               Left = 388
               Bottom = 188
               Right = 591
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "F"
            Begin Extent = 
               Top = 138
               Left = 1060
               Bottom = 268
               Right = 1302
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ReservedTime_View';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ReservedTime_View';

