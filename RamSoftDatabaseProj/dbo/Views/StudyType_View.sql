﻿
CREATE VIEW [dbo].[StudyType_View]
	AS
		SELECT InternalStudyTypeID, StudyType, Description, IsActive, JSON_VALUE(ExtJson, '$.duration') AS Duration, ExtJson
		FROM   dbo.StudyType;

