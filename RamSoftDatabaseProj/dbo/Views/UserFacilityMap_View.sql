﻿
CREATE VIEW [dbo].[UserFacilityMap_View]
	AS
	SELECT uf.*, f.OrganizationName, f.IsActive,
	        JSON_VALUE(f.Extjson, '$.address[0].line') AS [Address], 
	        JSON_VALUE(f.Extjson, '$.address[0].city') AS [City], 
	        JSON_VALUE(f.Extjson, '$.address[0].state') AS [State]
			FROM dbo.UserFacilityMap uf JOIN dbo.Organization f ON uf.InternalFacilityID = f.InternalOrganizationID

