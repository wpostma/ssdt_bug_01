﻿CREATE View [dbo].[Payer_View]
	as
	Select *, JSON_VALUE(ExtJson, '$.address[0].line') as Address, JSON_VALUE(ExtJson, '$.address[0].city') as City, JSON_VALUE(ExtJson, '$.address[0].postalCode') as ZipCode 
	From dbo.Payer
