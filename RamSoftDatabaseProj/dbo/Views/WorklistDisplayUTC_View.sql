﻿CREATE VIEW [dbo].[WorklistDisplayUTC_View]
	AS
	SELECT        sd.InternalStudyID,
				  sd.ImagingFacilityID,				  
				  sd.StatusValue,
				  sd.StudyUID,
				  sd.AccessionNumber,				  
				  sd.StudyDateTimeUTC AS StudyDateTime,
				  sd.StudyID,
				  sd.PriorityValue,
				  sd.InternalDepartmentID,
				  sd.ModalityCode,
				  sd.LateralityCode,
				  sd.InternalBodyPartID,
				  sd.InternalPatientID,
				  sd.InternalStudyTypeID,
				  sd.Description,
				  pa.InternalAssigningAuthorityID,
				  pa.PatientID,
				  pa.PatientName,
				  pa.PatientBalance,
				  JSON_VALUE(pa.ExtJson, '$.language') AS [Language],
				  JSON_VALUE(pa.ExtJson,'$.accountStatus') AS AccountStatus,				  
				  sx.Sex,
				  (SELECT sta.State FROM code.State sta WHERE sta.StateID = pa.StateID) AS State,
				  pa.Ssn,
				  (SELECT        JSON_VALUE(VALUE, '$.value') AS Expr1
				    FROM            OPENJSON(JSON_QUERY(pa.ExtJson, '$.contactPoint'))
				    WHERE        (JSON_VALUE(VALUE, '$.use') = 'mobile') AND (JSON_VALUE(VALUE, '$.system') = 'phone')) AS CellPhone,
				 (SELECT        JSON_VALUE(VALUE, '$.value') AS Expr1
				    FROM            OPENJSON(JSON_QUERY(pa.ExtJson, '$.contactPoint'))
				    WHERE        (JSON_VALUE(VALUE, '$.use') = 'home') AND (JSON_VALUE(VALUE, '$.system') = 'phone')) AS HomePhone,
				  CONVERT(DATE, pa.BirthDate) AS BirthDate,
				  (CASE WHEN pa.BirthDate IS NULL THEN NULL 
					    WHEN sd.StudyDateTime IS NULL THEN DATEDIFF(YEAR, pa.BirthDate, SYSDATETIME())  
					    WHEN pa.BirthDate > sd.StudyDateTime THEN 0
					    ELSE DATEDIFF(YEAR, pa.BirthDate, sd.StudyDateTime) END) AS Age, 
				  og.OrganizationName,
				  og.TimeZone, 				  				   
				  st.StatusName,
				  ro.InternalRoomID,
				  ro.Name AS RoomName,				  
				  bp.Value AS BodyPartValue,
				  pr.Name AS PriorityName,
				  aa.Name AS AssigningAuthorityName,

				  dbo.GetStudyPlayerPhysicianName(sd.InternalStudyID, 0) AS ReadingPhysician,
				  dbo.GetStudyPlayerOrganizationName(sd.InternalStudyID, 0) AS ReadingFacility,
				  dbo.GetStudyPlayerPhysicianName(sd.InternalStudyID, 1) AS ReferringPhysician,
				  dbo.GetStudyPlayerOrganizationName(sd.InternalStudyID, 1) AS ReferringFacility,
				  (SELECT JSON_VALUE(us.ExtJson, '$.address[0].line') FROM dbo.StudyPlayer sp 
						JOIN dbo.[User] us ON sp.InternalUserID = us.InternalUserID						
					WHERE sp.InternalStudyID = sd.InternalStudyID AND sp.StudyPlayerType = 1) AS ReferringAddress,
				  dbo.GetStudyPlayerPhysicianName(sd.InternalStudyID, 2) AS PerformingPhysician,
				  dbo.GetStudyPlayerOrganizationName(sd.InternalStudyID, 2) AS PerformingFacility,
				  dbo.GetStudyPlayerPhysicianName(sd.InternalStudyID, 3) AS PerformingTechnologist,
				  dbo.GetStudyPlayerOrganizationName(sd.InternalStudyID, 3) AS PerformingTechnologistFacility,
				  dbo.GetStudyPlayerPhysicianName(sd.InternalStudyID, 4) AS Transcriptionist,
				  dbo.GetStudyPlayerOrganizationName(sd.InternalStudyID, 4) AS TranscriptionistFacility,

				  sd.LastupdateUTC AS Lastupdated,
				  dbo.GetStudyStatusTransitionTime(sd.InternalStudyID, dbo.GetSystemConfigIntValue('ORDERED status value')) AS DateTimeOrdered,
				  dbo.GetStudyStatusTransitionTime(sd.InternalStudyID, dbo.GetSystemConfigIntValue('VERIFIED status value')) AS DateTimeVerified,
				  dbo.GetStudyStatusTransitionTime(sd.InternalStudyID, dbo.GetSystemConfigIntValue('READ status value')) AS DateTimeRead,
				  dbo.GetStudyStatusTransitionTime(sd.InternalStudyID, dbo.GetSystemConfigIntValue('TRANSCRIBED status value')) AS DateTimeTranscribed,
				  dbo.GetStudyStatusTransitionTime(sd.InternalStudyID, dbo.GetSystemConfigIntValue('SIGNED status value')) AS DateTimeSigned,
				  
				  dbo.GetStudyLastReceivedDate(sd.InternalStudyID) AS DateTimeReceived,				  
				  dbo.GetStudyFirstAmendmentDate(sd.InternalStudyID) AS DateTimeAddendum,
				  
				  JSON_VALUE(sd.ExtJson, '$.notes.comments') AS Comments,
				  JSON_VALUE(sd.ExtJson, '$.notes.clinical') AS Clinical,
				  JSON_VALUE(sd.ExtJson, '$.notes.history') AS History,
				  JSON_VALUE(sd.ExtJson, '$.customField1') AS customField1,
				  JSON_VALUE(sd.ExtJson, '$.customField2') AS customField2,
				  JSON_VALUE(sd.ExtJson, '$.customField3') AS customField3,
				  JSON_VALUE(sd.ExtJson, '$.customField4') AS customField4,
				  JSON_VALUE(sd.ExtJson, '$.customField5') AS customField5,
				  JSON_VALUE(sd.ExtJson, '$.customField6') AS customField6,

				  --Display Primary Financial Type 
				  (SELECT TOP 1 fc.FinancialClass  FROM phi.EncounterCoverage ec JOIN code.FinancialClass fc ON ec.FinancialClassID = fc.FinancialClassID
				  WHERE InternalEncounterID = fhir.GetPrimaryEncounter(sd.InternalStudyID) AND ec.CoverageLevelID = 1) AS FinancialType,
				  -- Payers
				  dbo.GetStudyInsurances(sd.InternalStudyID, 1) AS InsurancePayers,
				  -- Copays
				  dbo.GetStudyInsurances(sd.InternalStudyID, 2) AS InsuranceCopays,
				  -- EndDates
				  dbo.GetStudyInsurances(sd.InternalStudyID, 3) AS InsuranceExpires,
				  --Insurance Status
				  (SELECT ins.InsuranceStatusCode FROM Phi.Encounter iect JOIN code.InsuranceStatus ins ON iect.InsuranceStatusID = ins.InsuranceStatusID
				  WHERE iect.InternalEncounterID = fhir.GetPrimaryEncounter(sd.InternalStudyID)) AS InsuranceStatus,

				  --ProcedureCodes
				  dbo.GetStudyProcedureCodes(sd.InternalStudyID) AS ProcedureCodes,
				  --Allergies
				  dbo.GetStudyPatientAllergies(sd.InternalPatientID) AS Allergies,
				  --Authorizations
				  dbo.GetStudyAuthorizationNumbers(sd.InternalStudyID) AS AuthorizationNumbers,
				  --Authorization Periods
				  dbo.GetStudyAuthorizationPeriod(sd.InternalStudyID) AS AuthorizationPeriod,

				  (JSON_VALUE(sd.ExtJson, '$.cancellationReason')) AS CancellationReason,
				  (CASE WHEN sd.IsBilled = 1 THEN 'Completed' 
						WHEN sd.IsBilled = 0 AND sd.IsPosted = 1 THEN 'Started' END
				  ) AS ChargePosting,

				  dbo.GetStudyReferrerReport(sd.InternalStudyID) AS Delivery,
				  (SELECT cms.CommunicationStatusName FROM code.CommunicationStatus cms 
				  WHERE cms.CommunicationStatusID = sd.FaxStatusID) AS FaxStatus,				  

				  --Images
				  (SELECT COUNT(*) FROM phi.Instance ist JOIN code.SOPClass scl ON ist.InternalSOPClassID = scl.InternalSOPClassID
				  WHERE ist.InternalSeriesID IN 
				  (
				  	SELECT InternalSeriesID FROM phi.Series WHERE InternalStudyID = sd.InternalStudyID
				  )
				  AND scl.IsImage = 1
				  ) AS Images,

				  --Object Type
				  dbo.GetStudySerieModalities(sd.InternalStudyID) AS ObjectType,

				  DATEDIFF(n, sd.StartDateTime, sd.EndDateTime) AS Duration,
				  --Location
				   (SELECT JSON_VALUE(iec.ExtJson, '$.patientLocation') 
				   FROM phi.Encounter iec WHERE iec.InternalEncounterID = fhir.GetPrimaryEncounter(sd.InternalStudyID)) AS [Location],
				   --VisitNumber
				   (SELECT iec.AdmissionID FROM PHI.Encounter iec WHERE iec.InternalEncounterID = fhir.GetPrimaryEncounter(sd.InternalStudyID)) AS VisitNumber,
				   -- Scheduled Resource Name
				   (SELECT rsc.Name FROM dbo.ReservedTimeResource rts JOIN dbo.[Resource] rsc ON rts.InternalResourceID = rsc.InternalResourceID
					WHERE rts.InternalReservedTimeID = (
						SELECT rst.InternalReservedTimeID FROM dbo.ReservedTime rst WHERE rst.InternalStudyID = sd.InternalStudyID)
				   ) AS ScheduledResource,				   
				   (SELECT roex.Name FROM dbo.Room roex WHERE roex.InternalRoomID = sd.ScheduledExamRoomID)  AS ScheduledExamRoom,
				   bp.BodyPartExamined AS ScheduledBodyPart

	FROM          PHI.Study sd LEFT OUTER JOIN
				  code.Priority pr ON sd.PriorityValue = pr.PriorityValue LEFT OUTER JOIN
				  code.BodyPart bp ON sd.InternalBodyPartID = bp.InternalBodyPartID LEFT OUTER JOIN				  
				  dbo.Organization og ON sd.ImagingFacilityID = og.InternalOrganizationID LEFT OUTER JOIN
				  dbo.Room ro ON sd.InternalRoomID = ro.InternalRoomID LEFT OUTER JOIN
				  code.Status st ON sd.StatusValue = st.StatusValue LEFT OUTER JOIN
				  PHI.Patient pa ON sd.InternalPatientID = pa.InternalPatientID LEFT OUTER JOIN
				  code.Sex sx ON pa.SexID = sx.SexID LEFT OUTER JOIN
				  dbo.AssigningAuthority aa ON pa.InternalAssigningAuthorityID = aa.InternalAssigningAuthorityID
