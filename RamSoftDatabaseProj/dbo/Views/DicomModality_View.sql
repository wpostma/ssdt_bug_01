﻿CREATE VIEW [dbo].[DicomModality_View]
AS
SELECT * FROM code.Modality
WHERE IsModality = 1
