﻿CREATE View [dbo].[Organization_View]
	as 
	Select o.*, 
		Json_Value(o.ExtJson, '$.address[0].line') as [Address], 
		Json_Value(o.ExtJson, '$.address[0].city') as City, 
		Json_Value(o.ExtJson, '$.address[0].state') as [State]  
	From dbo.Organization o
