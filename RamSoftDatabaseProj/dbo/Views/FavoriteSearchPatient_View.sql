﻿CREATE view [dbo].[FavoriteSearchPatient_View]
as
SELECT        TOP (100) dbo.FrequentlySearchPatient.ID, PHI.Patient.InternalPatientID, dbo.FrequentlySearchPatient.InternalUserID, dbo.FrequentlySearchPatient.Frequently_Used, PHI.Patient.PatientID, 
                         PHI.Patient.PatientName, PHI.Patient.BirthDate,
                             (SELECT        TOP (1) JSON_VALUE(Value, '$.value') AS Expr1
                               FROM            OpenJson(Json_Value(PHI.Patient.ExtJson, '$.contactPoint'))
                               WHERE        (JSON_VALUE(Value, '$.use') = 'mobile') AND (JSON_VALUE(Value, '$.system') = 'phone')) AS CELLPHONE
FROM            dbo.FrequentlySearchPatient FULL OUTER JOIN
                         PHI.Patient ON dbo.FrequentlySearchPatient.InternalPatientID = PHI.Patient.InternalPatientID
ORDER BY dbo.FrequentlySearchPatient.Frequently_Used DESC
