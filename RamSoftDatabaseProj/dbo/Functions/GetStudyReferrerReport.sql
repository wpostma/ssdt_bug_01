﻿CREATE FUNCTION [dbo].[GetStudyReferrerReport](@InternalStudyID BIGINT)
RETURNS NVARCHAR(MAX)
AS
	BEGIN
		DECLARE @Reports NVARCHAR(MAX);
		DECLARE @Data NVARCHAR(64);
		DECLARE @Key NVARCHAR(64);
		DECLARE @ReferrerTypeID INT = 1;
		DECLARE @UserID INT; 

		SELECT @UserID = InternalUserID FROM dbo.StudyPlayer 
		WHERE InternalStudyID = @InternalStudyID AND StudyPlayerType = @ReferrerTypeID

		DECLARE ReportsCursor CURSOR LOCAL FOR
		SELECT [key], value FROM OPENJSON
		(
			(SELECT ExtJson FROM dbo.[User] WHERE InternalUserID = @UserID),
			'$.reportingDetails'
		)

		OPEN ReportsCursor
		FETCH NEXT FROM ReportsCursor INTO @Key, @Data;
		WHILE @@FETCH_STATUS = 0
		BEGIN
		
		IF @Data = 'true'
		BEGIN
		 SET @Reports = CONCAT(@Reports,' | ', @Key);
		END
		ELSE IF ISJSON(@Data) > 0
		BEGIN
			DECLARE @S NVARCHAR(15);
			DECLARE @T NVARCHAR(40);

			IF @Key = 'preliminary'			
				SET @S = ' Preliminary';
			ELSE							
				SET @S = ' Final';

			DECLARE ValueCursor CURSOR LOCAL FOR
			SELECT Value FROM OPENJSON(@Data);

			OPEN ValueCursor
			FETCH NEXT FROM ValueCursor INTO @T;
			WHILE @@FETCH_STATUS = 0
			BEGIN

				SET @Reports = CONCAT(@Reports,' | ', @T + @S);

			FETCH NEXT FROM ValueCursor INTO @T;
			END
			CLOSE ValueCursor; 
			DEALLOCATE ValueCursor; 					
		END

		FETCH NEXT FROM ReportsCursor INTO @Key, @Data;		
		END		
		CLOSE ReportsCursor; 
		DEALLOCATE ReportsCursor; 	

		SET @Reports = STUFF(@Reports,1,3,'');		
		RETURN @Reports;
	END
