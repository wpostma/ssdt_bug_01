﻿CREATE FUNCTION dbo.GetSystemConfigIntValue(@Item nvarchar(256))
	RETURNS bigint
	AS
	BEGIN
		RETURN 
		(SELECT CASE WHEN TRY_CONVERT(bigint, JSON_VALUE(C.ExtJson, '$.intValue')) IS NULL THEN NULL ELSE JSON_VALUE(C.ExtJson, '$.intValue') END  
			FROM dbo.Config c 
			JOIN dbo.[User] u ON u.InternalUserID = c.InternalUserID
			WHERE c.Item = @Item AND u.UserName  = 'SYSTEM');
	END
