﻿CREATE FUNCTION [dbo].[GetStudyLastReceivedDate](
		@InternalStudyID bigint)
	RETURNS DATETIME2(7)
	AS
	BEGIN
		RETURN 
		(SELECT TOP 1 TimeFinished FROM [log].[Receive] r
			WHERE r.InternalStudyID = @InternalStudyID
			ORDER BY r.TimeFinished DESC
		);
	END
