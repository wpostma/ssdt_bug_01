﻿CREATE FUNCTION [dbo].[GetStudyAuthorizationNumbers](@InternalStudyID BIGINT)
RETURNS NVARCHAR(MAX)
AS
	BEGIN
		DECLARE @Numbers NVARCHAR(MAX);
		DECLARE @Data NVARCHAR(64);

		DECLARE NumbersCursor CURSOR LOCAL FOR
		SELECT spm.AuthorizationNumber FROM dbo.StudyProcedureMap spm WHERE spm.InternalStudyID = @InternalStudyID

		OPEN NumbersCursor
		FETCH NEXT FROM NumbersCursor INTO @Data;
		WHILE @@FETCH_STATUS = 0
		BEGIN
		
		 SET @Numbers = CONCAT(@Numbers,' | ', @Data);
		FETCH NEXT FROM NumbersCursor INTO @Data;			
		END		
		CLOSE NumbersCursor; 
		DEALLOCATE NumbersCursor; 

		SET @Numbers = STUFF(@Numbers,1,3,'');		
		RETURN @Numbers;
	END
