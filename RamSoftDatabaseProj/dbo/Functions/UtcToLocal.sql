﻿CREATE FUNCTION [dbo].[UtcToLocal]
	(
		@GmtTime datetime2,
		@TimeZone sysname 
	)
	RETURNS datetime2
	AS
	BEGIN
		IF (@TimeZone IS NULL) OR (NOT EXISTS(SELECT 1 FROM [SYS].[time_zone_info] WHERE name = @TimeZone))
		BEGIN
			SET @TimeZone = dbo.ServerTimeZone(); 
		END

		DECLARE @Result datetime2 = convert(datetimeoffset, @GmtTime) AT TIME ZONE @TimeZone; 

		RETURN @Result;
	END
