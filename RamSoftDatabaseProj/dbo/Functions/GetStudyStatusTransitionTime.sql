﻿CREATE FUNCTION [dbo].[GetStudyStatusTransitionTime](
		@InternalStudyID bigint,
		@StatusValue smallint)
	RETURNS DATETIME2(7)
	AS
	BEGIN
		RETURN 
		(SELECT TransitionTimeUTC FROM dbo.StudyStatusTransition sst WHERE sst.InternalStudyID = @InternalStudyID AND sst.StatusValue = @StatusValue);
	END
