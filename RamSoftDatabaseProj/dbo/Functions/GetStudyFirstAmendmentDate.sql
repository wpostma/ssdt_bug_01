﻿CREATE FUNCTION [dbo].[GetStudyFirstAmendmentDate](
		@InternalStudyID bigint)
	RETURNS DATETIME2(7)
	AS
	BEGIN
		RETURN 
		(SELECT TOP 1 a.RequestDateTime FROM PHI.Amendment a 
			JOIN PHI.Instance obj on obj.InternalInstanceID = a.InternalInstanceID
			JOIN PHI.Series se ON se.InternalSeriesID = obj.InternalSeriesID
			WHERE se.InternalStudyID = @InternalStudyID
			ORDER BY a.RequestDateTime
		);
	END
