﻿CREATE FUNCTION [dbo].[ServerTimeZone]()
		RETURNS nVARCHAR(MAX)
	BEGIN
		DECLARE @TimeZone NVARCHAR(128);

		EXEC MASTER.dbo.xp_regread 
			'HKEY_LOCAL_MACHINE', 
			'SYSTEM\CurrentControlSet\Control\TimeZoneInformation', 
			'TimeZoneKeyName', 
			@TimeZone OUT;

		RETURN @TimeZone;
	END
