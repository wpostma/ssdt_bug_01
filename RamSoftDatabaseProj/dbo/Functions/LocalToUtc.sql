﻿
	CREATE FUNCTION [dbo].[LocalToUtc]
	(
		@LocalTime datetime2,
		@TimeZone sysname
	)
	RETURNS datetime2
	AS
	BEGIN
		IF (@TimeZone IS NULL) OR (NOT EXISTS(SELECT 1 FROM [SYS].[time_zone_info] WHERE name = @TimeZone))
		BEGIN
			SET @TimeZone = dbo.ServerTimeZone(); 
		END

		-- GET offset of minutes between UTC and Local based on Input Dtae to apply Day Light Saving Time
		DECLARE @BaseUTC datetime2 = convert(datetimeoffset, @LocalTime) AT TIME ZONE 'UTC';
		DECLARE @BaseTZTime datetime2 = convert(datetimeoffset, @LocalTime) AT TIME ZONE @TimeZone;

		DECLARE @OffsetMinutes int =  DATEDIFF(MINUTE, @BaseTZTime, @BaseUTC)

		DECLARE @Result datetime2 = DATEADD(MINUTE, @OffsetMinutes, @LocalTime);

		RETURN @Result;
	END

