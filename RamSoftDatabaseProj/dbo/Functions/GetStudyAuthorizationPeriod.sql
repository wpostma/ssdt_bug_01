﻿CREATE FUNCTION [dbo].[GetStudyAuthorizationPeriod](@InternalStudyID BIGINT)
RETURNS NVARCHAR(MAX)
AS
	BEGIN
		DECLARE @Periods NVARCHAR(MAX);
		DECLARE @Data NVARCHAR(150);

		DECLARE PeriodsCursor CURSOR LOCAL FOR
		SELECT CONCAT(CAST(spm.AuthorizationStartDate AS NVARCHAR(12)), ' -- ' , CAST(spm.AuthorizationEndDate AS NVARCHAR(12))) 
		FROM dbo.StudyProcedureMap spm WHERE spm.InternalStudyID = @InternalStudyID

		OPEN PeriodsCursor
		FETCH NEXT FROM PeriodsCursor INTO @Data;
		WHILE @@FETCH_STATUS = 0
		BEGIN
		
		 SET @Periods = CONCAT(@Periods,' | ', @Data);
		FETCH NEXT FROM PeriodsCursor INTO @Data;			
		END		
		CLOSE PeriodsCursor; 
		DEALLOCATE PeriodsCursor; 

		SET @Periods = STUFF(@Periods,1,3,'');		
		RETURN @Periods;
	END
