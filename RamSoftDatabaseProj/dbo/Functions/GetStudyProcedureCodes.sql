﻿CREATE FUNCTION [dbo].[GetStudyProcedureCodes](@InternalStudyID BIGINT)
RETURNS NVARCHAR(MAX)
AS
	BEGIN
		DECLARE @ProcedureCodes NVARCHAR(MAX);
		DECLARE @Data NVARCHAR(64);

		DECLARE ProcedureCursor CURSOR LOCAL FOR
		SELECT pc.ProcedureCode FROM dbo.StudyProcedureMap spm JOIN code.ProcedureCode pc ON  spm.InternalProcedureCodeID = pc.InternalProcedureCodeID
			WHERE spm.InternalStudyID = @InternalStudyID

		OPEN ProcedureCursor
		FETCH NEXT FROM ProcedureCursor INTO @Data;
		WHILE @@FETCH_STATUS = 0
		BEGIN
		
		 SET @ProcedureCodes = CONCAT(@ProcedureCodes,' | ', @Data);
		FETCH NEXT FROM ProcedureCursor INTO @Data;			
		END		
		CLOSE ProcedureCursor; 
		DEALLOCATE ProcedureCursor; 

		SET @ProcedureCodes = STUFF(@ProcedureCodes,1,3,'');		
		RETURN @ProcedureCodes;
	END
