﻿CREATE FUNCTION [dbo].[GetStudyPatientAllergies](@InternalPatientID BIGINT)
RETURNS NVARCHAR(MAX)
AS
	BEGIN
		DECLARE @Allergies NVARCHAR(MAX);
		DECLARE @Data NVARCHAR(64);

		DECLARE AllergiesCursor CURSOR LOCAL FOR
		SELECT JSON_VALUE(Value, '$.substance.coding.display') FROM OPENJSON(
			(SELECT ExtJson FROM  phi.Patient WHERE InternalPatientID = @InternalPatientID),
			'$.allergies'
		)

		OPEN AllergiesCursor
		FETCH NEXT FROM AllergiesCursor INTO @Data;
		WHILE @@FETCH_STATUS = 0
		BEGIN
		
		 SET @Allergies = CONCAT(@Allergies,' | ', @Data);
		FETCH NEXT FROM AllergiesCursor INTO @Data;			
		END		
		CLOSE AllergiesCursor; 
		DEALLOCATE AllergiesCursor; 

		SET @Allergies = STUFF(@Allergies,1,3,'');		
		RETURN @Allergies;
	END
	
