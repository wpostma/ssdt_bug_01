﻿CREATE FUNCTION [dbo].[ConvertJsonArrayToDicomString](@Json NVARCHAR(MAX))
RETURNS NVARCHAR(MAX)
AS
	BEGIN
		DECLARE @ConvertedData NVARCHAR(MAX);
		DECLARE @Data NVARCHAR(5);

		DECLARE DataCursor CURSOR LOCAL FOR
		SELECT VALUE FROM OPENJSON(@Json)

		OPEN DataCursor
		FETCH NEXT FROM DataCursor INTO @Data;
		WHILE @@FETCH_STATUS = 0
		BEGIN
		
		 SET @ConvertedData = CONCAT(@ConvertedData,' / ', @Data);
		FETCH NEXT FROM DataCursor INTO @Data;			
		END		
		CLOSE DataCursor; 
		DEALLOCATE DataCursor; 

		SET @ConvertedData = STUFF(@ConvertedData,1,3,'');				
		RETURN @ConvertedData
	END
