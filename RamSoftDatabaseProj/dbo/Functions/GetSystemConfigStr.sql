﻿CREATE FUNCTION dbo.GetSystemConfigStr(@Item nvarchar(256))
	RETURNS nvarchar(max)
	AS
	BEGIN
		RETURN 
		(SELECT JSON_VALUE(C.ExtJson, '$.strValue') 
			FROM dbo.Config c 
			JOIN dbo.[User] u ON u.InternalUserID = c.InternalUserID
			WHERE c.Item = @Item AND u.UserName  = 'SYSTEM');
	END
