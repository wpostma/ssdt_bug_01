﻿
CREATE FUNCTION [dbo].[GetReferenceURL] 
(
   @Url nvarchar(max)
)
RETURNS nvarchar(max)
AS
BEGIN
	
	DECLARE @base nvarchar(120) = 'api/v3/';
	DECLARE @fullUrl nvarchar(max) = Concat(@base,@Url)
	RETURN @fullUrl
END

