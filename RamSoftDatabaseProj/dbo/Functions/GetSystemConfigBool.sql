﻿CREATE FUNCTION dbo.GetSystemConfigBool(@Item nvarchar(256))
	RETURNS bit
	AS
	BEGIN
		RETURN 
		(SELECT CASE WHEN TRY_CONVERT(bit, JSON_VALUE(C.ExtJson, '$.boolValue')) IS NULL THEN NULL ELSE JSON_VALUE(C.ExtJson, '$.boolValue') END  
			FROM dbo.Config c 
			JOIN dbo.[User] u ON u.InternalUserID = c.InternalUserID
			WHERE c.Item = @Item AND u.UserName  = 'SYSTEM');
	END
