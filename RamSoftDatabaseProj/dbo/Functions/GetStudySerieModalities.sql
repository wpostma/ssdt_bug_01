﻿CREATE FUNCTION [dbo].[GetStudySerieModalities](@InternalStudyID BIGINT)
RETURNS NVARCHAR(MAX)
AS
	BEGIN
		DECLARE @Modalities NVARCHAR(MAX);
		DECLARE @Data NVARCHAR(64);

		DECLARE ModalityCursor CURSOR LOCAL FOR
		SELECT DISTINCT ModalityCode FROM phi.Series WHERE InternalStudyID = @InternalStudyID;

		OPEN ModalityCursor
		FETCH NEXT FROM ModalityCursor INTO @Data;
		WHILE @@FETCH_STATUS = 0
		BEGIN
		
		 SET @Modalities = CONCAT(@Modalities,' | ', @Data);
		FETCH NEXT FROM ModalityCursor INTO @Data;			
		END		
		CLOSE ModalityCursor; 
		DEALLOCATE ModalityCursor; 

		SET @Modalities = STUFF(@Modalities,1,3,'');		
		RETURN @Modalities;
	END	
