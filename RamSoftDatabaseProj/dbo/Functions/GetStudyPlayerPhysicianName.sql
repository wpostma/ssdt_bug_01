﻿CREATE FUNCTION [dbo].[GetStudyPlayerPhysicianName](
		@InternalStudyID BIGINT,
		@StudyPlayerType INT)
	RETURNS NVARCHAR(64)
	AS
	BEGIN
		RETURN 
		(SELECT us.Name FROM dbo.StudyPlayer sp 
			JOIN dbo.[User] us ON sp.InternalUserID = us.InternalUserID						
		WHERE sp.InternalStudyID = @InternalStudyID AND sp.StudyPlayerType = @StudyPlayerType);
	END
