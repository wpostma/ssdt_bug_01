﻿CREATE FUNCTION [dbo].[GetStudyPlayerOrganizationName](
		@InternalStudyID BIGINT,
		@StudyPlayerType INT)
	RETURNS NVARCHAR(64)
	AS
	BEGIN
		RETURN 
		(SELECT org.OrganizationName FROM dbo.StudyPlayer sp 
			JOIN dbo.Organization org ON sp.InternalFacilityID = org.InternalOrganizationID						
		WHERE sp.InternalStudyID = @InternalStudyID AND sp.StudyPlayerType = @StudyPlayerType);
	END
