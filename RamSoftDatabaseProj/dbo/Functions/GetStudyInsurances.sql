﻿CREATE FUNCTION [dbo].[GetStudyInsurances]
(
	@InternalStudyID BIGINT,
	@InsuranceInfo SMALLINT
)
RETURNS NVARCHAR(MAX)
AS
	BEGIN

		DECLARE @PayerName NVARCHAR(64);		
		DECLARE @EndDate NVARCHAR(12);
		DECLARE @Copay NVARCHAR(64);

		DECLARE @Data NVARCHAR(64);

		DECLARE DataCursor CURSOR LOCAL FOR
		SELECT 			
			py.CompanyName,
			ec.EndDate,
			CASE JSON_VALUE(ec.ExtJson, '$.copayType') 
				WHEN '$' THEN JSON_VALUE(ec.ExtJson, '$.copayAmount')
				ELSE CONCAT(JSON_VALUE(ec.ExtJson, '$.copayPercent'), JSON_VALUE(ec.ExtJson, '$.copayType')) 
			END	 AS Copay	
		FROM Phi.EncounterCoverage ec JOIN dbo.Payer py ON ec.InternalPayerID = py.InternalPayerID
		WHERE ec.InternalEncounterID = fhir.GetPrimaryEncounter(@InternalStudyID);

		OPEN DataCursor
		FETCH NEXT FROM DataCursor INTO @PayerName, @EndDate, @Copay;
		WHILE @@FETCH_STATUS = 0
		BEGIN
		 -- 1 is Payer list
		IF @InsuranceInfo = 1
		  SET @Data = CONCAT(@Data,' | ', @PayerName);
		--2 is Copay List
		ELSE IF @InsuranceInfo = 2
		  SET @Data = CONCAT(@Data,' | ', @Copay);	
		--3 is EndDate List
		ELSE
		  SET @Data = CONCAT(@Data,' | ', @EndDate);

		FETCH NEXT FROM DataCursor INTO @PayerName, @EndDate, @Copay;
		END		
		CLOSE DataCursor; 
		DEALLOCATE DataCursor; 

		SET @Data = STUFF(@Data,1,3,'');		
		RETURN @Data;
	END

