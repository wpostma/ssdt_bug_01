﻿CREATE PROCEDURE [dbo].[sp_GetFavoritePatient] @internaluserID bigint, @patientName nvarchar(64)
AS
Begin	
SELECT        TOP (10) temp.ID, PHI.Patient.InternalPatientID, temp.InternalUserID, temp.Frequently_Used, PHI.Patient.PatientID, 
                         PHI.Patient.PatientName, PHI.Patient.BirthDate,
						                              (SELECT        TOP (1) JSON_VALUE(Value, '$.value') AS Expr1
                               FROM            OPENJSON(JSON_VALUE(PHI.Patient.ExtJson, '$.contactPoint'))
                               WHERE        (JSON_VALUE(Value, '$.use') = 'mobile') AND (JSON_VALUE(Value, '$.system') = 'phone')) AS CELLPHONE
FROM            ( SELECT *
						FROM dbo.FrequentlySearchPatient 
						WHERE InternalUserID = @internaluserID) as temp RIGHT OUTER JOIN
                         PHI.Patient ON temp.InternalPatientID = PHI.Patient.InternalPatientID
WHERE PatientName LIKE @patientName
ORDER BY temp.Frequently_Used DESC
END

