﻿
	CREATE PROCEDURE [dbo].[SP_UpdatePatientFinancialClass]
		@InternalPatientID bigint
	AS
	BEGIN
		SET NOCOUNT ON;

		-- This will be executed in the trigger of PatientCoverage to sync Patient's Financial Class with Primary Insurance Company's Financial Class

		IF (NOT EXISTS (SELECT 1 FROM phi.PatientCoverage WHERE InternalPatientID = @InternalPatientID AND IsActive = 1))
		BEGIN
		    -- If no active covrages then set to Self-Pay
			DECLARE @SelfPayID int = (SELECT FinancialClassID FROM code.FinancialClass WHERE FinancialClass = 'SELF-PAY');

			IF (@SelfPayID > 0)
			BEGIN
				UPDATE phi.Patient SET FinancialClassID = @SelfPayID WHERE InternalPatientID = @InternalPatientID AND FinancialClassID <> @SelfPayID;
			END
		END
		ELSE
		BEGIN
			DECLARE @PrimaryCoverageLevelID int = (SELECT CoverageLevelID FROM code.CoverageLevel WHERE CoverageLevel = 'PRIMARY');
			DECLARE @ActivePrimaryFinancialClassID int;

			SELECT TOP 1 @ActivePrimaryFinancialClassID = py.FinancialClassID 
				FROM phi.PatientCoverage c 
				JOIN dbo.Payer py ON py.InternalPayerID = c.InternalPayerID
				WHERE c.InternalPatientID = @InternalPatientID AND c.IsActive = 1 AND c.CoverageLevelID = @PrimaryCoverageLevelID

			IF (@ActivePrimaryFinancialClassID > 0)
			BEGIN
				UPDATE phi.Patient SET FinancialClassID = @ActivePrimaryFinancialClassID WHERE InternalPatientID = @InternalPatientID AND FinancialClassID <> @ActivePrimaryFinancialClassID;
			END
		END
	END
