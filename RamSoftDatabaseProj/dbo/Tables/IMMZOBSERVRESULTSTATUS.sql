﻿CREATE TABLE [dbo].[IMMZOBSERVRESULTSTATUS] (
    [ID]          INT             NOT NULL,
    [CODE]        NVARCHAR (4)    NOT NULL,
    [DESCRIPTION] NVARCHAR (1000) NOT NULL,
    CONSTRAINT [IMMZOBSERVRESULTSTATUS_PK_IMMZOBSERVRESULTSTATUS] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IMMZOBSERVRESULTSTATUS_U_IMMZOBSERVRESULTSTATUSCODE]
    ON [dbo].[IMMZOBSERVRESULTSTATUS]([CODE] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IMMZOBSERVRESULTSTATUS_U_IMMZOBSERVRESULTSTATUSDESC]
    ON [dbo].[IMMZOBSERVRESULTSTATUS]([DESCRIPTION] ASC);

