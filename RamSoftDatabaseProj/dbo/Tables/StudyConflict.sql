﻿CREATE TABLE [dbo].[StudyConflict] (
    [InternalStudyID]            BIGINT NULL,
    [ConflictingInternalStudyID] BIGINT NULL,
    [IsAccessionNumberChanged]   BIT    CONSTRAINT [DF_StudyConflict_IsAccessionNumberChanged] DEFAULT ((0)) NOT NULL
);

