﻿CREATE TABLE [dbo].[DisplayCompression] (
    [EntryID]            BIGINT         IDENTITY (1, 1) NOT NULL,
    [ModalityCode]       NVARCHAR (8)   NOT NULL,
    [InternalBodyPartID] BIGINT         NULL,
    [Preset]             NVARCHAR (64)  NOT NULL,
    [ExtJson]            NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [COMPRESSIONSETTINGS_PK_COMPRESSIONSETTINGS] PRIMARY KEY NONCLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_DisplayCompression_BodyPart] FOREIGN KEY ([InternalBodyPartID]) REFERENCES [code].[BodyPart] ([InternalBodyPartID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [COMPRESSIONSETTINGS_UNIQUE_IDX_COMPRESSIONSETTINGS]
    ON [dbo].[DisplayCompression]([Preset] ASC, [ModalityCode] ASC, [InternalBodyPartID] ASC);

