﻿CREATE TABLE [dbo].[Organization] (
    [InternalOrganizationID]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalAssigningAuthorityID] BIGINT         NOT NULL,
    [LastupdateUTC]                DATETIME2 (7)  CONSTRAINT [DF_Organization_LastupdateUTC] DEFAULT (sysutcdatetime()) NULL,
    [OrganizationName]             NVARCHAR (64)  NOT NULL,
    [IsActive]                     BIT            CONSTRAINT [DF_Organization_IsActive] DEFAULT ((1)) NOT NULL,
    [IsImaging]                    BIT            CONSTRAINT [DF_Organization_IsImaging] DEFAULT ((0)) NOT NULL,
    [IsReferring]                  BIT            CONSTRAINT [DF_Organization_IsReferring] DEFAULT ((0)) NOT NULL,
    [IsMammoTracking]              BIT            CONSTRAINT [DF_Organization_IsMammoTracking] DEFAULT ((0)) NOT NULL,
    [ParentOrganizationID]         BIGINT         NULL,
    [ExtJson]                      NVARCHAR (MAX) NULL,
    [FeeScheduleID]                BIGINT         NULL,
    [SelfPayFeeScheduleID]         BIGINT         NULL,
    [TimeZone]                     [sysname]      CONSTRAINT [DF_Organization_TimeZone] DEFAULT ([dbo].[ServerTimeZone]()) NOT NULL,
    CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED ([InternalOrganizationID] ASC),
    CONSTRAINT [CK_Organization_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [FK_Organization_FeeSchedule] FOREIGN KEY ([FeeScheduleID]) REFERENCES [dbo].[FeeSchedule] ([FeeScheduleID]),
    CONSTRAINT [FK_Organization_Issuer] FOREIGN KEY ([InternalAssigningAuthorityID]) REFERENCES [dbo].[AssigningAuthority] ([InternalAssigningAuthorityID]),
    CONSTRAINT [FK_Organization_ParentOrg] FOREIGN KEY ([ParentOrganizationID]) REFERENCES [dbo].[Organization] ([InternalOrganizationID]),
    CONSTRAINT [FK_Organization_SelfPayFeeSchedule] FOREIGN KEY ([SelfPayFeeScheduleID]) REFERENCES [dbo].[FeeSchedule] ([FeeScheduleID])
);

