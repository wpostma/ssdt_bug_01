﻿CREATE TABLE [dbo].[Country] (
    [CountryID]   INT            IDENTITY (1, 1) NOT NULL,
    [CountryCode] CHAR (2)       NOT NULL,
    [CountryName] NVARCHAR (256) NOT NULL,
    [IsActive]    BIT            CONSTRAINT [DF_Country_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([CountryID] ASC),
    CONSTRAINT [CK_Country_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_Country_Code] UNIQUE NONCLUSTERED ([CountryCode] ASC)
);

