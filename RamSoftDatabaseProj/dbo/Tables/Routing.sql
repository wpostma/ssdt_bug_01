﻿CREATE TABLE [dbo].[Routing] (
    [EntryID]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [RuleName] NVARCHAR (64)  NOT NULL,
    [ExtJson]  NVARCHAR (MAX) CONSTRAINT [DF__ROUTINGLI__STATU__5011CCEA] DEFAULT ((100)) NOT NULL,
    CONSTRAINT [PK_Routing] PRIMARY KEY NONCLUSTERED ([EntryID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [Routing_RuleName]
    ON [dbo].[Routing]([RuleName] ASC);

