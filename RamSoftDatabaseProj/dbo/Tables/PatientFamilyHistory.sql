﻿CREATE TABLE [dbo].[PatientFamilyHistory] (
    [InternalFamilyHistoryID] INT            IDENTITY (1, 1) NOT NULL,
    [InternalPatientID]       BIGINT         NOT NULL,
    [InternalProblemCodeID]   BIGINT         NOT NULL,
    [FamilyMemberID]          INT            NOT NULL,
    [OnSet]                   DATETIME2 (7)  NULL,
    [DateModified]            DATETIME2 (7)  CONSTRAINT [DF_PatientFamilyHistory_DateModified] DEFAULT (sysdatetime()) NULL,
    [FamilyHistoryStatusID]   INT            NULL,
    [IsActive]                BIT            CONSTRAINT [DF_PatientFamilyHistory_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]                 NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_PatientFamilyHistory] PRIMARY KEY CLUSTERED ([InternalFamilyHistoryID] ASC),
    CONSTRAINT [CK_PatientFamilyHistory_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_PatientFamilyHistory_FamilyMember] FOREIGN KEY ([FamilyMemberID]) REFERENCES [code].[FamilyMember] ([FamilyMemberID]),
    CONSTRAINT [FK_PatientFamilyHistory_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_PatientFamilyHistory_ProblemCode] FOREIGN KEY ([InternalProblemCodeID]) REFERENCES [code].[Problem] ([InternalProblemCodeID]),
    CONSTRAINT [FK_PatientFamilyHistory_Status] FOREIGN KEY ([FamilyHistoryStatusID]) REFERENCES [code].[FamilyHistoryStatus] ([FamilyHistoryStatusID])
);

