﻿CREATE TABLE [dbo].[Worklist] (
    [InternalWorklistID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalUserID]     BIGINT         NULL,
    [InternalRoleID]     BIGINT         NULL,
    [WorklistName]       NVARCHAR (64)  NOT NULL,
    [ExtJson]            NVARCHAR (MAX) NULL,
    [TimeZoneTypeID]     INT            CONSTRAINT [DF_Worklist_TimeZoneType] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Worklist] PRIMARY KEY CLUSTERED ([InternalWorklistID] ASC),
    CONSTRAINT [FK_Worklist_Role] FOREIGN KEY ([InternalRoleID]) REFERENCES [dbo].[Role] ([InternalRoleID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Worklist_TimeZoneType] FOREIGN KEY ([TimeZoneTypeID]) REFERENCES [code].[TimeZoneType] ([TimeZoneTypeID]),
    CONSTRAINT [FK_Worklist_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID])
);

