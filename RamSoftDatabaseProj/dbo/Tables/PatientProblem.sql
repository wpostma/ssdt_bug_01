﻿CREATE TABLE [dbo].[PatientProblem] (
    [InternalPatientProblemID] INT            IDENTITY (1, 1) NOT NULL,
    [InternalPatientID]        BIGINT         NOT NULL,
    [InternalProblemCodeID]    BIGINT         NOT NULL,
    [OnSet]                    DATETIME2 (7)  NULL,
    [DateModified]             DATETIME2 (7)  CONSTRAINT [DF_PatientProblem_DateModified] DEFAULT (sysdatetime()) NULL,
    [ConditionVerStatusID]     INT            NULL,
    [IsActive]                 BIT            CONSTRAINT [DF_PatientProblem_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]                  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_PatientProblem] PRIMARY KEY CLUSTERED ([InternalPatientProblemID] ASC),
    CONSTRAINT [CK_PatientProblem_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_PatientProblem_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_PatientProblem_ProblemCode] FOREIGN KEY ([InternalProblemCodeID]) REFERENCES [code].[Problem] ([InternalProblemCodeID]),
    CONSTRAINT [FK_PatientProblem_Status] FOREIGN KEY ([ConditionVerStatusID]) REFERENCES [code].[ConditionVerStatus] ([ConditionVerStatusID])
);

