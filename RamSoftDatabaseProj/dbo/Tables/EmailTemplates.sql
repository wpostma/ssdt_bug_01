﻿CREATE TABLE [dbo].[EmailTemplates] (
    [Subject] NVARCHAR (256) NOT NULL,
    [ExtJson] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_EmailTemplates] PRIMARY KEY CLUSTERED ([Subject] ASC)
);

