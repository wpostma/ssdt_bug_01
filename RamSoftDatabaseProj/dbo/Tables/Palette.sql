﻿CREATE TABLE [dbo].[Palette] (
    [InternalPaletteID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]              NVARCHAR (64)  NOT NULL,
    [ExtJson]           NVARCHAR (MAX) NULL,
    CONSTRAINT [COLORMAPPING_INTEG_1540] PRIMARY KEY NONCLUSTERED ([InternalPaletteID] ASC)
);

