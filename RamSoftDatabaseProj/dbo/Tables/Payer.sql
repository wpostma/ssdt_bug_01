﻿CREATE TABLE [dbo].[Payer] (
    [InternalPayerID]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [CompanyName]      NVARCHAR (64)  NOT NULL,
    [FeeScheduleID]    BIGINT         NULL,
    [CarrierID]        NVARCHAR (64)  NOT NULL,
    [IsActive]         BIT            CONSTRAINT [DF_Payer_IsActive] DEFAULT ((1)) NULL,
    [ExtJson]          NVARCHAR (MAX) NULL,
    [StateID]          INT            NULL,
    [CountryID]        INT            NULL,
    [FinancialClassID] INT            NOT NULL,
    CONSTRAINT [PK_Payer] PRIMARY KEY CLUSTERED ([InternalPayerID] ASC),
    CONSTRAINT [Payer_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_Payer_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID]),
    CONSTRAINT [FK_Payer_FeeSchedule] FOREIGN KEY ([FeeScheduleID]) REFERENCES [dbo].[FeeSchedule] ([FeeScheduleID]),
    CONSTRAINT [FK_Payer_FinancialClass] FOREIGN KEY ([FinancialClassID]) REFERENCES [code].[FinancialClass] ([FinancialClassID]),
    CONSTRAINT [FK_Payer_State] FOREIGN KEY ([StateID]) REFERENCES [code].[State] ([StateID]),
    CONSTRAINT [U_Payer_CarrierID] UNIQUE NONCLUSTERED ([CarrierID] ASC)
);


GO
ALTER TABLE [dbo].[Payer] NOCHECK CONSTRAINT [FK_Payer_FeeSchedule];


GO
ALTER TABLE [dbo].[Payer] NOCHECK CONSTRAINT [FK_Payer_FinancialClass];


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_InsuranceCompany_CarrierID]
    ON [dbo].[Payer]([CarrierID] ASC);


GO

	CREATE TRIGGER [dbo].[Payer_AU_SetPatientFinancialClass]
	   ON  [dbo].[Payer]
	   FOR UPDATE 
	AS 
	BEGIN
		DECLARE @InternalPayerID bigint;
		DECLARE @NewInternalFinancialID int;
		DECLARE @PrimaryCoverageLevelID int

		-- If Financial Class has been updated then sync Patient Financial Class with the primary payer's Financial Class

		DECLARE Diff_Cursor CURSOR LOCAL FOR
		   SELECT i.InternalPayerID, i.FinancialClassID FROM inserted i
		      JOIN deleted d ON d.InternalPayerID = i.InternalPayerID
			  WHERE d.FinancialClassID <> i.FinancialClassID;

		OPEN Diff_Cursor;
		FETCH NEXT FROM Diff_Cursor INTO @InternalPayerID, @NewInternalFinancialID;

		IF (@@FETCH_STATUS = 0) SET @PrimaryCoverageLevelID = (SELECT CoverageLevelID FROM code.CoverageLevel WHERE CoverageLevel = 'PRIMARY');

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN

			-- To sync Patient's Financial Class with Primary Insurance Company's Financial Class
			UPDATE phi.Patient SET FinancialClassID = @NewInternalFinancialID 
				FROM PHI.PatientCoverage pc 
				WHERE pc.InternalPayerID = @internalPayerID AND pc.CoverageLevelID = @PrimaryCoverageLevelID AND pc.IsActive = 1
				  AND pc.InternalPatientID = phi.Patient.InternalPatientID AND phi.Patient.FinancialClassID <> @NewInternalFinancialID; 

			FETCH NEXT FROM Diff_Cursor INTO @InternalPayerID, @NewInternalFinancialID;
		END
		CLOSE Diff_Cursor;
	END
