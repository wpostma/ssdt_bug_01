﻿CREATE TABLE [dbo].[PRRelease] (
    [Target]      NVARCHAR (16) NOT NULL,
    [TimeStamp]   DATETIME2 (7) CONSTRAINT [DF_PRRelease_TimeStamp] DEFAULT (sysutcdatetime()) NULL,
    [VerNum]      SMALLINT      NOT NULL,
    [Description] NVARCHAR (64) NULL,
    CONSTRAINT [PRRELEASE_PK_PRRELEASE] PRIMARY KEY NONCLUSTERED ([Target] ASC)
);

