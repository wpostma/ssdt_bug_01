﻿CREATE TABLE [dbo].[Shortcut] (
    [InternalUserID]     BIGINT       NULL,
    [InternalShortcutID] BIGINT       NOT NULL,
    [ShortcutKey]        INT          NOT NULL,
    [ShortcutText]       VARCHAR (64) NOT NULL,
    [ShortcutKeyOrder]   INT          CONSTRAINT [DF_Shortcut_ShortcutKeyOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [FK_Shortcut_ShortcutAction] FOREIGN KEY ([InternalShortcutID]) REFERENCES [code].[ShortcutAction] ([InternalShortcutID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Shortcut_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Shortcut_User_Shortcut]
    ON [dbo].[Shortcut]([InternalUserID] ASC, [InternalShortcutID] ASC);

