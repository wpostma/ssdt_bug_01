﻿CREATE TABLE [dbo].[PatientLinked] (
    [EntryID]           INT            IDENTITY (1, 1) NOT NULL,
    [InternalPatientID] BIGINT         NOT NULL,
    [LinkID]            INT            NOT NULL,
    [ExtJson]           NVARCHAR (MAX) NULL,
    [LinkageTypeID]     INT            NULL,
    CONSTRAINT [PK_PatientLinked] PRIMARY KEY CLUSTERED ([EntryID] ASC),
    CONSTRAINT [CK_PatientLinked_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_PatientLinked_LinkageType] FOREIGN KEY ([LinkageTypeID]) REFERENCES [code].[LinkageType] ([LinkageTypeID]),
    CONSTRAINT [FK_PatientLinked_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [U_PatientLinked_Patient] UNIQUE NONCLUSTERED ([InternalPatientID] ASC)
);


GO

	CREATE TRIGGER [dbo].[PatientLinked_AD_CleanLinkID]
	   ON  [dbo].[PatientLinked]
	   AFTER DELETE 
	AS 
	BEGIN

   		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientLinked_AD_CleanLinkID -  STATRT', 'EntryID', (select top 1 entryID from deleted));

		DECLARE @LinkID int;
		DECLARE Deleted_Cursor CURSOR LOCAL FOR
		   SELECT DISTINCT LinkID FROM deleted;

		OPEN Deleted_Cursor;
		FETCH NEXT FROM Deleted_Cursor INTO @LinkID;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
			insert into fhir.LogTable(logitem, logint) values('PatientLinked_AD_CleanLinkID - deleted @LinkID', @LinkID);

			IF ((SELECT COUNT(*) FROM dbo.PatientLinked WHERE LinkID = @LinkID) = 1)
			BEGIN
				-- REMOVE isolated LinkID
				DELETE FROM dbo.PatientLinked WHERE LinkID = @LinkID; 
			END

			FETCH NEXT FROM Deleted_Cursor INTO @LinkID;
		END
		CLOSE Deleted_Cursor;

   		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientLinked_AD_CleanLinkID -  End', 'EntryID', (select top 1 entryID from deleted));
	END
