﻿CREATE TABLE [dbo].[Client] (
    [InternalClientID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [ClientName]       NVARCHAR (64)  NOT NULL,
    [ExtJson]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED ([InternalClientID] ASC)
);


GO
CREATE TRIGGER [dbo].[trClientDel]
   ON  [dbo].[Client]
   AFTER DELETE
AS 
BEGIN
   DELETE s FROM mem.Session s
   INNER JOIN deleted d ON d.InternalClientID=s.InternalClientID;
END
