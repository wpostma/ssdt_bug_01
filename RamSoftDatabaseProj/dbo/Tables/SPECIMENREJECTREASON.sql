﻿CREATE TABLE [dbo].[SPECIMENREJECTREASON] (
    [ID]   INT             NOT NULL,
    [CODE] NVARCHAR (128)  NOT NULL,
    [NAME] NVARCHAR (1000) NOT NULL,
    CONSTRAINT [SPECIMENREJECTREASON_PK_SPECIMENREJECTREASON] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [SPECIMENREJECTREASON_UK_SPECIMENREJECTREASON]
    ON [dbo].[SPECIMENREJECTREASON]([CODE] ASC);

