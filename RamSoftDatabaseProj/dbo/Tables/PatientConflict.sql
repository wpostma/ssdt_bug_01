﻿CREATE TABLE [dbo].[PatientConflict] (
    [InternalPatientID]            BIGINT NOT NULL,
    [ConflictingInternalPatientID] BIGINT NOT NULL,
    [IsPatientIDChanged]           BIT    CONSTRAINT [DF__CONFLICTT__PATIE__4959E263] DEFAULT ((0)) NOT NULL,
    [ConflictType]                 INT    NULL,
    CONSTRAINT [PK_ConflictPatient] PRIMARY KEY CLUSTERED ([InternalPatientID] ASC, [ConflictingInternalPatientID] ASC),
    CONSTRAINT [FK_ConflictPatient_ConflictingPatient] FOREIGN KEY ([ConflictingInternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]),
    CONSTRAINT [FK_ConflictPatient_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]) ON DELETE CASCADE ON UPDATE CASCADE
);

