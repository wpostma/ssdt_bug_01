﻿CREATE TABLE [dbo].[QueryModel] (
    [InternalQueryModelID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [QueryModelName]       NVARCHAR (64)  NOT NULL,
    [ExtJson]              NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_QueryModel] PRIMARY KEY CLUSTERED ([InternalQueryModelID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_QueryModel_Name]
    ON [dbo].[QueryModel]([QueryModelName] ASC);

