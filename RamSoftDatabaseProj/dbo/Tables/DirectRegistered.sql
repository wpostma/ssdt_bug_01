﻿CREATE TABLE [dbo].[DirectRegistered] (
    [DirectAddress]           NVARCHAR (128) NOT NULL,
    [InternalUserID]          BIGINT         NULL,
    [InternalFacilityID]      BIGINT         NOT NULL,
    [InternalDirectAddressID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [IsActive]                BIT            CONSTRAINT [DF_DirectRegistered_IsActive] DEFAULT ((1)) NULL,
    [LastUpdateUTC]           DATETIME2 (7)  CONSTRAINT [DF_DirectRegistered_LastUpdateUTC] DEFAULT (sysutcdatetime()) NULL,
    [ExtJson]                 NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_DirectRegistered] PRIMARY KEY NONCLUSTERED ([InternalDirectAddressID] ASC),
    CONSTRAINT [CK_DirectRegistered_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_DirectRegistered_Facility] FOREIGN KEY ([InternalFacilityID]) REFERENCES [dbo].[Organization] ([InternalOrganizationID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_DirectRegistered_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID]) NOT FOR REPLICATION,
    CONSTRAINT [U_DirectRegistered_Address] UNIQUE NONCLUSTERED ([DirectAddress] ASC)
);


GO
ALTER TABLE [dbo].[DirectRegistered] NOCHECK CONSTRAINT [FK_DirectRegistered_User];

