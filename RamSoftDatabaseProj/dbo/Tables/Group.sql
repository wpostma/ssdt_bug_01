﻿CREATE TABLE [dbo].[Group] (
    [InternalGroupID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [GroupName]       NVARCHAR (64)  NOT NULL,
    [ExtJson]         NVARCHAR (MAX) NULL,
    [IsActive]        BIT            CONSTRAINT [DF_Group_IsActive] DEFAULT ((1)) NULL,
    CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED ([InternalGroupID] ASC),
    CONSTRAINT [GROUP_ExtJSON_VALID] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_Group_Name] UNIQUE NONCLUSTERED ([GroupName] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Group_GroupName]
    ON [dbo].[Group]([GroupName] ASC);

