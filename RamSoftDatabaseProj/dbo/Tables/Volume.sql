﻿CREATE TABLE [dbo].[Volume] (
    [InternalVolumeID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [VolumeName]       NVARCHAR (64)  NOT NULL,
    [BasePath]         NVARCHAR (512) NOT NULL,
    [ReservedSpace]    BIGINT         CONSTRAINT [DF_Volume_ReservedSpace] DEFAULT ((0)) NOT NULL,
    [IsActive]         BIT            CONSTRAINT [DF_Volume_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Volume] PRIMARY KEY NONCLUSTERED ([InternalVolumeID] ASC),
    CONSTRAINT [CK_Volume_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [U_Volume_Name] UNIQUE NONCLUSTERED ([VolumeName] ASC)
);

