﻿CREATE TABLE [dbo].[StudyDiagnosisMap] (
    [EntryID]                 BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalStudyID]         BIGINT         NOT NULL,
    [ExtJson]                 NVARCHAR (MAX) NULL,
    [InternalDiagnosisCodeID] BIGINT         NULL,
    CONSTRAINT [PK_StudyDiagnosisMap] PRIMARY KEY CLUSTERED ([EntryID] ASC),
    CONSTRAINT [CK_StudyDiagnosisMap_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_StudyDiagnosisMap_Diagnosis] FOREIGN KEY ([InternalDiagnosisCodeID]) REFERENCES [code].[Diagnosis] ([InternalDiagnosisCodeID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_StudyDiagnosisMap_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [U_StudyDiagnosisMap] UNIQUE NONCLUSTERED ([InternalStudyID] ASC, [InternalDiagnosisCodeID] ASC)
);

