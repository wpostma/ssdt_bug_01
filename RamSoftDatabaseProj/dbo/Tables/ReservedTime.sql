﻿CREATE TABLE [dbo].[ReservedTime] (
    [InternalReservedTimeID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalStudyID]        BIGINT         NULL,
    [LastUpdateUserID]       BIGINT         NOT NULL,
    [LastUpdateDateTime]     DATETIME2 (7)  CONSTRAINT [DF_RESERVEDTIME_LastUpdateDateTime] DEFAULT (sysutcdatetime()) NOT NULL,
    [StartDate]              DATE           NOT NULL,
    [EndDate]                DATE           NULL,
    [StartTime]              TIME (7)       NULL,
    [EndTime]                TIME (7)       NULL,
    [IsActive]               BIT            CONSTRAINT [DF_RESERVEDTIME_IsActive] DEFAULT ((1)) NULL,
    [IsAppointmentAllowed]   BIT            NULL,
    [Display]                NVARCHAR (MAX) NOT NULL,
    [Recurrence]             NVARCHAR (MAX) NULL,
    [ExtJson]                NVARCHAR (MAX) NULL,
    [Claimed]                DATETIME2 (7)  NULL,
    [HL7Json]                NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_RESERVEDTIME_IDENTITY_NONCLUSTERED] PRIMARY KEY NONCLUSTERED ([InternalReservedTimeID] ASC),
    CONSTRAINT [CK_ReservedTime_HL7Json_Valid] CHECK (isjson([HL7Json])>(0)),
    CONSTRAINT [FK_RESERVEDTIME_InternalStudyID] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE NOT FOR REPLICATION,
    CONSTRAINT [FK_RESERVEDTIME_User] FOREIGN KEY ([LastUpdateUserID]) REFERENCES [dbo].[User] ([InternalUserID]) NOT FOR REPLICATION
);


GO
ALTER TABLE [dbo].[ReservedTime] NOCHECK CONSTRAINT [FK_RESERVEDTIME_User];


GO
CREATE CLUSTERED INDEX [IDX_RESERVED_TIME_BY_STARTDATE_CLUSTERED]
    ON [dbo].[ReservedTime]([StartDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_RESERVEDTIME_SCHEDULERQUERY]
    ON [dbo].[ReservedTime]([InternalReservedTimeID] ASC, [StartDate] ASC, [EndDate] ASC, [InternalStudyID] ASC)
    INCLUDE([LastUpdateUserID], [LastUpdateDateTime], [StartTime], [EndTime], [IsActive], [IsAppointmentAllowed], [Display], [Recurrence], [ExtJson], [Claimed]);


GO
CREATE NONCLUSTERED INDEX [IDX_RESERVED_TIME_BY_ENDDATE]
    ON [dbo].[ReservedTime]([EndDate] ASC);


GO
CREATE STATISTICS [STAT_RESERVEDTIME_SCHEDULERQUERY_ENDDATE_ID]
    ON [dbo].[ReservedTime]([EndDate], [InternalReservedTimeID]);


GO
CREATE STATISTICS [STAT_RESERVEDTIME_SCHEDULERQUERY_STARTEND_DATES]
    ON [dbo].[ReservedTime]([StartDate], [EndDate]);


GO
CREATE STATISTICS [STAT_RESERVEDTIME_SCHEDULERQUERY_STARTEND_ID]
    ON [dbo].[ReservedTime]([InternalStudyID], [StartDate], [EndDate]);


GO
CREATE STATISTICS [STAT_RESERVEDTIME_SCHEDULERQUERY_START_STUDY]
    ON [dbo].[ReservedTime]([InternalReservedTimeID], [StartDate], [InternalStudyID]);


GO
CREATE STATISTICS [STAT_RESERVEDTIME_SCHEDULERQUERY_STARTENDRES]
    ON [dbo].[ReservedTime]([StartDate], [InternalReservedTimeID], [EndDate], [InternalStudyID]);

