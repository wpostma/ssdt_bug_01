﻿CREATE TABLE [dbo].[Resource] (
    [InternalResourceID]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalRoomID]      BIGINT         NOT NULL,
    [InternalModalityID]  NVARCHAR (8)   NULL,
    [Name]                NVARCHAR (64)  NOT NULL,
    [Overbooking]         BIT            NOT NULL,
    [CanBlockTime]        BIT            NOT NULL,
    [ExtJson]             NVARCHAR (MAX) NULL,
    [CanReserveTimeSlots] BIT            CONSTRAINT [DF_Resource_CanReserveTimeSlots] DEFAULT ((1)) NOT NULL,
    [IsActive]            BIT            NULL,
    CONSTRAINT [RESOURCES_PK_RESOURCES] PRIMARY KEY NONCLUSTERED ([InternalResourceID] ASC),
    CONSTRAINT [RESOURCE_ExtJSON_VALID] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [FK_Resource_Modality] FOREIGN KEY ([InternalModalityID]) REFERENCES [code].[Modality] ([ModalityCode]),
    CONSTRAINT [FK_Resource_Room] FOREIGN KEY ([InternalRoomID]) REFERENCES [dbo].[Room] ([InternalRoomID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [U_Resource_Name] UNIQUE NONCLUSTERED ([Name] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_RESOURCE_ID_NAME]
    ON [dbo].[Resource]([InternalResourceID] ASC, [Name] ASC);


GO
CREATE STATISTICS [STAT_RESOURCE_ID_NAME]
    ON [dbo].[Resource]([Name], [InternalResourceID]);

