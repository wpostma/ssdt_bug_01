﻿CREATE TABLE [dbo].[DBVersion] (
    [InternalDBId] INT           IDENTITY (1, 1) NOT NULL,
    [Major]        INT           NOT NULL,
    [Minor]        INT           NOT NULL,
    [Updated]      DATETIME2 (7) CONSTRAINT [DF__DBVersion__Updat__3A827E7E] DEFAULT (getdate()) NOT NULL,
    [User_id]      [sysname]     CONSTRAINT [DF__DBVersion__User___3B76A2B7] DEFAULT (suser_sname()) NOT NULL,
    [Description]  NVARCHAR (64) NOT NULL,
    CONSTRAINT [PK_Major] PRIMARY KEY CLUSTERED ([InternalDBId] ASC)
);

