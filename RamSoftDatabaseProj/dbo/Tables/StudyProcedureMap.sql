﻿CREATE TABLE [dbo].[StudyProcedureMap] (
    [InternalStudyProcedureMapID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalStudyID]             BIGINT         NOT NULL,
    [Quantity]                    SMALLINT       CONSTRAINT [DF_StudyProcedureMap_Quantity] DEFAULT ((1)) NOT NULL,
    [Modifier]                    NVARCHAR (8)   NULL,
    [AuthorizationNumber]         NVARCHAR (64)  NULL,
    [AuthorizationStartDate]      DATE           NULL,
    [AuthorizationEndDate]        DATE           NULL,
    [InternalProcedureCodeID]     INT            NOT NULL,
    [InternalProcedureStatusID]   INT            NULL,
    [ExtJson]                     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_StudyProcedureMap] PRIMARY KEY CLUSTERED ([InternalStudyProcedureMapID] ASC),
    CONSTRAINT [CK_StudyProcedureMap_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_StudyProcedureMap_ProcedureCode] FOREIGN KEY ([InternalProcedureCodeID]) REFERENCES [code].[ProcedureCode] ([InternalProcedureCodeID]),
    CONSTRAINT [FK_StudyProcedureMap_ProcedureStatus] FOREIGN KEY ([InternalProcedureStatusID]) REFERENCES [code].[ProcedureStatus] ([InternalProcedureStatusID]),
    CONSTRAINT [FK_StudyProcedureMap_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [U_StudyProcedureMap] UNIQUE NONCLUSTERED ([InternalStudyID] ASC, [InternalProcedureCodeID] ASC, [Modifier] ASC)
);

