﻿CREATE TABLE [dbo].[Department] (
    [InternalDepartmentID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalFacilityID]   BIGINT         NOT NULL,
    [Department]           NVARCHAR (64)  NOT NULL,
    [ExtJson]              NVARCHAR (MAX) NULL,
    [IsActive]             BIT            CONSTRAINT [DF_Department_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED ([InternalDepartmentID] ASC),
    CONSTRAINT [FK_Department_Facility] FOREIGN KEY ([InternalFacilityID]) REFERENCES [dbo].[Organization] ([InternalOrganizationID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Department_Name]
    ON [dbo].[Department]([InternalFacilityID] ASC, [Department] ASC);

