﻿CREATE TABLE [dbo].[MailBox] (
    [InternalMailBoxID]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [MailBoxName]        NVARCHAR (64)  NOT NULL,
    [Description]        NVARCHAR (256) NULL,
    [InternalFacilityID] BIGINT         NULL,
    [IsActive]           BIT            CONSTRAINT [DF_MailBox_IsActive] DEFAULT ((1)) NULL,
    [LastUpdateUTC]      DATETIME2 (7)  CONSTRAINT [DF_MailBox_LastUpdateUTC] DEFAULT (sysutcdatetime()) NULL,
    [ExtJson]            NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_MailBox] PRIMARY KEY NONCLUSTERED ([InternalMailBoxID] ASC),
    CONSTRAINT [CK_MailBox_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_MailBox_Facility] FOREIGN KEY ([InternalFacilityID]) REFERENCES [dbo].[Organization] ([InternalOrganizationID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [U_MailBox_Name] UNIQUE NONCLUSTERED ([MailBoxName] ASC)
);


GO
CREATE TRIGGER [dbo].[trMailbox_IUD] 
   ON  [dbo].[MailBox] 
   FOR INSERT, UPDATE, DELETE
AS 
BEGIN
   DECLARE @OldExtJSON nvarchar(max) = null;
   DECLARE @OldDirectAddressJSON nvarchar(max);
   DECLARE @NewExtJSON nvarchar(max) = null;
   DECLARE @NewDirectAddressJSON nvarchar(max);
   DECLARE @InternalMailboxID bigint;
   DECLARE @Mailbox nvarchar(64);
   DECLARE @DirectAddressID bigint;
   DECLARE @ExtJson nvarchar(max) = null;

   SET @OldExtJSON = (SELECT d.ExtJson FROM deleted d);
   SET @NewExtJSON = (SELECT i.ExtJson FROM inserted i);
   SET @InternalMailboxID = (SELECT i.InternalMailboxID FROM inserted i);
   SET @Mailbox = (SELECT i.MailBoxName FROM inserted i);

   IF (@OldExtJSON IS NOT NULL) AND (ISJSON(@OldExtJSON) > 0)
   BEGIN
      SET @OldDirectAddressJSON = JSON_QUERY(@OldExtJSON, '$.directAddresses');

	  IF (ISJSON(@OldDirectAddressJSON) > 0) 
	  BEGIN
		DECLARE DirectAddress_Cursor CURSOR LOCAL FOR
					SELECT DISTINCT CAST(JSON_VALUE([value], '$.id') as bigint) from openjson(@OldDirectAddressJSON); 

		OPEN DirectAddress_Cursor;
		FETCH NEXT FROM DirectAddress_Cursor INTO @DirectAddressID;
		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
			IF (@DirectAddressID > 0)
			BEGIN
				UPDATE DBO.DirectRegistered SET ExtJson = NULL WHERE InternalDirectAddressID = @DirectAddressID;
			END

			FETCH NEXT FROM DirectAddress_Cursor INTO @DirectAddressID;
		END
		CLOSE DirectAddress_Cursor;
		DEALLOCATE DirectAddress_Cursor; 
	  END
   END

   IF (@NewExtJSON IS NOT NULL) AND (ISJSON(@NewExtJSON) > 0)
   BEGIN
      SET @ExtJson = '{}';

	  SET @ExtJson = JSON_MODIFY(@ExtJson, '$.internalMailBoxID', @InternalMailboxID);
	  SET @ExtJson = JSON_MODIFY(@ExtJson, '$.mailBoxName', @Mailbox);

      SET @NewDirectAddressJSON = JSON_QUERY(@NewExtJSON, '$.directAddresses');

	  IF (ISJSON(@NewDirectAddressJSON) > 0) 
	  BEGIN
		DECLARE NewDirectAddress_Cursor CURSOR LOCAL FOR
					SELECT DISTINCT CAST(JSON_VALUE([value], '$.id') as bigint) from openjson(@NewDirectAddressJSON); 

		OPEN NewDirectAddress_Cursor;
		FETCH NEXT FROM NewDirectAddress_Cursor INTO @DirectAddressID;
		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
			IF (@DirectAddressID > 0)
			BEGIN
				UPDATE DBO.DirectRegistered SET ExtJson = @ExtJson WHERE InternalDirectAddressID = @DirectAddressID;
			END

			FETCH NEXT FROM NewDirectAddress_Cursor INTO @DirectAddressID;
		END
		CLOSE NewDirectAddress_Cursor;
		DEALLOCATE NewDirectAddress_Cursor; 
	  END
   END
   
END
