﻿CREATE TABLE [dbo].[UserTemp] (
    [InternalUserID]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalGroupID] BIGINT         NOT NULL,
    [InternalRoleID]  BIGINT         NULL,
    [UserName]        NVARCHAR (64)  NOT NULL,
    [Name]            NVARCHAR (64)  NULL,
    [EmailAddress]    NVARCHAR (128) NULL,
    [ID]              NVARCHAR (16)  NULL,
    [PhoneNumber]     NVARCHAR (64)  NULL,
    [CellPhone]       NVARCHAR (64)  NULL,
    [PagerNumber]     NVARCHAR (64)  NULL,
    [FaxNumber]       NVARCHAR (64)  NULL,
    [Address]         NVARCHAR (256) NULL,
    [City]            NVARCHAR (24)  NULL,
    [State]           NVARCHAR (16)  NULL,
    [ZipCode]         NVARCHAR (16)  NULL,
    [CountryCode]     CHAR (2)       NULL,
    [IsActive]        BIT            NOT NULL,
    [ExtJson]         NVARCHAR (MAX) NULL,
    [PassWord]        NVARCHAR (64)  NULL,
    CONSTRAINT [PK__UserTemp__71A3910D6D1B12AF] PRIMARY KEY CLUSTERED ([InternalUserID] ASC)
);

