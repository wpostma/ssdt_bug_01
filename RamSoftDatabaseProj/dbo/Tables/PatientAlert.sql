﻿CREATE TABLE [dbo].[PatientAlert] (
    [InternalPatientAlertID] INT            IDENTITY (1, 1) NOT NULL,
    [InternalPatientID]      BIGINT         NOT NULL,
    [InternalUserID]         BIGINT         NOT NULL,
    [StartDate]              DATE           NULL,
    [EndDate]                DATE           NULL,
    [TimeCreated]            DATETIME2 (7)  CONSTRAINT [DF_PatientAlert_TimeCreated] DEFAULT (sysdatetime()) NOT NULL,
    [IsActive]               BIT            CONSTRAINT [DF_PatientAlert_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]                NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_PatientAlert] PRIMARY KEY CLUSTERED ([InternalPatientAlertID] ASC),
    CONSTRAINT [CK_PatientAlert_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_PatientAlert_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_PatientAlert_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE CASCADE ON UPDATE CASCADE
);

