﻿CREATE TABLE [dbo].[StudyTypeDetail] (
    [InternalStudyTypeDetailID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalStudyTypeID]       BIGINT         NOT NULL,
    [InternalProcedureCodeID]   INT            NOT NULL,
    [CombinedModifier]          NVARCHAR (8)   NULL,
    [Quantity]                  INT            CONSTRAINT [DF_StudyTypeDetail_Quantity] DEFAULT ((0)) NOT NULL,
    [IsActive]                  BIT            CONSTRAINT [DF_StudyTypeDetail_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]                   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_StudyTypeDetail] PRIMARY KEY NONCLUSTERED ([InternalStudyTypeDetailID] ASC),
    CONSTRAINT [CK_StudyTypeDetail_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [FK_StudyType_Procedure] FOREIGN KEY ([InternalProcedureCodeID]) REFERENCES [code].[ProcedureCode] ([InternalProcedureCodeID]),
    CONSTRAINT [FK_StudyTypeDetail_StudyType] FOREIGN KEY ([InternalStudyTypeID]) REFERENCES [dbo].[StudyType] ([InternalStudyTypeID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [U_StudyTypeDetail_Proc] UNIQUE NONCLUSTERED ([InternalStudyTypeID] ASC, [InternalProcedureCodeID] ASC, [CombinedModifier] ASC)
);

