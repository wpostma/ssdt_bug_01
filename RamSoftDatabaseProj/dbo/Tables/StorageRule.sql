﻿CREATE TABLE [dbo].[StorageRule] (
    [RuleID]             BIGINT         NOT NULL,
    [LastUpdateUserID]   BIGINT         NOT NULL,
    [LastUpdateDateTime] DATETIME2 (7)  CONSTRAINT [DF_StorageRule_LastUpdateDateTime] DEFAULT (sysutcdatetime()) NOT NULL,
    [ExtJson]            NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [COMPRESSIONRULE_PK_COMPRESSIONRULE] PRIMARY KEY NONCLUSTERED ([RuleID] ASC),
    CONSTRAINT [FK_StorageRule_User] FOREIGN KEY ([LastUpdateUserID]) REFERENCES [dbo].[User] ([InternalUserID]) NOT FOR REPLICATION
);


GO
ALTER TABLE [dbo].[StorageRule] NOCHECK CONSTRAINT [FK_StorageRule_User];

