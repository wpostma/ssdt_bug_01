﻿CREATE TABLE [dbo].[Room] (
    [InternalRoomID]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalDepartmentID] BIGINT         NOT NULL,
    [Name]                 NVARCHAR (64)  NOT NULL,
    [ExtJson]              NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED ([InternalRoomID] ASC),
    CONSTRAINT [FK_Room_Department] FOREIGN KEY ([InternalDepartmentID]) REFERENCES [dbo].[Department] ([InternalDepartmentID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [UK_Room] UNIQUE NONCLUSTERED ([InternalDepartmentID] ASC, [Name] ASC)
);

