﻿CREATE TABLE [dbo].[Integration] (
    [IntegrationID]   INT            NOT NULL,
    [IntegrationName] NVARCHAR (64)  NOT NULL,
    [ExtJson]         NVARCHAR (MAX) NULL,
    CONSTRAINT [PK__Integration] PRIMARY KEY CLUSTERED ([IntegrationID] ASC),
    CONSTRAINT [CK_Integration_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [UQ__IntegrationName] UNIQUE NONCLUSTERED ([IntegrationName] ASC)
);

