﻿CREATE TABLE [dbo].[ReservedTimeResource] (
    [InternalReservedTimeResourceID] BIGINT IDENTITY (1, 1) NOT NULL,
    [InternalReservedTimeID]         BIGINT NOT NULL,
    [InternalResourceID]             BIGINT NOT NULL,
    CONSTRAINT [FK_ReservedTimeResource_ReservedTimeID] FOREIGN KEY ([InternalReservedTimeID]) REFERENCES [dbo].[ReservedTime] ([InternalReservedTimeID]) ON DELETE CASCADE ON UPDATE CASCADE NOT FOR REPLICATION,
    CONSTRAINT [FK_ReservedTimeResource_ResourceID] FOREIGN KEY ([InternalResourceID]) REFERENCES [dbo].[Resource] ([InternalResourceID]) NOT FOR REPLICATION
);

