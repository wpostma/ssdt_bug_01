﻿CREATE TABLE [dbo].[MousePreset] (
    [MousePresetID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [PresetName]    VARCHAR (64)   NOT NULL,
    [ExtJson]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_MousePreset] PRIMARY KEY CLUSTERED ([MousePresetID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_MouserPreset_Name]
    ON [dbo].[MousePreset]([PresetName] ASC);

