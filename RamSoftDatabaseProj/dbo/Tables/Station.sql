﻿CREATE TABLE [dbo].[Station] (
    [InternalStationID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [StationGUID]       UNIQUEIDENTIFIER CONSTRAINT [DF_Station_StationGUID] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [StationName]       NVARCHAR (64)    NOT NULL,
    [ExtJson]           NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_Station] PRIMARY KEY CLUSTERED ([InternalStationID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Station_Name]
    ON [dbo].[Station]([StationName] ASC);

