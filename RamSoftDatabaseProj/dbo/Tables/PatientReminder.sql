﻿CREATE TABLE [dbo].[PatientReminder] (
    [InternalPatientReminderID]    INT            IDENTITY (1, 1) NOT NULL,
    [InternalPatientID]            BIGINT         NOT NULL,
    [InternalUserID]               BIGINT         NOT NULL,
    [InternalTemplateID]           BIGINT         NOT NULL,
    [StartDate]                    DATE           NULL,
    [EndDate]                      DATE           NULL,
    [TimeLastSent]                 DATETIME2 (7)  NULL,
    [TimeNext]                     DATETIME2 (7)  NULL,
    [FrequencyInDays]              INT            NULL,
    [IsActive]                     BIT            CONSTRAINT [DF_PatientReminder_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]                      NVARCHAR (MAX) NULL,
    [CommunicationRequestStatusID] INT            NULL,
    [ContactMethodID]              INT            NULL,
    CONSTRAINT [PK_PatientReminder] PRIMARY KEY CLUSTERED ([InternalPatientReminderID] ASC),
    CONSTRAINT [CK_PatientReminder_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_PatientReminder_ContactMethod] FOREIGN KEY ([ContactMethodID]) REFERENCES [code].[ContactMethod] ([ContactMethodID]),
    CONSTRAINT [FK_PatientReminder_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_PatientReminder_Status] FOREIGN KEY ([CommunicationRequestStatusID]) REFERENCES [code].[CommunicationRequestStatus] ([CommunicationRequestStatusID]),
    CONSTRAINT [FK_PatientReminder_Template] FOREIGN KEY ([InternalTemplateID]) REFERENCES [dbo].[PatientReminderTemplate] ([TemplateID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_PatientReminder_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE TRIGGER [dbo].[PatientReminder_AIU_TimeNext]
	   ON  [dbo].[PatientReminder]
	   FOR INSERT, UPDATE 
	AS 
	BEGIN
		UPDATE [dbo].[PatientReminder]
		SET TimeNext = (CASE WHEN i.TimeLastSent IS NULL THEN DATEADD(day, i.FrequencyInDays, i.StartDate) ELSE DATEADD(day, i.FrequencyInDays, i.TimeLastSent) END)
		FROM Inserted i
		WHERE i.InternalPatientReminderID = [dbo].[PatientReminder].InternalPatientReminderID;
	END
