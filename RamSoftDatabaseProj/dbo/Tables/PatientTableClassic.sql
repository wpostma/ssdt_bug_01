﻿CREATE TABLE [dbo].[PatientTableClassic] (
    [PatientID]              NVARCHAR (128)  NOT NULL,
    [PatientNameDicom]       NVARCHAR (250)  NOT NULL,
    [EmployersJS]            NVARCHAR (4000) NULL,
    [RecordTimestamp]        ROWVERSION      NOT NULL,
    [UpdatedDate]            DATETIME        NOT NULL,
    [Sex]                    CHAR (1)        NULL,
    [AddressesJS]            NVARCHAR (4000) NULL,
    [AccountInfoJS]          NVARCHAR (4000) NULL,
    [EmergencyContactInfoJS] NVARCHAR (4000) NULL,
    [ExtendJS]               NVARCHAR (4000) NULL,
    [StudyListJS]            NVARCHAR (4000) NULL,
    [ContactInfoJS]          NVARCHAR (4000) NULL,
    [PatientVersion]         INT             NOT NULL,
    [PatientEmail]           NVARCHAR (250)  NULL
);

