﻿CREATE TABLE [dbo].[StudyType] (
    [InternalStudyTypeID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [StudyType]           NVARCHAR (16)  NOT NULL,
    [Description]         NVARCHAR (64)  NOT NULL,
    [IsActive]            BIT            CONSTRAINT [DF_StudyType_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]             NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_STUDYTYPES] PRIMARY KEY NONCLUSTERED ([InternalStudyTypeID] ASC),
    CONSTRAINT [CK_StudyType_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_StudyType_Name] UNIQUE NONCLUSTERED ([StudyType] ASC)
);

