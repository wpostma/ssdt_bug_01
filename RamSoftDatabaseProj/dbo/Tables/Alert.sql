﻿CREATE TABLE [dbo].[Alert] (
    [InternalAlertID]    BIGINT   IDENTITY (1, 1) NOT NULL,
    [InternalGroupID]    BIGINT   NULL,
    [InternalRoleID]     BIGINT   NULL,
    [InternalFacilityID] BIGINT   NULL,
    [MinPriorityValue]   SMALLINT NULL,
    [MaxPriorityValue]   SMALLINT NULL,
    [MinStatusValue]     SMALLINT NULL,
    [MaxStatusValue]     SMALLINT NULL,
    [TimeThreshold]      INT      CONSTRAINT [DF_Alert_TimeThreshold] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Alert] PRIMARY KEY CLUSTERED ([InternalAlertID] ASC),
    CONSTRAINT [FK_Alert_Facility] FOREIGN KEY ([InternalFacilityID]) REFERENCES [dbo].[Organization] ([InternalOrganizationID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Alert_Group] FOREIGN KEY ([InternalGroupID]) REFERENCES [dbo].[Group] ([InternalGroupID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Alert_Role] FOREIGN KEY ([InternalRoleID]) REFERENCES [dbo].[Role] ([InternalRoleID]) ON DELETE CASCADE ON UPDATE CASCADE
);

