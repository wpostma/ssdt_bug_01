﻿CREATE TABLE [dbo].[Constraint] (
    [ConstraintID]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [ConstraintName] NVARCHAR (64)  NOT NULL,
    [ExtJson]        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Constraint] PRIMARY KEY CLUSTERED ([ConstraintID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Constraint_Name]
    ON [dbo].[Constraint]([ConstraintName] ASC);

