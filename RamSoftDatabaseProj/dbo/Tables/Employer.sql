﻿CREATE TABLE [dbo].[Employer] (
    [InternalEmployerID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [IsActive]           BIT            CONSTRAINT [DF_EMPLOYER_Active] DEFAULT ((1)) NOT NULL,
    [EmployerName]       NVARCHAR (64)  NOT NULL,
    [Address]            NVARCHAR (256) NOT NULL,
    [City]               NVARCHAR (64)  NULL,
    [State]              NVARCHAR (16)  NULL,
    [ZipCode]            NVARCHAR (16)  NULL,
    [CountryCode]        CHAR (2)       NULL,
    [ExtJson]            NVARCHAR (MAX) NULL,
    [Timestamp]          DATETIME2 (7)  CONSTRAINT [DF_Employer_Timestamp] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Employer] PRIMARY KEY CLUSTERED ([InternalEmployerID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Employer_Name]
    ON [dbo].[Employer]([EmployerName] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_EMPLOYER_NAME_ADDRESS_UNIQUE]
    ON [dbo].[Employer]([EmployerName] ASC, [Address] ASC);

