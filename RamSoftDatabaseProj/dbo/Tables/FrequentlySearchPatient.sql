﻿CREATE TABLE [dbo].[FrequentlySearchPatient] (
    [ID]                BIGINT IDENTITY (1, 1) NOT NULL,
    [InternalPatientID] BIGINT NOT NULL,
    [InternalUserID]    BIGINT NOT NULL,
    [Frequently_Used]   BIGINT NOT NULL,
    CONSTRAINT [PK_FrequentlySearchPatient] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_FrequentlySearchPatient_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]),
    CONSTRAINT [FK_FrequentlySearchPatient_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID])
);

