﻿CREATE TABLE [dbo].[UserFacilityMap] (
    [InternalFacilityID] BIGINT         NOT NULL,
    [InternalUserID]     BIGINT         NOT NULL,
    [FacilityUserID]     NVARCHAR (64)  NULL,
    [ExtJson]            NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_UserFacilityMap] PRIMARY KEY CLUSTERED ([InternalFacilityID] ASC, [InternalUserID] ASC),
    CONSTRAINT [CK_UserFacilityMap_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [FK_UserFacilityMap_Facility] FOREIGN KEY ([InternalFacilityID]) REFERENCES [dbo].[Organization] ([InternalOrganizationID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_UserFacilityMap_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_UserFacilityMap]
    ON [dbo].[UserFacilityMap]([InternalFacilityID] ASC, [InternalUserID] ASC);

