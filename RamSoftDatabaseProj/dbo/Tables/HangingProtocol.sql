﻿CREATE TABLE [dbo].[HangingProtocol] (
    [HangingProtocolID]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalUserID]     BIGINT         NULL,
    [ProtocolName]       NVARCHAR (64)  NOT NULL,
    [ModalityCode]       NVARCHAR (8)   NULL,
    [InternalBodyPartID] BIGINT         NULL,
    [IsPublished]        BIT            CONSTRAINT [DF__HANGINGPR__PUBLI__7073AF84] DEFAULT ((0)) NOT NULL,
    [IsActive]           BIT            CONSTRAINT [DF__HANGINGPR__HPACT__7167D3BD] DEFAULT ((1)) NOT NULL,
    [ExtJson]            NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_HangingProtocol] PRIMARY KEY CLUSTERED ([HangingProtocolID] ASC),
    CONSTRAINT [FK_HangingProtocol_BodyPart] FOREIGN KEY ([InternalBodyPartID]) REFERENCES [code].[BodyPart] ([InternalBodyPartID]),
    CONSTRAINT [FK_HangingProtocol_Modality] FOREIGN KEY ([ModalityCode]) REFERENCES [code].[Modality] ([ModalityCode]),
    CONSTRAINT [FK_HangingProtocol_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_HangingProtocol_Name]
    ON [dbo].[HangingProtocol]([InternalUserID] ASC, [ProtocolName] ASC);

