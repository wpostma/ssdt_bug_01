﻿CREATE TABLE [dbo].[StudyGroup] (
    [InternalStudyID]        BIGINT NOT NULL,
    [PrimaryInternalStudyID] BIGINT NOT NULL,
    CONSTRAINT [PK_StudyGroup] PRIMARY KEY CLUSTERED ([InternalStudyID] ASC, [PrimaryInternalStudyID] ASC),
    CONSTRAINT [FK_StudyGroup_PrimaryStudy] FOREIGN KEY ([PrimaryInternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]),
    CONSTRAINT [FK_StudyGroup_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE
);

