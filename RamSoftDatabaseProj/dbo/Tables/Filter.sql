﻿CREATE TABLE [dbo].[Filter] (
    [InternalFilterID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [ModalityCode]     NVARCHAR (8)   NULL,
    [Name]             NVARCHAR (64)  NOT NULL,
    [ExtJson]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Filter] PRIMARY KEY CLUSTERED ([InternalFilterID] ASC),
    CONSTRAINT [FK_Filter_Modality] FOREIGN KEY ([ModalityCode]) REFERENCES [code].[Modality] ([ModalityCode]) ON DELETE CASCADE ON UPDATE CASCADE
);

