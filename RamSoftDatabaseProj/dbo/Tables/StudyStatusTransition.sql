﻿CREATE TABLE [dbo].[StudyStatusTransition] (
    [InternalStudyID]   BIGINT         NOT NULL,
    [StatusValue]       SMALLINT       NOT NULL,
    [TransitionTimeUTC] DATETIME2 (7)  NOT NULL,
    [IsActive]          BIT            CONSTRAINT [DF_StudyStatusTransition_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_StudyStatusTransition] PRIMARY KEY CLUSTERED ([InternalStudyID] ASC, [StatusValue] ASC),
    CONSTRAINT [CK_StudyStatusTransition_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [FK_StudyStatusTransition_Status] FOREIGN KEY ([StatusValue]) REFERENCES [code].[Status] ([StatusValue]),
    CONSTRAINT [FK_StudyStatusTransition_Study] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE
);

