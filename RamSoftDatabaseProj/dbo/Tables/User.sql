﻿CREATE TABLE [dbo].[User] (
    [InternalUserID]            BIGINT          IDENTITY (1, 1) NOT NULL,
    [InternalGroupID]           BIGINT          NOT NULL,
    [InternalRoleID]            BIGINT          NULL,
    [UserName]                  NVARCHAR (64)   NOT NULL,
    [Name]                      NVARCHAR (64)   NULL,
    [ID]                        NVARCHAR (16)   NULL,
    [IsActive]                  BIT             CONSTRAINT [DF_User_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]                   NVARCHAR (MAX)  NULL,
    [Password]                  NVARCHAR (64)   NULL,
    [SexID]                     INT             NULL,
    [BirthDate]                 DATETIME        NULL,
    [PhysicianSpecialtyID]      BIGINT          NULL,
    [LastUpdateUTC]             DATETIME2 (7)   CONSTRAINT [DF_User_LastupdateUTC] DEFAULT (sysutcdatetime()) NULL,
    [LastupdateUserID]          BIGINT          NULL,
    [Signature]                 VARBINARY (MAX) NULL,
    [UseExternalAuthentication] BIT             CONSTRAINT [DF_User_UseExternalAuthentication] DEFAULT ((0)) NULL,
    [LastActiveUTC]             DATETIME2 (7)   NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([InternalUserID] ASC),
    CONSTRAINT [CK_User_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [FK_User_Group] FOREIGN KEY ([InternalGroupID]) REFERENCES [dbo].[Group] ([InternalGroupID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_User_PhysicianSpecialty] FOREIGN KEY ([PhysicianSpecialtyID]) REFERENCES [code].[PhysicianSpecialty] ([PhysicianSpecialtyID]),
    CONSTRAINT [FK_User_Role] FOREIGN KEY ([InternalRoleID]) REFERENCES [dbo].[Role] ([InternalRoleID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_User_Sex] FOREIGN KEY ([SexID]) REFERENCES [code].[Sex] ([SexID]),
    CONSTRAINT [U_User_UserName] UNIQUE NONCLUSTERED ([UserName] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_User_UserName]
    ON [dbo].[User]([UserName] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_User_Name]
    ON [dbo].[User]([Name] ASC);


GO
CREATE TRIGGER [dbo].[User_AD_CleanUP]
	   ON  [dbo].[User]
	   AFTER DELETE
	AS 
	BEGIN
	   DELETE s FROM mem.Session s
	   INNER JOIN deleted d ON d.InternalUserID=s.InternalUserID;
	   DELETE w FROM dbo.Worklist w
	   INNER JOIN deleted d ON d.InternalUserID=w.InternalUserID;
	END
