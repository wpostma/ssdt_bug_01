﻿CREATE TABLE [dbo].[FeeSchedule] (
    [FeeScheduleID]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [FeeScheduleName] NVARCHAR (64)  NOT NULL,
    [FeeScheduleType] NVARCHAR (16)  NOT NULL,
    [IsActive]        BIT            CONSTRAINT [DF_FeeSchedule_IsActive] DEFAULT ((1)) NOT NULL,
    [StartDate]       DATE           NULL,
    [EndDate]         DATE           NULL,
    [ExtJson]         NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_FeeSchedule] PRIMARY KEY NONCLUSTERED ([FeeScheduleID] ASC),
    CONSTRAINT [CK_FeeSchedule_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_FeeSchedule] UNIQUE NONCLUSTERED ([FeeScheduleName] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [FEESCHEDULES_U_FEESCHEDULES_FEENAME]
    ON [dbo].[FeeSchedule]([FeeScheduleName] ASC);


GO
CREATE NONCLUSTERED INDEX [FEESCHEDULES_I_FEESCHEDULES_ISACTIVE]
    ON [dbo].[FeeSchedule]([IsActive] ASC);


GO
CREATE NONCLUSTERED INDEX [FEESCHEDULES_I_FEESCHEDULES_STARTDATE]
    ON [dbo].[FeeSchedule]([StartDate] ASC);


GO
CREATE NONCLUSTERED INDEX [FEESCHEDULES_I_FEESCHEDULES_ENDDATE]
    ON [dbo].[FeeSchedule]([EndDate] ASC);

