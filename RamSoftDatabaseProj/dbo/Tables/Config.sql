﻿CREATE TABLE [dbo].[Config] (
    [InternalUserID] BIGINT         NOT NULL,
    [Item]           NVARCHAR (256) NOT NULL,
    [ExtJson]        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Config] PRIMARY KEY CLUSTERED ([InternalUserID] ASC, [Item] ASC),
    CONSTRAINT [FK_Config_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [CONFIG_I_CONFIG_USERNAME]
    ON [dbo].[Config]([InternalUserID] ASC);

