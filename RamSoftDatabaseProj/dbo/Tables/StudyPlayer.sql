﻿CREATE TABLE [dbo].[StudyPlayer] (
    [InternalStudyPlayerID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalStudyID]       BIGINT         NOT NULL,
    [InternalFacilityID]    BIGINT         NULL,
    [LastupdateUTC]         DATETIME2 (7)  CONSTRAINT [DF_StudyPlayer_LastupdateUTC] DEFAULT (sysutcdatetime()) NULL,
    [StudyPlayerType]       INT            NOT NULL,
    [InternalUserID]        BIGINT         NULL,
    [Name]                  NVARCHAR (256) NULL,
    [AssignedTime]          DATETIME2 (7)  NULL,
    [IsActive]              BIT            CONSTRAINT [DF_StudyPlayer_IsActive] DEFAULT ((1)) NULL,
    [ExtJson]               NVARCHAR (MAX) NULL,
    [LastUpdateUserID]      BIGINT         NULL,
    CONSTRAINT [PK_StudyPlayer] PRIMARY KEY CLUSTERED ([InternalStudyPlayerID] ASC),
    CONSTRAINT [CK_StudyPlayer_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [FK_StudyPlayer_Facility] FOREIGN KEY ([InternalFacilityID]) REFERENCES [dbo].[Organization] ([InternalOrganizationID]),
    CONSTRAINT [FK_StudyPlayer_InternalStudyID] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_StudyPlayer_LastUpdateUserID] FOREIGN KEY ([LastUpdateUserID]) REFERENCES [dbo].[User] ([InternalUserID]),
    CONSTRAINT [FK_StudyPlayer_PlayerType] FOREIGN KEY ([StudyPlayerType]) REFERENCES [code].[StudyPlayerType] ([StudyPlayerType]),
    CONSTRAINT [FK_StudyPlayer_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_StudyPlayer_StudyAndPlayerType]
    ON [dbo].[StudyPlayer]([InternalStudyID] ASC, [StudyPlayerType] ASC) WHERE ([StudyPlayerType]<(5));

