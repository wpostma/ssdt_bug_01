﻿CREATE TABLE [dbo].[Template] (
    [InternalTemplateID]           BIGINT          IDENTITY (1, 1) NOT NULL,
    [InternalRoleID]               BIGINT          NULL,
    [InternalUserID]               BIGINT          NULL,
    [InternalAssigningAuthorityID] BIGINT          NULL,
    [InternalFacilityID]           BIGINT          NULL,
    [DocumentTypeID]               INT             NULL,
    [TemplateName]                 NVARCHAR (64)   NOT NULL,
    [IsDefault]                    BIT             CONSTRAINT [DF_Template_IsDefault] DEFAULT ((0)) NOT NULL,
    [FromStatusValue]              SMALLINT        NULL,
    [ToStatusValue]                SMALLINT        NULL,
    [TemplateBinary]               VARBINARY (MAX) NOT NULL,
    CONSTRAINT [PK_Template] PRIMARY KEY CLUSTERED ([InternalTemplateID] ASC),
    CONSTRAINT [FK_Template_AssigningAuthority] FOREIGN KEY ([InternalAssigningAuthorityID]) REFERENCES [dbo].[AssigningAuthority] ([InternalAssigningAuthorityID]),
    CONSTRAINT [FK_Template_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [code].[DocumentType] ([DocumentTypeID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Template_Facility] FOREIGN KEY ([InternalFacilityID]) REFERENCES [dbo].[Organization] ([InternalOrganizationID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Template_Role] FOREIGN KEY ([InternalRoleID]) REFERENCES [dbo].[Role] ([InternalRoleID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Template_User] FOREIGN KEY ([InternalUserID]) REFERENCES [dbo].[User] ([InternalUserID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Template_TemplateName]
    ON [dbo].[Template]([TemplateName] ASC, [InternalAssigningAuthorityID] ASC, [InternalFacilityID] ASC, [InternalRoleID] ASC, [InternalUserID] ASC);

