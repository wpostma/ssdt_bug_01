﻿CREATE TABLE [dbo].[FeeScheduleDetail] (
    [FeeScheduleDetailID]     BIGINT         IDENTITY (1, 1) NOT NULL,
    [FeeScheduleID]           BIGINT         NOT NULL,
    [InternalProcedureCodeID] INT            NULL,
    [CombinedModifier]        NVARCHAR (8)   NULL,
    [ModalityCode]            NVARCHAR (8)   NULL,
    [ProcedureAllowedAmount]  MONEY          CONSTRAINT [DF_FeeScheduleDetail_ProcedureAllowedAmount] DEFAULT ((0)) NULL,
    [ProcedureChargeAmount]   MONEY          CONSTRAINT [DF_FeeScheduleDetail_ProcedureChargeAmount] DEFAULT ((0)) NULL,
    [ModalityChargeAmount]    MONEY          CONSTRAINT [DF_FeeScheduleDetail_ModalityChargeAmount] DEFAULT ((0)) NULL,
    [IsActive]                BIT            CONSTRAINT [DF_FeeScheduleDetail_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]                 NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_FeeScheduleDetail] PRIMARY KEY NONCLUSTERED ([FeeScheduleDetailID] ASC),
    CONSTRAINT [CK_FeeScheduleDetail_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [FK_FeeScheduleDetail_FeeSchedule] FOREIGN KEY ([FeeScheduleID]) REFERENCES [dbo].[FeeSchedule] ([FeeScheduleID]),
    CONSTRAINT [FK_FeeScheduleDetail_Modality] FOREIGN KEY ([ModalityCode]) REFERENCES [code].[Modality] ([ModalityCode]),
    CONSTRAINT [FK_FeeScheduleDetail_Procedure] FOREIGN KEY ([InternalProcedureCodeID]) REFERENCES [code].[ProcedureCode] ([InternalProcedureCodeID]),
    CONSTRAINT [U_FeeScheduleDetail] UNIQUE NONCLUSTERED ([FeeScheduleID] ASC, [InternalProcedureCodeID] ASC, [CombinedModifier] ASC, [ModalityCode] ASC)
);

