﻿CREATE TABLE [dbo].[PatientReminderTemplate] (
    [TemplateID]                   BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalAssigningAuthorityID] BIGINT         NULL,
    [TemplateName]                 NVARCHAR (255) NOT NULL,
    [ExtJson]                      NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_PatientReminderTemplate] PRIMARY KEY CLUSTERED ([TemplateID] ASC),
    CONSTRAINT [CK_PatientReminderTemplate_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_PatientReminderTemplate_Issuer] FOREIGN KEY ([InternalAssigningAuthorityID]) REFERENCES [dbo].[AssigningAuthority] ([InternalAssigningAuthorityID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_PatientReminderTemplate_Name]
    ON [dbo].[PatientReminderTemplate]([InternalAssigningAuthorityID] ASC, [TemplateName] ASC);

