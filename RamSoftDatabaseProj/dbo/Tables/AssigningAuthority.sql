﻿CREATE TABLE [dbo].[AssigningAuthority] (
    [InternalAssigningAuthorityID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]                         NVARCHAR (64)  NOT NULL,
    [IsActive]                     BIT            CONSTRAINT [DF_AssigningAuthority_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]                      NVARCHAR (MAX) NULL,
    [IsExternal]                   BIT            CONSTRAINT [DF_AssigningAuthority_IsExternal] DEFAULT ((1)) NULL,
    [UniversalEntityID]            NVARCHAR (512) NULL,
    [UniversalEntityIDTypeID]      INT            NULL,
    CONSTRAINT [PK_Issuer] PRIMARY KEY CLUSTERED ([InternalAssigningAuthorityID] ASC),
    CONSTRAINT [CK_AssigningAuthority_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [FK_AssigningAuthority_UEIType] FOREIGN KEY ([UniversalEntityIDTypeID]) REFERENCES [code].[UniversalEntityIDType] ([UniversalEntityIDTypeID]),
    CONSTRAINT [U_AssigningAuthority_Name] UNIQUE NONCLUSTERED ([Name] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_AssigningAuthority_UEI]
    ON [dbo].[AssigningAuthority]([UniversalEntityID] ASC) WHERE ([UniversalEntityID] IS NOT NULL);

