﻿CREATE TABLE [dbo].[ApplicationVersion] (
    [InternalApplicationId] INT           IDENTITY (1, 1) NOT NULL,
    [Application]           NVARCHAR (64) NOT NULL,
    [Major]                 INT           NOT NULL,
    [MinorMin]              INT           NOT NULL,
    [MinorMax]              INT           NOT NULL,
    [Updated]               DATETIME2 (7) CONSTRAINT [DF__AppVersion_DATETIME_DEFAULT] DEFAULT (getdate()) NOT NULL,
    [User_id]               [sysname]     CONSTRAINT [DF__AppVersion_SYSNAME_DEFAULT] DEFAULT (suser_sname()) NOT NULL,
    [Description]           NVARCHAR (64) NOT NULL
);

