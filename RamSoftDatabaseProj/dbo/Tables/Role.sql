﻿CREATE TABLE [dbo].[Role] (
    [InternalRoleID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (64)  NOT NULL,
    [ExtJson]        NVARCHAR (MAX) NULL,
    [IsActive]       BIT            CONSTRAINT [DF_Role_IsActive] DEFAULT ((1)) NULL,
    CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED ([InternalRoleID] ASC),
    CONSTRAINT [ROLE_ExtJSON_VALID] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_Role_Name] UNIQUE NONCLUSTERED ([Name] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Role_Name]
    ON [dbo].[Role]([Name] ASC);

