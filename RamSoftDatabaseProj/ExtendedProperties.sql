﻿EXECUTE sp_addextendedproperty @name = N'SQLSourceControl Database Revision', @value = 55601;


GO
EXECUTE sp_addextendedproperty @name = N'SQLSourceControl Migration Scripts Location', @value = N'<?xml version="1.0" encoding="utf-16" standalone="yes"?>
<ISOCCompareLocation version="1" type="SvnLocation">
  <RepositoryUrl>svn://svn.ramsoft.biz/core/trunk/html5/powerserver-html5/sql2016/pacs7_migration/</RepositoryUrl>
</ISOCCompareLocation>';


GO
EXECUTE sp_addextendedproperty @name = N'SQLSourceControl Scripts Location', @value = N'<?xml version="1.0" encoding="utf-16" standalone="yes"?>
<ISOCCompareLocation version="1" type="SvnLocation">
  <RepositoryUrl>svn://svn.ramsoft.biz/core/trunk/html5/powerserver-html5/sql2016/pacs7/</RepositoryUrl>
</ISOCCompareLocation>';

