﻿CREATE VIEW [fhir].[Room_View]
AS
SELECT r.internalRoomID,
       (SELECT
			-- room fields
		   'Room' AS resourceType,
		   r.InternalRoomID AS id, 
		   r.[Name] as [name],	
		   JSON_VALUE(r.ExtJson, '$.phoneNumber') as phoneNumber,
		   JSON_VALUE(r.ExtJson, '$.faxNumber') as faxNumber,
		   JSON_QUERY(r.ExtJson, '$.autoFax') as autoFax,

		   -- department fields
		   d.InternalDepartmentID as 'department.id',
		   d.Department as 'department.name',
		   JSON_VALUE(d.ExtJson, '$.floor') AS 'department.floor',
		   (CASE d.IsActive WHEN 1 THEN 'true' ELSE 'false' END) AS 'department.isActive',

		   -- facility fields
		   d.InternalFacilityID as 'facility.id',
		   o.OrganizationName as 'facility.name'

		   FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS JsonText

FROM dbo.Room r 
JOIN dbo.Department d ON d.InternalDepartmentID = r.InternalDepartmentID
JOIN dbo.Organization o ON o.InternalOrganizationID = d.InternalFacilityID
