﻿CREATE View [fhir].[Encounter_View]
as
SELECT        e.InternalEncounterID, e.AdmissionID, CONVERT(nvarchar(max),
                             (SELECT        'Encounter' AS resourceType, e.InternalEncounterID AS id, 'false' AS isPrimary, est.EncounterStateCode AS status, est.EncounterStateName AS statusName, e.AdmissionID AS visitNumber, 
                                                         fhir.GetPatientJson(e.InternalPatientID) AS patient, ec.EncounterClassCode AS class, ec.EncounterClassName AS encounterClassName, i.InsuranceStatusCode AS insuranceStatusCode, 
                                                         i.InsuranceStatusName AS insuranceStatusName, a.AmbulatoryStatusCode AS ambulatoryStatusCode, a.AmbulatoryStatusName AS ambulatoryStatusName, JSON_VALUE(e.ExtJson, 
                                                         '$.patientLocation') AS patientLocation, JSON_VALUE(e.ExtJson, '$.description') AS description FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)) AS JsonText
FROM            PHI.Encounter e LEFT JOIN
                         code.EncounterClass ec ON ec.EncounterClassID = e.EncounterClassID LEFT JOIN
                         code.AmbulatoryStatus a ON a.AmbulatoryStatusID = e.AmbulatoryStatusID LEFT JOIN
                         code.InsuranceStatus i ON i.InsuranceStatusID = e.InsuranceStatusID LEFT JOIN
                         code.Confidentiality c ON c.ConfidentialityID = e.ConfidentialityID LEFT JOIN
                         code.EncounterState est ON est.EncounterStateID = e.EncounterStateID
