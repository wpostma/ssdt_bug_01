﻿
CREATE view [fhir].[DiagnosisCode_View]
as
SELECT        d.InternalDiagnosisCodeID,  
              CONVERT(nvarchar(max),
              (SELECT        
			    'DiagnosisCode' AS resourceType, 
				d.InternalDiagnosisCodeID AS id, 
				d.CodingScheme as [source],
				JSON_QUERY((SELECT d.DiagnosisCode as code, d.[Description] as display FOR JSON PATH), '$') as [code.coding],
				d.[Description] as [code.text],
				JSON_VALUE(d.ExtJson, '$.sex') as sex,
				JSON_VALUE(d.ExtJson, '$.alertMessage') as alertMessage,
                (CASE d.IsActive WHEN 1 THEN 'true' ELSE 'false' END)  as isActive, 
				(CASE d.IsCritical WHEN 1 THEN 'true' ELSE 'false' END)  as isCritical
				
                FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)) AS JsonText
FROM          code.Diagnosis d 




