﻿CREATE VIEW [fhir].[Patient_View]
	AS
	  SELECT p.InternalPatientID, 
		 -- Encrypted columns cannot be in Json text, so these will be combined in Web API (to be implemented)
		 p.SSN as SSN,
		 p.BirthDate as BirthDate,
		 p.PatientID as PatientID, 

		 -- Json Text
		 (SELECT
		  'Patient' as resourceType,
		   JSON_QUERY(p.ExtJson, '$.identifier') as 'identifier',
		   cast(p.LastupdateUTC AS datetimeoffset) AS 'meta.lastUpdated',
		   (CASE p.IsActive WHEN 1 THEN 'true' ELSE 'false' END)  as active,
		   cast(p.InternalPatientID as nvarchar(16)) AS id, 
		   CONCAT(p.PatientID, ' - ', p.PatientName) as [value],
		   p.PatientName as [display],
		   fhir.GetHumanNameJson(p.PatientName) as name, 
		   JSON_QUERY(p.ExtJson, '$.contactPoint') as telecom, 
		   sx.Sex as gender,
		   p.BirthDate as birthDate, 
		   (CASE p.isDeceased WHEN 1 THEN 'true' ELSE 'false' END)  as deceasedBoolean,
		   p.DeceasedDate as deceasedDateTime,
		   JSON_QUERY(p.ExtJson, '$.address') as address,
		   ms.MaritalStatusCode as 'maritalStatus.coding.code', 
		   ms.MaritalStatus as 'maritalStatus.coding.display', 
		   CONCAT('assigningAuthority/', p.InternalAssigningAuthorityID) as 'managingOrganization.reference',
		   p.InternalAssigningAuthorityID as 'managingOrganization.id',
		   aa.Name as 'managingOrganization.display',

		   --fields for only for RamSoft
		   p.PatientID as patientID, 
		   p.Accountnumber as accountNumber, 
		   ac.AccountStatus as accountStatus, 
		   JSON_QUERY(p.ExtJson, '$.directAddress')  as directAddress,
		   fc.FinancialClass as financialClass,
		   p.PatientBalance as patientBalance,
		   p.InsuranceBalance as insuranceBalance,
		   p.SSN as ssn, 
		   e.InternalEmployerID as 'employer.id',
		   e.EmployerName as 'employer.name',
		  (e.EmployerName + ' - ' + e.Address) as 'employer.display',

		   JSON_VALUE(p.ExtJson, '$.language') as "language",
		   JSON_VALUE(p.ExtJson, '$.employmentStatus') as employmentStatus,		   
		   JSON_VALUE(p.ExtJson, '$.motherMaidenName') as motherMaidenName, 
		   JSON_VALUE(p.ExtJson, '$.ethnicity') as ethnicity, 
		   JSON_VALUE(p.ExtJson, '$.race') as race, 
		   JSON_VALUE(p.ExtJson, '$.smokingStatus') as smokingStatus,  
		   JSON_VALUE(p.ExtJson, '$.smokingStartDate') as smokingStartDate,  
		   JSON_VALUE(p.ExtJson, '$.preferredContact') as preferredContact,
		   JSON_VALUE(p.ExtJson, '$.otherNumber') as otherNumber,
		   JSON_VALUE(p.ExtJson, '$.notes') as notes, 

		   JSON_QUERY(p.ExtJson, '$.allergies') as allergies, 
		   JSON_QUERY(p.ExtJson, '$.emergencyContact') as emergencyContact

		   FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS JsonText
	  FROM phi.Patient p
	  LEFT JOIN dbo.AssigningAuthority aa on aa.InternalAssigningAuthorityID = p.InternalAssigningAuthorityID
	  LEFT JOIN code.FinancialClass fc on fc.FinancialClassID = p.FinancialClassID
	  LEFT JOIN code.AccountStatus ac on ac.AccountStatusID = p.AccountStatusID
	  LEFT JOIN code.Relationship rl on rl.RelationshipID = p.GuarantorRelationshipID 
	  LEFT JOIN code.Sex sx on sx.SexID = p.SexID
	  LEFT JOIN code.MaritalStatus ms on ms.MaritalStatusID = p.MaritalStatusID
	  LEFT JOIN phi.Patient guar on guar.InternalPatientID = p.GuarantorInternalPatientID
	  LEFT JOIN dbo.Employer e on e.InternalEmployerID = p.InternalEmployerID
