﻿


CREATE VIEW [fhir].[DiagnosticOrder_View]
AS
SELECT        do.InternalDiagnosticOrderID, do.InternalEncounterID, do.InternalPatientID, do.IsActive,
				(SELECT 
					'DiagnosticOrder' AS resourceType, 
					JSON_QUERY(do.ExtJson, '$.identifier') AS identifier,
					CAST(do.LastUpdateUTC AS DATETIMEOFFSET) AS 'meta.lastUpdated',
					do.LastUpdateUserID AS 'meta.lastUpdatedUser',
					do.InternalDiagnosticOrderID AS id,
					pr.Name AS priority,
					pr.PriorityValue AS priorityValue,
					-- Patient Reference
					'fhir/Patient/' + CAST(p.InternalPatientID AS NVARCHAR(20)) AS 'subject.reference',
					p.InternalPatientID AS 'subject.id',
					CONCAT(p.PatientID,' - ',p.PatientName) AS 'subject.value',
					p.PatientName AS 'subject.display',				
					-- Encounter Reference
					'fhir/Encounter/' + CAST(do.InternalEncounterID AS NVARCHAR(20)) AS 'encounter.reference',
					do.InternalEncounterID AS 'encounter.id',										
					-- Imaging Facility
					'fhir/Organization/' + CAST(o.InternalOrganizationID AS NVARCHAR(20)) AS 'organization.reference',
					o.InternalOrganizationID AS 'organization.id',					
					o.OrganizationName AS 'organization.display',
					-- Referring physician
					'fhir/Practitioner/' + CAST(do.ReferringPhysicianID AS NVARCHAR(20)) AS 'orderer.reference',
					do.ReferringPhysicianID AS 'orderer.id',
					(SELECT Name FROM dbo.[User] WHERE InternalUserID = do.ReferringPhysicianID) AS 'orderer.display',				
					do.ReferringFacilityID AS 'orderer.facility.id',
					(SELECT OrganizationName FROM dbo.Organization WHERE InternalOrganizationID = do.ReferringFacilityID) AS 'orderer.facility.display',
					--Reason										
					(SELECT dn.InternalDiagnosisCodeID AS internalDiagnosisCodeID, CodingScheme AS codingScheme, 
							DiagnosisCode AS diagnosisCode, Description AS description
					FROM code.Diagnosis dn   
					WHERE dn.InternalDiagnosisCodeID IN (SELECT VALUE FROM OPENJSON(do.ExtJson,'$.internalDiagnosisCodeID')) FOR JSON PATH) AS reason,
					--Consulters																			
					(SELECT 'fhir/Practitioner/' + CAST(s.InternalUserID AS NVARCHAR(20)) AS reference,
							s.InternalUserID AS id,
							s.Name AS display,
							(SELECT InternalOrganizationID AS id, OrganizationName AS display FROM Organization WHERE InternalOrganizationID 
							= (SELECT JSON_VALUE(value,'$.facilityID') FROM OPENJSON(do.ExtJson,'$.consultingPhysicians') WHERE JSON_VALUE(VALUE,'$.id') = s.InternalUserID) 
							FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS facility
					FROM dbo.[User] s WHERE InternalUserID IN (SELECT JSON_VALUE(value,'$.id') FROM OPENJSON(do.ExtJson, '$.consultingPhysicians'))					
					 FOR JSON PATH) AS consulters,
					--Items
					(SELECT VALUE FROM OPENJSON(do.ExtJson) WHERE [key] = 'studies') AS 'item',
					JSON_VALUE(do.ExtJson,'$.comment') AS comment,
					JSON_QUERY(do.ExtJson,'$.note') AS note

				FOR JSON PATH,  WITHOUT_ARRAY_WRAPPER) AS JsonText
                             
FROM            PHI.DiagnosticOrder do LEFT JOIN
                         phi.Patient p ON do.InternalPatientID = p.InternalPatientID LEFT JOIN
                         PHI.Encounter e ON do.InternalEncounterID = e.InternalEncounterID LEFT JOIN
						 dbo.Organization o ON do.InternalFacilityID = o.InternalOrganizationID LEFT JOIN
						 code.PRIORITY pr ON do.PriorityValue = pr.PriorityValue LEFT JOIN						 
						 dbo.[User] u ON do.ReferringPhysicianID = u.InternalUserID


