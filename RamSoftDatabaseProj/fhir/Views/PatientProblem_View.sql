﻿CREATE VIEW fhir.PatientProblem_View
	AS
	  SELECT 
		 pp.InternalPatientProblemID,
		 pp.InternalPatientID, 
		 pp.InternalProblemCodeID,

		 (SELECT
		  'Condition' as resourceType,
		  convert(nvarchar, pp.InternalPatientProblemID) as id, 
		  TODATETIMEOFFSET(pp.OnSet, DATEPART(TZOFFSET, SYSDATETIMEOFFSET())) as onset, 
		  CONCAT('Patient/', CAST(pp.InternalPatientID AS nvarchar)) as 'patient.reference',
		  CAST(pp.InternalPatientID AS nvarchar) as 'patient.id',
		  p.PatientName as 'patient.value',
		  cp.ProblemCode as 'code.coding.code', 
		  cp.Description as 'code.coding.display', 
		  TODATETIMEOFFSET(pp.DateModified, DATEPART(TZOFFSET, SYSDATETIMEOFFSET())) as dateRecorded, 
		  cvs.ConditionVerStatusName as verificationStatus, 
		  JSON_VALUE(pp.ExtJson, '$.notes') as notes
		  FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) as JsonText 
	  FROM phi.Patient p
	  JOIN dbo.PatientProblem pp ON pp.InternalPatientID = p.InternalPatientID
	  LEFT JOIN code.Problem cp ON cp.InternalProblemCodeID = pp.InternalPatientProblemID
	  LEFT JOIN code.ConditionVerStatus cvs ON cvs.ConditionVerStatusID = pp.ConditionVerStatusID
