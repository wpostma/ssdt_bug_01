﻿
CREATE VIEW [fhir].[PatientReminder_View]
AS
  SELECT 
     pr.InternalPatientReminderID, 
     p.InternalPatientID,  

     Convert(nvarchar(max), (SELECT
	  'CommunicationRequest' as resourceType,
	   cast(pr.InternalPatientReminderID as nvarchar(16)) AS id, 
 	   CONCAT('api/v2/fhir/Patient/', CAST(p.InternalPatientID AS nvarchar)) as 'recipient.reference',
	 	   p.InternalPatientID as 'recipient.id',
 		   p.PatientName as 'recipient.display',
	   CASE WHEN (pr.StartDate IS NULL AND pr.EndDate IS NULL) THEN NULL
	        ELSE (SELECT pr.StartDate AS [start], pr.EndDate as [end] 
			      FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) END AS 'scheduledPeriod',
	   convert(nvarchar, pr.TimeLastSent, 120) as 'sent',
	   convert(nvarchar, pr.TimeNext, 120) as 'nextDate',
	   pr.FrequencyInDays as 'frequencyInDays',
 	   CONCAT('api/v2/fhir/Practitioner/', CAST(pr.InternalUserID AS nvarchar)) as 'requester.reference',
		   pr.InternalUserID as 'requester.id',
		   u.UserName as 'requester.display',
	   prt.TemplateName  as 'reason.text',
	   cs.CommunicationRequestStatusName as 'status',
  	   --(CASE p.IsActive WHEN 1 THEN 'active' ELSE 'inactive' END)  as 'status',
	   cm.ContactMethodName as  'medium.text', --JSON_VALUE(pr.ExtJson, '$.medium') as 'medium',
  	   (CASE p.IsActive WHEN 1 THEN 'true' ELSE 'false' END)  as 'active'

	   FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)) AS JsonText
  FROM phi.Patient p
  JOIN dbo.PatientReminder pr ON pr.InternalPatientID = p.InternalPatientID
  JOIN dbo.PatientReminderTemplate prt ON prt.TemplateID = pr.InternalTemplateID
  LEFT JOIN dbo.[User] u ON u.InternaluserID = pr.InternaluserID
  LEFT JOIN code.ContactMethod cm ON cm.ContactMethodID = pr.ContactMethodID
  LEFT JOIN code.CommunicationRequestStatus cs ON cs.CommunicationRequestStatusID = pr.CommunicationRequestStatusID

