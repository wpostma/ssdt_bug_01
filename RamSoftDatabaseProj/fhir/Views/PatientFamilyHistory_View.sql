﻿CREATE VIEW fhir.PatientFamilyHistory_View
	AS
	  SELECT 
		 fh.InternalFamilyHistoryID,
		 fh.InternalPatientID, 
 		 fh.InternalProblemCodeID,
		 (SELECT
		  'FamilyMemberHistory' as resourceType,
		  convert(nvarchar, fh.InternalFamilyHistoryID) as id, 
		  fm.FamilyMemberCode as 'relationship.coding.code',
		  fm.FamilyMemberName as 'relationship.coding.display',
		  CONCAT('Patient/', CAST(fh.InternalPatientID AS nvarchar)) as 'patient.reference',
		  CAST(p.InternalPatientID AS nvarchar) as 'patient.id',
		  p.PatientName as 'patient.value',
		  cp.ProblemCode as 'condition.code.coding.code', 
		  cp.Description as 'condition.code.coding.display', 
		  --cast(fh.datemodified AS datetimeoffset) AS 'date',
		  TODATETIMEOFFSET(fh.datemodified, DATEPART(TZOFFSET, SYSDATETIMEOFFSET())) as [date], 
		  hs.FamilyHistoryStatusName as[status], 
		  JSON_VALUE(fh.ExtJson, '$.note') as 'note.text'
		  FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) as JsonText 
	  FROM phi.Patient p
	  JOIN dbo.PatientFamilyHistory fh ON fh.InternalPatientID = p.InternalPatientID
	  LEFT JOIN code.Problem cp ON cp.InternalProblemCodeID = fh.InternalProblemCodeID
	  LEFT JOIN code.FamilyMember fm ON fm.FamilyMemberID = fh.FamilyMemberID
	  LEFT JOIN code.FamilyHistoryStatus hs ON hs.FamilyHistoryStatusID = fh.FamilyHistoryStatusID
