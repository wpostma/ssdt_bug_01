﻿



CREATE VIEW [fhir].[ImagingStudy_View]
AS
SELECT 
	s.InternalStudyID, 
	s.StudyID, 
	CONVERT(NVARCHAR(MAX), (SELECT
	        'ImagingStudy' AS resourceType, 
			CAST(s.InternalStudyID AS NVARCHAR(16)) AS id, 
			s.StartDateTime AS started, 
			JSON_QUERY(fhir.GetPatientReferenceJson(s.InternalPatientID)) AS patient, 
			s.StudyID AS studyID, s.StudyUID AS uid, 
			CAST(s.LastupdateUTC AS DATETIMEOFFSET) AS 'meta.lastUpdated', 
			s.LastUpdateUserID AS 'meta.lastUpdatedUser', 			
			AccessionNumber AS accession, 			
			JSON_QUERY(fhir.GetModalitiyListJson(s.InternalStudyID)) AS modalityList, 
			NULL AS referrer, 
			ia.InstanceAvailabilityCode AS availability, 
			ia.InstanceAvailabilityID AS instanceAvailabilityID, 
			fhir.GetNumberOfSeriesByStudy(s.InternalStudyID) AS numberOfSeries,
			(SELECT 'fhir/imagingstudy/' + CAST(s.InternalStudyID AS NVARCHAR(20))) AS url,
			fhir.GetNumberOfInstanceByStudy(s.InternalStudyID) AS numberOfInstances, 
			--JSON_QUERY(fhir.GetProcedureJson(s.InternalStudyID)) AS [procedure], 
			NULL AS interpreter, s.Description AS description,
			pr.PriorityValue AS priorityValue, 
			pr.Name AS priorityName, 
			st.StatusValue AS statusValue, 
			st.StatusName AS statusName, 
			s.StudyDateTime AS studyDateTime, 
			s.ModalityCode AS modalityCode,
			s.InternalStudyTypeID AS internalStudyType,
			stc.StudyType AS studyType,
			mo.Description AS modalityDescription,
			s.LateralityCode AS lateralityCode, 
			la.Description AS lateralityDescription, 
			s.InternalBodyPartID AS internalBodyPartID, 
			bp.Value AS bodyPartValue, 
			org.InternalOrganizationID AS internalFacilityID, 
			org.OrganizationName AS facilityName,
			s.InternalDepartmentID AS internalDepartmentID, 
			dp.Department AS department, 
			s.InternalRoomID AS internalRoomID, 
			CASE s.InternalRoomID WHEN - 1 THEN NULL ELSE ro.Name END AS roomName, 
			s.PrimaryEncounterID AS primaryEncounterID,
			--JSON_QUERY(fhir.GetDiagnosisJson(s.InternalStudyID)) AS 'reason', 
			CONVERT(DATETIMEOFFSET, (CAST(rt.StartDate AS DATETIME) + CAST(rt.StartTime AS DATETIME))) AS 'reservedTime.startDate', 
			CONVERT(DATETIMEOFFSET, (CAST(rt.EndDate AS DATETIME) + CAST(rt.EndTime AS DATETIME))) AS 'reservedTime.endDate', 
			CONVERT(VARCHAR(5), CAST(rt.EndTime AS DATETIME) - CAST(rt.StartTime AS DATETIME), 108) AS 'reservedTime.duration', 
			/*fhir.GetPrimaryEncounter(s.InternalStudyID) AS 'primaryEncounterID',*/ 
			JSON_VALUE(s.ExtJson, '$.notes.history') AS 'notes.history', 
			JSON_VALUE(s.ExtJson, '$.notes.clinical') AS 'notes.clinical', 
			JSON_VALUE(s.ExtJson, '$.notes.comments') AS 'notes.comments', 
			JSON_QUERY(fhir.GetSeriesJson(s.InternalStudyID)) AS series,
			JSON_VALUE(s.ExtJson, '$.customField1') AS customField1,
			JSON_VALUE(s.ExtJson, '$.customField2') AS customField2,
			JSON_VALUE(s.ExtJson, '$.customField3') AS customField3,
			JSON_VALUE(s.ExtJson, '$.customField4') AS customField4,
			JSON_VALUE(s.ExtJson, '$.customField5') AS customField5,
			JSON_VALUE(s.ExtJson, '$.customField6') AS customField6,
			JSON_VALUE(s.ExtJson, '$.customMemo1') AS customMemo1
			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)) AS JsonText

FROM PHI.Study s
	LEFT OUTER JOIN dbo.StudyType stc ON s.InternalStudyTypeID = stc.InternalStudyTypeID
	LEFT OUTER JOIN code.Priority pr ON s.PriorityValue = pr.PriorityValue
	LEFT OUTER JOIN code.BodyPart bp ON s.InternalBodyPartID = bp.InternalBodyPartID 
	LEFT OUTER JOIN code.Modality mo ON s.ModalityCode = mo.ModalityCode 
	LEFT OUTER JOIN code.Laterality la ON s.LateralityCode = la.LateralityCode 
	LEFT OUTER JOIN code.InstanceAvailability ia ON s.InstanceAvailabilityID = ia.InstanceAvailabilityID 
	LEFT OUTER JOIN PHI.Patient pa ON s.InternalPatientID = pa.InternalPatientID  
	LEFT OUTER JOIN dbo.Organization org ON s.ImagingFacilityID = org.InternalOrganizationID 
	LEFT OUTER JOIN dbo.Department dp ON s.InternalDepartmentID =dp.InternalDepartmentID 
	LEFT OUTER JOIN dbo.Room ro ON s.InternalRoomID = ro.InternalRoomID 
	LEFT OUTER JOIN code.Status st ON s.StatusValue = st.StatusValue  
	LEFT OUTER JOIN dbo.ReservedTime rt ON s.InternalStudyID = rt.InternalStudyID




