﻿CREATE view [fhir].[ProblemCode_View]
as
SELECT        p.InternalProblemCodeID,  
              CONVERT(nvarchar(max),
              (SELECT        
			    'SNOMEDCT' AS resourceType, 
				p.InternalProblemCodeID AS id, 
				JSON_QUERY((SELECT p.ProblemCode as code, p.[Description] as display FOR JSON PATH), '$') as [code.coding],
				p.Description as [code.text],
				(CASE p.IsActive WHEN 1 THEN 'true' ELSE 'false' END)  as isActive, 
				(CASE p.IsCritical WHEN 1 THEN 'true' ELSE 'false' END)  as isCritical,							
				JSON_VALUE(p.ExtJson, '$.alertMessage') as alertMessage                
                FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)) AS JsonText
FROM          code.[Problem] p 
