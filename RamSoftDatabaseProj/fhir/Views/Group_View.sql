﻿CREATE View [fhir].[Group_View]
	as
	SELECT g.InternalGroupID as InternalGroupID,
	(SELECT 
	 'Group' as resourctType,
	 g.InternalGroupID as id, 
	 g.GroupName AS name, 
	 (CASE g.IsActive WHEN 1 THEN 'true' ELSE 'false' END)  as active, 
	  	JSON_VALUE(g.ExtJson, '$.accessType') as  accessType,
		(CASE WHEN JSON_VALUE(g.ExtJson, '$.accessType') IS NOT NULL THEN (SELECT AccessTypeName FROM code.AccessType WHERE AccessTypeID = (JSON_VALUE(g.ExtJson, '$.accessType'))) END) as accessTypeName,
		JSON_VALUE(g.ExtJson, '$.statusStart') as statusStart,
		(CASE WHEN JSON_VALUE(g.ExtJson, '$.statusStart') IS NOT NULL THEN (SELECT StatusName FROM code.[Status] WHERE StatusValue = (JSON_VALUE(g.ExtJson, '$.statusStart'))) END) as statusNameStart,
		JSON_VALUE(g.ExtJson, '$.statusEnd') as  statusEnd,
		(CASE WHEN JSON_VALUE(g.ExtJson, '$.statusEnd') IS NOT NULL THEN (SELECT StatusName FROM code.[Status] WHERE StatusValue = (JSON_VALUE(g.ExtJson, '$.statusEnd'))) END) as statusNameEnd,
		JSON_VALUE(g.ExtJson, '$.accessAllFacilities') as  accessAllFacilities,
		JSON_VALUE(g.ExtJson, '$.accessAllIssuers') as  accessAllIssuers,
		JSON_VALUE(g.ExtJson, '$.accessAllPriorStudies') as  accessAllPriorStudies,
		(SELECT j.id as id, a.[name] as [name] 
	            FROM (SELECT CAST(JSON_VALUE(value, '$.id') as bigint) as id FROM OPENJSON(JSON_QUERY(g.ExtJson, '$.issuers'))) j
                JOIN dbo.AssigningAuthority a ON a.InternalAssigningAuthorityID = j.id 
                FOR JSON PATH)  as issuers	                
	  FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) as JsonText
	FROM dbo.[Group] g

