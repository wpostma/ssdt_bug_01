﻿
CREATE VIEW [fhir].[PatientCoverage_View]
	AS
	  SELECT 
		 pc.InternalCoverageID, 
		 p.InternalPatientID,  -- patient can have multiple coverage
		 pc.InsuredID, --  --(to be)Encrypted field cannot be included in Json, should be passed separately through SQL Parameter

		 (SELECT
			  'Coverage' AS resourceType,
			   CAST(pc.InternalCoverageID AS NVARCHAR(16)) AS id, 
			   JSON_QUERY(fhir.GetPatientReferenceJson(pc.InternalPatientID)) AS 'beneficiaryReference',
			   (CASE pc.IsActive WHEN 1 THEN 'true' ELSE 'false' END)  AS active,

			   py.InternalPayerID AS internalPayerID, 
			   py.CompanyName AS issuer,
	    		(SELECT JSON_VALUE(value,'$.value') FROM OPENJSON(py.ExtJson, '$.telecom') WHERE JSON_VALUE(value,'$.system') = 'other') AS payerWebsite,
     			JSON_VALUE(py.ExtJson, '$.phone') AS authorizationPhone, 

			   Pc.InsuredID AS 'identifier.value', --if encrypted then it will be replaced in C# code
			   pc.CoverageLevelID AS [sequence],
			   cl.CoverageLevel AS coverageLevel,
			   pc.GroupID AS [group],
			   CASE WHEN (pc.StartDate IS NULL AND pc.EndDate IS NULL) THEN NULL
					ELSE (SELECT pc.StartDate AS [start], pc.EndDate AS [end] FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) END AS 'period',

			   fc.FinancialClassCode AS 'financialClass.code', 
			   fc.FinancialClass AS 'financialClass.display', 

			   (CASE WHEN JSON_VALUE(pc.ExtJson, '$.copayType') IS NULL AND JSON_VALUE(pc.ExtJson, '$.copayPercent') > 0 THEN '%'
					 WHEN JSON_VALUE(pc.ExtJson, '$.copayType') IS NULL THEN '$'
					 ELSE JSON_VALUE(pc.ExtJson, '$.copayType') END) AS copayType,

 			   JSON_VALUE(pc.ExtJson, '$.copayAmount') AS copayAmount, 
 			   JSON_VALUE(pc.ExtJson, '$.copayPercent') AS copayPercent, 

 			   JSON_VALUE(pc.ExtJson, '$.accidentType') AS accidentType, 
			   JSON_VALUE(pc.ExtJson, '$.accidentPlace') AS accidentPlace, 
			   JSON_VALUE(pc.ExtJson, '$.dateOfInjury') AS dateOfInjury,
			   JSON_VALUE(pc.ExtJson, '$.notes') AS notes,

			   JSON_QUERY(pc.ExtJson, '$.claimAdjustor') AS claimAdjustor,

			   (CASE WHEN (pc.InsuredInternalPatientID = p.InternalPatientID) THEN '1' ELSE rl.BeneficiaryRelationshipCode END) AS 'relationship.code',				
			   (CASE WHEN (pc.InsuredInternalPatientID = p.InternalPatientID) THEN 'Self' ELSE rl.BeneficiaryRelationshipName END) AS 'relationship.display',

			   JSON_QUERY((CASE WHEN (pc.InsuredInternalPatientID > 0) THEN JSON_QUERY(fhir.GetPatientReferenceJson(pc.InsuredInternalPatientID)) ELSE NULL END)) AS 'planholderReference'

			   FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS JsonText
	  FROM phi.Patient p
	  JOIN PHI.PatientCoverage pc ON pc.InternalPatientID = p.InternalPatientID
	  JOIN dbo.Payer py ON py.InternalPayerID = pc.InternalPayerID 
	  LEFT JOIN code.FinancialClass fc ON fc.FinancialClassID = py.FinancialClassID
	  LEFT JOIN code.BeneficiaryRelationship rl ON rl.BeneficiaryRelationshipID = pc.InsuredRelationshipID 
	  LEFT JOIN code.CoverageLevel cl ON cl.CoverageLevelID = pc.CoverageLevelID
