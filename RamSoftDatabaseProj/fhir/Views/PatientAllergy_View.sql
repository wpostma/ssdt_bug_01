﻿CREATE VIEW fhir.PatientAllergy_View
	AS
	  SELECT 
		 p.InternalPatientID, CAST([Key] as bigint) as ID, 
		 JSON_VALUE([Value], '$.substance') as AllergyName,

		 Convert(nvarchar(max), (SELECT
		  'AllergyIntolerance' as resourceType,
		  [Key] as id, 
		  convert(date, JSON_VALUE([VALUE], '$.onset')) as onset,
		  CONCAT('Patient/', CAST(p.InternalPatientID AS nvarchar)) as 'patient.reference',
		  JSON_VALUE([VALUE], '$.substance') as 'substance.coding.display', 
		  JSON_VALUE([VALUE], '$.status') as [status], 
		  JSON_VALUE([VALUE], '$.criticality') as [criticality], 
		  JSON_VALUE([VALUE], '$.type') as [type], 
		  JSON_VALUE([VALUE], '$.category') as category, 
		  JSON_VALUE([VALUE], '$.note') as note
		  FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)) as JsonText 
	  FROM phi.Patient p
		 CROSS APPLY OPENJSON(JSON_QUERY(p.ExtJson, '$.allergies')) 
	  -- When you have to expand JSON arrays stored in individual fields and join them with their parent rows, you typically use the Transact-SQL CROSS APPLY operator.
