﻿CREATE view [fhir].[ProcCode_View]
as
SELECT        pc.InternalProcedureCodeID,  
              CONVERT(nvarchar(max),
              (SELECT        
			    'Procedure' AS resourceType, 
				pc.InternalProcedureCodeID AS id, 
				ps.Display as status,
				(CASE WHEN JSON_VALUE(pc.ExtJson, '$.subject.reference') is null THEN 'absolute' ELSE JSON_VALUE(pc.ExtJson, '$.subject.reference') END)  as [subject.reference], 
				JSON_VALUE(pc.ExtJson, '$.subject.display') as [subject.display], 
				JSON_QUERY((SELECT pc.ProcedureCode as code, pc.Description as display FOR JSON PATH), '$') as [code.coding],
				pc.Description as [code.text],
				pc.RVUTechnical as RVUTechnical,
				pc.RVUProfessional as RVUProfessional,
				JSON_VALUE(pc.ExtJson, '$.alertMessage') as alertMessage,
                (CASE pc.IsActive WHEN 1 THEN 'true' ELSE 'false' END)  as isActive, 
				(CASE pc.IsCritical WHEN 1 THEN 'true' ELSE 'false' END)  as isCritical, 
				(CASE pc.IsBillable WHEN 1 THEN 'true' ELSE 'false' END)  as isBillable, 
				(CASE pc.IsOfficeVisit WHEN 1 THEN 'true' ELSE 'false' END)  as isOfficeVisit, 
				(CASE pc.IsMammo WHEN 1 THEN 'true' ELSE 'false' END)  as isMammo 
                FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)) AS JsonText
FROM          code.ProcedureCode pc 
LEFT JOIN     code.ProcedureStatus ps ON ps.InternalProcedureStatusID = pc.InternalProcedureStatusID



