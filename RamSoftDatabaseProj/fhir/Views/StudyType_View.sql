﻿

CREATE VIEW [fhir].[StudyType_View] AS
   SELECT s.InternalStudyTypeID,
          (SELECT 'StudyType' AS resourceType, 
		           s.InternalStudyTypeID as id, 
			       CASE s.IsActive WHEN 1 THEN 'true' ELSE 'false' END AS active,
				   s.StudyType as studyType,
				   s.[Description] as [description],
				   JSON_VALUE(s.ExtJson, '$.codeValue') as codeValue,
				   JSON_VALUE(s.ExtJson, '$.codeScheme') as codeScheme,
				   JSON_QUERY(s.ExtJson, '$.modality') as modality,
				   JSON_QUERY(s.ExtJson, '$.modalityModifier') as modalityModifier,
				   JSON_VALUE(s.ExtJson, '$.laterality') as laterality,
				   JSON_QUERY(s.ExtJson, '$.bodyPart') as bodyPart,
				   JSON_QUERY(s.ExtJson, '$.anatomicFocus') as anatomicFocus,
				   JSON_QUERY(s.ExtJson, '$.pharmaceutical') as pharmaceutical,
				   JSON_VALUE(s.ExtJson, '$.technique') as technique,
				   JSON_QUERY(s.ExtJson, '$.procedureModifier') as procedureModifier,
				   JSON_VALUE(s.ExtJson, '$.duration') as duration,
				   JSON_VALUE(s.ExtJson, '$.view') as [view],
				   CASE ISNUMERIC(JSON_VALUE(s.ExtJson, '$.physicianRequired')) 
				      WHEN 1 THEN CASE JSON_VALUE(s.ExtJson, '$.physicianRequired') WHEN 1 THEN 'true' ELSE 'false' END 
				      ELSE JSON_VALUE(s.ExtJson, '$.physicianRequired') END as physicianRequired,
				   JSON_VALUE(s.ExtJson, '$.instructions') as instructions,
				   (SELECT j.id as id, r.[name] as [name], rm.[Name] as room, d.department as department, o.OrganizationName as facility
	                   FROM (SELECT CAST(JSON_VALUE(value, '$.id') as bigint) as id FROM OPENJSON(JSON_QUERY(s.ExtJson, '$.resource'))) j
                       JOIN dbo.[Resource] r ON r.InternalResourceID = j.id
					   JOIN dbo.Room rm ON rm.InternalRoomID = r.InternalRoomID
					   JOIN dbo.Department d ON rm.InternalDepartmentID = d.InternalDepartmentID
					   JOIN dbo.Organization o ON o.InternalOrganizationID = d.InternalFacilityID
                       FOR JSON PATH)  as [resource],					   
				   (SELECT d.InternalStudyTypeDetailID as id, d.InternalStudyTypeID as studyTypeID,
				           p.ProcedureCode as procedureCode, p.[Description] as [description], 
						   p.ProcedureCode + ', ' + p.[Description] as display,
						   d.CombinedModifier as modifiers,
						   d.Quantity as quantity
				    FROM dbo.StudyTypeDetail d
				    INNER JOIN code.ProcedureCode p ON p.InternalProcedureCodeID = d.InternalProcedureCodeID
					WHERE d.InternalStudyTypeID = s.InternalStudyTypeID
				    FOR JSON PATH) AS [procedures]

		  FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) as JSONTEXT
   FROM [dbo].StudyType  s


