﻿CREATE view [fhir].[ProcedureCode_View]
as
SELECT        pc.InternalProcedureCodeID, s.InternalStudyID, sp.InternalStudyProcedureMapID, CONVERT(nvarchar(max),
                             (SELECT        'Procedure' AS resourceType, sp.InternalStudyProcedureMapID AS id, fhir.GetPatientJson(s.InternalPatientID) AS [subject], ps.Code AS status,
                                                             (SELECT        pc.ProcedureCode AS coding, pc.Description AS text FOR json path, WITHOUT_ARRAY_WRAPPER) AS code, AuthorizationNumber AS authorizationNumber, 
                                                         AuthorizationStartDate AS authorizationStartDate, AuthorizationEndDate AS authorizationEndDate, Modifier AS modifier, Quantity AS quantity, pc.[Description] AS description FOR JSON PATH, 
                                                         WITHOUT_ARRAY_WRAPPER)) AS JsonText
FROM            dbo.StudyProcedureMap sp JOIN
                         phi.Study s ON sp.InternalStudyID = s.InternalStudyID JOIN
                         code.ProcedureCode pc ON sp.InternalProcedureCodeID = pc.InternalProcedureCodeID LEFT JOIN
                         code.ProcedureStatus ps ON sp.InternalProcedureStatusID = ps.InternalProcedureStatusID
