﻿
CREATE VIEW [fhir].[PatientAlert_View]
AS
  SELECT 
     pa.InternalPatientAlertID, 
     p.InternalPatientID,  

     (SELECT
	  'Flag' as resourceType,
	   cast(pa.InternalPatientAlertID as nvarchar(16)) AS id, 
 	   CONCAT('api/v2/fhir/Patient/', CAST(p.InternalPatientID AS nvarchar)) as 'subject.reference',
 		   p.InternalPatientID as 'subject.id',
 		   p.PatientName as 'subject.display',
	   convert(nvarchar, pa.TimeCreated, 120) as 'lastUpdated',
 	   CONCAT('api/v2/fhir/Practitioner/', CAST(pa.InternalUserID AS nvarchar)) as 'author.reference',
		   pa.InternalUserID as 'author.id',
		   u.UserName as 'author.display',
	   JSON_VALUE(pa.ExtJson, '$.summary') as 'code.text',
	   JSON_QUERY(pa.ExtJson, '$.recipient') as 'recipient',
	   CASE WHEN (pa.StartDate IS NULL AND pa.EndDate IS NULL) THEN NULL
	        ELSE (SELECT pa.StartDate AS [start], pa.EndDate as [end] 
			      FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) END AS 'period'

	   FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS JsonText
  FROM phi.Patient p
  JOIN dbo.PatientAlert pa ON pa.InternalPatientID = p.InternalPatientID
  LEFT JOIN dbo.[User] u ON u.InternaluserID = pa.InternaluserID
