﻿
	CREATE VIEW [fhir].[EncounterCoverage_View]
	AS
	SELECT  ec.InternalEncounterCoverageID, 
			e.InternalEncounterID, 
			ec.InsuredID, --(to be)Encrypted field cannot be included in Json, should be passed separately through SQL Parameter
			(CASE WHEN (JSON_VALUE(ec.ExtJson, '$.coverageStatus') IS NOT NULL) THEN JSON_VALUE(ec.ExtJson, '$.coverageStatus') 
			      WHEN (ec.IsActive = 1) THEN 'ACTIVE' ELSE 'INACTIVE' END) AS coverageStatus, 	 
			ec.CoverageLevelID,

			(SELECT 'EncounterCoverage' AS resourceType, 
					CAST(ec.InternalEncounterCoverageID AS NVARCHAR(16)) AS id, 
				    JSON_QUERY(fhir.GetEncounterReferenceJson(e.InternalEncounterID)) AS 'encounterReference',
				    JSON_QUERY(fhir.GetPatientReferenceJson(e.InternalPatientID)) AS 'beneficiaryReference',

					(CASE ec.IsActive WHEN 1 THEN 'true' ELSE 'false' END) AS active, 

					py.InternalPayerID AS internalPayerID, 
					py.CompanyName AS issuer, 
					(SELECT JSON_VALUE(value,'$.value') FROM OPENJSON(py.ExtJson, '$.telecom') WHERE JSON_VALUE(value,'$.system') = 'other') AS payerWebsite,
					JSON_VALUE(py.ExtJson, '$.phone') AS authorizationPhone, 

					ec.InsuredID AS 'identifier.value', --if encrypted then it will be replaced in C# code
					ec.CoverageLevelID AS [sequence], 
					cl.CoverageLevel AS coverageLevel, 
					(CASE WHEN (JSON_VALUE(ec.ExtJson, '$.coverageStatus') IS NOT NULL) THEN JSON_VALUE(ec.ExtJson, '$.coverageStatus') 
			              WHEN (ec.IsActive = 1) THEN 'ACTIVE' ELSE 'INACTIVE' END) AS coverageStatus, 	 					
					ec.GroupID AS [group], 
					JSON_QUERY(CASE WHEN (ec.StartDate IS NULL AND ec.EndDate IS NULL) THEN NULL 
							ELSE (SELECT ec.StartDate AS [start], ec.EndDate AS [end] FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) END) AS period, 
				
					fc.FinancialClassCode AS 'financialClass.code', 
					fc.FinancialClass AS 'financialClass.display', 
					
				   (CASE WHEN JSON_VALUE(ec.ExtJson, '$.copayType') IS NULL AND JSON_VALUE(ec.ExtJson, '$.copayPercent') > 0 THEN '%'
						 WHEN JSON_VALUE(ec.ExtJson, '$.copayType') IS NULL THEN '$'
						 ELSE JSON_VALUE(ec.ExtJson, '$.copayType') END) AS copayType,
					JSON_VALUE(ec.ExtJson, '$.copayAmount') AS copayAmount, 
					JSON_VALUE(ec.ExtJson, '$.copayPercent') AS copayPercent,

					JSON_VALUE(ec.ExtJson, '$.accidentType') AS accidentType, 
					JSON_VALUE(ec.ExtJson, '$.accidentPlace') AS accidentPlace, 
					JSON_VALUE(ec.ExtJson, '$.dateOfInjury') AS dateOfInjury, 
					JSON_VALUE(ec.ExtJson, '$.notes') AS notes,
					JSON_QUERY(ec.ExtJson, '$.claimAdjustor') AS claimAdjustor,

					/*Payer info*/
					(CASE WHEN (ec.InsuredInternalPatientID = e.InternalPatientID) THEN '1' ELSE rl.BeneficiaryRelationshipCode END) AS 'relationship.code',				
					(CASE WHEN (ec.InsuredInternalPatientID = e.InternalPatientID) THEN 'Self' ELSE rl.BeneficiaryRelationshipName END) AS 'relationship.display',
					JSON_QUERY((CASE WHEN (ec.InsuredInternalPatientID > 0) THEN JSON_QUERY(fhir.GetPatientReferenceJson(ec.InsuredInternalPatientID)) ELSE NULL END)) AS 'planholderReference'
					
					FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS JsonText

	FROM phi.Encounter e 
		JOIN PHI.EncounterCoverage ec ON ec.InternalEncounterID = e.InternalEncounterID 
		JOIN dbo.Payer py ON py.InternalPayerID = ec.InternalPayerID 
		LEFT JOIN code.FinancialClass fc ON fc.FinancialClassID = py.FinancialClassID 
		LEFT JOIN code.BeneficiaryRelationship rl ON rl.BeneficiaryRelationshipID = ec.InsuredRelationshipID 
		LEFT JOIN code.CoverageLevel cl ON cl.CoverageLevelID = ec.CoverageLevelID
