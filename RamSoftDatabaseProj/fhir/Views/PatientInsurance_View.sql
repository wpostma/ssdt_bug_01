﻿
	CREATE VIEW [fhir].[PatientInsurance_View]
		AS
		  SELECT p.InternalPatientID, 
				 (CASE WHEN p.GuarantorInternalPatientID IS NULL THEN p.InternalPatientID ELSE p.GuarantorInternalPatientID END) AS GuarantorInternalPatientID,
			 (SELECT
			  'PatientInsurance' AS resourceType,
			   CAST(p.InternalPatientID AS NVARCHAR(16)) AS id, 
			   CONCAT(p.PatientID, ' - ', p.PatientName) AS [value],
			   fhir.GetPatientDisplayStr(p.InternalPatientID) AS display,
			   JSON_QUERY(FHIR.GetPatientReferenceJson(p.InternalPatientID)) as 'beneficiaryReference', 
	   		   fc.FinancialClass as financialClass,

			   brl.BeneficiaryRelationshipName AS 'guarantorRelationToPatient',
			   JSON_QUERY(CASE WHEN p.GuarantorInternalPatientID IS NULL THEN JSON_QUERY(FHIR.GetPatientReferenceJson(p.InternalPatientID))
					 ELSE JSON_QUERY(FHIR.GetPatientReferenceJson(p.GuarantorInternalPatientID)) END) AS guarantorReference,

			   (SELECT CAST(pc.InternalCoverageID AS NVARCHAR(16)) AS id, CL.CoverageLevel AS coverageLevel, 
						(CASE WHEN pc.IsActive = 1 AND pc.EndDate < SYSDATETIME() THEN 'Expired'
							  WHEN pc.IsActive = 1 THEN 'Active' ELSE 'Inactive' END) AS coverageStatus,
					py.CompanyName AS payer, 
					(SELECT TOP 1 JSON_VALUE(value,'$.value') FROM OPENJSON(py.ExtJson, '$.telecom') WHERE JSON_VALUE(value,'$.system') = 'other') AS payerWebsite,
					pc.StartDate AS 'period.start',pc.EndDate AS 'period.end'
					FROM phi.PatientCoverage pc
					LEFT JOIN CODE.CoverageLevel CL ON CL.CoverageLevelID = PC.CoverageLevelID
					LEFT JOIN dbo.Payer py ON py.InternalPayerID = pc.InternalPayerID
					WHERE PC.InternalPatientID = p.InternalPatientID
					ORDER BY pc.IsActive DESC, pc.CoverageLevelID
					FOR JSON PATH) AS coverages

			   FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS JsonText
		  FROM phi.Patient p
		  LEFT JOIN phi.Patient guar ON guar.InternalPatientID = p.GuarantorInternalPatientID
		  LEFT JOIN code.BeneficiaryRelationship brl ON brl.BeneficiaryRelationshipID = p.GuarantorRelationshipID 
		  LEFT JOIN code.FinancialClass fc ON fc.FinancialClassID = p.FinancialClassID
