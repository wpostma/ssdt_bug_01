﻿
	CREATE VIEW [fhir].[StudyInsurance_View]
		AS
		  SELECT s.InternalStudyID, 
				 s.PrimaryEncounterID,
				 s.InternalPatientID,
				 (SELECT
				   'StudyInsurance' AS resourceType,		
				   CAST(S.InternalStudyID AS NVARCHAR(16)) AS id, 
				   s.AccessionNumber AS [value],
				   CONCAT('ACCESSION: ', s.AccessionNumber, ' - PATIENT: ', fhir.GetPatientDisplayStr(s.InternalPatientID)) AS display,

				   JSON_QUERY(CASE WHEN s.PrimaryEncounterID IS NULL THEN NULL
								   ELSE FHIR.GetEncounterReferenceJson(s.PrimaryEncounterID) END) AS primaryEncounterReference,

				   (SELECT CAST(ec.InternalEncounterCoverageID AS NVARCHAR(16)) AS id, CL.CoverageLevel AS coverageLevel, 
							(CASE WHEN ec.IsActive = 1 AND (s.StartDateTime < ec.StartDate OR s.StartDateTime > ec.EndDate) THEN 'Expired'
								  WHEN ec.IsActive = 1 THEN 'Active' ELSE 'Inactive' END) AS coverageStatus,
						py.CompanyName AS payer, 
						ec.InsuredID as insuredID,
						ec.GroupID as groupNumber,
						JSON_Value(py.ExtJson, '$.authorizationPhoneNumber') as authorizationPhoneNumber, 
						(CASE WHEN JSON_VALUE(ec.ExtJson, '$.copayType') = '%' AND JSON_VALUE(ec.ExtJson, '$.copayPercent') >  0 THEN CONCAT(JSON_VALUE(ec.ExtJson, '$.copayPercent'), '%')
						      WHEN JSON_VALUE(ec.ExtJson, '$.copayAmount') > 0 THEN JSON_VALUE(ec.ExtJson, '$.copayAmount') END) as copay,
						(SELECT TOP 1 JSON_VALUE(value,'$.value') FROM OPENJSON(py.ExtJson, '$.telecom') WHERE JSON_VALUE(value,'$.system') = 'other') AS payerWebsite,
						ec.StartDate AS 'period.start', ec.EndDate AS 'period.end'
						FROM phi.EncounterCoverage ec
						LEFT JOIN CODE.CoverageLevel CL ON CL.CoverageLevelID = ec.CoverageLevelID
						LEFT JOIN dbo.Payer py ON py.InternalPayerID = ec.InternalPayerID
						WHERE ec.InternalEncounterID = e.InternalEncounterID
						ORDER BY ec.IsActive DESC, ec.CoverageLevelID
						FOR JSON PATH) AS coverages 

			   FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS JsonText
		  FROM phi.Study s
		  JOIN phi.Patient p ON p.InternalPatientID = s.InternalPatientID
		  LEFT JOIN phi.Encounter e ON e.InternalEncounterID = s.PrimaryEncounterID
