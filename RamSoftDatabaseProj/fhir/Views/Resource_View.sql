﻿

CREATE VIEW [fhir].[Resource_View]
	AS
	SELECT r.InternalResourceID, r.Name , ro.InternalRoomID ,de.InternalDepartmentID, org.InternalOrganizationID,
	 (SELECT 
	 'Schedule' AS resourceType, 	 	 
	  r.InternalResourceID AS id, 
	  r.IsActive AS active,
	  r.Name AS resourceName,
	  CONCAT('fhir/Location/', CAST(r.InternalRoomID AS VARCHAR(20))) AS 'actor.reference',
	  r.InternalRoomID AS 'actor.id',
	  ro.Name AS 'actor.value',
	  CONCAT(de.Department, ': ', ro.Name) AS 'actor.display',
	  JSON_VALUE(r.ExtJson, '$.STARTTIME') AS 'planningHorizon.start',
	  JSON_VALUE(r.ExtJson, '$.ENDTIME') AS 'planningHorizon.end',
	  CAST (JSON_VALUE(r.ExtJson, '$.DEFAULTSTUDYORDERDURATION') AS INT) AS defaultDuration,
	  JSON_VALUE(r.ExtJson, '$.Color') AS color,	 
	  JSON_QUERY(r.ExtJson, '$.modalities') AS modalities,	  
	  dbo.ConvertJsonArrayToDicomString(JSON_QUERY(r.ExtJson, '$.modalities')) AS modalitiesDisplay,	  
	  r.Overbooking AS overBooking,
	  r.CanBlockTime AS canBlockTime,
	  r.CanReserveTimeSlots AS canReserveTimeSlots,
	  JSON_VALUE(r.ExtJson, '$.timeMultiplier') AS timeMultiplier

	  FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS JsonText

	FROM dbo.Resource r JOIN 
		(dbo.Room ro JOIN 
		(dbo.Department de JOIN	dbo.Organization org ON de.InternalFacilityID = org.InternalOrganizationID) 
		ON ro.InternalDepartmentID = de.InternalDepartmentID) ON r.InternalRoomID = ro.InternalRoomID		


