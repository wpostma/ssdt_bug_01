﻿

CREATE View [fhir].[AssigningAuthority_View]
	as
	SELECT a.InternalAssigningAuthorityID as InternalAssigningAuthorityID,	      
	 (SELECT 
	 'Organization' AS resourceType, 	
	 'prov' as Type, 
	 CAST(a.InternalAssigningAuthorityID AS nvarchar(19)) AS id,
	 a.name as name, 
	 JSON_QUERY((CASE WHEN a.UniversalEntityID is null THEN null
      ELSE (SELECT a.UniversalEntityID as value, u.UniversalEntityIDTypeCode as type FOR JSON PATH)
      END), '$') as identifier,
	 (CASE a.IsActive WHEN 1 THEN 'true' ELSE 'false' END)  as active, 
	 (CASE a.IsExternal WHEN 1 THEN 'true' ELSE 'false' END)  as isExternal,
	 JSON_VALUE(a.ExtJson, '$.directAddress') as directAddress,
	 JSON_QUERY(a.ExtJson, '$.contact') as contact,
     JSON_QUERY(a.ExtJson, '$.address') as address, 
	 JSON_QUERY(a.ExtJson, '$.telecom') as telecom,
	 JSON_QUERY(a.ExtJson, '$.preliminary') as preliminary,
	 JSON_QUERY(a.ExtJson, '$.final') as final	  
	  FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) as JsonText
	FROM dbo.AssigningAuthority a
	LEFT JOIN code.UniversalEntityIDType u ON u.UniversalEntityIDTypeID = a.UniversalEntityIDTypeID
	






