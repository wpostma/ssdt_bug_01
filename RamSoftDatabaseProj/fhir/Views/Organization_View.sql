﻿CREATE VIEW [fhir].[Organization_View]
	AS
	  SELECT org.InternalOrganizationID, 
		 -- Json Text
		 (SELECT

			-- FHIR fields
		   'Organization' AS resourceType,
		   org.InternalOrganizationID AS id, 
		   JSON_QUERY(org.ExtJson, '$.identifier') AS identifier,
		   (CASE org.IsActive WHEN 1 THEN 'true' ELSE 'false' END)  AS active,
		   JSON_VALUE(org.ExtJson, '$.type') AS type,
		   org.OrganizationName AS name,
		   JSON_QUERY(org.ExtJson, '$.contactPoint') AS telecom,
		   JSON_QUERY(org.ExtJson, '$.address') AS address,
		   (CASE WHEN org.ParentOrganizationID IS NULL THEN NULL 
				 ELSE dbo.GetReferenceURL('fhir/organizaion/' + CAST(org.ParentOrganizationID AS NVARCHAR)) END) AS 'partOf.reference',
			org.ParentOrganizationID AS 'partOf.id',

			-- Other fields
			CAST(org.LastupdateUTC AS DATETIMEOFFSET) AS 'meta.lastUpdated',
			(SELECT dbo.GetReferenceURL('fhir/organizaion/' + CAST(org.InternalOrganizationID AS NVARCHAR))) AS url,
			org.TimeZone AS timeZone,

			-- Practice Type
		   (CASE org.IsImaging WHEN 1 THEN 'true' ELSE 'false' END) AS imaging,
		   (CASE org.IsReferring WHEN 1 THEN 'true' ELSE 'false' END) AS referring,
		   (CASE org.IsMammoTracking WHEN 1 THEN 'true' ELSE 'false' END) AS stana,
		   fhir.GetOrganizationTypeDisplayStr(org.IsImaging, org.IsReferring, org.IsMammoTracking) as facilityTypeDisplay, 

		   CONCAT('assigningAuthority/', org.InternalAssigningAuthorityID) AS 'assigningAuthority.reference',
		   org.InternalAssigningAuthorityID AS 'assigningAuthority.id',
		   aa.Name AS 'assigningAuthority.name',

		   -- Addition Information
		   (SELECT us.UserName AS userName, us.Name AS name, us.ID AS id, uf.FacilityUserID AS facilityUserID 
				FROM dbo.[User] AS us INNER JOIN dbo.UserFacilityMap AS uf ON us.InternalUserID = uf.InternalUserID AND uf.InternalFacilityID = org.InternalOrganizationID
				FOR JSON PATH) AS userOrganizationMap,
			JSON_VALUE(org.ExtJson, '$.directAddress') AS directAddress,
			JSON_QUERY(org.ExtJson, '$.notes') AS notes,

			--Distribution infor
			JSON_QUERY(org.ExtJson, '$.preliminary') AS preliminary,
			JSON_QUERY(org.ExtJson, '$.final') AS final,
			(CASE JSON_VALUE(org.ExtJson, '$.distributeToFacilityOnly') WHEN 1 THEN 'true' ELSE 'false' END)  AS distributeToFacilityOnly,

			-- Facility Details
			org.FeeScheduleID AS feeScheduleID,
			fe.FeeScheduleName AS feeScheduleName,
			org.SelfPayFeeScheduleID AS selfPayFeeScheduleID,
			sfe.FeeScheduleName AS selfPayFeeScheduleName,
			(CASE JSON_VALUE(org.ExtJson, '$.rx') WHEN 1 THEN 'true' ELSE 'false' END)  AS rx

		   FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS JsonText

	  FROM dbo.Organization org
	  LEFT JOIN dbo.AssigningAuthority aa ON aa.InternalAssigningAuthorityID = org.InternalAssigningAuthorityID
	  LEFT JOIN dbo.FeeSchedule fe ON fe.FeeScheduleID = org.FeeScheduleID
	  LEFT JOIN dbo.FeeSchedule sfe ON sfe.FeeScheduleID = org.SelfPayFeeScheduleID
