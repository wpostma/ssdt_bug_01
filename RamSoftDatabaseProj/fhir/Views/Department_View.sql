﻿
CREATE VIEW [fhir].[Department_View]
AS
SELECT d.InternalDepartmentID,
       (SELECT
			-- department fields
		   'Department' AS resourceType,
		   d.InternalDepartmentID AS id, 
		   d.Department as [name],	
		   JSON_VALUE(d.ExtJson, '$.floor') AS floor,
		   (CASE d.IsActive WHEN 1 THEN 'true' ELSE 'false' END) AS isActive,

		   -- facility fields
		   d.InternalFacilityID as 'facility.id',
		   o.OrganizationName as 'facility.name',			

		   -- rooms Information
		   (SELECT r.InternalRoomID as id,
		           r.Name as [name], 
				   JSON_VALUE(r.ExtJson, '$.phoneNumber') as phoneNumber,
				   JSON_VALUE(r.ExtJson, '$.faxNumber') as faxNumber,
				   JSON_QUERY(r.ExtJson, '$.autoFax') AS autoFax
				FROM dbo.[Room] AS r 
				WHERE (r.InternalDepartmentID = d.InternalDepartmentID) AND
				      (r.InternalRoomID > 0)
				FOR JSON PATH) AS rooms		

		   FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS JsonText

FROM dbo.Department d
JOIN dbo.Organization o ON o.InternalOrganizationID = d.InternalFacilityID
