﻿CREATE VIEW [fhir].[Practitioner_View] AS
		SELECT 
			u.InternalUserID,
			u.Signature,
			u.IsActive,
			/* Json Text*/ 
			(SELECT 
				'Practitioner' AS resourceType, 
				CASE u.IsActive WHEN 1 THEN 'true' ELSE 'false' END AS active,

				u.LastUpdateUTC AS 'meta.lastUpdatedUTC', 
				u.LastupdateUserID AS 'meta.lastUpdatedUserID',

				CAST(u.InternalUserID AS NVARCHAR(19)) AS id,
				JSON_QUERY(u.ExtJson, '$.identifier') AS identifier,
				CONCAT(u.Name, ' - ', u.ID) AS value,
				u.Name AS display,
				u.Name AS fullName,
				JSON_QUERY(fhir.GetHumanNameJson(u.Name), '$') AS name,
				u.UserName AS userName,
				LOWER(sx.Sex) AS gender,
				sx.SexCode AS sexCode,
				u.BirthDate AS birthDate,
				u.ID AS userID,
				u.UseExternalAuthentication AS isUseExternalAuthenticate,

				JSON_QUERY(u.ExtJson, '$.contactPoint') AS telecom, 
				JSON_QUERY(u.ExtJson, '$.address') AS address,
				JSON_VALUE(u.ExtJson, '$.directAddress') AS directAddress,
				JSON_VALUE(u.ExtJson, '$.notes') AS notes,
				
				CASE (CAST(JSON_VALUE(r.ExtJson, '$.isReferringPhysician') AS BIT)) WHEN 1 THEN JSON_QUERY(u.ExtJson, '$.reportingDetails') END AS reportingDetails,

				JSON_VALUE(u.ExtJson, '$.license.dea') AS 'license.dea',
				JSON_VALUE(u.ExtJson, '$.license.licenseNumber') AS 'license.licenseNumber',
				JSON_VALUE(u.ExtJson, '$.license.countryCode') AS 'license.countryCode',
				JSON_VALUE(u.ExtJson, '$.license.stateCode') AS 'license.stateCode',
				(SELECT CountryName  FROM dbo.Country c WHERE c.CountryCode = JSON_VALUE(u.ExtJson, '$.license.countryCode')) AS 'license.country',
				(SELECT c.State  FROM code.State c WHERE c.StateCode = JSON_VALUE(u.ExtJson, '$.license.stateCode')) AS 'license.state',

				--Integration data
				JSON_QUERY(u.ExtJson, '$.integrations') AS integrations,

				r.InternalRoleID AS 'role.id',
				r.Name AS 'role.name',
				r.IsActive AS 'role.active',
				CAST(JSON_VALUE(r.ExtJson, '$.isAdmin') AS BIT) AS 'role.isAdmin',
				CAST(JSON_VALUE(r.ExtJson, '$.isSyswideAdmin') AS BIT) AS 'role.isSyswideAdmin',
				CAST(JSON_VALUE(r.ExtJson, '$.isIT') AS BIT) AS 'role.isIT',
				CAST(JSON_VALUE(r.ExtJson, '$.isReadingPhysician') AS BIT) AS 'role.isReadingPhysician', 
				CAST(JSON_VALUE(r.ExtJson, '$.isReferringPhysician') AS BIT) AS 'role.isReferringPhysician', 
				CAST(JSON_VALUE(r.ExtJson, '$.isPerformingPhysician') AS BIT) AS 'role.isPerformingPhysician', 
				CAST(JSON_VALUE(r.ExtJson, '$.isPerformingTechnologist') AS BIT) AS 'role.isPerformingTechnologist', 
				CAST(JSON_VALUE(r.ExtJson, '$.isTranscriptionist') AS BIT) AS 'role.isTranscriptionist', 							

				g.InternalGroupID AS 'group.id',
				g.GroupName AS 'group.name',
			
				ps.PhysicianSpecialtyID AS 'specialty.id',
				ps.PhysicianSpecialtyName AS 'specialty.name',


				CASE WHEN (CAST(JSON_VALUE(r.ExtJson, '$.isReadingPhysician') AS BIT)) = 1 OR 
				(CAST(JSON_VALUE(r.ExtJson, '$.isPerformingPhysician') AS BIT) = 1) THEN 'fhir/Practitioner/' + CAST(u.InternalUserID AS NVARCHAR) + '/signature' 
				END AS signatureUrl,			
			
				(SELECT org.OrganizationName AS name, uf.FacilityUserID AS id, uf.InternalFacilityID AS internalFacilityID 
					FROM dbo.UserFacilityMap uf
					INNER JOIN dbo.Organization org ON org.InternalOrganizationID = uf.InternalFacilityID
					WHERE uf.InternalUserID = u.InternalUserID
					FOR JSON PATH) AS userFacilities
			

				/*fhir.GetPractitionerRolesJson(sp.InternalUserID) AS 'practitionerRole'*/

				FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS JsonText

		FROM dbo.[User] u
			LEFT JOIN code.Sex sx ON sx.SexID = u.SexID 
			LEFT JOIN dbo.[Role] r ON r.InternalRoleID = u.InternalRoleID
			LEFT JOIN dbo.[Group] g ON g.InternalGroupID = u.InternalGroupID
			LEFT JOIN code.PhysicianSpecialty ps ON ps.PhysicianSpecialtyID = u.PhysicianSpecialtyID
