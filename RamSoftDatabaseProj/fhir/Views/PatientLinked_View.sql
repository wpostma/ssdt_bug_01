﻿
CREATE VIEW [fhir].[PatientLinked_View]
AS
  SELECT 
     L.EntryID,
	 L.InternalPatientID,  
	 L.LinkID,
	 (CASE WHEN L.LinkageTypeID IS NULL THEN 1 ELSE L.LinkageTypeID END) as LinkageTypeID,
     (SELECT
	    'Linkage' as resourceType,
	     cast(L.EntryID as nvarchar(16)) AS id, 
	     cast(L.LinkID as nvarchar(16)) AS linkId, 

		 (SELECT 
		        lt.LinkageTypeCode as [type],
				lt.LinkageTypeName as typeName,
				CONCAT('Patient/', CAST(P.InternalPatientID AS nvarchar)) as 'resource.reference',
 			    P.InternalPatientID as 'resource.id', P.PatientName as 'resource.display',
				CONCAT(P.PatientID, ' - ', P.PatientName) as 'resource.value',
				P.PatientID as patientID, P.PatientName as patientName,
				P.BirthDate as birthDate, sx.Sex as sex, aa.Name as assigningAuthority
				FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS item
	   FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS JsonText


  FROM dbo.PatientLinked L
  JOIN  phi.Patient p ON L.InternalPatientID = P.InternalPatientID
  LEFT JOIN code.Sex sx ON sx.SexID = P.SexID
  LEFT JOIN dbo.AssigningAuthority aa ON aa.InternalAssigningAuthorityID = P.InternalAssigningAuthorityID
  LEFT JOIN code.LinkageType lt ON lt.LinkageTypeID = L.LinkageTypeID 
