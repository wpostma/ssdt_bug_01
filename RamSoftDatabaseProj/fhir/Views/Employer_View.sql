﻿
CREATE View [fhir].[Employer_View]
	as
	SELECT e.InternalEmployerID as InternalEmployerID,
	 (SELECT 
	 'Organization' AS resourceType, 
	 'other' AS type, 
	 e.InternalEmployerID as id, 
	 e.EmployerName AS name, 
	 (CASE e.IsActive WHEN 1 THEN 'true' ELSE 'false' END)  as active,  
	  JSON_VALUE(e.ExtJson, '$.notes') as notes,
	  JSON_QUERY(e.ExtJson, '$.contact') as contact,
      JSON_QUERY(e.ExtJson, '$.address') as address, 
	  JSON_QUERY(e.ExtJson, '$.telecom') as telecom
	  FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) as JsonText
	FROM dbo.Employer e




