﻿CREATE VIEW [fhir].[PatientStudyHistory_View]
	AS
	  SELECT 
		 s.InternalStudyID,
		 p.InternalPatientID, 
		 convert(datetime, s.StudyDateTime) as StudyDateTime, 
		 (SELECT
			  'StudyHistory' as resourceType,
			  CONCAT('ImagingStudy/', CAST(s.InternalStudyID AS nvarchar)) as 'imagingStudy.reference',
			  CAST(s.InternalStudyID AS nvarchar) as 'imagingStudy.id',
			  convert(datetime, s.StudyDateTime) as [dateTime],
			  s.[Description] as studyDescription, 
			  pl.Name AS readingPhysician, 
			  f.OrganizationName as facility,
			  st.StatusName  as [status]
			  FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) as JsonText 
	  FROM phi.Patient p
		JOIN PHI.Study s ON p.InternalPatientID = s.InternalPatientID
		LEFT JOIN dbo.StudyPlayer pl ON pl.InternalStudyID = s.InternalStudyID AND pl.StudyPlayerType = 0 
		LEFT JOIN dbo.Organization f ON f.InternalOrganizationID = s.ImagingFacilityID 
		LEFT JOIN code.Status st ON st.StatusValue = s.StatusValue;
