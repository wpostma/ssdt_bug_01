﻿CREATE View [fhir].[Payer_View]
	as
	SELECT p.InternalPayerID as InternalPayerID,
	 (SELECT 
	 'Organization' AS resourceType, 
	 JSON_QUERY(p.ExtJson, '$.identifier') as identifier,
	 'ins' AS type, 
	 p.InternalPayerID as id, 
	 CompanyName AS name, 
	 (CASE p.IsActive WHEN 1 THEN 'true' ELSE 'false' END)  as active, 
	  fe.FeeScheduleID as feeScheduleID, 
	  fe.FeeScheduleName as feeScheduleName, 	  
	  fc.FinancialClassCode as financialClassCode, 
	  fc.FinancialClass as financialClass, 
	  (SELECT JSON_VALUE(value,'$.value') FROM OPENJSON(p.ExtJson, '$.telecom') WHERE JSON_VALUE(value,'$.system') = 'other') AS website,
	  JSON_VALUE(p.ExtJson, '$.payerID') as payerID,
      JSON_VALUE(p.ExtJson, '$.eligibilityPayerID') as eligibilityPayerID, 
	  JSON_QUERY(p.ExtJson, '$.contact') as contact,
      JSON_QUERY(p.ExtJson, '$.address') as address, 
	  JSON_QUERY(p.ExtJson, '$.telecom') as telecom,
	  JSON_QUERY(p.ExtJson, '$.otherNumbers') as otherNumbers
	  FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) as JsonText
	FROM dbo.Payer p
	LEFT JOIN dbo.FeeSchedule fe on fe.FeeScheduleID = p.FeeScheduleID
	LEFT JOIN code.FinancialClass fc on fc.FinancialClassID = p.FinancialClassID
