﻿CREATE TABLE [fhir].[LogTable] (
    [EntryID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [logItem] NVARCHAR (MAX) NULL,
    [logdesc] NVARCHAR (MAX) NULL,
    [logint]  INT            NULL,
    [logtime] DATETIME2 (7)  CONSTRAINT [DF_LogTable_LogTime] DEFAULT (sysutcdatetime()) NULL,
    CONSTRAINT [PK_LogTable] PRIMARY KEY CLUSTERED ([EntryID] ASC)
);

