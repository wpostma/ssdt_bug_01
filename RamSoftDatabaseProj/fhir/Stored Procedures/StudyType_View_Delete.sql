﻿CREATE PROCEDURE [fhir].[StudyType_View_Delete]
		@InternalStudyTypeID bigint,
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;	
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DELETE FROM dbo.StudyType	
			OUTPUT deleted.InternalStudyTypeID INTO @ProcessedIDTab
			WHERE (InternalStudyTypeID = @InternalStudyTypeID); 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END
