﻿CREATE PROCEDURE [fhir].[Study_View_InsertUpdate]
		@InternalStudyID bigint,
		@StudyID nvarchar(64),
		@JsonInput nvarchar(max), 		
		@Result bigint output
AS 
BEGIN
	SET NOCOUNT ON;				
	
	DECLARE @CurKey nvarchar(512);
 	DECLARE	@CurValue nvarchar(MAX);
	DECLARE	@CurType int;

	DECLARE @NewPatientID bigint;
	DECLARE @NewStudyID nvarchar(64) = NULL;		
	DECLARE @NewID nvarchar(64);
	DECLARE @NewPriorityValue smallint;
	DECLARE @NewStatusValue smallint;
	DECLARE @NewStudyDescription nvarchar(max);
	DECLARE @NewRequestedProcedureID nvarchar(64);
	DECLARE @NewInstanceAvailabilityCode nvarchar(64);		
	DECLARE @NewAccession nvarchar(64);
	DECLARE @NewModality nvarchar(8);
	DECLARE @NewLaterality nvarchar(8);		
	DECLARE @NewBodyPart nvarchar(16);
	DECLARE @NewFacility bigint;
	DECLARE @NewDepartment bigint;
	DECLARE @NewStudyDateTime datetimeoffset;
	DECLARE @NewRoomID bigint;
	DECLARE @NewHistory nvarchar(max);
	DECLARE @NewClinical nvarchar(max);
	DECLARE @NewComments nvarchar(max);
	DECLARE @NewNotes nvarchar(max);
	DECLARE @NewStudyTypeID bigint;
	DECLARE @LastUpdateUserID bigint; -- should be get from session user info, how???	
	DECLARE @CustomField1 nvarchar(64);
	DECLARE @CustomField2 nvarchar(64);
	DECLARE @CustomField3 nvarchar(64);
	DECLARE @CustomField4 nvarchar(64);
	DECLARE @CustomField5 nvarchar(64);
	DECLARE @CustomField6 nvarchar(64);
	DECLARE @CustomMemo1 nvarchar(max);

	DECLARE Json_Cursor CURSOR LOCAL FOR SELECT * FROM OPENJSON(@JsonInput);

	OPEN Json_Cursor;
	FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

	WHILE (@@FETCH_STATUS = 0) 
	BEGIN
    		
			IF (@CurKey = 'meta')
			Begin
			 SET @LastupdateUserID = JSON_VALUE(@CurValue,'$.lastUpdatedUser');
			End
			IF (@CurKey = 'studyID') SET @NewStudyID = @CurValue;
			IF (@CurKey = 'priorityValue') SET @NewPriorityValue = @CurValue;
			IF (@CurKey = 'statusValue') SET @NewStatusValue = @CurValue;
			IF (@CurKey = 'description') SET @NewStudyDescription = @CurValue;
			IF (@CurKey = 'requestedProcedureID') SET @NewRequestedProcedureID = @CurValue;
			IF (@CurKey = 'availability') SET @NewInstanceAvailabilityCode = @CurValue;
			IF (@CurKey = 'accession') SET @NewAccession = @CurValue;
			IF (@CurKey = 'modalityCode') SET @NewModality = @CurValue;
			IF (@CurKey = 'lateralityCode') SET @NewLaterality = @CurValue;
			IF (@CurKey = 'bodyPartValue') SET @NewBodyPart = @CurValue;
			IF (@CurKey = 'internalStudyType') SET @NewStudyTypeID = @CurValue;
			IF (@CurKey = 'internalFacilityID')
			Begin
				IF @CurValue <> -1
				Begin
					SET @NewFacility = @CurValue;
				End
			End
			IF (@CurKey = 'internalDepartmentID')
			Begin
				IF @CurValue <> -1
				Begin
					SET @NewDepartment = @CurValue;
				End
			End 
			IF (@CurKey = 'internalRoomID') 
			Begin
				IF @CurValue <> -1
				Begin
					SET @NewRoomID = @CurValue;
				End					
			End
			IF (@CurKey = 'patient') SET @NewPatientID = JSON_VALUE(@CurValue,'$.id');
			IF (@CurKey = 'studyDateTime') SET @NewStudyDateTime = @CurValue;				
			IF (@CurKey = 'notes') SET @NewNotes = JSON_Query(@CurValue);
			IF (@CurKey = 'notes') SET @NewHistory = JSON_Value(@CurValue,'$.history');
			IF (@CurKey = 'notes') SET @NewClinical = JSON_Value(@CurValue,'$.clinical');
			IF (@CurKey = 'notes') SET @NewComments = JSON_Value(@CurValue,'$.comments');
			IF (@CurKey = 'customField1') SET @CustomField1 = @CurValue;
			IF (@CurKey = 'customField2') SET @CustomField2 = @CurValue;
			IF (@CurKey = 'customField3') SET @CustomField3 = @CurValue;
			IF (@CurKey = 'customField4') SET @CustomField4 = @CurValue;
			IF (@CurKey = 'customField5') SET @CustomField5 = @CurValue;
			IF (@CurKey = 'customField6') SET @CustomField6 = @CurValue;
			IF (@CurKey = 'customMemo1') SET @CustomMemo1 = @CurValue;

		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
	END
	CLOSE Json_Cursor; 
	DEALLOCATE Json_Cursor; 
 	
	DECLARE @ExtJson nvarchar (max) = NULL;		
	DECLARE @TimeStamp datetime2(7);
	DECLARE @AvailabilityID nvarchar(64);
	DECLARE @BodyPartID bigint;
	DECLARE @IsBilled bit;
	DECLARE @IsPosted bit;
	DECLARE @IsHL7Update bit;
	DECLARE @StudyUID nvarchar(64);
	DECLARE @LastUpdateUtc datetime2;

	--SET @studyID = @newStudyID;		
	SET @TimeStamp = sysutcdatetime();
	SET @AvailabilityID  = (SELECT InstanceAvailabilityID FROM code.InstanceAvailability WHERE InstanceAvailabilityCode = @NewInstanceAvailabilityCode)
	SET @BodyPartID = (SELECT InternalBodyPartID FROM code.BodyPart WHERE VALUE = @NewBodyPart)
	SET @IsBilled = 0;
	SET @IsHL7Update = 0;
	SET @IsPosted = 0;
	SET @StudyUID = NEWID();
	SET @LastUpdateUtc = sysutcdatetime();
	SET @LastUpdateUserID = 100 --should be get from session

	DECLARE @ProcessedIDTab TABLE (id bigint);

	SELECT @ExtJson = ExtJson FROM Phi.Study WHERE InternalStudyID = @InternalStudyID;
	IF (@ExtJson IS NULL) OR (@ExtJson = '')
	BEGIN
		SET @ExtJson = '{ }'; -- Initialize as Json text
	END
	SET @ExtJson = JSON_MODIFY(@ExtJson, '$.customField1', @CustomField1);
	SET @ExtJson = JSON_MODIFY(@ExtJson, '$.customField2', @CustomField2);
	SET @ExtJson = JSON_MODIFY(@ExtJson, '$.customField3', @CustomField3);
	SET @ExtJson = JSON_MODIFY(@ExtJson, '$.customField4', @CustomField4);
	SET @ExtJson = JSON_MODIFY(@ExtJson, '$.customField5', @CustomField5);
	SET @ExtJson = JSON_MODIFY(@ExtJson, '$.customField6', @CustomField6);
	SET @ExtJson = JSON_MODIFY(@ExtJson, '$.customMemo1', @CustomMemo1);

	IF (@InternalStudyID > 0)
	BEGIN			
		IF (JSON_QUERY(@ExtJson, '$.notes') IS NULL)
		BEGIN				
			SET @ExtJson = JSON_MODIFY(@ExtJson, 'lax $.notes', JSON_QUERY('{"history":"","clinical":"","comments":""}'));				
		END			
		SET @ExtJson = JSON_MODIFY(@ExtJson, 'lax $.notes.history', @NewHistory);
		SET @ExtJson = JSON_MODIFY(@ExtJson, 'lax $.notes.clinical', @NewClinical);
		SET @ExtJson = JSON_MODIFY(@ExtJson, 'lax $.notes.comments', @NewComments);
		UPDATE phi.Study SET
				InternalPatientID = @NewPatientID,
				StudyID = @StudyID,
				PriorityValue = @NewPriorityValue,
				StatusValue = @NewStatusValue,
				InternalStudyTypeID = @NewStudyTypeID,
				Description = @NewStudyDescription,					
				InstanceAvailabilityID = @AvailabilityID,
				AccessionNumber = @NewAccession,
				ModalityCode = @NewModality,
				LateralityCode = @NewLaterality,
				InternalBodyPartID = @BodyPartID,
				ImagingFacilityID = @NewFacility,
				InternalDepartmentID = @NewDepartment,
				StudyDateTime = @NewStudyDateTime,
				InternalRoomID = @NewRoomID,
				--PrimaryEncounterID = @primaryEncounterID,
				ExtJson = @ExtJson,
				--Lastupdateuser = @LastupdateUserID,
				LastUpdateUserID = @LastUpdateUserID,
				LastupdateUTC = @TimeStamp
			OUTPUT inserted.InternalStudyID INTO @ProcessedIDTab
		WHERE InternalStudyID = @InternalStudyID; 
						
	END
	ELSE
	BEGIN


	INSERT INTO phi.Study(
					[InternalDepartmentID],
					[InternalStudyTypeID],
					[ModalityCode],
					[LateralityCode],
					[InternalBodyPartID],
					[PriorityValue],
					[StatusValue],
					--[ForeignDBStudyID],
					--[Lastupdateuser],
					[LastUpdateUserID],
					[LastupdateUTC],
					[StudyDateTime],
					--[StudyDateTimeUTC],
					--[StartDateTime],
					--[EndDateTime],
					[Description],
					[StudyID],					  
					[IsHL7Updated],					  
					[IsBilled],
					[IsPosted],
					--[Source],
					[ExtJson],
					[StudyUID],
					[AccessionNumber],
					[InternalPatientID],
					[ImagingFacilityID],
					[InstanceAvailabilityID],
					[InternalRoomID]
					)
			OUTPUT inserted.InternalStudyID INTO @ProcessedIDTab
			VALUES (
					@NewDepartment,
					@NewStudyTypeID,
					@NewModality,
					@NewLaterality,
					@BodyPartID,
					@NewPriorityValue,
					@NewStatusValue,
					@LastUpdateUserID,
					@LastUpdateUtc,
					@NewStudyDateTime,
					@NewStudyDescription,
					@studyID,
					@IsHL7Update,
					@IsBilled,
					@IsPosted,
					@ExtJson,
					@StudyUID,
					@NewAccession,
					@NewPatientID,
					@NewFacility,
					@AvailabilityID,
					@NewRoomID
					);
	END

	SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
END



