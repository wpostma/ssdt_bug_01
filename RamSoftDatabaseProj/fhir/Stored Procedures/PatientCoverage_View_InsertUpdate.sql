﻿CREATE PROCEDURE [fhir].[PatientCoverage_View_InsertUpdate]
		@InternalCoverageID bigint,
		@JsonText nvarchar(max),
        @InsuredID nvarchar(1000),  --encrypted value (to be)
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientCoverage_View_InsertUpdate - Start @JsonText=', substring(@JsonText, 1, 4000), @InternalCoverageID);

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @InternalPatientID bigint;
		DECLARE @IsActive bit = 1;  --default: true
		DECLARE @Payer nvarchar(64) = NULL;
		DECLARE @CoverageLevelID int;
		DECLARE @CoverageLevel nvarchar(64) = NULL;
		DECLARE @GroupID nvarchar(64) = NULL;
		DECLARE @StartDate date = NULL;
		DECLARE @EndDate date = NULL;
		DECLARE @AccidentType nvarchar(1000) = NULL;
		DECLARE @AccidentPlace nvarchar(1000) = NULL;
		DECLARE @DateOfInjury nvarchar(10) = NULL;
		DECLARE @CopayType nvarchar(10) = NULL;
		DECLARE @CopayAmount nvarchar(100) = NULL;
		DECLARE @CopayPercent nvarchar(100) = NULL;
		DECLARE @Notes nvarchar(4000) = NULL;
		DECLARE @InsuredInternalPatientID bigint;
		DECLARE @InsuredRelationshipCode nvarchar(64);
		DECLARE @InsuredRelationshipDisplay nvarchar(64);
		DECLARE @ClaimAdjustor nvarchar(max) = NULL;

		DECLARE Json_Cursor CURSOR LOCAL FOR
			  SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
    		--insert into fhir.LogTable(logitem, logdesc, logint) values(@CurKey, @CurValue, @CurType);

			IF (@CurKey = 'active') SET @IsActive = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'issuer') SET @Payer = @CurValue;

			IF (@CurKey = 'group') SET @GroupID = @CurValue;

			IF (@CurKey = 'sequence') SET @CoverageLevelID = @CurValue; 
			IF (@CurKey = 'coverageLevel') SET @CoverageLevel = @CurValue; 

			IF (@CurKey = 'copayType') SET @CopayType = @CurValue;

			IF (@CurKey = 'copayAmount')
			BEGIN
				SET @CopayAmount = (SELECT CASE WHEN TRY_CONVERT(smallmoney, @CurValue) IS NULL THEN NULL  ELSE @CurValue END);
			END

			IF (@CurKey = 'copayPercent') 
			BEGIN
				SET @CopayPercent = (SELECT CASE WHEN TRY_CONVERT(int, @CurValue) IS NULL THEN NULL  ELSE @CurValue END);
			END

			IF (@CurKey = 'period') 
			BEGIN
			   SET @StartDate = CONVERT(date, JSON_VALUE(@CurValue, '$.start'));
			   SET @EndDate = CONVERT(DATE, JSON_VALUE(@CurValue, '$.end'));
			END 

			IF (@CurKey = 'beneficiaryReference')  -- this is prior then beneficiaryIdentifier
			BEGIN
			   SET @InternalPatientID = (SELECT CASE WHEN TRY_CONVERT(bigint, JSON_VALUE(@CurValue, '$.id')) IS NULL THEN NULL  ELSE JSON_VALUE(@CurValue, '$.id') END);
			END
			IF (@CurKey = 'beneficiaryIdentifier') AND (@InternalPatientID IS NULL OR @InternalPatientID <= 0)
			BEGIN
			   SET @InternalPatientID = (SELECT CASE WHEN TRY_CONVERT(bigint, JSON_VALUE(@CurValue, '$.value')) IS NULL THEN NULL  ELSE JSON_VALUE(@CurValue, '$.value') END);
			END 

			IF (@CurKey = 'planholderReference')  -- this is prior then planholderIdentifier
			BEGIN
			   SET @InsuredInternalPatientID = (SELECT CASE WHEN TRY_CONVERT(bigint, JSON_VALUE(@CurValue, '$.id')) IS NULL THEN NULL  ELSE JSON_VALUE(@CurValue, '$.id') END);

			   IF (JSON_VALUE(@CurValue, '$.updatePatientInfo') > 0)
			   BEGIN
				  DECLARE @return_value int, @UpdateResult bigint;
				  EXEC	@return_value = [fhir].[UpdatePatientWithReferenceJson]
						@InternalPatientID = @InsuredInternalPatientID,
						@InputJson = @CurValue,
						@Result = @UpdateResult OUTPUT;
			   END
			END
			IF (@CurKey = 'planholderIdentifier') AND (@InsuredInternalPatientID IS NULL OR @InsuredInternalPatientID <= 0)
			BEGIN
			   SET @InsuredInternalPatientID = (SELECT CASE WHEN TRY_CONVERT(bigint, JSON_VALUE(@CurValue, '$.value')) IS NULL THEN NULL  ELSE JSON_VALUE(@CurValue, '$.value') END);
			END 

			IF (@CurKey = 'relationship') 
			BEGIN
			   SET @InsuredRelationshipCode = JSON_VALUE(@CurValue, '$.code');
			   SET @InsuredRelationshipDisplay = JSON_VALUE(@CurValue, '$.display');
			END

			IF (@CurKey = 'accidentType') SET @AccidentType = @CurValue;
			IF (@CurKey = 'accidentPlace') SET @AccidentPlace = @CurValue;
			IF (@CurKey = 'dateOfInjury') SET @DateOfInjury = @CurValue;

			IF (@CurKey = 'notes') SET @Notes = @CurValue;
			IF (@CurKey = 'claimAdjustor') SET @ClaimAdjustor = @CurValue;

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor; 
		DEALLOCATE Json_Cursor;
	
		DECLARE @InternalPayerID int = (SELECT InternalPayerID FROM dbo.Payer WHERE CompanyName = @Payer);

		DECLARE @InsuredRelationshipID int = (SELECT BeneficiaryRelationshipID FROM code.BeneficiaryRelationship WHERE BeneficiaryRelationshipName = @InsuredRelationshipDisplay);
		IF (@InsuredRelationshipID IS NULL) OR (@InsuredRelationshipID <= 0) 
		BEGIN
		   SET @InsuredRelationshipID = (SELECT BeneficiaryRelationshipID FROM code.BeneficiaryRelationship WHERE BeneficiaryRelationshipCode = @InsuredRelationshipCode);
		END

		IF (@CoverageLevel IS NOT NULL) AND (EXISTS(SELECT 1 FROM code.CoverageLevel WHERE CoverageLevelID = @CoverageLevelID))
		BEGIN
		   SET @CoverageLevelID = (SELECT CoverageLevelID FROM code.CoverageLevel WHERE CoverageLevel = @CoverageLevel);
		END

		DECLARE @ExtJson nvarchar(max) = null;
		IF (@InternalCoverageID > 0)
		BEGIN
  			SET @ExtJson = (SELECT ExtJson FROM phi.PatientCoverage WHERE InternalCoverageID = @InternalCoverageID);
		END

		IF (@ExtJson IS NULL) OR (@ExtJson = '')
		BEGIN	
			SET @ExtJson = '{ }'; -- Make it empty Json
		END

		IF (@CopayType IS NULL) OR (@CopayType NOT IN ('$', '%')) 
		BEGIN
			SET @CopayType = '$';
		END
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.copayType', @CopayType);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.copayAmount', @CopayAmount);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.copayPercent', @CopayPercent);

		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.notes', @Notes);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.accidentType', @AccidentType);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.accidentPlace', @AccidentPlace);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.dateOfInjury', @DateOfInjury);

		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.claimAdjustor', JSON_QUERY(@ClaimAdjustor));
	
		--insert into fhir.LogTable(logitem, logdesc, logtime) values('PatientCoverage_View_InsertUpdate @ExtJson', @ExtJson, default);

		IF (@InternalCoverageID > 0)
		BEGIN
			UPDATE phi.PatientCoverage SET
					  InternalPatientID = @InternalPatientID,
					  CoverageLevelID = @CoverageLevelID,
					  InternalPayerID = @InternalPayerID,
					  InsuredID = @InsuredID,
					  GroupID = @GroupID,
					  StartDate = @StartDate,
					  EndDate = @EndDate,
					  IsActive = @IsActive,
					  ExtJson = @ExtJson,
					  InsuredInternalPatientID = @InsuredInternalPatientID,
					  InsuredRelationshipID = @InsuredRelationshipID
					OUTPUT inserted.InternalCoverageID INTO @ProcessedIDTab
				WHERE InternalCoverageID = @InternalCoverageID;		

			--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientCoverage_View_InsertUpdate', 'UPDATED', @InternalCoverageID);
		END
		ELSE
		BEGIN
			insert into phi.PatientCoverage (
					  InternalPatientID,
					  CoverageLevelID,
					  InternalPayerID,
					  InsuredID,
					  GroupID,
					  StartDate,
					  EndDate,
					  IsActive,
					  ExtJson,
					  InsuredInternalPatientID,
					  InsuredRelationshipID)
					OUTPUT inserted.InternalCoverageID INTO @ProcessedIDTab
			   values (
					  @InternalPatientID,
					  @CoverageLevelID,
					  @InternalPayerID,
					  @InsuredID, 
					  @GroupID,
					  @StartDate,
					  @EndDate,
					  @IsActive,
					  @ExtJson,
					  @InsuredInternalPatientID,
					  @InsuredRelationshipID);

			--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientCoverage_View_InsertUpdate', 'INSERTED', @InternalCoverageID);
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
		--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientCoverage_View_InsertUpdate End', 'OK', @Result);
	END
