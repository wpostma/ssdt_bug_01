﻿CREATE PROCEDURE [fhir].[ProcCode_View_InsertUpdate]
		@InternalProcedureCodeID bigint,
		@JsonText nvarchar(max),
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @IsActive bit = 1;  --default: true
		DECLARE @ProcedureCode nvarchar(16);
		DECLARE @Description nvarchar(64);
		DECLARE @RVUTechnical int = null;
		DECLARE @RVUProfessional int = null;
		DECLARE @IsCritical bit = 0;
		DECLARE @IsBillable bit = 0;
		DECLARE @IsOfficeVisit bit = 0;
		DECLARE @IsSeenByPhysician bit = 0;
		DECLARE @IsMammo bit = 0;
		DECLARE @InternalProcedureStatusID int = 1;
		DECLARE @ProcedureStatusCode nvarchar(20) = null;
		DECLARE @ExtJson nvarchar (max) = NULL;

		DECLARE Json_Cursor CURSOR LOCAL FOR
			  SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
    		
			IF (@CurKey = 'isActive') SET @IsActive = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'RVUTechnical') SET @RVUTechnical = @CurValue;
			IF (@CurKey = 'RVUProfessional') SET @RVUProfessional = @CurValue;
			IF (@CurKey = 'isCritical') SET @IsCritical = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'isBillable') SET @IsBillable = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'isOfficeVisit') SET @IsOfficeVisit = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'isSeenByPhysician') SET @IsSeenByPhysician = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'isMammo') SET @IsMammo = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'status') SET @ProcedureStatusCode = @CurValue;

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor; 
		DEALLOCATE Json_Cursor;

		SET @ProcedureCode = JSON_VALUE(@JsonText, '$.code.coding[0].code');
		SET @Description = JSON_VALUE(@JsonText, '$.code.coding[0].display');
		IF (@Description IS NULL)
		BEGIN
		   SET @Description = JSON_VALUE(@JsonText, '$.code.text');
		END

		IF (@ProcedureStatusCode IS NOT NULL)
		BEGIN
		   SET @InternalProcedureStatusID = (SELECT InternalProcedureStatusID FROM code.ProcedureStatus WHERE (Code = @ProcedureStatusCode) OR (Display = @ProcedureStatusCode));

		   IF (@InternalProcedureStatusID IS NULL) 
		   BEGIN
		      SET @InternalProcedureStatusID = 1; -- SET DEFAULT VALUE
		   END
		END		

		IF (@InternalProcedureCodeID > 0)
		BEGIN
           SET @ExtJson = (SELECT ExtJson FROM code.ProcedureCode WHERE InternalProcedureCodeID = @InternalProcedureCodeID);
		END

		IF @ExtJson IS NULL
		BEGIN
		   SET @ExtJson = '{ }';
		END

		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.subject', JSON_query(@JsonText, '$.subject'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.alertMessage', JSON_VALUE(@JsonText, '$.alertMessage'));

		IF (@InternalProcedureCodeID > 0)
		BEGIN
			UPDATE code.ProcedureCode SET
					  ProcedureCode = @ProcedureCode,
					  [Description] = @Description,
					  RVUTechnical = @RVUTechnical,
					  RVUProfessional = @RVUProfessional,
					  IsActive = @IsActive,
					  IsCritical = @IsCritical,
					  IsBillable = @IsBillable,
					  IsOfficeVisit = @IsOfficeVisit,
					  IsSeenByPhysician = @IsSeenByPhysician,
					  IsMammo = @IsMammo,
					  ExtJson = @ExtJson,
					  InternalProcedureStatusID = @InternalProcedureStatusID
					OUTPUT inserted.InternalProcedureCodeID INTO @ProcessedIDTab
				WHERE InternalProcedureCodeID = @InternalProcedureCodeID;		
		END
		ELSE
		BEGIN
			insert into code.ProcedureCode (
					  ProcedureCode,
					  [Description],
					  RVUTechnical,
					  RVUProfessional,
					  IsActive,
					  IsCritical,
					  IsBillable,
					  IsOfficeVisit,
					  IsSeenByPhysician,
					  IsMammo,
					  ExtJson,
					  InternalProcedureStatusID)
					OUTPUT inserted.InternalProcedureCodeID INTO @ProcessedIDTab
			   values (
					  @ProcedureCode,
					  @Description,
					  @RVUTechnical,
					  @RVUProfessional,
					  @IsActive,
					  @IsCritical,
					  @IsBillable,
					  @IsOfficeVisit,
					  @IsSeenByPhysician,
					  @IsMammo,
					  @ExtJson,
					  @InternalProcedureStatusID);			
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END

