﻿CREATE PROCEDURE [fhir].[Organization_View_InsertUpdate] 
	(
		@InternalOrganizationID bigint,
		@JsonText nvarchar(max),
		@Result bigint OUTPUT
	)
	AS
	BEGIN	
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		DECLARE @ProcessedIDTab TABLE (id bigint);

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		--Declare NewFields to Insert
		DECLARE @AuthorityID bigint;
		DECLARE @LastUpdateUtc datetimeoffset;
		DECLARE @OrganizationName nvarchar(64);
		DECLARE @IsActive bit = 0;
		DECLARE @IsImaging bit = 0;
		DECLARE @IsReferring bit = 0;
		DECLARE @IsMammoTracking bit = 0;
		DECLARE @TimeZone sysname;
		DECLARE @ParentOrganizationID bigint;
		DECLARE @FeeScheduleID bigint;
		DECLARE @SelfPayFeeScheduleID bigint;

		DECLARE @ExtJson nvarchar (max) = NULL;
		IF (@InternalOrganizationID > 0)  -- update
		BEGIN
			SELECT @ExtJson = ExtJson FROM dbo.Organization WHERE InternalOrganizationID = @InternalOrganizationID; --Get the current one 
		END
		IF (@ExtJson IS NULL) OR (@ExtJson = '')
		BEGIN
			SET @ExtJson = '{ }'; -- Initialize as Json text
		END
			
		DECLARE Json_Cursor CURSOR LOCAL FOR SELECT * FROM OPENJSON(@JsonText);
		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN		
			IF (@CurKey = 'assigningAuthority') SET @AuthorityID = JSON_Value(@CurValue,'$.id');
			SET @LastUpdateUtc = sysutcdatetime();
			IF (@CurKey = 'name') SET @OrganizationName = @CurValue;
			IF (@CurKey = 'active') SET @IsActive = (CASE @CurValue WHEN 'true' THEN 1 ELSE 0 END);
			IF (@CurKey = 'imaging') SET @IsImaging = (CASE @CurValue WHEN 'true' THEN 1 ELSE 0 END);
			IF (@CurKey = 'referring') SET @IsReferring = (CASE @CurValue WHEN 'true' THEN 1 ELSE 0 END);
			IF (@CurKey = 'stana') SET @IsMammoTracking = (CASE @CurValue WHEN 'true' THEN 1 ELSE 0 END);
			IF (@CurKey = 'timeZone') SET @TimeZone = @CurValue;
			IF (@CurKey = 'partOf') SET @ParentOrganizationID = JSON_Value(@CurValue,'$.id');
			IF (@CurKey = 'feeScheduleID') SET @FeeScheduleID = @CurValue;
			IF (@CurKey = 'selfPayFeeScheduleID') SET @SelfPayFeeScheduleID = @CurValue;

			IF (@CurKey = 'identifier') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.identifier', @CurValue);
			IF (@CurKey = 'address') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.address', JSON_QUERY(@CurValue));
			IF (@CurKey = 'telecom') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.contactPoint', JSON_QUERY(@CurValue));
			IF (@CurKey = 'type') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.type', @CurValue);
			IF (@CurKey = 'preliminary') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.preliminary', JSON_QUERY(@CurValue));
			IF (@CurKey = 'final') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.final', JSON_QUERY(@CurValue));
			IF (@CurKey = 'distributeToFacilityOnly') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.distributeToFacilityOnly', (CASE @CurValue WHEN 'true' THEN 1 ELSE 0 END));
			IF (@CurKey = 'notes') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.notes', JSON_QUERY(@CurValue));
			IF (@CurKey = 'directAddress') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.directAddress', @CurValue);
			IF (@CurKey = 'rx') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.rx', (CASE @CurValue WHEN 'true' THEN 1 ELSE 0 END));

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor; 
		DEALLOCATE Json_Cursor;

		IF (@TimeZone IS NULL) OR (NOT EXISTS(SELECT 1 FROM SYS.time_zone_info WHERE name = @TimeZone))
		BEGIN
			SET @TimeZone = dbo.ServerTimeZone(); 
		END

		IF (@InternalOrganizationID > 0)
		BEGIN
			UPDATE dbo.Organization SET
					InternalAssigningAuthorityID = @AuthorityID,
					LastupdateUTC = @LastupdateUTC, 
					OrganizationName = @OrganizationName,
					IsActive = @IsActive,
					IsImaging = @IsImaging,
					IsReferring = @IsReferring,
					IsMammoTracking = @IsMammoTracking,
					TimeZone = @TimeZone,
					ParentOrganizationID = @ParentOrganizationID,
					FeeScheduleID = @FeeScheduleID,
					SelfPayFeeScheduleID = @SelfPayFeeScheduleID,
					ExtJson = @ExtJson
			OUTPUT inserted.InternalOrganizationID INTO @ProcessedIDTab
			WHERE InternalOrganizationID = @InternalOrganizationID;
		END
		ELSE
		BEGIN
			-- Insert statements for trigger here
			insert into dbo.Organization(
						[InternalAssigningAuthorityID],
						[LastupdateUTC],
						[OrganizationName],
						[IsActive],
						[IsImaging],
						[IsReferring],
						[IsMammoTracking],
						[TimeZone],
						[ParentOrganizationID],
						[FeeScheduleID],
						[SelfPayFeeScheduleID],
						[ExtJson]
						)
				OUTPUT inserted.InternalOrganizationID INTO @ProcessedIDTab
				values (
						@AuthorityID,
						@LastUpdateUtc,
						@OrganizationName,
						@IsActive,
						@IsImaging,
						@IsReferring,
						@IsMammoTracking,
						@TimeZone,
						@ParentOrganizationID,
						@FeeScheduleID,
						@SelfPayFeeScheduleID,
						@ExtJson
						);
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END
