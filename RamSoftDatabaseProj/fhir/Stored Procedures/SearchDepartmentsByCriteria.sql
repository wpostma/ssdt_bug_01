﻿CREATE PROCEDURE [fhir].[SearchDepartmentsByCriteria]
		@SearchParams nvarchar(max)
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @WhereStr nvarchar(max) = ' (d.InternalDepartmentID > 0) ';
		DECLARE @ConditionStr nvarchar(4000) = null;
		DECLARE @CurParam nvarchar(max) = null;
		DECLARE @CurKey nvarchar(max) = null;
		DECLARE @CurValue nvarchar(max) = null;
		DECLARE @ParamSplitor nvarchar(1) = '&';
		DECLARE @ValueSplitor nvarchar(1) = '=';
		DECLARE @ValueSplitorPos int;
		DECLARE @ReturnCount int = 100;
		DECLARE @HasCriteria bit = 0;

		IF (CHARINDEX('?', @SearchParams) = 1)
		BEGIN
			SET @SearchParams = STUFF(@SearchParams, 1, 1, ''); 
		END
				
		IF (@SearchParams > '')
		BEGIN
			declare param_cursor cursor LOCAL for
				select value from string_split(@SearchParams, @ParamSplitor);
		
			OPEN param_cursor;
			FETCH NEXT from param_cursor into @CurParam;

			while (@@FETCH_STATUS = 0) 
			begin
				SET @ValueSplitorPos = CHARINDEX(@ValueSplitor, @CurParam);

				IF (@ValueSplitorPos > 1)
				BEGIN
					SET @CurKey = SUBSTRING(@CurParam, 1, @ValueSplitorPos-1);
					SET @CurValue = SUBSTRING(@CurParam, @ValueSplitorPos+1, LEN(@CurParam)-@ValueSplitorPos);

					SET @CurValue = REPLACE(@CurValue, '%20', ' '); 

					SET @ConditionStr =  
					   CASE 
						   WHEN (@CurKey = 'facilityID') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'd.InternalFacilityID', 'number')
						   WHEN (@CurKey = 'facilityName') THEN  fhir.ComposeWhereCondititon (@CurKey + ':EXACT', @CurValue, 'o.OrganizationName', default)
						   WHEN (@CurKey = 'name') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'd.Department', default)
						   WHEN (@CurKey = 'roomID') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'r.InternalRoomID', 'number')
						   WHEN (@CurKey = 'roomName') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'r.Name', default)
						   ELSE NULL
   					   END

					IF (@ConditionStr IS NOT NULL) AND (@ConditionStr != '')
					BEGIN
					   IF (@WhereStr IS NULL) 
					   BEGIN
						  SET @WhereStr =  @ConditionStr; 
					   END
					   ELSE
					   BEGIN
						  SET @WhereStr = @WhereStr + ' AND ' + @ConditionStr; 
					   END
				   END

				   IF (@CurKey = 'Count') 
				   BEGIN
				      SET @ReturnCount = CASE WHEN Try_CAST(@CurValue as int) IS NOT NULL THEN CAST(@CurValue as int) ELSE @ReturnCount END;
				   END
				END

  				FETCH NEXT from param_cursor into @CurParam;
			end
			CLOSE param_cursor;
		END

		DECLARE @Dep TABLE ( depID bigint);

		DECLARE @SQL nvarchar(max) = 'SELECT DISTINCT top ' + CAST(@ReturnCount AS nvarchar) + ' d.InternalDepartmentID from dbo.[Department] d JOIN dbo.Organization o ON o.InternalOrganizationID = d.InternalFacilityID ' + 
		                             ' LEFT JOIN dbo.Room r ON d.InternalDepartmentID = r.InternalDepartmentID '; 

		IF (@WhereStr IS NOT NULL)
		BEGIN
		   SET @SQL = @SQL + ' WHERE ' + @WhereStr;
		END

		IF (((@SearchParams > '') AND (@WhereStr IS NOT NULL)) OR (@SearchParams = ''))
		BEGIN
		   INSERT INTO @Dep
		      EXEC (@SQL);
		END

		SELECT v.* FROM @Dep d
			  JOIN fhir.Department_View v on v.InternalDepartmentID = d.depID 
			  JOIN dbo.Department dp on dp.InternalDepartmentID = d.depID
			  ORDER BY dp.Department;
	END
