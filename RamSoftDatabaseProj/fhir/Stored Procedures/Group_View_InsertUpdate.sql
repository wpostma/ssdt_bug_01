﻿CREATE PROCEDURE [fhir].[Group_View_InsertUpdate]
		@InternalGroupID bigint,
		@JsonText nvarchar(max),
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DECLARE @IsActive bit = 1;  --default: true
		DECLARE @GroupName nvarchar(64) = NULL;
		DECLARE @ExtJson nvarchar(max) = NULL;
		DECLARE @AccessType bigint = null;
		DECLARE @AccessTypeName nvarchar(256) = null;
		DECLARE @StatusStart int = null;
		DECLARE @StatusNameStart nvarchar(64) = null;
		DECLARE @StatusEnd int = null;
		DECLARE @StatusNameEnd nvarchar(64) = null;
		
		IF (ISJSON(@JsonText) > 0)
		BEGIN
		   SET @IsActive = JSON_VALUE(@JsonText, '$.active');
		   SET @GroupName = JSON_VALUE(@JsonText, '$.name');

		   SET @AccessType = JSON_VALUE(@JsonText, '$.accessType');
		   SET @AccessTypeName = JSON_VALUE(@JsonText, '$.accessTypeName');

		   IF (@AccessType IS NULL) AND (@AccessTypeName IS NOT NULL)
		   BEGIN
		      SET @AccessType = (SELECT TOP 1 AccessTypeID FROM code.[AccessType] WHERE AccessTypeName = @AccessTypeName);
		   END

		   SET @StatusStart = JSON_VALUE(@JsonText, '$.statusStart');
		   SET @StatusNameStart = JSON_VALUE(@JsonText, '$.statusNameStart');

		   IF (@StatusStart IS NULL) AND (@StatusNameStart IS NOT NULL)
		   BEGIN
		      SET @StatusStart = (SELECT TOP 1 StatusValue FROM code.[Status] WHERE StatusName = @StatusNameStart);
		   END

		   SET @StatusEnd = JSON_VALUE(@JsonText, '$.statusEnd');
		   SET @StatusNameEnd = JSON_VALUE(@JsonText, '$.statusNameEnd');

		   IF (@StatusEnd IS NULL) AND (@StatusNameEnd IS NOT NULL)
		   BEGIN
		      SET @StatusEnd = (SELECT TOP 1 StatusValue FROM code.[Status] WHERE StatusName = @StatusNameEnd);
		   END
		   
		   IF (@InternalGroupID > 0)
		   BEGIN
		      SET @ExtJson = (SELECT ExtJson from dbo.[group] where InternalGroupID = @InternalGroupID) ;
		   END
		   
		   IF (@ExtJson IS NULL)
		   BEGIN
		      SET @ExtJson = '{ }';
		   END
		   
		   SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.accessType', @AccessType);
		   SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.statusStart', @StatusStart);
		   SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.statusEnd', @StatusEnd);
		   SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.accessAllFacilities', JSON_VALUE(@JsonText, '$.accessAllFacilities'));
		   SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.accessAllIssuers', JSON_VALUE(@JsonText, '$.accessAllIssuers'));
		   SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.accessAllPriorStudies', JSON_VALUE(@JsonText, '$.accessAllPriorStudies'));
		   SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.issuers', JSON_QUERY(@JsonText, '$.issuers'));
			
		   IF (@IsActive IS NULL)
		   BEGIN
		      SET @IsActive = 1;
		   END

		   IF (@InternalGroupID > 0)
		   BEGIN
			   UPDATE dbo.[Group] SET
				   	  GroupName = @GroupName,
					  IsActive = @IsActive,
					  ExtJson = @ExtJson
					OUTPUT inserted.InternalGroupID INTO @ProcessedIDTab
				WHERE (InternalGroupID = @InternalGroupID);
		   END
		   ELSE
		   BEGIN
			  INSERT INTO dbo.[Group] (
					  GroupName,
					  IsActive,
					  ExtJson)
					OUTPUT inserted.InternalGroupID INTO @ProcessedIDTab
			   VALUES (
					  @GroupName,
					  @IsActive,
					  @ExtJson);			
		   END

		   SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);

		END
		ELSE
		BEGIN
		   SET @Result = -1;
		END
	END
