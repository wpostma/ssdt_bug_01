﻿
CREATE PROCEDURE [fhir].[Organization_View_Delete] 
(
	@InternalOrganizationID bigint,
	@Result bigint OUTPUT
)
AS
BEGIN	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	

	DECLARE @ProcessedIDTab TABLE (id bigint);
	DELETE FROM dbo.Organization 
		OUTPUT deleted.InternalOrganizationID INTO @ProcessedIDTab
		WHERE InternalOrganizationID = @InternalOrganizationID;
	SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
END
