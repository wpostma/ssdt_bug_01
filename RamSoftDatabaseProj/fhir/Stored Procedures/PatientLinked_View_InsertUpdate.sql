﻿	CREATE PROCEDURE [fhir].[PatientLinked_View_InsertUpdate]
		@EntryID bigint,
		@InternalPatientID bigint,
	    @JsonText nvarchar(max),
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientLinked_View_InsertUpdate - Start @JsonText=', substring(@JsonText, 1, 4000), @EntryID);

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @LinkedInternalPatientID bigint;
		DECLARE @LinkageTypeCode nvarchar(64);
		DECLARE @LinkageTypeName nvarchar(64);

		DECLARE Json_Cursor CURSOR LOCAL FOR
			  SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
    		insert into fhir.LogTable(logitem, logdesc, logint) values(@CurKey, @CurValue, @CurType);

			IF (@CurKey = 'id') 
			BEGIN
				SET @EntryID = @CurValue;
			END

			IF (@CurKey = 'item') 
			BEGIN
				--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientLinked_View_InsertUpdate -  ITEM[0]=', @CurValue, @InternalPatientID);

				DECLARE @ItemJson nvarchar(max) = (select top 1 [value] from OPENJSON(@CurValue)); 
				IF (@ItemJson IS NULL) SET @ItemJson = @CurValue;

			    SET @LinkedInternalPatientID = JSON_VALUE(@ItemJson, '$.resource.id'); 
				SET @LinkageTypeCode = JSON_VALUE(@ItemJson, '$.type');
				SET @LinkageTypeName = JSON_VALUE(@ItemJson, '$.typeName');

				insert into fhir.LogTable(logitem, logdesc, logint) values('PatientLinked_View_InsertUpdate - ', @LinkageTypeCode, @LinkedInternalPatientID);
			END 

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor; 
		DEALLOCATE Json_Cursor;

		DECLARE @LinkageTypeID int = (SELECT LinkageTypeID FROM code.LinkageType WHERE LinkageTypeCode = @LinkageTypeCode);
		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientLinked_View_InsertUpdate - @LinkageTypeCode ', @LinkageTypeCode, @LinkageTypeID);
		IF (@LinkageTypeID IS NULL) 
		BEGIN
 			SET @LinkageTypeID = (SELECT LinkageTypeID FROM code.LinkageType WHERE LinkageTypeName = @LinkageTypeName);
			insert into fhir.LogTable(logitem, logdesc, logint) values('PatientLinked_View_InsertUpdate - @@LinkageTypeName ', @LinkageTypeName, @LinkageTypeID);
		END

		IF (@EntryID > 0)
		BEGIN
		    UPDATE dbo.PatientLinked SET LinkageTypeID = @LinkageTypeID 
				OUTPUT inserted.EntryID INTO @ProcessedIDTab
				WHERE EntryID = @EntryID;
		END
		ELSE IF (@InternalPatientID > 0) AND (@LinkedInternalPatientID > 0) AND (@InternalPatientID <> @LinkedInternalPatientID) 
		BEGIN 
		   -- Find LinkID of current patient first
		   DECLARE @LinkIDOfBasePatient int = (SELECT LinkID FROM dbo.PatientLinked WHERE InternalPatientID = @InternalPatientID);
		   DECLARE @LinkIDOfLinkedPatient int = (SELECT LinkID FROM dbo.PatientLinked WHERE InternalPatientID = @LinkedInternalPatientID);
		   IF (@LinkIDOfBasePatient > 0) AND (@LinkIDOfLinkedPatient IS NULL OR @LinkIDOfLinkedPatient = 0)
		   BEGIN
		      -- Already linked to other patients, Just add with curent Linkedpatient
			  INSERT INTO dbo.PatientLinked(InternalPatientID, LinkID, LinkageTypeID) 
	  				OUTPUT inserted.EntryID INTO @ProcessedIDTab
				VALUES (@LinkedInternalPatientID, @LinkIDOfBasePatient, @LinkageTypeID);
		   END
		   ELSE IF (@LinkIDOfLinkedPatient > 0) AND (@LinkIDOfBasePatient IS NULL OR @LinkIDOfBasePatient = 0)
		   BEGIN
		      -- Add base Patient with LinkID of Linked patient
			  INSERT INTO dbo.PatientLinked(InternalPatientID, LinkID) 
  	  				OUTPUT inserted.EntryID INTO @ProcessedIDTab
				VALUES (@InternalPatientID, @LinkIDOfLinkedPatient);  --don't know linkage type yet.
		   END
		   ELSE IF (@LinkIDOfBasePatient > 0) AND (@LinkIDOfLinkedPatient > 0)
		   BEGIN
		       -- both have already linked then we need to merge them, just update @LinkIDOfBasePatient to @LinkIDOfLinkedPatient 
			   UPDATE dbo.PatientLinked SET LinkID = @LinkIDOfLinkedPatient 
					OUTPUT inserted.EntryID INTO @ProcessedIDTab
					WHERE LinkID = @LinkIDOfBasePatient;
		   END
		   ELSE
		   BEGIN
		      -- Generate next LinkID
			   DECLARE @NewLinkID int = (SELECT Max(LinkID) FROM dbo.PatientLinked);
			   IF (@NewLinkID >= 0) SET @NewLinkID = @NewLinkID + 1
			   ELSE SET @NewLinkID = 1;

			  -- Add both withnew LinkID - base and linked patient 
			  INSERT INTO dbo.PatientLinked(InternalPatientID, LinkID, LinkageTypeID) VALUES (@InternalPatientID, @NewLinkID, 
					(SELECT LinkageTypeID FROM code.LinkageType WHERE LinkageTypeCode = 'source'));  -- set LinkageType as source.

			  INSERT INTO dbo.PatientLinked(InternalPatientID, LinkID, LinkageTypeID) 
   	  				OUTPUT inserted.EntryID INTO @ProcessedIDTab
				VALUES (@LinkedInternalPatientID, @NewLinkID, @LinkageTypeID);
		   END
		END
		
		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientLinked_View_InsertUpdate - End', 'OK', @Result);
	END
