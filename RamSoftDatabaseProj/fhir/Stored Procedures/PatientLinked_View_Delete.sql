﻿CREATE PROCEDURE [fhir].[PatientLinked_View_Delete]
		@EntryID bigint,
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		--insert into fhir.LogTable(logitem, logint) values('PatientLinked_View_Delete - Start @EntryID', @EntryID);
		DELETE FROM dbo.PatientLinked	
			OUTPUT deleted.EntryID INTO @ProcessedIDTab
			WHERE EntryID = @EntryID; 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);

		--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientLinked_View_Delete - End', 'OK', @Result);
	END
