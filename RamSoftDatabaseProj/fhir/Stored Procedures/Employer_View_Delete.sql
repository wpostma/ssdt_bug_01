﻿CREATE PROCEDURE [fhir].[Employer_View_Delete]
		@InternalEmployerID bigint,
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;	
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DELETE FROM dbo.Employer	
			OUTPUT deleted.InternalEmployerID INTO @ProcessedIDTab
			WHERE InternalEmployerID = @InternalEmployerID; 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END
