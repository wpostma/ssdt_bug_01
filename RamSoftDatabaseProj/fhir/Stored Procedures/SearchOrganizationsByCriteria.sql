﻿
	CREATE PROCEDURE [fhir].[SearchOrganizationsByCriteria]
		@SearchParams nvarchar(max)
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @WhereStr nvarchar(max) = null;
		DECLARE @ConditionStr nvarchar(4000) = null;
		DECLARE @CurParam nvarchar(max) = null;
		DECLARE @CurKey nvarchar(max) = null;
		DECLARE @CurValue nvarchar(max) = null;
		DECLARE @ParamSplitor nvarchar(1) = '&';
		DECLARE @ValueSplitor nvarchar(1) = '=';
		DECLARE @ValueSplitorPos int;

		IF (CHARINDEX('?', @SearchParams) = 1)
		BEGIN
			SET @SearchParams = STUFF(@SearchParams, 1, 1, ''); 
		END
		--insert into fhir.LogTable(logItem, logdesc) values('SearchOrganizationsByCriteria @SearchParams=', @SearchParams);
		
		IF (@SearchParams > '')
		BEGIN
			declare param_cursor cursor LOCAL for
				select value from string_split(@SearchParams, @ParamSplitor);
		
			OPEN param_cursor;
			FETCH NEXT from param_cursor into @CurParam;

			while (@@FETCH_STATUS = 0) 
			begin
				SET @ValueSplitorPos = CHARINDEX(@ValueSplitor, @CurParam);

				IF (@ValueSplitorPos > 1)
				BEGIN
					SET @CurKey = SUBSTRING(@CurParam, 1, @ValueSplitorPos-1);
					SET @CurValue = SUBSTRING(@CurParam, @ValueSplitorPos+1, LEN(@CurParam)-@ValueSplitorPos);

					SET @CurValue = REPLACE(@CurValue, '%20', ' '); 

					SET @ConditionStr =  
					   CASE 
						   WHEN (@CurKey = 'name') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'o.OrganizationName', default)
						   WHEN (@CurKey = 'issuer') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'a.Name', default)
						   WHEN (@CurKey = 'issuerId') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'a.InternalAssigningAuthorityID', 'number')
						END

					IF (@ConditionStr IS NOT NULL) AND (@ConditionStr != '')
					BEGIN
					   IF (@WhereStr IS NULL) 
					   BEGIN
						  SET @WhereStr =  @ConditionStr; 
					   END
					   ELSE
					   BEGIN
						  SET @WhereStr = @WhereStr + ' AND ' + @ConditionStr; 
					   END
				   END
				END

  				FETCH NEXT from param_cursor into @CurParam;
			end
			CLOSE param_cursor;
		END

		DECLARE @Org TABLE ( orgID bigint);

		DECLARE @SQL nvarchar(max) = 'SELECT top 100 InternalOrganizationID from dbo.Organization o ' +
			'JOIN dbo.AssigningAuthority a ON a.InternalAssigningAuthorityID = o.InternalAssigningAuthorityID '; 

		IF (@WhereStr IS NOT NULL)
		BEGIN
		   SET @SQL = @SQL + ' WHERE ' + @WhereStr;
		END
		SET @SQL = @SQL + ' ORDER BY O.OrganizationName ';
		--insert into fhir.LogTable(logItem, logdesc) values('SearchOrganizationsByCriteria', @SQL);

		insert into @Org
		   EXEC (@SQL);

		-- return searched organizations reading from View
		SELECT o.* FROM @Org r
			  JOIN fhir.Organization_View o on r.orgID = o.InternalOrganizationID;
	END
