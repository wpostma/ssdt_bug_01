﻿CREATE PROCEDURE [fhir].[SearchUsersForMailbox]
		@UserName nvarchar(64),
		@MailBoxJSON nvarchar(max)
	AS
	BEGIN
	    -- Meta-Search user by userName, which should belong to the facilities of the direct address registered and do not need to show the same user which already in the same mailbox

		SET NOCOUNT ON;

		DECLARE @DirectAddressArrayJSON nvarchar(max);
		DECLARE @UserArrayJSON nvarchar(max);
		DECLARE @WhereStr nvarchar(max) = null;
		DECLARE @SQL nvarchar(max);
		DECLARE @ArrayNum int = 0;
		DECLARE @FacilityIDs nvarchar(max) = null;
		DECLARE @FacilityID bigint;
		DECLARE @UserIDs nvarchar(max) = null;
		DECLARE @UserID bigint;

		DECLARE @ReturnCount int = 100;

		--Declare a temp table it will be use to store all the internalUserIDs 
		DECLARE @UserMapTemp TABLE (
			internalUserID int
		);

		IF (@UserName > '')
		BEGIN
			SET @WhereStr = '((u.UserName  LIKE ''' + @UserName + '%'') OR (u.Name  LIKE ''' + @UserName + '%''))';		

			SET @SQL = 'SELECT DISTINCT TOP ' + CAST(@ReturnCount AS nvarchar) + ' u.InternalUserID as internalUserID '+ 
						'FROM dbo.[User_View] u ' +
						'LEFT JOIN dbo.USERFACILITYMAP m ON m.InternalUserID = u.InternalUserID ';			

			IF (ISJSON(@MailBoxJSON) > 0)
			BEGIN
			   SET @DirectAddressArrayJSON = JSON_QUERY(@MailBoxJSON, '$.directAddresses');

			   IF (ISJSON(@DirectAddressArrayJSON) > 0)
			   BEGIN
			      SET @ArrayNum = (SELECT Count(JSON_VALUE([value], '$.internalFacilityID')) from openjson(@DirectAddressArrayJSON));

				  IF (@ArrayNum > 0)
				  BEGIN
				     --Declare Address_Cursor for looping throught all failities of address
				     DECLARE Address_Cursor CURSOR LOCAL FOR SELECT DISTINCT JSON_VALUE([value], '$.internalFacilityID') as internalFacilityID from openjson(@DirectAddressArrayJSON);	

					 OPEN Address_Cursor;
					 FETCH NEXT FROM Address_Cursor INTO @FacilityID;
					 WHILE (@@FETCH_STATUS = 0) 
					 BEGIN
					 	IF (@FacilityID > 0)
						BEGIN
						   IF (@FacilityIDs IS NOT NULL)
						   BEGIN
						      SET @FacilityIDs = @FacilityIDs + ', ' + CAST(@FacilityID as nvarchar(19));
						   END
						   ELSE
						   BEGIN
						      SET @FacilityIDs = CAST(@FacilityID as nvarchar(19));
						   END;
						END
						FETCH NEXT FROM Address_Cursor INTO @FacilityID;
					 END	
					 CLOSE Address_Cursor;
					 DEALLOCATE Address_Cursor; 
				  END
			   END

			   SET @UserArrayJSON = JSON_QUERY(@MailBoxJSON, '$.users');
			   IF (ISJSON(@UserArrayJSON) > 0)
			   BEGIN
			      SET @ArrayNum = (SELECT Count(JSON_VALUE([value], '$.id')) from openjson(@UserArrayJSON));

				  IF (@ArrayNum > 0)
				  BEGIN
				     --Declare User_Cursor for looping throught all users
				     DECLARE User_Cursor CURSOR LOCAL FOR SELECT DISTINCT JSON_VALUE([value], '$.id') as internalUserID from openjson(@UserArrayJSON);	

					 OPEN User_Cursor;
					 FETCH NEXT FROM User_Cursor INTO @UserID;
					 WHILE (@@FETCH_STATUS = 0) 
					 BEGIN
					 	IF (@UserID > 0)
						BEGIN
						   IF (@UserIDs IS NOT NULL)
						   BEGIN
						      SET @UserIDs = @UserIDs + ', ' + CAST(@UserID as nvarchar);
						   END
						   ELSE
						   BEGIN
						      SET @UserIDs = CAST(@UserID as nvarchar);
						   END;
						END
						FETCH NEXT FROM User_Cursor INTO @UserID;
					 END	
					 CLOSE User_Cursor;
					 DEALLOCATE User_Cursor; 
				  END
			   END
			END
						
			IF (@FacilityIDs IS NOT NULL)
			BEGIN
			    SET @WhereStr = @WhereStr + 'AND (m.InternalFacilityID in (' + @FacilityIDs + ')) ';				 
			END 

			IF (@UserIDs IS NOT NULL)
			BEGIN
			    SET @WhereStr = @WhereStr + 'AND (m.InternalUserID not in (' + @UserIDs + ')) ';				 
			END 

			SET @SQL = @SQL + ' WHERE ' + @WhereStr;

		    INSERT INTO @UserMapTemp
		      EXEC (@SQL);

			SELECT u.* FROM dbo.[User_View] u
					   JOIN @UserMapTemp m ON m.InternalUserID = u.InternalUserID 
						WHERE (u.IsUserActive = 1) AND (u.InternalUserID > 0)
						ORDER BY u.UserName;

		END
	END

