﻿CREATE PROCEDURE [fhir].[PatientFamilyHistory_View_InsertUpdate]
		@InternalFamilyHistoryID bigint,
		@JsonText nvarchar(max),
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientFamilyHistory_View_InsertUpdate - Start @JsonText=', substring(@JsonText, 1, 4000), @InternalFamilyHistoryID);

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @InternalPatientID bigint = NULL;
   		DECLARE @DateModified DateTime2(7) = NULL;
		DECLARE @RelationshipID int = NULL;
   		DECLARE @Relationship nvarchar(64) = NULL;
   		DECLARE @RelationshipDisplay nvarchar(512) = NULL;
 		DECLARE @ProblemCodeID bigint = NULL;
 		DECLARE @ProblemCode nvarchar(64) = NULL;
 		DECLARE @ProblemDescription nvarchar(512) = NULL;
		DECLARE @StatusID int = NULL;
		DECLARE @Status nvarchar(64) = NULL;
		DECLARE @Note nvarchar(1000) = NULL;

		DECLARE Json_Cursor CURSOR LOCAL FOR
			  SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
    		--insert into fhir.LogTable(logitem, logdesc, logint) values(@CurKey, @CurValue, @CurType);

			IF (@CurKey = 'id') AND (@InternalFamilyHistoryID <= 0 OR @InternalFamilyHistoryID IS NULL)
			BEGIN
			  SET @InternalFamilyHistoryID = @CurValue;
			END

			IF (@CurKey = 'patient') 
			BEGIN
			  SET @InternalPatientID = JSON_VALUE(@CurValue, '$.id');
			END

			--IF (@CurKey = 'date') SET @DateModified = (SELECT CASE WHEN TRY_CONVERT(datetime2(7), @CurValue) IS NULL THEN NULL ELSE @CurValue END);

			IF (@CurKey = 'condition')
			BEGIN
			    IF (IsJson(@CurValue) = 0)
				BEGIN
				    SET @ProblemCode = @CurValue;
				END
				ELSE
				BEGIN
					IF (JSON_VALUE(@CurValue, '$.code.text') IS NOT NULL) 
						SET @ProblemCode = JSON_VALUE(@CurValue, '$.code.text');
					ELSE 
					BEGIN
						SET @ProblemCode = JSON_VALUE(@CurValue, '$.code.coding.code');
						SET @ProblemDescription = JSON_VALUE(@CurValue, '$.code.coding.display')
					END
				END
			END

			IF (@CurKey = 'relationship') 
			BEGIN
			    IF (IsJson(@CurValue) = 0) 
				BEGIN
					SET @Relationship = @CurValue;
				END
				ELSE 
				BEGIN
					SET @Relationship = JSON_VALUE(@CurValue, '$.coding.code');
					SET @RelationshipDisplay = JSON_VALUE(@CurValue, '$.coding.display');
				END
			END

			IF (@CurKey = 'status') SET @Status = @CurValue;

			IF (@CurKey = 'note')
			BEGIN
				IF (JSON_VALUE(@CurValue, '$.text') IS NOT NULL) 
					SET @Note = JSON_VALUE(@CurValue, '$.text');
				ELSE 
					SET @Note = @CurValue;
			END

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor;
		DEALLOCATE Json_Cursor; 

     	insert into fhir.LogTable(logitem, logdesc, logtime) values('PatientFamilyHistory_View_InsertUpdate - Found @Code', @ProblemCode, default);

		SET @ProblemCodeID = (SELECT InternalProblemCodeID FROM code.Problem where ProblemCode = @ProblemCode);
		IF (@ProblemCodeID IS NULL) OR (@ProblemCodeID = 0)  
		BEGIN
			SET @ProblemCodeID = (SELECT InternalProblemCodeID FROM code.Problem where [Description] = @ProblemDescription);
		END 

		SET @RelationshipID = (SELECT FamilyMemberID FROM code.FamilyMember WHERE FamilyMemberName = @RelationshipDisplay);
		IF (@RelationshipID IS NULL) OR (@RelationshipID = 0)  
		BEGIN
			SET @RelationshipID = (SELECT FamilyMemberID FROM code.FamilyMember WHERE FamilyMemberCode = @Relationship);
		END 

		SET @StatusID = (SELECT TOP 1 FamilyHistoryStatusID FROM code.FamilyHistoryStatus where FamilyHistoryStatusName = @Status OR FamilyHistoryStatusCode = @Status);

		DECLARE @ExtJson nvarchar(max) = NULL;
		IF (@InternalFamilyHistoryID > 0)
		BEGIN
			SET @ExtJson = (Select ExtJson from dbo.PatientFamilyHistory where InternalFamilyHistoryID = @InternalFamilyHistoryID);
		END

		IF (@ExtJson IS NULL) 
		BEGIN
			SET @ExtJson = '{ }';   --make an empty json
		END

		IF (IsJson(@Note) = 0) SET @ExtJson = JSON_MODIFY(@ExtJson, '$.note', @Note);
		ELSE SET @ExtJson = JSON_MODIFY(@ExtJson, '$.note', JSON_QUERY(@Note)); 

		SET @DateModified = SYSDATETIME();

		IF (@InternalFamilyHistoryID > 0)
		BEGIN
			UPDATE dbo.PatientFamilyHistory SET 
						InternalPatientID = @InternalPatientID,
						InternalProblemCodeID = @ProblemCodeID,
						FamilyMemberID = @RelationshipID,
						DateModified = @DateModified,
						FamilyHistoryStatusID = @StatusID,
						ExtJson = @ExtJson 
					OUTPUT inserted.InternalFamilyHistoryID INTO @ProcessedIDTab
				WHERE InternalFamilyHistoryID = @InternalFamilyHistoryID;

			insert into fhir.LogTable(logitem, logdesc, logint) values('PatientFamilyHistory_View_InsertUpdate - ', 'UPDATED', @InternalFamilyHistoryID);
		END
		ELSE
		BEGIN
			INSERT INTO dbo.PatientFamilyHistory(
						InternalPatientID,
						InternalProblemCodeID,
						FamilyMemberID,
						DateModified,
						FamilyHistoryStatusID,
						ExtJson)
					OUTPUT inserted.InternalFamilyHistoryID INTO @ProcessedIDTab
				VALUES(
						@InternalPatientID,
						@ProblemCodeID,
						@RelationshipID,
						@DateModified,
						@StatusID,
						@ExtJson);

			insert into fhir.LogTable(logitem, logdesc, logint) values('PatientFamilyHistory_View_InsertUpdate - ', 'INSERTED', @InternalFamilyHistoryID);
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);

		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientFamilyHistory_View_InsertUpdate - End', 'OK', @Result);
	END
