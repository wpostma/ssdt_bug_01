﻿
	CREATE PROCEDURE [fhir].[UpdatePatientWithReferenceJson] 
		@InternalPatientID BIGINT = 0,
		@InputJson nvarchar(max),
	    @Result bigint OUTPUT
	AS
	BEGIN
		SET NOCOUNT ON;
		SET @Result = 0;

		IF (ISJSON(@InputJson) =  1) AND 
		   (EXISTS (SELECT 1 FROM PHI.Patient WHERE InternalPatientID = @InternalPatientID))
		BEGIN
			DECLARE @PatientName nvarchar(64) = NULL;
			DECLARE	@SSN nvarchar(64) = NULL;
			DECLARE	@BirthDate nvarchar(10) = NULL;

			DECLARE @ContactPointJson nvarchar (4000) = NULL;
			DECLARE @SexID int = NULL;
			DECLARE @State nvarchar (64) = NULL;
			DECLARE @CountryCode char (2) = NULL;
			DECLARE @StateID int = NULL;
			DECLARE @CountryID int = NULL;
			DECLARE @MaritalStatusID int = NULL;
			DECLARE @Address nvarchar(4000) = NULL;

			DECLARE @EmploymentStatus nvarchar(64) = NULL;
			DECLARE @InternalEmployerID bigint = NULL; 
			DECLARE @ExtJson nvarchar (max) = NULL;

			DECLARE @CurKey NVARCHAR(512);
 			DECLARE	@CurValue NVARCHAR(MAX);
			DECLARE	@CurType int;

			DECLARE Json_Cursor CURSOR LOCAL FOR
					SELECT * FROM OPENJSON(@InputJson);

			OPEN Json_Cursor;
			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

			WHILE (@@FETCH_STATUS = 0) 
			BEGIN
				--IF (@CurKey = 'name') SET @PatientName = fhir.ParseHumanNameJson(@CurValue);
				IF (@CurKey = 'gender') SET @SexID = (SELECT SexID FROM code.Sex WHERE Sex = @CurValue);
				IF (@CurKey = 'birthdate') SET @BirthDate = @CurValue;
				IF (@CurKey = 'ssn') SET @Ssn = @CurValue;
				IF (@CurKey = 'telecom') SET @ContactPointJson = @CurValue;
				
				IF (@CurKey = 'maritalStatus') SET @MaritalStatusID = fhir.GetMaritalStatusIDByJson(@CurValue);
				IF (@CurKey = 'employmentStatus') SET @EmploymentStatus = @CurValue;
				IF (@CurKey = 'employer') 
				BEGIN
					SET @InternalEmployerID = (SELECT CASE WHEN TRY_CONVERT(bigint, JSON_VALUE(@CurValue, '$.id')) IS NULL THEN NULL ELSE JSON_VALUE(@CurValue, '$.id') END);
					IF (@InternalEmployerID <= 0) OR (@InternalEmployerID IS NULL)
					BEGIN
						SET @InternalEmployerID = (SELECT InternalEmployerID FROM dbo.Employer WHERE EmployerName = JSON_VALUE(@CurValue, '$.name'));
					END  
				END

				IF (@CurKey = 'address') 
				BEGIN
					SET @Address = @CurValue;

					SET @CountryID = NULL;
					SET @StateID = NULL;
					SELECT TOP 1 @State = JSON_VALUE([value], '$.state'), @CountryCode = JSON_VALUE([value], '$.country')
						FROM OPENJSON(@CurValue);
       
					SELECT @CountryID = CountryID from dbo.Country where CountryCode = @CountryCode;
					IF (@CountryID IS NULL) OR (@CountryID <= 0)
					BEGIN
						SELECT @CountryID = CountryID from dbo.Country where CountryName = @CountryCode;
					END

					SELECT @StateID = StateID from code.State where [State] = @State and CountryID = @CountryID;
					IF (@StateID IS NULL) OR (@StateID <= 0)
					BEGIN
						SELECT @StateID = StateID from code.State where StateCode = @State and CountryID = @CountryID;
					END
				END

				FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
			END
			CLOSE Json_Cursor; 
			DEALLOCATE Json_Cursor; 
 
			SELECT @ExtJson = ExtJson FROM phi.Patient WHERE InternalPatientID = @InternalPatientID; --Get the current one 
			IF (@ExtJson IS NULL) OR (@ExtJson = '')
			BEGIN
				SET @ExtJson = '{ }'; -- Initialize as Json text
			END

			SET @ExtJson = JSON_MODIFY(@ExtJson, '$.address', JSON_QUERY(@Address));
			SET @ExtJson = JSON_MODIFY(@ExtJson, '$.contactPoint', JSON_QUERY(@ContactPointJson));
			SET @ExtJson = JSON_MODIFY(@ExtJson, '$.employmentStatus', @EmploymentStatus);

			UPDATE phi.patient SET
					--PatientName = @PatientName, 
					BirthDate = @BirthDate,  
					SexID= @SexID, 
					StateID = @StateID, 
					CountryID = @CountryID, 
					Ssn = @Ssn, 
					ExtJson = @ExtJson, 
					MaritalStatusID = @MaritalStatusID,
					InternalEmployerID = @InternalEmployerID 
			WHERE InternalPatientID = @InternalPatientID; 
		END

		RETURN @Result
	END
