﻿CREATE PROCEDURE [fhir].[PatientAlert_View_InsertUpdate]
		@InternalPatientAlertID bigint,
		@JsonText nvarchar(max),
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @InternalPatientID bigint;
		DECLARE @Summary nvarchar(max) = NULL;
		DECLARE @Recipient nvarchar(max) = NULL;
		DECLARE @StartDate date = NULL;
		DECLARE @EndDate date = NULL;
		DECLARE @UserName nvarchar(64) = NULL;
		DECLARE @InternalUserID int = 0;

		DECLARE Json_Cursor CURSOR LOCAL FOR
			  SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
    		--insert into fhir.LogTable(logitem, logdesc, logint) values(@CurKey, @CurValue, @CurType);

			IF (@CurKey = 'id') AND (@InternalPatientAlertID <= 0 OR @InternalPatientAlertID IS NULL)
			BEGIN
			  SET @InternalPatientAlertID = @CurValue;
			END

			IF (@CurKey = 'subject') 
			BEGIN
			  SET @InternalPatientID = JSON_VALUE(@CurValue, '$.id');
			END

			IF (@CurKey = 'author') 
			BEGIN
			   SET @UserName = substring(JSON_VALUE(@CurValue, '$.display'), 1, 64);
			   SET @InternalUserID = (SELECT InternalUserID FROM dbo.[User] WHERE UserName = @UserName);
			   IF (@InternalUserID IS NULL) OR (@InternalUserID = 0)
			   BEGIN
				   SET @InternalUserID = (CASE TRY_CONVERT(int, JSON_VALUE(@CurValue, '$.id')) WHEN NULL THEN NULL ELSE JSON_VALUE(@CurValue, '$.id') END);
			   END
    		   insert into fhir.LogTable(logitem, logdesc, logint) values(@CurKey, @CurValue, @InternalUserID);
			END

			IF (@CurKey = 'code') SET @Summary = Json_Value(@CurValue, '$.text');
			IF (@CurKey = 'recipient') SET @Recipient = @CurValue;

			IF (@CurKey = 'period') 
			BEGIN
			   SET @StartDate = CONVERT(date, JSON_VALUE(@CurValue, '$.start'));
			   SET @EndDate = CONVERT(DATE, JSON_VALUE(@CurValue, '$.end'));
			END 

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor; 
		DEALLOCATE Json_Cursor;
	
		DECLARE @ExtJson nvarchar(max) = NULL;
		IF (@InternalPatientAlertID > 0)
		BEGIN
			SET @ExtJson = (SELECT ExtJson FROM dbo.PatientAlert WHERE InternalPatientAlertID = @InternalPatientAlertID);
		END

		IF (@ExtJson is NULL) OR (@ExtJson = '')
		BEGIN
		     SET @ExtJson = '{ }';   --make an empty json
		END

	    SET @ExtJson =  JSON_MODIFY(@ExtJson, '$.summary', @Summary);
	    SET @ExtJson =  JSON_MODIFY(@ExtJson, '$.recipient', JSON_QUERY(@Recipient));

		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientAlert_View_InsertUpdate @ExtJson', @ExtJson, @InternalPatientAlertID);
		
		DECLARE @ProcessedIDTab TABLE (id bigint);

		IF (@InternalPatientAlertID > 0)
		BEGIN
			UPDATE PatientAlert SET
					  InternalPatientID = @InternalPatientID,
					  InternalUserID = @InternalUserID,
					  StartDate = @StartDate,
					  EndDate = @EndDate,
					  ExtJson = @ExtJson
				 OUTPUT inserted.InternalPatientAlertID INTO @ProcessedIDTab
			   WHERE InternalPatientAlertID = @InternalPatientAlertID;

			insert into fhir.LogTable(logitem, logdesc, logint) values('PatientAlert_View_InsertUpdate', 'UPDATED', @InternalPatientAlertID);
		END
		ELSE
		BEGIN
			insert into dbo.PatientAlert (
					  InternalPatientID,
					  InternalUserID,
					  StartDate,
					  EndDate,
					  ExtJson)
				 OUTPUT inserted.InternalPatientAlertID INTO @ProcessedIDTab
			   values (
					  @InternalPatientID,
					  @InternalUserID,
					  @StartDate,
					  @EndDate,
					  @ExtJson);

 		    SET @InternalPatientID = (SELECT IDENT_CURRENT('dbo.PatientAlert')); 
			insert into fhir.LogTable(logitem, logdesc, logint) values('PatientAlert_View_InsertUpdate', 'INSERTED', @InternalPatientAlertID);
		END
		
		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);

		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientAlert_View_InsertUpdate End', 'OK', @InternalPatientAlertID);
	END
