﻿
CREATE PROCEDURE [fhir].[Role_InsertUpdate]
		@InternalRoleID bigint,
		@JsonText nvarchar(max),
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DECLARE @IsActive bit = 1;  --default: true
		DECLARE @Name nvarchar(64) = NULL;
		DECLARE @ExtJson nvarchar(max) = NULL;
		
		IF (ISJSON(@JsonText) > 0)
		BEGIN
		   SET @IsActive = JSON_VALUE(@JsonText, '$.active');
		   SET @Name = JSON_VALUE(@JsonText, '$.name');
		   
		   IF (@InternalRoleID > 0)
		   BEGIN
		      SET @ExtJson = (SELECT ExtJson from dbo.Role where InternalRoleID = @InternalRoleID) ;
		   END
		   
		   IF (@ExtJson IS NULL)
		   BEGIN
		      SET @ExtJson = '{ }';
		   END
		   
		    SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.isAdmin                         ', JSON_VALUE(@JsonText, '$.isAdmin                         '));
		    SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.isSyswideAdmin                  ', JSON_VALUE(@JsonText, '$.isSyswideAdmin                  '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.isIT                            ', JSON_VALUE(@JsonText, '$.isIT                            '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.isReadingPhysician              ', JSON_VALUE(@JsonText, '$.isReadingPhysician              '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.isReferringPhysician            ', JSON_VALUE(@JsonText, '$.isReferringPhysician            '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.isPerformingPhysician           ', JSON_VALUE(@JsonText, '$.isPerformingPhysician           '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.isPerformingTechnologist        ', JSON_VALUE(@JsonText, '$.isPerformingTechnologist        '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.isTranscriptionist              ', JSON_VALUE(@JsonText, '$.isTranscriptionist              '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canEditUser                     ', JSON_VALUE(@JsonText, '$.canEditUser                     '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canEditGroup                    ', JSON_VALUE(@JsonText, '$.canEditGroup                    '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canEditRole                     ', JSON_VALUE(@JsonText, '$.canEditRole                     '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canCreateNewStudyPlayer         ', JSON_VALUE(@JsonText, '$.canCreateNewStudyPlayer         '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canSeeWorklist                  ', JSON_VALUE(@JsonText, '$.canSeeWorklist                  '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canQuickSearch                  ', JSON_VALUE(@JsonText, '$.canQuickSearch                  '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAdvancedSearch               ', JSON_VALUE(@JsonText, '$.canAdvancedSearch               '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canRemoteSearch                 ', JSON_VALUE(@JsonText, '$.canRemoteSearch                 '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canSeeRemoteWorklist            ', JSON_VALUE(@JsonText, '$.canSeeRemoteWorklist            '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canPatientSearch                ', JSON_VALUE(@JsonText, '$.canPatientSearch                '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canViewAuditTrail               ', JSON_VALUE(@JsonText, '$.canViewAuditTrail               '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canEmergencyAccess              ', JSON_VALUE(@JsonText, '$.canEmergencyAccess              '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAccessPortal                 ', JSON_VALUE(@JsonText, '$.canAccessPortal                 '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canViewEssence                  ', JSON_VALUE(@JsonText, '$.canViewEssence                  '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAccessFeeSchedules           ', JSON_VALUE(@JsonText, '$.canAccessFeeSchedules           '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAccessPublicTemplates        ', JSON_VALUE(@JsonText, '$.canAccessPublicTemplates        '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAccessUserTemplates          ', JSON_VALUE(@JsonText, '$.canAccessUserTemplates          '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canSend                         ', JSON_VALUE(@JsonText, '$.canSend                         '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canGroupStudies                 ', JSON_VALUE(@JsonText, '$.canGroupStudies                 '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canCustomizeWorklist            ', JSON_VALUE(@JsonText, '$.canCustomizeWorklist            '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAccessRx                     ', JSON_VALUE(@JsonText, '$.canAccessRx                     '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canMammoAdmin                   ', JSON_VALUE(@JsonText, '$.canMammoAdmin                   '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canSendFax                      ', JSON_VALUE(@JsonText, '$.canSendFax                      '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canSendEmail                    ', JSON_VALUE(@JsonText, '$.canSendEmail                    '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canBurnCD                       ', JSON_VALUE(@JsonText, '$.canBurnCD                       '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canArchive                      ', JSON_VALUE(@JsonText, '$.canArchive                      '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDicomPrint                   ', JSON_VALUE(@JsonText, '$.canDicomPrint                   '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canSignAnyReport                ', JSON_VALUE(@JsonText, '$.canSignAnyReport                '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canBilling                      ', JSON_VALUE(@JsonText, '$.canBilling                      '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAccessRxAdmin                ', JSON_VALUE(@JsonText, '$.canAccessRxAdmin                '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canMammoTracking                ', JSON_VALUE(@JsonText, '$.canMammoTracking                '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canScheduleStudies              ', JSON_VALUE(@JsonText, '$.canScheduleStudies              '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canBlockTime                    ', JSON_VALUE(@JsonText, '$.canBlockTime                    '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.autoBookmark                    ', JSON_VALUE(@JsonText, '$.autoBookmark                    '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.passwordNeverExpire             ', JSON_VALUE(@JsonText, '$.passwordNeverExpire             '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAccessPowerReader            ', JSON_VALUE(@JsonText, '$.canAccessPowerReader            '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.allowedMultipleLogin            ', JSON_VALUE(@JsonText, '$.allowedMultipleLogin            '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canSendViaHL7                   ', JSON_VALUE(@JsonText, '$.canSendViaHL7                   '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAccessPracticeSuite          ', JSON_VALUE(@JsonText, '$.canAccessPracticeSuite          '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAccessMessaging              ',	JSON_VALUE(@JsonText, '$.canAccessMessaging              '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canTranscribe                   ',	JSON_VALUE(@JsonText, '$.canTranscribe                   '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canCreateDOC                    ',	JSON_VALUE(@JsonText, '$.canCreateDOC                    '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAmendReport                  ',	JSON_VALUE(@JsonText, '$.canAmendReport                  '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canSaveHC                       ',	JSON_VALUE(@JsonText, '$.canSaveHC                       '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDictate                      ',	JSON_VALUE(@JsonText, '$.canDictate                      '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canRecordVoiceNote              ',	JSON_VALUE(@JsonText, '$.canRecordVoiceNote              '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canModifyCommunicationLogEntry  ',	JSON_VALUE(@JsonText, '$.canModifyCommunicationLogEntry  '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canCreateEssence                ',	JSON_VALUE(@JsonText, '$.canCreateEssence                '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canSaveKey                      ',	JSON_VALUE(@JsonText, '$.canSaveKey                      '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canSavePR                       ',	JSON_VALUE(@JsonText, '$.canSavePR                       '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canSaveImages                   ',	JSON_VALUE(@JsonText, '$.canSaveImages                   '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canViewImages                   ',	JSON_VALUE(@JsonText, '$.canViewImages                   '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canViewHC                       ',	JSON_VALUE(@JsonText, '$.canViewHC                       '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canViewKey                      ',	JSON_VALUE(@JsonText, '$.canViewKey                      '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canReadSR                       ',	JSON_VALUE(@JsonText, '$.canReadSR                       '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canViewDOC                      ',	JSON_VALUE(@JsonText, '$.canViewDOC                      '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canPlayVoiceNote                ',	JSON_VALUE(@JsonText, '$.canPlayVoiceNote                '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canPlayAU                       ',	JSON_VALUE(@JsonText, '$.canPlayAU                       '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeleteImages                 ',	JSON_VALUE(@JsonText, '$.canDeleteImages                 '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeletePR                     ',	JSON_VALUE(@JsonText, '$.canDeletePR                     '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeleteCommunicationLogEntry  ',	JSON_VALUE(@JsonText, '$.canDeleteCommunicationLogEntry  '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeletePatients               ',	JSON_VALUE(@JsonText, '$.canDeletePatients               '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeleteStudies                ',	JSON_VALUE(@JsonText, '$.canDeleteStudies                '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeleteSeries                 ',	JSON_VALUE(@JsonText, '$.canDeleteSeries                 '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeleteKey                    ',	JSON_VALUE(@JsonText, '$.canDeleteKey                    '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeleteAU                     ',	JSON_VALUE(@JsonText, '$.canDeleteAU                     '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeleteVoiceNote              ',	JSON_VALUE(@JsonText, '$.canDeleteVoiceNote              '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeleteSR                     ',	JSON_VALUE(@JsonText, '$.canDeleteSR                     '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeleteHC                     ',	JSON_VALUE(@JsonText, '$.canDeleteHC                     '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeleteDOC                    ',	JSON_VALUE(@JsonText, '$.canDeleteDOC                    '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canCreatenewPatient             ',	JSON_VALUE(@JsonText, '$.canCreatenewPatient             '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canCreateNewStudy               ',	JSON_VALUE(@JsonText, '$.canCreateNewStudy               '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canMakeNewOrder                 ',	JSON_VALUE(@JsonText, '$.canMakeNewOrder                 '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canResolveConflicts             ',	JSON_VALUE(@JsonText, '$.canResolveConflicts             '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canModifyWindowOptions          ',	JSON_VALUE(@JsonText, '$.canModifyWindowOptions          '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canPeerReview                   ',	JSON_VALUE(@JsonText, '$.canPeerReview                   '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canMergePatients                ',	JSON_VALUE(@JsonText, '$.canMergePatients                '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canMergeStudies                 ',	JSON_VALUE(@JsonText, '$.canMergeStudies                 '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canImportDicom                  ',	JSON_VALUE(@JsonText, '$.canImportDicom                  '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAssignReadingPhysician       ',	JSON_VALUE(@JsonText, '$.canAssignReadingPhysician       '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canModifyDescriptions           ',	JSON_VALUE(@JsonText, '$.canModifyDescriptions           '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canUpdatePatientInfo            ',	JSON_VALUE(@JsonText, '$.canUpdatePatientInfo            '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canUpdateStudyInfo              ',	JSON_VALUE(@JsonText, '$.canUpdateStudyInfo              '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canChangeStatus                 ',	JSON_VALUE(@JsonText, '$.canChangeStatus                 '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAssignStudyPlayers           ',	JSON_VALUE(@JsonText, '$.canAssignStudyPlayers           '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canChangeAfterPreventStatus     ',	JSON_VALUE(@JsonText, '$.canChangeAfterPreventStatus     '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canAddPatientAlerts             ',	JSON_VALUE(@JsonText, '$.canAddPatientAlerts             '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canEditCDSAlerts                ',	JSON_VALUE(@JsonText, '$.canEditCDSAlerts                '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeletePatientAlerts          ',	JSON_VALUE(@JsonText, '$.canDeletePatientAlerts          '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canViewPatientAlert             ',	JSON_VALUE(@JsonText, '$.canViewPatientAlert             '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canDeleteAnyPatientAlerts       ',	JSON_VALUE(@JsonText, '$.canDeleteAnyPatientAlerts       '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canViewClinicalAlert            ',	JSON_VALUE(@JsonText, '$.canViewClinicalAlert            '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canViewPatientInfo              ',	JSON_VALUE(@JsonText, '$.canViewPatientInfo              '));
			SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.canViewStudyInfo                ',	JSON_VALUE(@JsonText, '$.canViewStudyInfo                '));
			
		   IF (@IsActive IS NULL)
		   BEGIN
		      SET @IsActive = 1;
		   END

		   IF (@InternalRoleID > 0)
		   BEGIN
			   UPDATE dbo.Role SET
				   	  Name = @Name,
					  IsActive = @IsActive,
					  ExtJson = @ExtJson
					OUTPUT inserted.InternalRoleID INTO @ProcessedIDTab
				WHERE (InternalRoleID = @InternalRoleID);
		   END
		   ELSE
		   BEGIN
			  INSERT INTO dbo.Role (
					  Name,
					  IsActive,
					  ExtJson)
					OUTPUT inserted.InternalRoleID INTO @ProcessedIDTab
			   VALUES (
					  @Name,
					  @IsActive,
					  @ExtJson);			
		   END

		   SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
		   INSERT INTO fhir.LogTable(logitem, logdesc, logint) values('Role_InsertUpdate End', 'OK', @Result);

		END
		ELSE
		BEGIN
		   SET @Result = -1;
		   insert into fhir.LogTable(logitem, logdesc, logint) values('Role_InsertUpdate End', 'Invalid JSON', @InternalRoleID);
		END
	END
