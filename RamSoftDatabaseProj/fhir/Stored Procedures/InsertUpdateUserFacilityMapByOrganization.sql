﻿
CREATE procedure [fhir].[InsertUpdateUserFacilityMapByOrganization]
(
	@InternalOrganizationID bigint,
	@JsonInput nvarchar(max)
)
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	    
	    
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
			
			IF @JsonInput = ''
			BEGIN
				DELETE from dbo.UserFacilityMap Where InternalFacilityID = @InternalOrganizationID
				Return
			END

			DECLARE @CurKey NVARCHAR(512);
 			DECLARE	@CurValue NVARCHAR(MAX);
			DECLARE	@CurType int;

			DECLARE @InternalUserID bigint;
			DECLARE @FacilityUserID nvarchar(64);
			DECLARE @ExtJson nvarchar (max) = NULL;

			--Declare a temp table it will be use to store all the UserIDs in the new update model from client
			DECLARE @UserFacilityMapTemp TABLE (
			   InternalUserMapID bigint
			);

			--Declare Json Cursor for loop through each row in the model Json
			DECLARE Json_Cursor CURSOR LOCAL FOR SELECT * FROM OPENJSON(@JsonInput, '$');

			OPEN Json_Cursor;
			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
			WHILE (@@FETCH_STATUS = 0) 
			BEGIN
				IF (@CurValue <> '')
				BEGIN
					DECLARE @ProKey NVARCHAR(512);
 					DECLARE	@ProValue NVARCHAR(MAX);
					DECLARE	@ProType int;

					--Declare Property_Cursor for looping throught each property and get value
					DECLARE Property_Cursor CURSOR LOCAL FOR SELECT * FROM OPENJSON(@CurValue)	

					OPEN Property_Cursor;
					FETCH NEXT FROM Property_Cursor INTO @ProKey, @ProValue, @ProType;
					WHILE (@@FETCH_STATUS = 0) 
					BEGIN
						IF (@ProKey = 'id') SELECT @InternalUserID = InternalUserID FROM dbo.User_View WHERE ID = @ProValue;
						IF (@ProKey = 'facilityUserID') SET @FacilityUserID = @ProValue;

						FETCH NEXT FROM Property_Cursor INTO @ProKey, @ProValue, @ProType;
					END	
					CLOSE Property_Cursor;
					DEALLOCATE Property_Cursor; 			
				END			

				--Check if the the current userfacility is exist in database 
				IF EXISTS (SELECT InternalFacilityID FROM dbo.UserFacilityMap WHERE InternalFacilityID = @InternalOrganizationID AND InternalUserID = @InternalUserID)					
				BEGIN
				--IF exist then update the current
					UPDATE dbo.UserFacilityMap SET
							FacilityUserID = @FacilityUserID
					WHERE InternalFacilityID = @InternalOrganizationID AND InternalUserID = @InternalUserID;
				END
				ELSE
				BEGIN
					--IF not exists then insert the new one
					INSERT INTO dbo.UserFacilityMap(
						[InternalFacilityID], 
						[InternalUserID], 
						[FacilityUserID], 
						[ExtJson])
					VALUES(
						@InternalOrganizationID,
						@InternalUserID,
						@FacilityUserID,
						@ExtJson
						);
				END
	
				--Store all the Update UserFacility into table. This will be use to compare and remove Userfacility that don't belong in here.
				INSERT INTO @UserFacilityMapTemp Values(@InternalUserID);

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
			END
			CLOSE Json_Cursor;
			DEALLOCATE Json_Cursor;

			--Check and Delete if there are any result that dont exist in the new update model
			--Delete all that dont appear in the new Model
			DELETE FROM dbo.UserFacilityMap WHERE InternalFacilityID = @InternalOrganizationID AND InternalUserID NOT IN (select InternalUserMapID from @UserFacilityMapTemp)

			COMMIT TRANSACTION	
		END TRY
		BEGIN CATCH
			insert into fhir.LogTable(logitem, logdesc, logtime) values('ERROR OCCURED ROLLBACK', @InternalOrganizationID , default);		

			Declare @ErrorNumber   int;
			Declare @ErrorMessage  nvarchar(max);
			Declare @ErrorSeverity int;
			Declare @ErrorState    int;

			SELECT @ErrorNumber   = ERROR_NUMBER(),
				   @ErrorMessage  = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState    = ERROR_STATE();
			insert into fhir.LogTable(logitem, logdesc, logtime) values('ERROR MESSAGE ', @ErrorMessage , default);		
			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);			
			ROLLBACK TRANSACTION
		END CATCH
	END
END
