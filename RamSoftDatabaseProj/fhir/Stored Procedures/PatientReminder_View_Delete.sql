﻿CREATE PROCEDURE [fhir].[PatientReminder_View_Delete]
		@InternalPatientReminderID int,
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);
		--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientReminder_View_Delete trigger Start', 'OK', @InternalPatientReminderID);

		DELETE FROM dbo.PatientReminder	
			OUTPUT deleted.InternalPatientReminderID INTO @ProcessedIDTab
			WHERE InternalPatientReminderID = @InternalPatientReminderID; 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
		--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientReminder_View_Delete trigger End', 'OK', @Result);
	END
