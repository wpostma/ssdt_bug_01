﻿CREATE Procedure [fhir].[GetEncounterByStudy] @internalStudyID bigint
as
Begin	
DECLARE @PrimaryEncounterID bigint = NULL;
Select @PrimaryEncounterID = PrimaryEncounterID From phi.Study Where InternalStudyID = @internalStudyID;

IF (@PrimaryEncounterID is null)
Begin

Set @PrimaryEncounterID  = (Select Top 1 InternalEncounterID From fhir.Encounter_View e where InternalEncounterID in
	(select Do.InternalEncounterID from phi.Study s Cross apply OpenJson(JSON_QUERY(s.ExtJson,'$.internalDiagnosticOrderID')) Dids
	JOIN PHI.DiagnosticOrder Do on Do.InternalDiagnosticOrderID = Dids.Value Where s.InternalStudyID = @internalStudyID))

End
	
select e.AdmissionID, e.InternalEncounterID, CASE e.InternalEncounterID When @PrimaryEncounterID Then JSON_MODIFY(e.JsonText,'$.isPrimary', 'True') Else JSON_MODIFY(e.JsonText,'$.isPrimary', 'False') End as 'JsonText' from fhir.Encounter_View e where e.InternalEncounterID in 
--> Get Encounters for study with internal study id = 1					  
(select Do.InternalEncounterID from phi.Study s Cross apply OpenJson(JSON_QUERY(s.ExtJson,'$.internalDiagnosticOrderID')) Dids
JOIN PHI.DiagnosticOrder Do on Do.InternalDiagnosticOrderID = Dids.Value Where s.InternalStudyID = @internalStudyID)			

End
