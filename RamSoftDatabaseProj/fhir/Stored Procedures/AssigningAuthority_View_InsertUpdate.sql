﻿

CREATE PROCEDURE [fhir].[AssigningAuthority_View_InsertUpdate]
		@InternalAssigningAuthorityID bigint,
		@JsonText nvarchar(max),
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @UniversalEntityID nvarchar(512) = NULL;
		DECLARE @UniversalEntityIDTypeID int = null;
		DECLARE @UniversalEntityIDTypeCode nvarchar(64) = NULL;
		DECLARE @IsActive bit = 1;  --default: true
		DECLARE @Name nvarchar(64) = NULL;
		DECLARE @IsExternal bit = 1;  --default: true
		DECLARE @ExtJson nvarchar (max) = NULL;

		DECLARE @IdentifierJson nvarchar(max) = NULL;
		DECLARE @HumanNameJson nvarchar(max) = NULL;
					
		DECLARE Json_Cursor CURSOR LOCAL FOR
			  SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN    		
			IF (@CurKey = 'active') SET @IsActive = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'name') SET @Name = @CurValue;
			IF (@CurKey = 'isExternal') SET @IsExternal = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
		
			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor; 
		DEALLOCATE Json_Cursor;

		SET @IdentifierJson = JSON_query(@JsonText, '$.identifier');
		IF (@IdentifierJson IS NOT NULL)
		BEGIN
		    SET @HumanNameJson = JSON_QUERY(@IdentifierJson,'$[0]');

			IF (@HumanNameJson IS NULL)
			BEGIN
			    SET @UniversalEntityID = JSON_VALUE(@IdentifierJson, '$.value');
			    SET @UniversalEntityIDTypeCode = JSON_VALUE(@IdentifierJson, '$.type');
			END
			ELSE
			BEGIN
			    SET @UniversalEntityID = JSON_VALUE(@HumanNameJson, '$.value');
			    SET @UniversalEntityIDTypeCode = JSON_VALUE(@HumanNameJson, '$.type');
			END
		END	

		IF (@UniversalEntityIDTypeCode IS NOT NULL) 
		BEGIN
		   SET @UniversalEntityIDTypeID = (SELECT UniversalEntityIDTypeID FROM code.UniversalEntityIDType WHERE UniversalEntityIDTypeCode = @UniversalEntityIDTypeCode);

		   /* Generate UUID if it is null */
		   IF (@UniversalEntityID IS NULL) AND (@UniversalEntityIDTypeCode = 'UUID')
		   BEGIN
		      SET @UniversalEntityID = NEWID();              
		   END
		END
		ELSE IF (@UniversalEntityID IS NULL)  /* Both are null, Set UUID as default and generate UUID */
		BEGIN
		   /* Set UUID as default */
		   SET @UniversalEntityIDTypeID = (SELECT UniversalEntityIDTypeID FROM code.UniversalEntityIDType WHERE UniversalEntityIDTypeCode = 'UUID');

		   /* Generate UUID if it is null */
		   SET @UniversalEntityID = NEWID();		   
		END

		SET @ExtJson = '{ }';		
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.directAddress', JSON_VALUE(@JsonText, '$.directAddress'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.contact', JSON_QUERY(@JsonText, '$.contact'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.address', JSON_QUERY(@JsonText, '$.address'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.telecom', JSON_QUERY(@JsonText, '$.telecom'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.preliminary', JSON_QUERY(@JsonText, '$.preliminary'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.final', JSON_QUERY(@JsonText, '$.final'));
		
		IF (@InternalAssigningAuthorityID > 0)
		BEGIN
			UPDATE dbo.AssigningAuthority SET
					  Name = @Name,
					  UniversalEntityIDTypeID = @UniversalEntityIDTypeID,
					  UniversalEntityID  = @UniversalEntityID,
					  IsActive = @IsActive,
					  IsExternal = @IsExternal,
					  ExtJson = @ExtJson
					OUTPUT inserted.InternalAssigningAuthorityID INTO @ProcessedIDTab
				WHERE InternalAssigningAuthorityID = @InternalAssigningAuthorityID;		
		END
		ELSE
		BEGIN
			insert into dbo.AssigningAuthority (
					  Name,
					  UniversalEntityIDTypeID,
					  UniversalEntityID,
					  IsActive,
					  IsExternal,
					  ExtJson)
					OUTPUT inserted.InternalAssigningAuthorityID INTO @ProcessedIDTab
			   values (
					  @Name,
					  @UniversalEntityIDTypeID,
					  @UniversalEntityID,
					  @IsActive,
					  @IsExternal,
					  @ExtJson);			
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
		-- insert into fhir.LogTable(logitem, logdesc, logint) values('AssigningAuthority_View_InsertUpdate', @ExtJson, @Result);
	END
