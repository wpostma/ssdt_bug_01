﻿CREATE PROCEDURE [fhir].[SearchRoomsByCriteria]
		@SearchParams nvarchar(max)
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @WhereStr nvarchar(max) = ' (r.InternalRoomID > 0) ';
		DECLARE @ConditionStr nvarchar(4000) = null;
		DECLARE @CurParam nvarchar(max) = null;
		DECLARE @CurKey nvarchar(max) = null;
		DECLARE @CurValue nvarchar(max) = null;
		DECLARE @ParamSplitor nvarchar(1) = '&';
		DECLARE @ValueSplitor nvarchar(1) = '=';
		DECLARE @ValueSplitorPos int;
		DECLARE @ReturnCount int = 100;

		IF (CHARINDEX('?', @SearchParams) = 1)
		BEGIN
			SET @SearchParams = STUFF(@SearchParams, 1, 1, ''); 
		END
				
		IF (@SearchParams > '')
		BEGIN
			declare param_cursor cursor LOCAL for
				select value from string_split(@SearchParams, @ParamSplitor);
		
			OPEN param_cursor;
			FETCH NEXT from param_cursor into @CurParam;

			while (@@FETCH_STATUS = 0) 
			begin
				SET @ValueSplitorPos = CHARINDEX(@ValueSplitor, @CurParam);

				IF (@ValueSplitorPos > 1)
				BEGIN
					SET @CurKey = SUBSTRING(@CurParam, 1, @ValueSplitorPos-1);
					SET @CurValue = SUBSTRING(@CurParam, @ValueSplitorPos+1, LEN(@CurParam)-@ValueSplitorPos);

					SET @CurValue = REPLACE(@CurValue, '%20', ' '); 

					SET @ConditionStr =  
					   CASE 
						   WHEN (@CurKey = 'facilityID') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'd.InternalFacilityID', 'number')
						   WHEN (@CurKey = 'facilityName') THEN  fhir.ComposeWhereCondititon (@CurKey + ':EXACT', @CurValue, 'o.OrganizationName', default)
						   WHEN (@CurKey = 'name') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'r.Name', default)
						   WHEN (@CurKey = 'departmentID') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'r.InternalDepartmentID', 'number')
						   WHEN (@CurKey = 'departmentName') THEN  fhir.ComposeWhereCondititon (@CurKey + ':EXACT', @CurValue, 'd.Department', default)
   					   END

					IF (@ConditionStr IS NOT NULL) AND (@ConditionStr != '')
					BEGIN
					   IF (@WhereStr IS NULL) 
					   BEGIN
						  SET @WhereStr =  @ConditionStr; 
					   END
					   ELSE
					   BEGIN
						  SET @WhereStr = @WhereStr + ' AND ' + @ConditionStr; 
					   END
				   END

				   IF (@CurKey = 'Count') 
				   BEGIN
				      SET @ReturnCount = CASE WHEN Try_CAST(@CurValue as int) IS NOT NULL THEN CAST(@CurValue as int) ELSE @ReturnCount END;
				   END
				END

  				FETCH NEXT from param_cursor into @CurParam;
			end
			CLOSE param_cursor;
		END

		DECLARE @Room TABLE ( roomID bigint);

		DECLARE @SQL nvarchar(max) = 'SELECT top ' + CAST(@ReturnCount AS nvarchar) + ' r.InternalRoomID from dbo.[Room] r JOIN dbo.Department d ON d.InternalDepartmentID = r.InternalDepartmentID  ' +
		                             ' JOIN dbo.Organization o ON o.InternalOrganizationID = d.InternalFacilityID '; 

		IF (@WhereStr IS NOT NULL)
		BEGIN
		   SET @SQL = @SQL + ' WHERE ' + @WhereStr;
		END
		SET @SQL = @SQL + ' ORDER BY r.Name ';

		PRINT @SQL;

		IF ((@SearchParams > '') AND (@WhereStr IS NOT NULL)) OR (@SearchParams = '')
		BEGIN
		   INSERT INTO @Room
		      EXEC (@SQL);
		END

		SELECT v.* FROM @Room r
			  JOIN fhir.Room_View v on v.InternalRoomID = r.roomID;
	END
