﻿CREATE PROCEDURE [fhir].[ProblemCode_View_Delete]
		@InternalProblemCodeID bigint,
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;	
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DELETE FROM code.Problem	
			OUTPUT deleted.InternalProblemCodeID INTO @ProcessedIDTab
			WHERE InternalProblemCodeID = @InternalProblemCodeID; 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END

