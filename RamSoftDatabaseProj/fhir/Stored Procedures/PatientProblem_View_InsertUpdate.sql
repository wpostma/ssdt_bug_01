﻿CREATE PROCEDURE [fhir].[PatientProblem_View_InsertUpdate]
		@InternalPatientProblemID bigint,
	    @JsonText nvarchar(max),
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientProblem_View_InsertUpdate - Start @JsonText=', @JsonText, @InternalPatientProblemID);

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @InternalPatientID bigint = NULL;
   		DECLARE @Onset DateTime2(7) = NULL;
   		DECLARE @DateModified DateTime2(7) = NULL;
 		DECLARE @ProblemCodeID bigint = NULL;
 		DECLARE @ProblemCode nvarchar(64) = NULL;
 		DECLARE @ProblemDescription nvarchar(256) = NULL;
		DECLARE @StatusID int = NULL;
		DECLARE @Status nvarchar(64) = NULL;
		DECLARE @Notes nvarchar(1000) = NULL;

		DECLARE Json_Cursor CURSOR LOCAL FOR
			  SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
    		--insert into fhir.LogTable(logitem, logdesc, logint) values(@CurKey, @CurValue, @CurType);

			IF (@CurKey = 'id') AND (@InternalPatientProblemID <= 0 OR @InternalPatientProblemID IS NULL)
			BEGIN
			  SET @InternalPatientProblemID = @CurValue;
			END

			IF (@CurKey = 'patient') 
			BEGIN
			  SET @InternalPatientID = JSON_VALUE(@CurValue, '$.id');
			END

			IF (@CurKey = 'onset') SET @Onset = (SELECT CASE WHEN TRY_CONVERT(datetime2(7), @CurValue) IS NULL THEN NULL ELSE @CurValue END);

			IF (@CurKey = 'code') 
			BEGIN 
			   IF (IsJson(@CurValue) = 0)
			   BEGIN
			      SET @ProblemCode = @CurValue;
			   END
			   ELSE
			   BEGIN
				   IF (JSON_VALUE(@CurValue, '$.text') IS NOT NULL) 
					  SET @ProblemCode = JSON_VALUE(@CurValue, '$.text');
				   ELSE IF (JSON_VALUE(@CurValue, '$.coding.code') IS NOT NULL) 
					  SET @ProblemCode = JSON_VALUE(@CurValue, '$.coding.code');
				   ELSE IF (JSON_VALUE(@CurValue, '$.coding.display') IS NOT NULL) 
					  SET @ProblemDescription = JSON_VALUE(@CurValue, '$.coding.display')
			   END
			END

			IF (@CurKey = 'verificationStatus') SET @Status = @CurValue;

			IF (@CurKey = 'notes') SET @Notes = @CurValue;

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor;
		DEALLOCATE Json_Cursor; 

     	insert into fhir.LogTable(logitem, logdesc, logtime) values('PatientProblem_View_InsertUpdate - Found @Code', @ProblemCode, default);

		SET @ProblemCodeID = (SELECT InternalProblemCodeID FROM code.Problem where ProblemCode = @ProblemCode);
		IF (@ProblemCodeID IS NULL) OR (@ProblemCodeID = 0)  
		BEGIN
			SET @ProblemCodeID = (SELECT InternalProblemCodeID FROM code.Problem where Description = @ProblemDescription);
		END 

		SET @StatusID = (SELECT TOP 1 ConditionVerStatusID FROM code.ConditionVerStatus where ConditionVerStatusName = @Status OR ConditionVerStatusCode = @Status);

		DECLARE @ExtJson nvarchar(max) = NULL;
		IF (@InternalPatientProblemID > 0)
		BEGIN
			SET @ExtJson = (Select ExtJson from dbo.PatientProblem where InternalPatientProblemID = @InternalPatientProblemID);
		END

		IF (@ExtJson IS NULL) 
		BEGIN
			SET @ExtJson = '{ }';   --make an empty json
		END

		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.notes', @Notes); 

		SET @DateModified = SYSDATETIME();

		IF (@InternalPatientProblemID > 0)
		BEGIN
			UPDATE dbo.PatientProblem SET 
						InternalPatientID = @InternalPatientID,
						InternalProblemCodeID = @ProblemCodeID,
						OnSet = @Onset,
						DateModified = @DateModified,
						ConditionVerStatusID = @StatusID,
						ExtJson = @ExtJson 
					OUTPUT inserted.InternalPatientProblemID INTO @ProcessedIDTab
				WHERE InternalPatientProblemID = @InternalPatientProblemID;

			insert into fhir.LogTable(logitem, logdesc, logint) values('PatientProblem_insertUpdate - ', 'UPDATED', @InternalPatientProblemID);
		END
		ELSE
		BEGIN
			INSERT INTO dbo.PatientProblem(
						InternalPatientID,
						InternalProblemCodeID,
						OnSet,
						DateModified,
						ConditionVerStatusID,
						ExtJson)
					OUTPUT inserted.InternalPatientProblemID INTO @ProcessedIDTab
				VALUES(
						@InternalPatientID,
						@ProblemCodeID,
						@Onset,
						@DateModified,
						@StatusID,
						@ExtJson );

			insert into fhir.LogTable(logitem, logdesc, logint) values('PatientProblem_insertUpdate - ', 'INSERTED', @InternalPatientProblemID);
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);;
		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientProblem_insertUpdate - End', 'OK', @Result);
	END
