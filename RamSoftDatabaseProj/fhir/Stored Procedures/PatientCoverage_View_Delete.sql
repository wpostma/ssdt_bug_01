﻿CREATE PROCEDURE [fhir].[PatientCoverage_View_Delete]
		@InternalCoverageID bigint,
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientCoverage_View_Delete - Start', 'OK', @InternalCoverageID);
		DELETE FROM phi.PatientCoverage	
			OUTPUT deleted.InternalCoverageID INTO @ProcessedIDTab
			WHERE InternalCoverageID = @InternalCoverageID; 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);

		--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientCoverage_View_Delete - End', 'OK', @Result);
	END
