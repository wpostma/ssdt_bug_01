﻿
CREATE PROCEDURE [fhir].[AssigningAuthority_View_Delete]
		@InternalAssigningAuthorityID bigint,
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;	
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DELETE FROM dbo.AssigningAuthority	
			OUTPUT deleted.InternalAssigningAuthorityID INTO @ProcessedIDTab
			WHERE InternalAssigningAuthorityID = @InternalAssigningAuthorityID; 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END



