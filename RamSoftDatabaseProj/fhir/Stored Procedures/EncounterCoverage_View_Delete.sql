﻿CREATE PROCEDURE [fhir].[EncounterCoverage_View_Delete]
		@InternalEncounterCoverageID bigint,
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		--insert into fhir.LogTable(logitem, logdesc, logint) values('EncounterCoverage_View_Delete - Start', '', @InternalEncounterCoverageID);
		DELETE FROM phi.EncounterCoverage	
			OUTPUT deleted.InternalEncounterCoverageID INTO @ProcessedIDTab
			WHERE InternalEncounterCoverageID = @InternalEncounterCoverageID; 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);

		--insert into fhir.LogTable(logitem, logdesc, logint) values('EncounterCoverage_View_Delete End', 'OK', @Result);
	END
