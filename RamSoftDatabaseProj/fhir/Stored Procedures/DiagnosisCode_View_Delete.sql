﻿CREATE PROCEDURE [fhir].[DiagnosisCode_View_Delete]
		@InternalDiagnosisCodeID bigint,
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;	
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DELETE FROM code.Diagnosis	
			OUTPUT deleted.InternalDiagnosisCodeID INTO @ProcessedIDTab
			WHERE InternalDiagnosisCodeID = @InternalDiagnosisCodeID; 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END

