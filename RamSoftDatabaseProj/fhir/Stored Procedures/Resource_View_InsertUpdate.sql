﻿CREATE PROCEDURE [fhir].[Resource_View_InsertUpdate]
		@InternalResourceID BIGINT,
		@JsonText NVARCHAR(MAX),
		@Result BIGINT OUTPUT
	AS
	BEGIN

		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id BIGINT);
		DECLARE @ModalityCode NVARCHAR(8); -- Will remove this field
		DECLARE @LocationID BIGINT;
		DECLARE @ResourceName NVARCHAR(64);
		DECLARE @IsActive BIT;
		DECLARE @StartTime NVARCHAR(30);
		DECLARE @EndTime NVARCHAR(30);
		DECLARE @DefaultDuration NVARCHAR(10);
		DECLARE @Color NVARCHAR(10);
		DECLARE @Modalities NVARCHAR(Max);
		DECLARE @OverBooking BIT;
        DECLARE @CanBlockTime BIT;
		DECLARE @CanReserveTimeSlots BIT;
		DECLARE @TimeMultiplier INT;
		DECLARE @ExtJson NVARCHAR(MAX);
		 
		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE Json_Cursor CURSOR LOCAL FOR
		SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
    		
			IF (@CurKey = 'active') SET @IsActive = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'resourceName') SET @ResourceName = @CurValue;
			IF (@CurKey = 'actor') SET @LocationID = JSON_VALUE(@CurValue, '$.id');
			IF (@CurKey = 'internalModalityID') SET @ModalityCode = @CurValue;
			IF (@CurKey = 'planningHorizon')
			BEGIN
				SET @StartTime = JSON_VALUE(@CurValue, '$.start');
				SET @EndTime = JSON_VALUE(@CurValue, '$.end');
			END 
			IF (@CurKey = 'defaultDuration') SET @DefaultDuration = @CurValue;
			IF (@CurKey = 'color') SET @Color = @CurValue;
			IF (@CurKey = 'modalities') SET @Modalities = @CurValue;
			IF (@CurKey = 'overBooking') SET @OverBooking = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'canBlockTime') SET @CanBlockTime = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'canReserveTimeSlots') SET @CanReserveTimeSlots = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF(@CurKey = 'timeMultiplier') SET @TimeMultiplier = @CurValue;

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor; 
		DEALLOCATE Json_Cursor;
		
		SELECT @ExtJson = ExtJson FROM dbo.Resource WHERE InternalResourceID = @InternalResourceID;

		IF @ExtJson IS NULL OR @ExtJson = ''
			SET @ExtJson = '{}'

		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.STARTTIME', @StartTime);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.ENDTIME', @EndTime);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.Color', @Color);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.modalities', JSON_QUERY(@Modalities));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.DEFAULTSTUDYORDERDURATION', @DefaultDuration);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.timeMultiplier', @TimeMultiplier);

		IF (@InternalResourceID > 0)
		BEGIN
			UPDATE dbo.Resource SET
					  InternalRoomID = @LocationID,
					  InternalModalityID = @ModalityCode, -- Will remove this field				  					  
					  Name = @ResourceName,
					  Overbooking = @OverBooking,
					  CanBlockTime = @CanBlockTime,
					  CanReserveTimeSlots = @CanReserveTimeSlots,
					  IsActive = @IsActive,
					  ExtJson = @ExtJson					  
					OUTPUT inserted.InternalResourceID INTO @ProcessedIDTab
				WHERE InternalResourceID = @InternalResourceID;		
		END
		ELSE
		BEGIN
			INSERT INTO dbo.Resource(
					  InternalRoomID,
					  InternalModalityID, -- Will remove this field
					  Name,
					  Overbooking,
					  CanBlockTime,
					  CanReserveTimeSlots,
					  IsActive,					  
					  ExtJson)
					OUTPUT inserted.InternalResourceID INTO @ProcessedIDTab
			   values (
					  @LocationID,
					  @ModalityCode,
					  @ResourceName,
					  @OverBooking,
					  @CanBlockTime,
					  @CanReserveTimeSlots,
					  @IsActive,
					  @ExtJson);			
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END

