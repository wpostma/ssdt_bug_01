﻿CREATE PROCEDURE [fhir].[PatientInsurance_View_InsertUpdate]
		@InternalPatientID bigint,
		@JsonText nvarchar(max),
		@Result bigint output
	AS 
	BEGIN
		--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientInsurance_View_InsertUpdate Start', '@InternalPatientID', @InternalPatientID);

		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DECLARE @GuarantorReferenceJson NVARCHAR(MAX);
		DECLARE @GuarantorRelationship NVARCHAR(MAX);
 		DECLARE	@GuarantorRelationshipID int;
		DECLARE	@GuarantorInternalPatientID bigint;

		SET @GuarantorReferenceJson = JSON_QUERY(@JsonText, '$.guarantorReference');
		IF (@GuarantorReferenceJson IS NOT NULL)
		BEGIN
			SET @GuarantorInternalPatientID = 
				(SELECT CASE WHEN TRY_CONVERT(bigint, JSON_VALUE(@GuarantorReferenceJson, '$.id')) IS NULL THEN NULL  ELSE JSON_VALUE(@GuarantorReferenceJson, '$.id') END);

			IF (@GuarantorInternalPatientID > 0) AND (JSON_VALUE(@GuarantorReferenceJson, '$.updatePatientInfo') > 0)
			BEGIN
				DECLARE @return_value int, @UpdateResult bigint;
				EXEC	@return_value = [fhir].[UpdatePatientWithReferenceJson]
					@InternalPatientID = @GuarantorInternalPatientID,
					@InputJson = @GuarantorReferenceJson,
					@Result = @UpdateResult OUTPUT;
			END
		END

		--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientInsurance_View_InsertUpdate ', '@GuarantorInternalPatientID', @GuarantorInternalPatientID);
		
		SET @GuarantorRelationship = JSON_VALUE(@JsonText, '$.guarantorRelationToPatient');
		SET @GuarantorRelationshipID = (SELECT BeneficiaryRelationshipID FROM code.BeneficiaryRelationship WHERE BeneficiaryRelationshipName = @GuarantorRelationship);

		IF (@GuarantorRelationshipID IS NULL) OR (@GuarantorRelationshipID = 0) OR 
 		   (@GuarantorInternalPatientID IS NULL) OR (@GuarantorInternalPatientID = 0) OR (@GuarantorInternalPatientID = @InternalPatientID) 
		BEGIN
			SET @GuarantorRelationshipID = (SELECT BeneficiaryRelationshipID FROM code.BeneficiaryRelationship WHERE BeneficiaryRelationshipName = 'SELF');
			SET @GuarantorInternalPatientID = @InternalPatientID;
		END
			
		--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientInsurance_View_InsertUpdate ', '@GuarantorRelationshipID', @GuarantorRelationshipID);

		UPDATE PHI.Patient 
			SET GuarantorInternalPatientID = @GuarantorInternalPatientID, GuarantorRelationshipID = @GuarantorRelationshipID,
				LastUpdateUTC = SYSUTCDATETIME()  
			OUTPUT inserted.InternalPatientID INTO @ProcessedIDTab
			WHERE InternalPatientID = @InternalPatientID;

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
		--insert into fhir.LogTable(logitem, logdesc, logint) values('PatientInsurance_View_InsertUpdate ', '@Result', @Result);
	END
