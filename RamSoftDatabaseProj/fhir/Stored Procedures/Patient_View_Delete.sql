﻿CREATE PROCEDURE [fhir].[Patient_View_Delete]
		@InternalPatientID bigint,
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DELETE FROM phi.patient	
			OUTPUT deleted.InternalPatientID INTO @ProcessedIDTab
			WHERE InternalPatientID = @InternalPatientID; 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END
