﻿
CREATE PROCEDURE [fhir].[Group_View_Delete]
		@InternalGroupID bigint,
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;	
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DELETE FROM dbo.[Group]	
			OUTPUT deleted.InternalGroupID INTO @ProcessedIDTab
			WHERE (InternalGroupID = @InternalGroupID) AND 
			      (InternalGroupID > 0); 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END


