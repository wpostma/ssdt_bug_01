﻿CREATE PROCEDURE [fhir].[PatientAlert_View_Delete]
		@InternalPatientAlertID int,
		@Result int output
	AS 
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DELETE FROM dbo.PatientAlert	
			OUTPUT deleted.InternalPatientAlertID INTO @ProcessedIDTab
			WHERE InternalPatientAlertID = @InternalPatientAlertID; 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END
