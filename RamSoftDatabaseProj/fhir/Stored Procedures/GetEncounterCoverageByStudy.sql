﻿CREATE Procedure [fhir].[GetEncounterCoverageByStudy] @internalStudyID bigint
as
Begin	


DECLARE @primaryEncounter bigint;
SET @primaryEncounter = (select InternalEncounterID From phi.Encounter e JOIN phi.Study s ON s.PrimaryEncounterID = e.InternalEncounterID Where s.InternalStudyID = @internalStudyID)
print @primaryEncounter

select * from fhir.EncounterCoverage_View e where e.InternalEncounterID = ( 
Case 
When (@primaryEncounter is not null) Then @primaryEncounter
ELSE (select Top 1 Do.InternalEncounterID from phi.Study s Cross apply OpenJson(JSON_QUERY(s.ExtJson,'$.internalDiagnosticOrderID')) Dids
JOIN PHI.DiagnosticOrder Do on Do.InternalDiagnosticOrderID = Dids.Value Where s.InternalStudyID = @internalStudyID)	
End	
)
End

