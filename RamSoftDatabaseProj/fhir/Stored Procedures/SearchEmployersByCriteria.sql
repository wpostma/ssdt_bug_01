﻿CREATE PROCEDURE [fhir].[SearchEmployersByCriteria]
		@SearchParams nvarchar(max)
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @WhereStr nvarchar(max) = null;
		DECLARE @ConditionStr nvarchar(4000) = null;
		DECLARE @CurParam nvarchar(max) = null;
		DECLARE @CurKey nvarchar(max) = null;
		DECLARE @CurValue nvarchar(max) = null;
		DECLARE @ParamSplitor nvarchar(1) = '&';
		DECLARE @ValueSplitor nvarchar(1) = '=';
		DECLARE @ValueSplitorPos int;

		IF (@SearchParams > '')
		BEGIN
			declare param_cursor cursor LOCAL for
				select value from string_split(@SearchParams, @ParamSplitor);
		
			OPEN param_cursor;
			FETCH NEXT from param_cursor into @CurParam;

			while (@@FETCH_STATUS = 0) 
			begin
				SET @ValueSplitorPos = CHARINDEX(@ValueSplitor, @CurParam);

				IF (@ValueSplitorPos > 1)
				BEGIN
					SET @CurKey = SUBSTRING(@CurParam, 1, @ValueSplitorPos-1);
					SET @CurValue = SUBSTRING(@CurParam, @ValueSplitorPos+1, LEN(@CurParam)-@ValueSplitorPos);
				
					SET @ConditionStr =  
						CASE 
							WHEN (CHARINDEX('name', @CurKey) > 0) THEN fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'v.Name', default)
							WHEN (CHARINDEX('contact', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey+':CONTAINS', @CurValue, 'v.Contact', default) 

							WHEN (CHARINDEX('address', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey+':CONTAINS', @CurValue, 'v.Address', default) 
							WHEN (CHARINDEX('city', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey+':EXACT', @CurValue, 'v.City', default) 
							WHEN (CHARINDEX('state', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey+':EXACT', @CurValue, 'v.State', default) 
							WHEN (CHARINDEX('country', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey+':EXACT', @CurValue, 'v.Country', default) 
							WHEN (CHARINDEX('postalcode', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'v.Postalcode', default) 

							WHEN (CHARINDEX('phone', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'v.Phone', default) 
							WHEN (CHARINDEX('fax', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'v.Fax', default) 
							WHEN (CHARINDEX('email', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'v.Email', default) 
						END

					IF (@ConditionStr IS NOT NULL) AND (@ConditionStr != '')
					BEGIN
						IF (@WhereStr IS NULL) 
						BEGIN
							SET @WhereStr =  @ConditionStr; 
						END
						ELSE
						BEGIN
							SET @WhereStr = @WhereStr + ' AND ' + @ConditionStr; 
						END
					END
				END

  				FETCH NEXT from param_cursor into @CurParam;
			end
			CLOSE param_cursor;
		END

		DECLARE @SQL nvarchar(max) = '
		select v.InternalEmployerID, v.JsonText 
		from  (select  InternalEmployerID,
					   JsonText,
					   JSON_VALUE(JsonText, ''$.name'') as [Name],
					   JSON_VALUE(JsonText, ''$.contact[0].name.text'') as Contact,
					   JSON_VALUE(JsonText, ''$.address[0].line'') as [Address],
					   JSON_VALUE(JsonText, ''$.address[0].city'') as City,
					   JSON_VALUE(JsonText, ''$.address[0].state'') as [State],
					   JSON_VALUE(JsonText, ''$.address[0].country'') as Country,
					   JSON_VALUE(JsonText, ''$.address[0].postalCode'') as PostalCode,
					   CASE WHEN JSON_VALUE(JsonText, ''$.telecom[0].system'') = ''phone'' then JSON_VALUE(JsonText, ''$.telecom[0].value'') 
							WHEN JSON_VALUE(JsonText, ''$.telecom[1].system'') = ''phone'' then JSON_VALUE(JsonText, ''$.telecom[1].value'')
							WHEN JSON_VALUE(JsonText, ''$.telecom[2].system'') = ''phone'' then JSON_VALUE(JsonText, ''$.telecom[2].value'')
					   ELSE NULL     
					   END as Phone,
					   CASE WHEN JSON_VALUE(JsonText, ''$.telecom[0].system'') = ''fax'' then JSON_VALUE(JsonText, ''$.telecom[0].value'') 
							WHEN JSON_VALUE(JsonText, ''$.telecom[1].system'') = ''fax'' then JSON_VALUE(JsonText, ''$.telecom[1].value'')
							WHEN JSON_VALUE(JsonText, ''$.telecom[2].system'') = ''fax'' then JSON_VALUE(JsonText, ''$.telecom[2].value'')
					   ELSE NULL     
					   END as Fax,
					   CASE WHEN JSON_VALUE(JsonText, ''$.telecom[0].system'') = ''email'' then JSON_VALUE(JsonText, ''$.telecom[0].value'') 
							WHEN JSON_VALUE(JsonText, ''$.telecom[1].system'') = ''email'' then JSON_VALUE(JsonText, ''$.telecom[1].value'')
							WHEN JSON_VALUE(JsonText, ''$.telecom[2].system'') = ''email'' then JSON_VALUE(JsonText, ''$.telecom[2].value'')
					   ELSE NULL     
					   END as Email
		       from [fhir].[Employer_View]) v ';

		IF (@WhereStr IS NOT NULL)
		BEGIN
		   SET @SQL = @SQL + ' WHERE ' + @WhereStr;
		END
		SET @SQL = @SQL + ' ORDER BY v.Name ';

		-- return searched employer from View
        EXEC (@SQL);		
	END
