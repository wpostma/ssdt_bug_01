﻿CREATE PROCEDURE [fhir].[DiagnosticOrder_View_InsertUpdate] 
	(
		@InternalDiagnosticOrderID bigint,
		@InternalEncounterID bigint,
		@InternalPatientID bigint,
		@JsonText nvarchar(max),
		@Result bigint OUTPUT
	)
	AS
	BEGIN	
			-- SET NOCOUNT ON added to prevent extra result sets from		
			SET NOCOUNT ON;		
			DECLARE @ProcessedIDTab TABLE (id bigint);

			DECLARE @CurKey nvarchar(512);
 			DECLARE	@CurValue nvarchar(max);
			DECLARE	@CurType int;
		
			DECLARE @PriorityValue int;				
			DECLARE @LastUpdateUtc datetimeoffset = sysutcdatetime();
			DECLARE @LastUpdateUserID bigint;
			DECLARE @FacilityID bigint;
			DECLARE @ReferringPhysicianID bigint;
			DECLARE @ReferringFacilityID bigint;
			DECLARE @Comment nvarchar(max);
			DECLARE @Note nvarchar(max);	
			DECLARE @DiagnosisID nvarchar(max);
			DECLARE @ConsultingPhysicianIDs nvarchar(max);
			DECLARE @IsActive bit = 1;
			DECLARE @InternalAssigningAuthority bigint;
			DECLARE @ExtJson nvarchar(max);		

			IF (@InternalDiagnosticOrderID > 0)  -- update
			BEGIN
				SELECT @ExtJson = ExtJson FROM PHI.DiagnosticOrder WHERE InternalDiagnosticOrderID = @InternalDiagnosticOrderID; --Get the current one 
			END
			IF (@ExtJson IS NULL) OR (@ExtJson = '')
			BEGIN
				SET @ExtJson = '{ }'; -- Initialize as Json text
			END		

			DECLARE Json_Cursor CURSOR LOCAL FOR SELECT * FROM OPENJSON(@JsonText);
			OPEN Json_Cursor;
			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

			WHILE (@@FETCH_STATUS = 0) 
			BEGIN				
				IF (@CurKey = 'priorityValue') SET @PriorityValue = @CurValue;						
				IF (@CurKey = 'organization') SET @FacilityID = JSON_VALUE(@CurValue, '$.id');
				IF (@CurKey = 'orderer')
				BEGIN
					SET @ReferringPhysicianID = JSON_VALUE(@CurValue, '$.id');
					SET @ReferringFacilityID = JSON_VALUE(@CurValue, '$.facility.id');
				END	
				IF (@CurKey = 'meta')
				BEGIN							
					SET @LastUpdateUserID = JSON_VALUE(@CurValue, '$.internalUserID');
				END		
				IF (@CurKey = 'identifier') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.identifier', @CurValue);			
				IF (@CurKey = 'reason') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.internalDiagnosisCodeID', JSON_QUERY(@CurValue));			
				IF (@CurKey = 'consulters') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.consultingPhysicians', JSON_QUERY(@CurValue));			
				IF (@CurKey = 'item') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.studies', JSON_QUERY(@CurValue));
				IF (@CurKey = 'comment') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.comment', @CurValue);
				IF (@CurKey = 'note') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.note', JSON_QUERY(@CurValue));
												
				FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
			END
			CLOSE Json_Cursor; 
			DEALLOCATE Json_Cursor;		

			SELECT @InternalAssigningAuthority = InternalAssigningAuthorityID FROM Phi.Patient WHERE InternalPatientID = @InternalPatientID;
		
			IF (@InternalDiagnosticOrderID > 0)
			BEGIN
				UPDATE phi.DiagnosticOrder SET
						InternalEncounterID = @InternalEncounterID,
						InternalPatientID = @InternalPatientID,
						InternalFacilityID = @FacilityID,
						LastUpdateUserID = @LastUpdateUserID,
						LastupdateUTC = @LastUpdateUtc,
						ReferringPhysicianID = @ReferringPhysicianID,
						ReferringFacilityID = @ReferringFacilityID,
						PriorityValue = @PriorityValue,
						InternalAssigningAuthorityID = @InternalAssigningAuthority,
						ExtJson = @ExtJson
				OUTPUT INSERTED.InternalDiagnosticOrderID INTO @ProcessedIDTab
				WHERE InternalDiagnosticOrderID = @InternalDiagnosticOrderID;
			END
			ELSE
			BEGIN						
				INSERT INTO PHI.DiagnosticOrder(																		
							[InternalPatientID],
							[IsActive],
							[InternalEncounterID],
							[InternalFacilityID],
							[InternalAssigningAuthorityID],						
							[LastupdateUTC],	
							[LastUpdateUserID],			
							[ExtJson],
							[DiagnosticOrderDate],
							[ReferringPhysicianID],
							[ReferringFacilityID],
							[PriorityValue]
							)
					OUTPUT INSERTED.InternalDiagnosticOrderID INTO @ProcessedIDTab
					VALUES (
							@InternalPatientID,
							@IsActive,
							@InternalEncounterID, 
							@FacilityID,
							@InternalAssigningAuthority,
							@LastUpdateUtc,
							@LastUpdateUserID,
							@ExtJson,
							@LastUpdateUtc,
							@ReferringPhysicianID,
							@ReferringFacilityID,
							@PriorityValue						
							);
			END
			SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);		
	END
