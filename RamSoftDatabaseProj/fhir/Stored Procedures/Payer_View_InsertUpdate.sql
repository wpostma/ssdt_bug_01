﻿
CREATE PROCEDURE [fhir].[Payer_View_InsertUpdate]
		@InternalPayerID bigint,
		@JsonText nvarchar(max),
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @IsActive bit = 1;  --default: true
		DECLARE @CompanyName nvarchar(64) = NULL;
		DECLARE @FeeScheduleID int = -1;
		DECLARE @FeescheduleName nvarchar(64) = NULL;
		DECLARE @CarrierID nvarchar(64) = NULL;
		DECLARE @FinancialClassID int = NULL;
		DECLARE @FinancialClassCode nvarchar(16) = NULL;
		DECLARE @financialClass nvarchar(64) = NULL;
		DECLARE @ExtJson nvarchar (max) = NULL;

		DECLARE @IdentifierJson nvarchar(max) = NULL;
		DECLARE @HumanNameJson nvarchar(max) = NULL;
					
		DECLARE Json_Cursor CURSOR LOCAL FOR
			  SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
    		
			IF (@CurKey = 'active') SET @IsActive = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'name') SET @CompanyName = @CurValue;
			IF (@CurKey = 'feeScheduleID') SET @FeeScheduleID = @CurValue;
			IF (@CurKey = 'feeScheduleName') SET @FeescheduleName = @CurValue;
			IF (@CurKey = 'financialClassCode') SET @FinancialClassCode = @CurValue;
			IF (@CurKey = 'financialClass') SET @FinancialClass = @CurValue;

            IF (@CurKey = 'identifier')
			BEGIN
			   SET @IdentifierJson = JSON_query(@JsonText, '$.identifier');
			   IF (@IdentifierJson IS NOT NULL)
			   BEGIN
			      SET @HumanNameJson = JSON_QUERY(@IdentifierJson,'$[0]');

				  IF (@HumanNameJson IS NOT NULL)
				  BEGIN
				     SET @CarrierID = JSON_VALUE(@HumanNameJson, '$.value');
				  END
			   END			   
			END			

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor; 
		DEALLOCATE Json_Cursor;

		IF ((@FeeScheduleID < 0) AND (@FeescheduleName IS NOT NULL))
		BEGIN
		   SET @FeeScheduleID = (SELECT FeeScheduleID FROM dbo.FeeSchedule WHERE FeeScheduleName = @FeescheduleName);
		END

		IF (@FinancialClassCode IS NOT NULL)
		BEGIN
		   SET @FinancialClassID = (SELECT FinancialClassID FROM code.FinancialClass WHERE FinancialClassCode = @FinancialClassCode);

		   IF (@FinancialClassID IS NULL) 
		   BEGIN
		      SET @FinancialClassID = (SELECT FinancialClassID FROM code.FinancialClass WHERE FinancialClass = @FinancialClassCode);
		   END

		   IF ((@FinancialClassID IS NULL) AND (@FinancialClass IS NOT NULL))
		   BEGIN
		      SET @FinancialClassID = (SELECT FinancialClassID FROM code.FinancialClass WHERE FinancialClass = @FinancialClass);
		   END
		END
		ELSE IF (@FinancialClass IS NOT NULL)
		BEGIN
		   SET @FinancialClassID = (SELECT FinancialClassID FROM code.FinancialClass WHERE FinancialClass = @FinancialClass);
		END

		SET @ExtJson = '{ }';
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.identifier', JSON_query(@JsonText, '$.identifier'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.contact', JSON_query(@JsonText, '$.contact'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.address', JSON_query(@JsonText, '$.address'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.telecom', JSON_query(@JsonText, '$.telecom'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.otherNumbers', JSON_query(@JsonText, '$.otherNumbers'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.payerID', JSON_VALUE(@JsonText, '$.payerID'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.eligibilityPayerID', JSON_VALUE(@JsonText, '$.eligibilityPayerID')); 

		IF (@InternalPayerID > 0)
		BEGIN
			UPDATE dbo.Payer SET
					  CompanyName = @CompanyName,
					  FeeScheduleID = @FeeScheduleID,
					  FinancialClassID = @FinancialClassID,
					  CarrierID = @CarrierID,
					  IsActive = @IsActive,
					  ExtJson = @ExtJson
					OUTPUT inserted.InternalPayerID INTO @ProcessedIDTab
				WHERE InternalPayerID = @InternalPayerID;		
		END
		ELSE
		BEGIN
			insert into dbo.Payer (
					  CompanyName,
					  FeeScheduleID,
					  FinancialClassID,
					  CarrierID,
					  IsActive,
					  ExtJson)
					OUTPUT inserted.InternalPayerID INTO @ProcessedIDTab
			   values (
					  @CompanyName,
					  @FeeScheduleID,
					  @FinancialClassID,
					  @CarrierID,
					  @IsActive,
					  @ExtJson);			
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
		insert into fhir.LogTable(logitem, logdesc, logint) values('Payer_View_InsertUpdate End', 'OK', @Result);
	END

