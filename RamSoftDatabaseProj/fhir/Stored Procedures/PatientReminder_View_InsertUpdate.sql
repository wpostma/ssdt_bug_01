﻿CREATE PROCEDURE [fhir].[PatientReminder_View_InsertUpdate]
		@InternalPatientReminderID int,
	    @JsonText nvarchar(max),
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @InternalPatientID bigint;

		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientReminder_View_InsertUpdate - Start @JsonText=', substring(@JsonText, 1, 4000), @InternalPatientReminderID);

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @TemplateName nvarchar(256) = NULL;
		DECLARE @TemplateID bigint = NULL;
		DECLARE @ContactMethod nvarchar(256) = NULL;
		DECLARE @ContactMethodID int = NULL;
		DECLARE @StartDate date = NULL;
		DECLARE @EndDate date = NULL;
		DECLARE @UserName nvarchar(64) = NULL;
		DECLARE @InternalUserID int = NUll;
		DECLARE @IsActive bit = 1;;
		DECLARE @FrequncyInDays int = NULL
		DECLARE @CommunicationRequestStatusID int = NULL;
		DECLARE @CommunicationRequestStatus nvarchar(256) = NULL;

		DECLARE Json_Cursor CURSOR LOCAL FOR
				SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
    		--insert into fhir.LogTable(logitem, logdesc, logint) values(@CurKey, @CurValue, @CurType);

			IF (@CurKey = 'id') AND (@InternalPatientReminderID <= 0 OR @InternalPatientReminderID IS NULL)
			BEGIN
				SET @InternalPatientReminderID = @CurValue;
			END

			IF (@CurKey = 'recipient') AND (@InternalPatientID <= 0 OR @InternalPatientID IS NULL)
			BEGIN
				SET @InternalPatientID = JSON_VALUE(@CurValue, '$.id');
			END

			IF (@CurKey = 'requester') 
			BEGIN
				SET @UserName = substring(JSON_VALUE(@CurValue, '$.display'), 1, 64);
				SET @InternalUserID = (SELECT InternalUserID FROM dbo.[User] WHERE UserName = @UserName);

				IF (@InternalUserID IS NULL) OR (@InternalUserID = 0)
				BEGIN
					SET @InternalUserID = (CASE TRY_CONVERT(int, JSON_VALUE(@CurValue, '$.id')) WHEN NULL THEN NULL ELSE JSON_VALUE(@CurValue, '$.id') END);
				END
	    		insert into fhir.LogTable(logitem, logdesc, logint) values(@CurKey, @CurValue, @InternalUserID);
			END

			IF (@CurKey = 'reason') 
			BEGIN
				SET @TemplateName = substring(Json_Value(@CurValue, '$.text'), 1, 256);
				SET @TemplateID = (SELECT TemplateID FROM dbo.PatientReminderTemplate WHERE TemplateName = @TemplateName);
			END

			IF (@CurKey = 'medium') 
			BEGIN
				SET @ContactMethod = substring(Json_Value(@CurValue, '$.text'), 1, 256);
				SET @ContactMethodID = (SELECT ContactMethodID FROM code.ContactMethod WHERE ContactMethodName = @ContactMethod);

				IF (@ContactMethodID IS NULL)
				BEGIN
					SET @ContactMethodID = (SELECT ContactMethodID FROM code.ContactMethod WHERE ContactMethodCode = @ContactMethod);
				END
			END

			IF (@CurKey = 'status') 
			BEGIN
				SET @CommunicationRequestStatus = @CurValue;
				SET @CommunicationRequestStatusID = (SELECT CommunicationRequestStatusID FROM code.CommunicationRequestStatus WHERE CommunicationRequestStatusName = @CommunicationRequestStatus);

				IF (@CommunicationRequestStatusID IS NULL)
				BEGIN
					SET @CommunicationRequestStatusID = (SELECT CommunicationRequestStatusID FROM code.CommunicationRequestStatus WHERE CommunicationRequestStatusCode = @CommunicationRequestStatus);
				END
			   
			END

			IF (@CurKey = 'scheduledPeriod') 
			BEGIN
				SET @StartDate = CONVERT(date, JSON_VALUE(@CurValue, '$.start'));
				SET @EndDate = CONVERT(DATE, JSON_VALUE(@CurValue, '$.end'));
			END 

			IF (@CurKey = 'active') SET @IsActive = (CASE WHEN @CurValue = 'true' THEN 1 ELSE 0 END); 

			IF (@CurKey = 'frequencyInDays') 
			BEGIN
				SET @FrequncyInDays = (CASE TRY_CONVERT(int, @CurValue) WHEN NULL THEN NULL ELSE @CurValue END);
			END

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor; 
		DEALLOCATE Json_Cursor;
	
		DECLARE @ProcessedIDTab TABLE (id bigint);

		IF (@InternalPatientReminderID > 0)
		BEGIN
			UPDATE PatientReminder SET
						InternalPatientID = @InternalPatientID,
						InternalUserID = @InternalUserID,
						StartDate = @StartDate,
						EndDate = @EndDate,
						InternalTemplateID = @TemplateID,
						IsActive = @IsActive,
						FrequencyInDays = @FrequncyInDays,
						ContactMethodID = @ContactMethodID,
						CommunicationRequestStatusID = @CommunicationRequestStatusID
					OUTPUT inserted.InternalPatientReminderID INTO @ProcessedIDTab
				WHERE InternalPatientReminderID = @InternalPatientReminderID;

			insert into fhir.LogTable(logitem, logdesc, logint) values('PatientReminder_View_InsertUpdate', 'UPDATED', @InternalPatientReminderID);
		END
		ELSE
		BEGIN
			insert into dbo.PatientReminder (
						InternalPatientID,
						InternalUserID,
						StartDate,
						EndDate,
						InternalTemplateID,
						IsActive,
						FrequencyInDays,
						ContactMethodID,
						CommunicationRequestStatusID)
					OUTPUT inserted.InternalPatientReminderID INTO @ProcessedIDTab
				values (
						@InternalPatientID,
						@InternalUserID,
						@StartDate,
						@EndDate,
						@TemplateID,
						@IsActive,
						@FrequncyInDays,
						@ContactMethodID,
						@CommunicationRequestStatusID);

			insert into fhir.LogTable(logitem, logdesc, logint) values('PatientReminder_View_InsertUpdate', 'INSERTED', @InternalPatientReminderID);
		END
		
		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);

		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientReminder_View_InsertUpdate End', 'OK', @Result);
	END
