﻿CREATE PROCEDURE [fhir].[SearchPatientsByCriteria]
		@SearchParams nvarchar(max)
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @WhereStr nvarchar(max) = null;
		DECLARE @ConditionStr nvarchar(4000) = null;
		DECLARE @CurParam nvarchar(max) = null;
		DECLARE @CurKey nvarchar(max) = null;
		DECLARE @CurValue nvarchar(max) = null;
		DECLARE @ParamSplitor nvarchar(1) = '&';
		DECLARE @ValueSplitor nvarchar(1) = '=';
		DECLARE @ValueSplitorPos int;

		IF (@SearchParams > '')
		BEGIN
			declare param_cursor cursor LOCAL for
				select value from string_split(@SearchParams, @ParamSplitor);
		
			OPEN param_cursor;
			FETCH NEXT from param_cursor into @CurParam;

			while (@@FETCH_STATUS = 0) 
			begin
				SET @ValueSplitorPos = CHARINDEX(@ValueSplitor, @CurParam);

				IF (@ValueSplitorPos > 1)
				BEGIN
					SET @CurKey = SUBSTRING(@CurParam, 1, @ValueSplitorPos-1);
					SET @CurValue = SUBSTRING(@CurParam, @ValueSplitorPos+1, LEN(@CurParam)-@ValueSplitorPos);
				
					SET @ConditionStr =  
						CASE 
							WHEN (@CurKey = 'id') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'InternalPatientID', 'number')

							WHEN (@CurKey = 'active') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'IsActive', 'bool')
							WHEN (@CurKey = 'deceased') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'IsDeceased', 'bool')

							WHEN (CHARINDEX('name', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'PatientName', default)
							WHEN (CHARINDEX('gender', @CurKey) > 0) THEN fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'Sex', default)

							WHEN (CHARINDEX('address', @CurKey) > 0) and (CHARINDEX('-', @CurKey) = 0) THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'Address', default)
							WHEN (CHARINDEX('address-city', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'City', default)
							WHEN (CHARINDEX('address-state', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'State', default)
							WHEN (CHARINDEX('address-country', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'CountryCode', default)
							WHEN (CHARINDEX('address-postalcode', @CurKey) > 0) THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'ZipCode', default)

							WHEN (@CurKey ='birthdate') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'BirthDate', 'date')
						END

					IF (@ConditionStr IS NOT NULL) AND (@ConditionStr != '')
					BEGIN
						IF (@WhereStr IS NULL) 
						BEGIN
							SET @WhereStr =  @ConditionStr; 
						END
						ELSE
						BEGIN
							SET @WhereStr = @WhereStr + ' AND ' + @ConditionStr; 
						END
					END
				END

  				FETCH NEXT from param_cursor into @CurParam;
			end
			CLOSE param_cursor;
		END

		DECLARE @Pat TABLE ( patID bigint);

		DECLARE @SQL nvarchar(max) = 'SELECT top 100 InternalPatientID from phi.Patient ';

		IF (@WhereStr IS NOT NULL)
		BEGIN
		   SET @SQL = @SQL + ' WHERE ' + @WhereStr;
		END
		SET @SQL = @SQL + ' ORDER BY PatientName ';

		insert into @Pat
		   EXEC (@SQL);

		-- return searched patients reading from View
		SELECT p.* FROM @Pat r 
		  JOIN fhir.Patient_View p on r.patID = p.InternalPatientID;
   
	END
