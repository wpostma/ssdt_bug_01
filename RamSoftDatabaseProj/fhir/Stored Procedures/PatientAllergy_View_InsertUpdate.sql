﻿CREATE PROCEDURE [fhir].[PatientAllergy_View_InsertUpdate]
		@InternalPatientID bigint,
		@JsonText nvarchar(max),
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientAllergy_View_InsertUpdate - Start - @JsonText=', substring(@JsonText, 1, 4000), @InternalPatientID);

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @AllergiesTab TABLE (
		   onset NVARCHAR(64),
		   substance NVARCHAR(64), 
		   [status] NVARCHAR(64),
		   criticality NVARCHAR(64), 
		   [type] NVARCHAR(64), 
		   category NVARCHAR(64), 
		   note NVARCHAR(1000));

   		DECLARE @Onset nvarchar(64) = NULL;
 		DECLARE @Substance nvarchar(64) = NULL;
		DECLARE @Status nvarchar(64) = NULL;
		DECLARE @Criticality nvarchar(64) = NULL;
		DECLARE @Type nvarchar(64) = NULL;
		DECLARE @Category nvarchar(64) = NULL;
		DECLARE @Note nvarchar(1000) = NULL;

		DECLARE Json_Cursor CURSOR LOCAL FOR
			  SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
    		insert into fhir.LogTable(logitem, logdesc, logint) values(@CurKey, @CurValue, @CurType);

			IF (@CurKey = 'onset') SET @Onset = @CurValue;
			IF (@CurKey = 'substance') 
			BEGIN 
			   IF (IsJson(@CurValue) = 0)
			   BEGIN
			      SET @Substance = @CurValue;
			   END
			   ELSE
			   BEGIN
				   IF (JSON_VALUE(@CurValue, '$.coding.display') IS NOT NULL) 
					  SET @Substance = JSON_VALUE(@CurValue, '$.coding.display')
				   ELSE IF (JSON_VALUE(@CurValue, '$.coding.code') IS NOT NULL) 
					  SET @Substance = JSON_VALUE(@CurValue, '$.coding.code');
			   END
			END

			IF (@CurKey = 'status')
			BEGIN
			   SET @Status = (SELECT TOP 1 AllergyStatusName FROM code.AllergyStatus where AllergyStatusName = @CurValue or AllergyStatusCode = @CurValue);
			   IF (@Status IS NULL) OR (@Status = '')  SET @Status = @CurValue;
			END

			IF (@CurKey = 'criticality') 
			BEGIN
			   SET @Criticality = (SELECT AllergyCriticalityName FROM code.AllergyCriticality where AllergyCriticalityName = @CurValue OR AllergyCriticalityCode = @CurValue);
			   IF (@Criticality IS NULL) OR (@Criticality = '') SET @Criticality = @CurValue;
			END

			IF (@CurKey = 'type') SET @Type = @CurValue;
			IF (@CurKey = 'category') SET @Category = @CurValue;
			IF (@CurKey = 'note') SET @Note = @CurValue;

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor;
		DEALLOCATE Json_Cursor; 

     	insert into fhir.LogTable(logitem, logdesc, logtime) values('PatientAllergy_View_InsertUpdate - Found @Substance', @Substance, default);

		IF (@Substance IS NOT NULL) AND (@Substance <> '')
		BEGIN

			DECLARE @ExtJson nvarchar(max) = (Select ExtJson from PHI.Patient where InternalPatientID = @InternalPatientID);
 			DECLARE @AllergiesJson nvarchar(max) = JSON_QUERY(@ExtJson, '$.allergies');

			insert into fhir.LogTable(logitem, logdesc, logtime) values('PatientAllergy_View_InsertUpdate - OLD @AllergiesJson', @AllergiesJson, default);

			DELETE FROM @AllergiesTab;  -- CLEAN before insert
	
			INSERT INTO @AllergiesTab 
			   SELECT JSON_VALUE([VALUE], '$.onset'), JSON_VALUE([VALUE], '$.substance'), JSON_VALUE([VALUE], '$.status'), JSON_VALUE([VALUE], '$.criticality'), 
					  JSON_VALUE([VALUE], '$.type'), JSON_VALUE([VALUE], '$.category'), JSON_VALUE([VALUE], '$.note') 
			   FROM OPENJSON(@AllergiesJson);
   
			IF (EXISTS (SELECT 1 FROM @AllergiesTab WHERE substance = @Substance))
			BEGIN
			   UPDATE @AllergiesTab SET onset = @Onset, [status] = @Status, criticality = @Criticality, [type] = @Type, category = @Category, note = @Note
				 WHERE substance = @Substance;   
			END
			ELSE
			BEGIN
			  INSERT INTO @AllergiesTab VALUES( @Onset, @Substance, @Status, @Criticality, @Type, @Category, @Note);
			END

			SET @AllergiesJson =(SELECT * FROM @AllergiesTab FOR JSON PATH);

			insert into fhir.LogTable(logitem, logdesc, logtime) values('PatientAllergy_View_InsertUpdate - NEW @AllergiesJson', @AllergiesJson, default);

			IF (@ExtJson IS NULL) 
			BEGIN
			   SET @ExtJson = '{ }';   --make an empty json
			END

			SET @ExtJson = JSON_MODIFY(@ExtJson, '$.allergies', JSON_QUERY(@AllergiesJson)); 
	 
			UPDATE PHI.Patient SET ExtJson = @ExtJson 
				OUTPUT inserted.InternalPatientID INTO @ProcessedIDTab
				WHERE InternalPatientID = @InternalPatientID;
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientAllergy_View_InsertUpdate - End', 'OK', @Result);
	END
