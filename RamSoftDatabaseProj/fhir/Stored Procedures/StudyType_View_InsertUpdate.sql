﻿CREATE PROCEDURE [fhir].[StudyType_View_InsertUpdate]
		@InternalStudyTypeID bigint,
		@JsonText nvarchar(max),
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DECLARE @NewInternalStudyTypeID bigint;
		DECLARE @IsActive bit = 1;  --default: true
		DECLARE @StudyType nvarchar(16) = NULL;
		DECLARE @Description nvarchar(64) = NULL;
		DECLARE @ExtJson nvarchar(max) = NULL;
		DECLARE @proceduresJson nvarchar(max) = NULL;		
		
		BEGIN TRY
		BEGIN TRANSACTION
			IF (ISJSON(@JsonText) > 0)
			BEGIN
				SET @IsActive = (CASE JSON_VALUE(@JsonText, '$.active') WHEN 'true' THEN 1 ELSE 0 END);
				SET @StudyType = JSON_VALUE(@JsonText, '$.studyType');
				SET @Description = JSON_VALUE(@JsonText, '$.description');
				SET @proceduresJson = JSON_QUERY(@JsonText, '$.procedures');		

				IF (@InternalStudyTypeID > 0)
				BEGIN
					SET @ExtJson = (SELECT ExtJson from dbo.StudyType where InternalStudyTypeID = @InternalStudyTypeID) ;
				END
		   
				IF (@ExtJson IS NULL)
				BEGIN
					SET @ExtJson = '{ }';
				END
		   
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.codeValue', JSON_VALUE(@JsonText, '$.codeValue'));
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.codeScheme', JSON_VALUE(@JsonText, '$.codeScheme'));
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.modality', JSON_QUERY(@JsonText, '$.modality'));	
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.modalityModifier', JSON_QUERY(@JsonText, '$.modalityModifier'));
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.laterality', JSON_VALUE(@JsonText, '$.laterality'));
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.bodyPart', JSON_QUERY(@JsonText, '$.bodyPart'));	
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.anatomicFocus', JSON_QUERY(@JsonText, '$.anatomicFocus'));	
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.pharmaceutical', JSON_QUERY(@JsonText, '$.pharmaceutical'));	
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.technique', JSON_VALUE(@JsonText, '$.technique'));	
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.procedureModifier', JSON_QUERY(@JsonText, '$.procedureModifier'));	
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.duration', JSON_VALUE(@JsonText, '$.duration'));
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.physicianRequired', JSON_VALUE(@JsonText, '$.physicianRequired'));
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.instructions', JSON_VALUE(@JsonText, '$.instructions'));	
				SET @ExtJson = JSON_MODIFY(@ExtJson,  '$.resource', JSON_QUERY(@JsonText, '$.resource'));		   	
			
				IF (@IsActive IS NULL)
				BEGIN
					SET @IsActive = 1;
				END

				IF (@InternalStudyTypeID > 0)
				BEGIN
					UPDATE dbo.StudyType SET
				   			StudyType = @StudyType,
							[Description] = @Description,
							IsActive = @IsActive,
							ExtJson = @ExtJson
						OUTPUT inserted.InternalStudyTypeID INTO @ProcessedIDTab
					WHERE (InternalStudyTypeID = @InternalStudyTypeID);
				END
				ELSE
				BEGIN
					INSERT INTO dbo.StudyType (
							StudyType,
							[Description],
							IsActive,
							ExtJson)
						OUTPUT inserted.InternalStudyTypeID INTO @ProcessedIDTab
					VALUES (
							@StudyType,
							@Description,
							@IsActive,
							@ExtJson);			
				END

				SET @NewInternalStudyTypeID = (SELECT TOP 1 id FROM @ProcessedIDTab);

				-- Save procedures in table StudyTypeDetail
				Exec fhir.[InsertUpdateStudyTypeDetail] @NewInternalStudyTypeID, @proceduresJson;

				SET @Result = @NewInternalStudyTypeID;
				INSERT INTO fhir.LogTable(logitem, logdesc, logint) values('StudyType_View_InsertUpdate End', 'OK', @Result);

			END
			ELSE
			BEGIN
				SET @Result = -1;
				insert into fhir.LogTable(logitem, logdesc, logint) values('StudyType_View_InsertUpdate End', 'Invalid JSON', @InternalStudyTypeID);
			END

			COMMIT TRANSACTION	
		END TRY
		BEGIN CATCH
			insert into fhir.LogTable(logitem, logdesc, logtime) values('StudyType_View_InsertUpdate ERROR OCCURED ROLLBACK', @InternalStudyTypeID , default);		

			Declare @ErrorNumber   int;
			Declare @ErrorMessage  nvarchar(max);
			Declare @ErrorSeverity int;
			Declare @ErrorState    int;

			SELECT @ErrorNumber   = ERROR_NUMBER(),
				   @ErrorMessage  = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState    = ERROR_STATE();
			insert into fhir.LogTable(logitem, logdesc, logtime) values('StudyType_View_InsertUpdate ERROR MESSAGE ', @ErrorMessage , default);		
			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);			
			ROLLBACK TRANSACTION
		END CATCH
	END

