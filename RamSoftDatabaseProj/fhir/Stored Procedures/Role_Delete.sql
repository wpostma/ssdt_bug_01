﻿
CREATE PROCEDURE [fhir].[Role_Delete]
		@InternalRoleID bigint,
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;	
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DELETE FROM dbo.Role	
			OUTPUT deleted.InternalRoleID INTO @ProcessedIDTab
			WHERE (InternalRoleID = @InternalRoleID) AND 
			      (InternalRoleID > 0); 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END

