﻿
	CREATE PROCEDURE [fhir].[EncounterCoverage_View_InsertUpdate]
			@InternalEncounterCoverageID bigint,
			@JsonText nvarchar(max),
			@InsuredID nvarchar(1000),  --encrypted value (to be)
			@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		--insert into fhir.LogTable(logitem, logdesc, logint) values('EncounterCoverage_View_InsertUpdate - Start - @JsonText=', substring(@JsonText, 1, 4000), @InternalEncounterCoverageID);

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @IsActive bit = 1;  --default: true
		DECLARE @InternalEncounterID bigint;
		DECLARE @Payer nvarchar(64) = NULL;
		DECLARE @CoverageLevelID int;
		DECLARE @CoverageLevel nvarchar(64) = NULL;
		DECLARE @CoverageStatus nvarchar(64) = NULL;
		DECLARE @GroupID nvarchar(64) = NULL;
		DECLARE @StartDate date = NULL;
		DECLARE @EndDate date = NULL;
		DECLARE @AccidentType nvarchar(1000) = NULL;
		DECLARE @AccidentPlace nvarchar(1000) = NULL;
		DECLARE @DateOfInjury nvarchar(10) = NULL;
		DECLARE @CopayType nvarchar(10) = NULL;
		DECLARE @CopayAmount nvarchar(100) = NULL;
		DECLARE @CopayPercent nvarchar(100) = NULL;
		DECLARE @Notes nvarchar(4000) = NULL;
		DECLARE @InsuredInternalPatientID bigint;
		DECLARE @InsuredRelationshipCode nvarchar(64);
		DECLARE @InsuredRelationshipDisplay nvarchar(64);

		DECLARE Json_Cursor CURSOR LOCAL FOR
				SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
    		--insert into fhir.LogTable(logitem, logdesc, logint) values(@CurKey, @CurValue, @CurType);

			IF (@CurKey = 'active') SET @IsActive = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'issuer') SET @Payer = @CurValue;

			IF (@CurKey = 'group') SET @GroupID = @CurValue;

			IF (@CurKey = 'sequence') SET @CoverageLevelID = @CurValue; 
			IF (@CurKey = 'coverageLevel') SET @CoverageLevel = @CurValue; 
			IF (@CurKey = 'coverageStatus') SET @CoverageStatus = @CurValue; 

			IF (@CurKey = 'copayType') SET @CopayType = @CurValue;

			IF (@CurKey = 'copayAmount')
			BEGIN
				SET @CopayAmount = (SELECT CASE WHEN TRY_CONVERT(smallmoney, @CurValue) IS NULL THEN NULL  ELSE @CurValue END);
			END

			IF (@CurKey = 'copayPercent') 
			BEGIN
				SET @CopayPercent = (SELECT CASE WHEN TRY_CONVERT(int, @CurValue) IS NULL THEN NULL  ELSE @CurValue END);
			END

			IF (@CurKey = 'period') 
			BEGIN
				SET @StartDate = CONVERT(date, JSON_VALUE(@CurValue, '$.start'));
				SET @EndDate = CONVERT(DATE, JSON_VALUE(@CurValue, '$.end'));
			END 

			IF (@CurKey = 'encounterReference') 
			BEGIN
				SET @InternalEncounterID = (SELECT CASE WHEN TRY_CONVERT(bigint, JSON_VALUE(@CurValue, '$.id')) IS NULL THEN NULL  ELSE JSON_VALUE(@CurValue, '$.id') END);
			END

			IF (@CurKey = 'planholderReference')  -- this is prior then planholderIdentifier
			BEGIN
			   SET @InsuredInternalPatientID = (SELECT CASE WHEN TRY_CONVERT(bigint, JSON_VALUE(@CurValue, '$.id')) IS NULL THEN NULL  ELSE JSON_VALUE(@CurValue, '$.id') END);

			   IF (JSON_VALUE(@CurValue, '$.updatePatientInfo') > 0)
			   BEGIN
				  DECLARE @return_value int, @UpdateResult bigint;
				  EXEC	@return_value = [fhir].[UpdatePatientWithReferenceJson]
						@InternalPatientID = @InsuredInternalPatientID,
						@InputJson = @CurValue,
						@Result = @UpdateResult OUTPUT;
			   END
			END
			IF (@CurKey = 'planholderIdentifier') AND (@InsuredInternalPatientID IS NULL OR @InsuredInternalPatientID <= 0) 
			BEGIN
				SET @InsuredInternalPatientID = (SELECT CASE WHEN TRY_CONVERT(bigint, JSON_VALUE(@CurValue, '$.value')) IS NULL THEN NULL  ELSE JSON_VALUE(@CurValue, '$.value') END);
			END 

			IF (@CurKey = 'relationship') 
			BEGIN
				SET @InsuredRelationshipCode = JSON_VALUE(@CurValue, '$.code');
				SET @InsuredRelationshipDisplay = JSON_VALUE(@CurValue, '$.display');
			END

			IF (@CurKey = 'accidentType') SET @AccidentType = @CurValue;
			IF (@CurKey = 'accidentPlace') SET @AccidentPlace = @CurValue;
			IF (@CurKey = 'dateOfInjury') SET @DateOfInjury = @CurValue;
			IF (@CurKey = 'notes') SET @Notes = @CurValue;

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor; 
		DEALLOCATE Json_Cursor;
	
		DECLARE @InternalPayerID int = (SELECT InternalPayerID FROM dbo.Payer WHERE CompanyName = @Payer);

		DECLARE @InsuredRelationshipID int = (SELECT BeneficiaryRelationshipID FROM code.BeneficiaryRelationship WHERE BeneficiaryRelationshipName = @InsuredRelationshipDisplay);
		IF (@InsuredRelationshipID IS NULL) OR (@InsuredRelationshipID <= 0) 
		BEGIN
			SET @InsuredRelationshipID = (SELECT BeneficiaryRelationshipID FROM code.BeneficiaryRelationship WHERE BeneficiaryRelationshipCode = @InsuredRelationshipCode);
		END

		IF (@CoverageLevel IS NOT NULL) AND (EXISTS(SELECT 1 FROM code.CoverageLevel WHERE CoverageLevel = @CoverageLevel))
		BEGIN
			SET @CoverageLevelID = (SELECT CoverageLevelID FROM code.CoverageLevel WHERE CoverageLevel = @CoverageLevel);
		END

		IF (@CoverageStatus IS NOT NULL) 
		BEGIN
			IF (UPPER(@CoverageStatus) <> 'ACTIVE') 
			BEGIN
				SET @IsActive = 0;
			END
		END
		ELSE
		BEGIN
			SET @CoverageStatus = (CASE @CurValue WHEN 'false' THEN 'INACTIVE' ELSE 'ACTIVE' END);
		END

		DECLARE @ExtJson nvarchar(max) = NULL;
		IF (@InternalEncounterCoverageID > 0)
		BEGIN
			SET @ExtJson = (SELECT ExtJson FROM phi.EncounterCoverage WHERE InternalEncounterCoverageID = @InternalEncounterCoverageID);
		END

		IF (@ExtJson IS NULL) OR (@ExtJson = '')
		BEGIN	
			SET @ExtJson = '{ }'; 
		END
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.coverageStatus', @CoverageStatus);

		IF (@CopayType IS NULL) OR (@CopayType NOT IN ('$', '%')) 
		BEGIN
			SET @CopayType = '$';
		END
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.copayType', @CopayType);		
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.copayAmount', @CopayAmount);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.copayPercent', @CopayPercent);

		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.notes', @Notes);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.accidentType', @AccidentType);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.accidentPlace', @AccidentPlace);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.dateOfInjury', @DateOfInjury);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.claimAdjustor', JSON_QUERY(@JsonText, '$.claimAdjustor'));
	
		--insert into fhir.LogTable(logitem, logdesc, logtime) values('EncounterCoverage_View_InsertUpdate @ExtJson', @ExtJson, default);

		IF (@InternalEncounterCoverageID > 0)
		BEGIN
			UPDATE phi.EncounterCoverage SET
						InternalEncounterID = @InternalEncounterID,
						CoverageLevelID = @CoverageLevelID,
						InternalPayerID = @InternalPayerID,
						InsuredID = @InsuredID,
						GroupID = @GroupID,
						StartDate = @StartDate,
						EndDate = @EndDate,
						IsActive = @IsActive,
						ExtJson = @ExtJson,
						InsuredInternalPatientID = @InsuredInternalPatientID,
						InsuredRelationshipID = @InsuredRelationshipID
					OUTPUT inserted.InternalEncounterCoverageID INTO @ProcessedIDTab
				WHERE InternalEncounterCoverageID = @InternalEncounterCoverageID;		

			--insert into fhir.LogTable(logitem, logdesc, logint) values('EncounterCoverage_View_InsertUpdate', 'UPDATED', @InternalEncounterCoverageID);
		END
		ELSE
		BEGIN
			  insert into phi.EncounterCoverage (
						InternalEncounterID,
						CoverageLevelID,
						InternalPayerID,
						InsuredID,
						GroupID,
						StartDate,
						EndDate,
						IsActive,
						ExtJson,
						InsuredInternalPatientID,
						InsuredRelationshipID)
					OUTPUT inserted.InternalEncounterCoverageID INTO @ProcessedIDTab
				values (
						@InternalEncounterID,
						@CoverageLevelID,
						@InternalPayerID,
						@InsuredID, 
						@GroupID,
						@StartDate,
						@EndDate,
						@IsActive,
						@ExtJson,
						@InsuredInternalPatientID,
						@InsuredRelationshipID);

			--insert into fhir.LogTable(logitem, logdesc, logint) values('EncounterCoverage_View_InsertUpdate', 'INSERTED', @InternalEncounterCoverageID);
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
		--insert into fhir.LogTable(logitem, logdesc, logint) values('EncounterCoverage_View_InsertUpdate End', null, @Result);
	END
