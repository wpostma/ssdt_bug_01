﻿
CREATE procedure [fhir].[sp_InsertUpdateEncounterByStudy]
(
	@internalStudyID bigint,
	@JsonInput nvarchar(max)
)
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	    
	    
	BEGIN
		--insert into fhir.LogTable(logitem, logdesc, logint) values('[START] - InsertUpdateEncounterID', @JsonInput, @internalStudyID);
		BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @Count int = 0;
			DECLARE @PrimaryEncounterID bigint;
			DECLARE @CurKey NVARCHAR(512);
 			DECLARE	@CurValue NVARCHAR(MAX);
			DECLARE	@CurType int;

			--Declare a temp table it will be use to store all the procedureIDs in the new update model from client
			DECLARE @StudyEncounterTab TABLE (
			   EncounterID bigint
			   );
			
			DECLARE @EncounterID bigint;
			DECLARE @newVisitNumber nvarchar(64) = NULL;
			DECLARE @newStatusStateID nvarchar(64) = NULL;
			DECLARE @newEncounterClassID nvarchar(64) = NULL;
			DECLARE @newInsuranceStatusID nvarchar(64) = NULL
			DECLARE @newAmbulatoryStatusID nvarchar(64) = NULL
			DECLARE @newPatientLocation nvarchar(4000) = NULL;
			DECLARE @newDescription nvarchar(4000) = NULL;
			DECLARE @newPatientID bigint;
			DECLARE @ExtJson nvarchar(max);			


			--Declare Json Cursor for loop through each row in the model Json
			DECLARE Json_Cursor CURSOR LOCAL FOR
				  SELECT * FROM OPENJSON(@JsonInput, '$.entry');

			OPEN Json_Cursor;
			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

			WHILE (@@FETCH_STATUS = 0) 
			BEGIN
				insert into fhir.LogTable(logitem, logdesc, logint) values('[Json_Cursur] ' ,  @CurValue, @CurType);
    			--insert into fhir.LogTable(logitem, logdesc, logint) values(@CurKey, @CurValue, @CurType);

				IF (@CurValue <> '')
				BEGIN
					insert into fhir.LogTable(logitem, logdesc, logint) values('EnterCurValue', @CurValue, @CurType);
					DECLARE @ProKey NVARCHAR(512);
 					DECLARE	@ProValue NVARCHAR(MAX);
					DECLARE	@ProType int;

					--Declare Property_Cursor for looping throught each property and get value
					DECLARE Property_Cursor CURSOR LOCAL FOR
						SELECT * FROM OPENJSON(@CurValue)	

					OPEN Property_Cursor;
					FETCH NEXT FROM Property_Cursor INTO @ProKey, @ProValue, @ProType;
					WHILE (@@FETCH_STATUS = 0) 
					BEGIN
					insert into fhir.LogTable(logitem, logdesc, logint) values(@ProKey, @ProValue, @ProType);
						IF (@ProKey = 'id') SET @EncounterID = @ProValue;
						IF (@ProKey = 'status') SET @newStatusStateID = (Select EncounterStateID from code.EncounterState Where EncounterStateCode = @ProValue);
						IF (@ProKey = 'visitNumber') SET @newVisitNumber = @ProValue;
						IF (@ProKey = 'patient') SET @newPatientID = JSON_VALUE(@ProValue,'$.id');
						IF (@ProKey = 'class') SET @newEncounterClassID = (Select EncounterClassID From code.EncounterClass Where EncounterClassCode = @ProValue);
						IF (@ProKey = 'insuranceStatusCode') SET @newInsuranceStatusID = (Select InsuranceStatusID From code.InsuranceStatus Where InsuranceStatusCode = @ProValue);
						IF (@ProKey = 'ambulatoryStatusCode') SET @newAmbulatoryStatusID = (Select AmbulatoryStatusID From code.AmbulatoryStatus Where AmbulatoryStatusCode = @ProValue);
						IF (@ProKey = 'patientLocation') SET @newPatientLocation = @ProValue;
						IF (@ProKey = 'description') SET @newDescription = @ProValue;
						IF (@ProKey = 'isPrimary')
						Begin						 
						 IF @ProValue = 'true'						 
						 Begin														
							Set @PrimaryEncounterID = @EncounterID;
							Set @Count = @Count + 1
						 End
						End													
						FETCH NEXT FROM Property_Cursor INTO @ProKey, @ProValue, @ProType;
					END	
					CLOSE Property_Cursor;
					DEALLOCATE Property_Cursor; 			
				END

			
			--insert into fhir.LogTable(logitem, logdesc, logtime) values('Checking IF The EncounterID is Exist or not', @internalStudyID , default);		
			--insert into fhir.LogTable(logitem, logdesc, logtime) values('Checking IF The EncounterID is Exist or not', @EncounterID , default);			

			--Check if the the current encounter is exist in the targeted Study 
			IF EXISTS (select Do.InternalEncounterID from phi.Study s Cross apply OpenJson(JSON_Value(s.ExtJson,'$.internalDiagnosticOrderID')) Dids
								JOIN PHI.DiagnosticOrder Do on Do.InternalDiagnosticOrderID = Dids.Value Where s.InternalStudyID = @internalStudyID 
								AND InternalEncounterID = @EncounterID)					
			BEGIN

			--IF exist then Update the current
			Select @ExtJson = ExtJson From Phi.Encounter Where InternalEncounterID = @EncounterID
			
			SET @ExtJson = JSON_MODIFY(@ExtJson, 'lax $.patientLocation', @newPatientLocation);
			SET @ExtJson = JSON_MODIFY(@ExtJson, 'lax $.description', @newDescription);

			--insert into fhir.LogTable(logitem, logdesc, logtime) values('Updating The Encounter is Exist', @newVisitNumber , default);		
				Update Phi.Encounter Set
						AdmissionID = @newVisitNumber,
						--AdmissionDateTime = sysutcdatetime(),
						EncounterClassID = @newEncounterClassID,
						InsuranceStatusID = @newInsuranceStatusID,
						AmbulatoryStatusID = @newAmbulatoryStatusID,
						EncounterStateID = @newStatusStateID,
						ExtJson = @ExtJson
				Where InternalEncounterID = @EncounterID			
			--insert into fhir.LogTable(logitem, logdesc, logtime) values('Update Successfull', @newVisitNumber , default);		
			END
			
			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
			END
			CLOSE Json_Cursor;
			DEALLOCATE Json_Cursor; 	

			IF @Count > 1
			Begin
				DECLARE @ErrorMsg nvarchar(2048) = 
				FORMATMESSAGE(N'A Study can not have two primary encounter');   
				THROW 51000, @ErrorMsg, 1;
			End

			IF @PrimaryEncounterID is not null
			Begin
				Update phi.Study Set PrimaryEncounterID = @PrimaryEncounterID Where InternalStudyID = @internalStudyID
			End
			ELSE
			Begin
				Update phi.Study Set PrimaryEncounterID = NULL Where InternalStudyID = @internalStudyID
			End		
			COMMIT TRANSACTION
			--insert into fhir.LogTable(logitem, logdesc, logtime) values('SUCCESS', @InternalStudyProcedureMapID , default);		
		END TRY
		BEGIN CATCH
			--insert into fhir.LogTable(logitem, logdesc, logtime) values('ERROR OCCURED ROLLBACK', @InternalStudyProcedureMapID , default);		

			Declare @ErrorNumber   int;
			Declare @ErrorMessage  nvarchar(max);
			Declare @ErrorSeverity int;
			Declare @ErrorState    int;

			SELECT @ErrorNumber   = ERROR_NUMBER(),
				   @ErrorMessage  = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState    = ERROR_STATE();
			insert into fhir.LogTable(logitem, logdesc, logtime) values('ERROR MESSAGE ', @ErrorMessage , default);		
			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);			
			ROLLBACK TRANSACTION
		END CATCH
	END
END




