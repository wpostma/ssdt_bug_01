﻿CREATE PROCEDURE [fhir].[Payer_View_Delete]
		@InternalPayerID bigint,
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;	
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DELETE FROM dbo.Payer	
			OUTPUT deleted.InternalPayerID INTO @ProcessedIDTab
			WHERE InternalPayerID = @InternalPayerID; 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END
