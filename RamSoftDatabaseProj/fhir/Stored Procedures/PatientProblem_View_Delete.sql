﻿CREATE PROCEDURE [fhir].[PatientProblem_View_Delete]
		@InternalPatientProblemID bigint,
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DELETE FROM dbo.PatientProblem
				OUTPUT deleted.InternalPatientProblemID INTO @ProcessedIDTab
				WHERE InternalPatientProblemID = @InternalPatientProblemID;

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientProblem_View_Delete - End', 'OK', @Result);
	END
