﻿CREATE PROCEDURE [fhir].[SearchStudyTypesByCriteria]
		@SearchParams nvarchar(max)
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @WhereStr nvarchar(max) = null;
		DECLARE @ConditionStr nvarchar(4000) = null;
		DECLARE @CurParam nvarchar(max) = null;
		DECLARE @CurKey nvarchar(max) = null;
		DECLARE @CurValue nvarchar(max) = null;
		DECLARE @ParamSplitor nvarchar(1) = '&';
		DECLARE @ValueSplitor nvarchar(1) = '=';
		DECLARE @ValueSplitorPos int;
		DECLARE @ReturnCount int = 100;

		IF (CHARINDEX('?', @SearchParams) = 1)
		BEGIN
			SET @SearchParams = STUFF(@SearchParams, 1, 1, ''); 
		END
				
		IF (@SearchParams > '')
		BEGIN
			declare param_cursor cursor LOCAL for
				select value from string_split(@SearchParams, @ParamSplitor);
		
			OPEN param_cursor;
			FETCH NEXT from param_cursor into @CurParam;

			while (@@FETCH_STATUS = 0) 
			begin
				SET @ValueSplitorPos = CHARINDEX(@ValueSplitor, @CurParam);

				IF (@ValueSplitorPos > 1)
				BEGIN
					SET @CurKey = SUBSTRING(@CurParam, 1, @ValueSplitorPos-1);
					SET @CurValue = SUBSTRING(@CurParam, @ValueSplitorPos+1, LEN(@CurParam)-@ValueSplitorPos);

					SET @CurValue = REPLACE(@CurValue, '%20', ' '); 
					
					SET @ConditionStr =  
					    CASE 
						    WHEN (@CurKey = 'Code') THEN  '(s.StudyType  LIKE ''' + @CurValue + '%'')'
							WHEN (@CurKey = 'Description') THEN  '(s.Description LIKE ''%' + @CurValue+ '%'')'
   					    END					

					IF (@ConditionStr IS NOT NULL) AND (@ConditionStr != '')
					BEGIN
					   IF (@WhereStr IS NULL) 
					   BEGIN
						  SET @WhereStr =  @ConditionStr; 
					   END
					   ELSE
					   BEGIN
						  SET @WhereStr = @WhereStr + ' AND ' + @ConditionStr; 
					   END

				   END

				   IF (@CurKey = 'Count') AND (CAST(@CurValue as int) > 0)
				   BEGIN
				      SET @ReturnCount = @CurValue;
				   END
				END

  				FETCH NEXT from param_cursor into @CurParam;
			end
			CLOSE param_cursor;
		END

		DECLARE @Org TABLE ( orgID bigint);

		DECLARE @SQL nvarchar(max) = 'SELECT top ' + CAST(@ReturnCount AS nvarchar) + ' InternalStudyTypeID from dbo.[StudyType] s '; 

		IF (@WhereStr IS NOT NULL)
		BEGIN
		   SET @SQL = @SQL + ' WHERE ' + @WhereStr;
		END
		SET @SQL = @SQL + ' ORDER BY s.StudyType ';

		IF ((@SearchParams > '') AND (@WhereStr IS NOT NULL)) OR (@SearchParams IS NULL) OR (@SearchParams = '')
		BEGIN
		   INSERT INTO @Org
		      EXEC (@SQL);

		   SELECT v.* FROM @Org o
			    JOIN fhir.StudyType_View v on o.orgID = v.InternalStudyTypeID;
		END
	END

