﻿
	CREATE PROCEDURE [fhir].[Patient_View_InsertUpdate]
		@InternalPatientID bigint,
		@JsonText nvarchar(max), 
		@SSN nvarchar(64),  --TO BE encrypted 
		@BirthDate nvarchar(10), -- TO BE encrypted 
		@PatientID nvarchar(64), -- TO BE encrypted 
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;

		--insert into fhir.LogTable(logitem, logdesc, logint) values('Patient_View_InsertUpdate - Start', SUBSTRING(@JsonText,1,4000), @InternalPatientID);

		IF (@InternalPatientID > 0)  -- Check integrity of data for existing patient
		BEGIN
			DECLARE @MetaLastUpdatedStr nvarchar(100) = Json_Value(Json_query(@JsonText, '$.meta'), '$.lastUpdated');
			DECLARE @MetaLastUpdatedUTC datetime2(7) = (SELECT CASE WHEN TRY_CONVERT(datetime2(7), @MetaLastUpdatedStr) IS NULL THEN NULL ELSE @MetaLastUpdatedStr END);
			DECLARE @DBLastUpdatedUTC datetime2(7) = (SELECT lastUpdateUTC FROM PHI.Patient WHERE InternalPatientID = @InternalPatientID);

			IF (@MetaLastUpdatedUTC IS NOT NULL) AND (@DBLastUpdatedUTC > @MetaLastUpdatedUTC) 
			BEGIN
				--insert into fhir.LogTable(logitem, logdesc, logint) values('Patient_View_InsertUpdate -Old Info- @MetaLastUpdatedStr', @MetaLastUpdatedStr, @InternalPatientID);
				--insert into fhir.LogTable(logitem, logdesc, logint) values('Patient_View_InsertUpdate -Old Info- @DBLastUpdatedUTC', Convert(nvarchar, @DBLastUpdatedUTC), @InternalPatientID);

				DECLARE @ErrorMsg nvarchar(2048) = 
					FORMATMESSAGE(N'This Patient (%s) has been updated at %s(UTC) by another user . Please retry after refreshing patient info.', 
					convert(nvarchar, @InternalPatientID), Convert(nvarchar, @DBLastUpdatedUTC));   
				THROW 51000, @ErrorMsg, 1;
			END
			
		END
	
		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @IsActive bit = 0;
		DECLARE @IsDeceased bit = 0;
		DECLARE @PatientName nvarchar(64) = NULL;

		declare @ContactPointJson nvarchar (4000) = NULL;
		declare @InternalAssigningAuthorityID bigint = null;
		declare @PatientGUID nvarchar(256);
		declare @LastupdateUserID bigint;
		declare @SexID int = NULL;
		declare @State nvarchar (64) = NULL;
		declare @CountryCode char (2) = NULL;
		declare @StateID int = NULL;
		declare @CountryID int = NULL;
		declare @FinancialClassID int = NULL;
		declare @AccountStatusID int = NULL;
		declare @PatientBalance smallmoney = 0;
		declare @InsuranceBalance smallmoney = 0;
		declare @ExtJson nvarchar (max) = NULL;
		declare @MaritalStatusID int = NULL;

		DECLARE @DeceasedDate RSDate = NULL;
		DECLARE @Address nvarchar(4000) = NULL;
		DECLARE @ManagingOrganization nvarchar(4000) = NULL;

		DECLARE @Identifier nvarchar(512) = NULL;
		DECLARE @Accountnumber nvarchar(64) = NULL; 
		DECLARE @AccountStatus nvarchar(64) = NULL;
		DECLARE @DirectAddressJson nvarchar(1000) = NULL;
		DECLARE @FinancialClass nvarchar(64) = NULL;

		DECLARE @Language nvarchar(512) = NULL;
		DECLARE @EmploymentStatus nvarchar(64) = NULL;
		DECLARE @Employer nvarchar(512) = NULL; 
		DECLARE @MotherMaidenName nvarchar(64) = NULL;
		DECLARE @Ethnicity nvarchar(512) = NULL; 
		DECLARE @Race nvarchar(512) = NULL; 
		DECLARE @SmokingStatus nvarchar(64) = NULL;  
		DECLARE @SmokingStartDate nvarchar(10) = NULL;  
		DECLARE @PreferredContact nvarchar(64) = NULL;  
		DECLARE @OtherNumber nvarchar(512) = NULL;
		DECLARE @Notes nvarchar(max) = NULL;
		DECLARE @Extension nvarchar(max) = NULL;

		DECLARE @Allergies nvarchar(max) = NULL;
		DECLARE @EmergencyContact nvarchar(max) = NULL;
		DECLARE @InternalEmployerID bigint = NULL;

		DECLARE Json_Cursor CURSOR LOCAL FOR
				SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
    		--insert into fhir.LogTable(logitem, logdesc, logint) values(@CurKey, @CurValue, @CurType);

			IF (@CurKey = 'identifier') SET @Identifier = @CurValue;

			IF (@CurKey = 'active') SET @IsActive = (CASE @CurValue WHEN 'true' THEN 1 ELSE 0 END);
			IF (@CurKey = 'deceasedBoolean') SET @IsDeceased = (CASE @CurValue WHEN 'true' THEN 1 ELSE 0 END);

			IF (@CurKey = 'name') SET @PatientName = fhir.ParseHumanNameJson(@CurValue);

			IF (@CurKey = 'telecom') SET @ContactPointJson = @CurValue;

			IF (@CurKey = 'gender') SET @SexID = (SELECT SexID FROM code.Sex WHERE Sex = @CurValue);

			IF (@CurKey = 'deceasedDate') SET @DeceasedDate = @CurValue;

			IF (@CurKey = 'address') 
			BEGIN
				--insert into fhir.LogTable(logitem, logdesc, logtime) values('Patient_View_InsertUpdate new Address ', @CurValue, default);
				SET @Address = @CurValue;

				SET @CountryID = NULL;
				SET @StateID = NULL;
				SELECT TOP 1 @State = JSON_VALUE([value], '$.state'), @CountryCode = JSON_VALUE([value], '$.country')
					FROM OPENJSON(@CurValue);
       
				SELECT @CountryID = CountryID from dbo.Country where CountryCode = @CountryCode;
				IF (@CountryID IS NULL) OR (@CountryID <= 0)
				BEGIN
					SELECT @CountryID = CountryID from dbo.Country where CountryName = @CountryCode;
				END

				SELECT @StateID = StateID from code.State where [State] = @State and CountryID = @CountryID;
				IF (@StateID IS NULL) OR (@StateID <= 0)
				BEGIN
					SELECT @StateID = StateID from code.State where StateCode = @State and CountryID = @CountryID;
				END
			END

			IF (@CurKey = 'maritalStatus') SET @MaritalStatusID = fhir.GetMaritalStatusIDByJson(@CurValue);

			IF (@CurKey = 'managingOrganization') 
			BEGIN
				SET @InternalAssigningAuthorityID = NULL;

				DECLARE @AssigningAuthorityName nvarchar(64) = JSON_VALUE(@CurValue, '$.display');
				IF ((@AssigningAuthorityName IS NOT NULL) AND (@AssigningAuthorityName != ''))
				BEGIN
					SELECT @InternalAssigningAuthorityID = InternalAssigningAuthorityID FROM dbo.AssigningAuthority WHERE Name = @AssigningAuthorityName;
					IF (@InternalAssigningAuthorityID IS NULL) 
					BEGIN 
	  					INSERT INTO dbo.AssigningAuthority(Name, IsActive) VALUES (@AssigningAuthorityName, 1);
						-- GET the new ID
						SELECT @InternalAssigningAuthorityID = InternalAssigningAuthorityID FROM dbo.AssigningAuthority WHERE Name = @AssigningAuthorityName;
					END
				END
			END

			IF (@CurKey = 'extension') SET @Extension = @CurValue;

			-- if encrypted then it should be used
			IF (@CurKey = 'patientID') AND (@PatientID is Null) SET @PatientID = @CurValue;
			IF (@CurKey = 'ssn') AND (@Ssn is NULL) SET @Ssn = @CurValue;
			IF (@CurKey = 'birthdate') AND (@BirthDate is NULL) SET @BirthDate = @CurValue;

			IF (@CurKey = 'accountNumber') SET @Accountnumber = @CurValue;
			IF (@CurKey = 'accountStatus') SET @AccountStatus = @CurValue;
			IF (@CurKey = 'financialClass') SET @FinancialClass = @CurValue;

			IF (@CurKey = 'directAddress') SET @DirectAddressJson = @CurValue;

			IF (@CurKey = 'patientBalance') SET @PatientBalance = (SELECT CASE WHEN TRY_CONVERT(smallmoney, @CurValue) IS NULL THEN NULL ELSE @CurValue END);
			IF (@CurKey = 'insuranceBalance') SET @InsuranceBalance = (SELECT CASE WHEN TRY_CONVERT(smallmoney, @CurValue) IS NULL THEN NULL ELSE @CurValue END);;

			IF (@CurKey = 'language') SET @Language = @CurValue;
			IF (@CurKey = 'employmentStatus') SET @EmploymentStatus = @CurValue;
			IF (@CurKey = 'employer') 
			BEGIN
				SET @InternalEmployerID = (SELECT CASE WHEN TRY_CONVERT(bigint, JSON_VALUE(@CurValue, '$.id')) IS NULL THEN NULL ELSE JSON_VALUE(@CurValue, '$.id') END);
				IF (@InternalEmployerID <= 0) OR (@InternalEmployerID IS NULL)
				BEGIN
					SET @InternalEmployerID = (SELECT InternalEmployerID FROM dbo.Employer WHERE EmployerName = JSON_VALUE(@CurValue, '$.name'));
				END  
			END

			IF (@CurKey = 'motherMaidenName') SET @MotherMaidenName = @CurValue;
			IF (@CurKey = 'ethnicity') SET @Ethnicity = @CurValue;
			IF (@CurKey = 'race') SET @Race = @CurValue;
			IF (@CurKey = 'smokingStatus') SET @SmokingStatus = @CurValue;
			IF (@CurKey = 'smokingStartDate') SET @SmokingStartDate = @CurValue;
			IF (@CurKey = 'preferredContact') SET @PreferredContact = @CurValue;
			IF (@CurKey = 'otherNumber') SET @OtherNumber = @CurValue;
			IF (@CurKey = 'notes') SET @Notes = @CurValue;

			IF (@CurKey = 'allergies') SET @Allergies = @CurValue;

			IF (@CurKey = 'emergencyContact') SET @EmergencyContact = @CurValue;

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor; 
		DEALLOCATE Json_Cursor; 
 
		SELECT @AccountStatusID = AccountStatusID FROM code.AccountStatus WHERE AccountStatus = @AccountStatus; 
		IF (@AccountStatusID IS NULL) OR (@AccountStatusID <= 0)
		BEGIN
			SELECT @AccountStatusID = AccountStatusID FROM code.AccountStatus WHERE AccountStatusCode = @AccountStatus; 
		END

		SELECT @FinancialClassID = FinancialClassID FROM code.FinancialClass WHERE FinancialClass = @FinancialClass; 
		IF (@FinancialClassID IS NULL) OR (@FinancialClassID <= 0)
		BEGIN
			SELECT @FinancialClassID = FinancialClassID FROM code.FinancialClass WHERE FinancialClassCode = @FinancialClass; 
		END

		IF (@InternalPatientID > 0)
		BEGIN
			SELECT @ExtJson = ExtJson FROM phi.Patient WHERE InternalPatientID = @InternalPatientID; --Get the current one 
		END
		IF (@ExtJson IS NULL) OR (@ExtJson = '')
		BEGIN
			SET @ExtJson = '{ }'; -- Initialize as Json text
		END

		IF (ISJSON(@Identifier) > 0)
 			SET @ExtJson = JSON_MODIFY(@ExtJson, '$.identifier', JSON_QUERY(@Identifier));
		ELSE
 			SET @ExtJson = JSON_MODIFY(@ExtJson, '$.identifier', @Identifier);

		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.address', JSON_QUERY(@Address));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.contactPoint', JSON_QUERY(@ContactPointJson));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.directAddress', JSON_QUERY(@DirectAddressJson));
		
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.language', @Language);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.employmentStatus', @EmploymentStatus);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.motherMaidenName', @MotherMaidenName);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.ethnicity', @Ethnicity);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.race', @Race);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.smokingStatus', @SmokingStatus);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.smokingStartDate', @SmokingStartDate);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.preferredContact', @PreferredContact);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.otherNumber', @OtherNumber);
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.notes', @Notes);

		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.allergies', JSON_QUERY(@Allergies));

		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.extension', JSON_QUERY(@Extension));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.emergencyContact', JSON_QUERY(@EmergencyContact));

		SET @LastupdateUserID = 1; -- should be get from session user info, how???

		DECLARE @ProcessedIDTab TABLE (id bigint);

		IF (@InternalPatientID > 0)
		BEGIN
			UPDATE phi.patient SET
					InternalAssigningAuthorityID = @InternalAssigningAuthorityID, 
					IsActive = @IsActive, 
					LastupdateUserID = @LastupdateUserID, 
					PatientID = @PatientID, 
					PatientName = @PatientName, 
					BirthDate = @BirthDate,  
					SexID= @SexID, 
					StateID = @StateID, 
					CountryID = @CountryID, 
					Accountnumber = @Accountnumber, 
					Ssn = @Ssn, 
					FinancialClassID = @FinancialClassID, 
					AccountStatusID = @AccountStatusID, 
					PatientBalance = @PatientBalance, 
					InsuranceBalance = @InsuranceBalance, 
					ExtJson = @ExtJson, 
					IsDeceased = @IsDeceased,
					DeceasedDate = @DeceasedDate,
					MaritalStatusID = @MaritalStatusID,
					InternalEmployerID = @InternalEmployerID
				OUTPUT inserted.InternalPatientID INTO @ProcessedIDTab
			WHERE InternalPatientID = @InternalPatientID; 

			--insert into fhir.LogTable(logitem, logdesc, logint) values('Patient_View_InsertUpdate', 'UPDATED', @InternalPatientID);
		END
		ELSE
		BEGIN
			insert into phi.patient (
						InternalAssigningAuthorityID,
						LastupdateUserID,
						PatientID,
						PatientName,
						BirthDate,
						SexID,
						StateID,
						CountryID,
						Accountnumber,
						Ssn,
						FinancialClassID,
						AccountStatusID,
						PatientBalance,
						InsuranceBalance,
						ExtJson,
						IsDeceased,
						DeceasedDate,
						MaritalStatusID,
						InternalEmployerID)
					OUTPUT inserted.InternalPatientID INTO @ProcessedIDTab
				values (
						@InternalAssigningAuthorityID,
						@LastupdateUserID,
						@PatientID, 
						@PatientName,
						@BirthDate, 
						@SexID,
						@StateID,
						@CountryID,
						@Accountnumber,
						@SSN, 
						@FinancialClassID,
						@AccountStatusID,
						@PatientBalance,
						@InsuranceBalance,
						@ExtJson,
						@IsDeceased,
						@DeceasedDate,
						@MaritalStatusID,
						@InternalEmployerID);

			--insert into fhir.LogTable(logitem, logdesc, logint) values('Patient_View_InsertUpdate', 'INSERTED', @InternalPatientID);
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);

		--insert into fhir.LogTable(logitem, logdesc, logint) values('Patient_View_insertUpdate End', 'OK', @Result);
	END
