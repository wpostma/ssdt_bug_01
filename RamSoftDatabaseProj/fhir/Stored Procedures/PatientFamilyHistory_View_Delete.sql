﻿CREATE PROCEDURE [fhir].[PatientFamilyHistory_View_Delete]
		@InternalFamilyHistoryID bigint,
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DELETE FROM dbo.PatientFamilyHistory
				OUTPUT deleted.InternalFamilyHistoryID INTO @ProcessedIDTab
				WHERE InternalFamilyHistoryID = @InternalFamilyHistoryID;

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);

		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientFamilyHistory_View_Delete - End', 'OK', @Result);
	END
