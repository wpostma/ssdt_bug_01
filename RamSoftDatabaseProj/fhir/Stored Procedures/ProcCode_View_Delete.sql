﻿
CREATE PROCEDURE [fhir].[ProcCode_View_Delete]
		@InternalProcedureCodeID bigint,
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;	
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DELETE FROM code.ProcedureCode	
			OUTPUT deleted.InternalProcedureCodeID INTO @ProcessedIDTab
			WHERE InternalProcedureCodeID = @InternalProcedureCodeID; 

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END

