﻿
CREATE PROCEDURE [fhir].[SearchRolesByCriteria]
		@SearchParams nvarchar(max)
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @WhereStr nvarchar(max) = ' (r.InternalRoleID > 0) ';
		DECLARE @ConditionStr nvarchar(4000) = null;
		DECLARE @CurParam nvarchar(max) = null;
		DECLARE @CurKey nvarchar(max) = null;
		DECLARE @CurValue nvarchar(max) = null;
		DECLARE @ParamSplitor nvarchar(1) = '&';
		DECLARE @ValueSplitor nvarchar(1) = '=';
		DECLARE @ValueSplitorPos int;
		DECLARE @ReturnCount int = 100;

		IF (CHARINDEX('?', @SearchParams) = 1)
		BEGIN
			SET @SearchParams = STUFF(@SearchParams, 1, 1, ''); 
		END
				
		IF (@SearchParams > '')
		BEGIN
			declare param_cursor cursor LOCAL for
				select value from string_split(@SearchParams, @ParamSplitor);
		
			OPEN param_cursor;
			FETCH NEXT from param_cursor into @CurParam;

			while (@@FETCH_STATUS = 0) 
			begin
				SET @ValueSplitorPos = CHARINDEX(@ValueSplitor, @CurParam);

				IF (@ValueSplitorPos > 1)
				BEGIN
					SET @CurKey = SUBSTRING(@CurParam, 1, @ValueSplitorPos-1);
					SET @CurValue = SUBSTRING(@CurParam, @ValueSplitorPos+1, LEN(@CurParam)-@ValueSplitorPos);

					SET @CurValue = REPLACE(@CurValue, '%20', ' '); 

					SET @ConditionStr =  
					   CASE 
						   WHEN (@CurKey = 'name') THEN  fhir.ComposeWhereCondititon (@CurKey, @CurValue, 'r.Name', default)
   					   END

					IF (@ConditionStr IS NOT NULL) AND (@ConditionStr != '')
					BEGIN
					   IF (@WhereStr IS NULL) 
					   BEGIN
						  SET @WhereStr =  @ConditionStr; 
					   END
					   ELSE
					   BEGIN
						  SET @WhereStr = @WhereStr + ' AND ' + @ConditionStr; 
					   END
				   END

				   IF (@CurKey = 'Count') AND (CAST(@CurValue as int) > 0)
				   BEGIN
				      SET @ReturnCount = @CurValue;
				   END
				END

  				FETCH NEXT from param_cursor into @CurParam;
			end
			CLOSE param_cursor;
		END

		DECLARE @Org TABLE ( orgID bigint);

		DECLARE @SQL nvarchar(max) = 'SELECT top ' + CAST(@ReturnCount AS nvarchar) + ' InternalRoleID from dbo.[Role] r '; 

		IF (@WhereStr IS NOT NULL)
		BEGIN
		   SET @SQL = @SQL + ' WHERE ' + @WhereStr;
		END
		SET @SQL = @SQL + ' ORDER BY r.Name ';

		insert into @Org
		   EXEC (@SQL);

		SELECT r.* FROM @Org o
			  JOIN fhir.Role_View r on o.orgID = r.InternalRoleID;
	END
