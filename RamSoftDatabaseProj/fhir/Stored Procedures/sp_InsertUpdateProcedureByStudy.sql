﻿
CREATE procedure [fhir].[sp_InsertUpdateProcedureByStudy]
(
	@internalStudyID bigint,
	@JsonInput nvarchar(max)
)
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	    
	    
	BEGIN
		--insert into fhir.LogTable(logitem, logdesc, logint) values('[START] - InsertUpdateProcedureCodeByID', '', @internalStudyID);
		BEGIN TRY
		BEGIN TRANSACTION

			IF @JsonInput = ''
			BEGIN
				--insert into fhir.LogTable(logitem, logdesc, logint) values('Json Input is empty', '', @internalStudyID);
				DELETE from StudyProcedureMap Where InternalStudyID = @internalStudyID
				Return
			END

			DECLARE @CurKey NVARCHAR(512);
 			DECLARE	@CurValue NVARCHAR(MAX);
			DECLARE	@CurType int;

			--Declare a temp table it will be use to store all the procedureIDs in the new update model from client
			DECLARE @StudyProcedureMapTab TABLE (
			   InternalStudyProcedureMapID bigint
			   );
			
   			DECLARE @InternalStudyProcedureMapID bigint = NULL; 		

			DECLARE @newQuantity smallint = null;
			DECLARE @newModifier nvarchar(8) = NULL;
			DECLARE @newAuthorizationNumber nvarchar(64) = NULL;
			DECLARE @newAuthorizationStartDate date = NULL;
			DECLARE @newAuthorizationEndDate date = NULL;
			DECLARE @newInternalProcedureCodeID int = NULL;
			DECLARE @newInternalProcedureStatusID int = NULL;
			DECLARE @newExtJson nvarchar(max) = NULL;

			--Declare Json Cursor for loop through each row in the model Json
			DECLARE Json_Cursor CURSOR LOCAL FOR
				  SELECT * FROM OPENJSON(@JsonInput, '$.entry');

			OPEN Json_Cursor;
			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

			WHILE (@@FETCH_STATUS = 0) 
			BEGIN
				--insert into fhir.LogTable(logitem, logdesc, logint) values('[Json_Cursur] ' ,  @CurValue, @CurType);
    			--insert into fhir.LogTable(logitem, logdesc, logint) values(@CurKey, @CurValue, @CurType);

				IF (@CurValue <> '')
				BEGIN
					--insert into fhir.LogTable(logitem, logdesc, logint) values('EnterCurValue', @CurValue, @CurType);
					DECLARE @ProKey NVARCHAR(512);
 					DECLARE	@ProValue NVARCHAR(MAX);
					DECLARE	@ProType int;

					--Declare Property_Cursor for looping throught each property and get value
					DECLARE Property_Cursor CURSOR LOCAL FOR
						SELECT * FROM OPENJSON(@CurValue)	

					OPEN Property_Cursor;
					FETCH NEXT FROM Property_Cursor INTO @ProKey, @ProValue, @ProType;
					WHILE (@@FETCH_STATUS = 0) 
					BEGIN
					insert into fhir.LogTable(logitem, logdesc, logint) values(@ProKey, @ProValue, @ProType);
						IF (@ProKey = 'id') SET @InternalStudyProcedureMapID = @ProValue;
						IF (@ProKey = 'code') SET @newInternalProcedureCodeID = (Select InternalProcedureCodeID from code.ProcedureCode Where ProcedureCode = JSON_VALUE(@ProValue,'$.coding'));
						IF (@ProKey = 'modifier') SET @newModifier = @ProValue;
						IF (@ProKey = 'quantity') SET @newQuantity = @ProValue;
						IF (@ProKey = 'authorizationNumber') SET @newAuthorizationNumber = @ProValue;
						IF (@ProKey = 'authorizationStartDate') SET @newAuthorizationStartDate = @ProValue;
						IF (@ProKey = 'authorizationEndDate') SET @newAuthorizationEndDate = @ProValue;									
						FETCH NEXT FROM Property_Cursor INTO @ProKey, @ProValue, @ProType;
					END	
					CLOSE Property_Cursor;
					DEALLOCATE Property_Cursor; 			
				END						

			--Check if the the current procedure is exist in database 
			IF EXISTS (Select InternalStudyProcedureMapID From dbo.StudyProcedureMap where InternalStudyID = @internalStudyID AND InternalStudyProcedureMapID = @InternalStudyProcedureMapID)					
			BEGIN
			--IF exist then Update the current
			insert into fhir.LogTable(logitem, logdesc, logtime) values('Updating The Procedure is Exist', @newModifier , default);		
				Update dbo.StudyProcedureMap Set
						Quantity = @newQuantity,
						Modifier = @newModifier,
						AuthorizationNumber = @newAuthorizationNumber,
						AuthorizationStartDate = @newAuthorizationStartDate,
						AuthorizationEndDate = @newAuthorizationEndDate,
						InternalProcedureCodeID = @newInternalProcedureCodeID
						--InternalProcedureStatusID = @newInternalProcedureStatusID
				Where InternalStudyProcedureMapID = @InternalStudyProcedureMapID			
			insert into fhir.LogTable(logitem, logdesc, logtime) values('Update Successfull', @newModifier , default);		
			END
			ELSE
			BEGIN
				--IF not exists then insert the new one
				IF NOT EXISTS (Select InternalStudyProcedureMapID From dbo.StudyProcedureMap where InternalStudyID = @internalStudyID AND InternalStudyProcedureMapID = @InternalStudyProcedureMapID)					
				BEGIN
					insert into fhir.LogTable(logitem, logdesc, logtime) values('Inserting The Procedure is not Exist', @newModifier , default);		

					Insert into dbo.StudyProcedureMap(InternalStudyID, Quantity, Modifier, AuthorizationNumber, AuthorizationStartDate, AuthorizationEndDate, InternalProcedureCodeID, InternalProcedureStatusID, ExtJson)				
					VALUES(@internalStudyID,@newQuantity, @newModifier, @newAuthorizationNumber, @newAuthorizationStartDate, @newAuthorizationEndDate, @newInternalProcedureCodeID, @newInternalProcedureStatusID, @newExtJson);
					
					--Store the newly inserted studyProcedureMap					
					Set @InternalStudyProcedureMapID = (SELECT SCOPE_IDENTITY())
					insert into fhir.LogTable(logitem, logdesc, logtime) values('New ID', @InternalStudyProcedureMapID , default);		
				END
			END
			insert into fhir.LogTable(logitem, logdesc, logtime) values('Add ProcedureMapID to temp table', @InternalStudyProcedureMapID , default);		
			--Store all the Update procedure into table. This will be use to compare and remove procedure that don't belong in here.
			Insert into @StudyProcedureMapTab Values(@InternalStudyProcedureMapID);

			--Set to null when completed update a Procedure code
			Set @InternalStudyProcedureMapID = NULL;
			Set @newInternalProcedureCodeID = NULL;
			Set @newModifier = NULL;
			Set @newQuantity = NULL;
			Set @newAuthorizationNumber = NULL;
			Set @newAuthorizationStartDate = NULL;
			Set @newAuthorizationEndDate = NULL;

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
			END
			CLOSE Json_Cursor;
			DEALLOCATE Json_Cursor; 	

			--Check and Delete if there are any result that dont exist in the new update model
			--Delete all the dont appear in the new Model
			Delete from dbo.StudyProcedureMap where InternalStudyProcedureMapID in 
				(select InternalStudyProcedureMapID from dbo.StudyProcedureMap where InternalStudyID = @internalStudyID     
				AND InternalStudyProcedureMapID NOT IN (Select InternalStudyProcedureMapID from @StudyProcedureMapTab))	
			COMMIT TRANSACTION
			insert into fhir.LogTable(logitem, logdesc, logtime) values('SUCCESS', @InternalStudyProcedureMapID , default);		
		END TRY
		BEGIN CATCH
			insert into fhir.LogTable(logitem, logdesc, logtime) values('ERROR OCCURED ROLLBACK', @InternalStudyProcedureMapID , default);		

			Declare @ErrorNumber   int;
			Declare @ErrorMessage  nvarchar(max);
			Declare @ErrorSeverity int;
			Declare @ErrorState    int;

			SELECT @ErrorNumber   = ERROR_NUMBER(),
				   @ErrorMessage  = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState    = ERROR_STATE();
			insert into fhir.LogTable(logitem, logdesc, logtime) values('ERROR MESSAGE ', @ErrorMessage , default);		
			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);			
			ROLLBACK TRANSACTION
		END CATCH
	END
END







