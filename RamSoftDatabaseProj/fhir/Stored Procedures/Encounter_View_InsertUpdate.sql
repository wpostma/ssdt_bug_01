﻿
CREATE PROCEDURE [fhir].[Encounter_View_InsertUpdate] 
(	
	@InternalEncounterID bigint,
	@AdmissionID nvarchar(64),
	@JsonText nvarchar(max),
	@Result bigint output
)
AS
BEGIN	
		-- SET NOCOUNT ON added to prevent extra result sets from		
		SET NOCOUNT ON;		
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DECLARE @CurKey nvarchar(512);
 		DECLARE	@CurValue nvarchar(max);
		DECLARE	@CurType int;
		
		DECLARE @InternalAssigningAuthorityID bigint;
		DECLARE @FacilityID bigint;
		DECLARE @ConfidentialityID int;
		DECLARE @PatientID bigint;				
		DECLARE @EncounterStateID int;	
		DECLARE @EncounterClassID int;
		DECLARE @InsuranceStatusID int;
		DECLARE @AmbulatoryStatusID int;		
		DECLARE @AddmissionDateTime datetime2;
		DECLARE @LastUpdateUserID bigint;
		DECLARE @ExtJson nvarchar(max);		

		IF (@InternalEncounterID > 0)  -- update
		BEGIN
			SELECT @ExtJson = ExtJson FROM PHI.Encounter WHERE InternalEncounterID = @InternalEncounterID; --Get the current one 
		END
		IF (@ExtJson IS NULL) OR (@ExtJson = '')
		BEGIN
			SET @ExtJson = '{ }'; -- Initialize as Json text
		END		

		DECLARE Json_Cursor CURSOR LOCAL FOR SELECT * FROM OPENJSON(@JsonText);
		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN				
			IF (@CurKey = 'status') SET @EncounterStateID = (SELECT EncounterStateID FROM code.EncounterState Where EncounterStateCode = @CurValue);
			IF (@CurKey = 'patient') SET @PatientID  = JSON_VALUE(@CurValue,'$.id');			
			IF (@CurKey = 'facilityID') SET @FacilityID = @CurValue;
			IF (@CurKey = 'admissionDate') SET @AddmissionDateTime = CAST(@CurValue AS datetime2);
			IF (@CurKey = 'class') SET @EncounterClassID = (SELECT EncounterClassID FROM code.EncounterClass Where EncounterClassCode = @CurValue);
			IF (@CurKey = 'insuranceStatusCode') SET @InsuranceStatusID = (SELECT InsuranceStatusID FROM code.InsuranceStatus Where InsuranceStatusCode = @CurValue);
			IF (@CurKey = 'ambulatoryStatusCode') SET @AmbulatoryStatusID = (SELECT AmbulatoryStatusID FROM code.AmbulatoryStatus Where AmbulatoryStatusCode = @CurValue);
			IF (@CurKey = 'patientLocation') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.patientLocation', @CurValue);
			IF (@CurKey = 'description') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.description', @CurValue);
			IF (@CurKey = 'identifier') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.identifier', @CurValue);						
			IF (@CurKey = 'meta')
			BEGIN							
				SET @LastUpdateUserID = JSON_VALUE(@CurValue, '$.lastUpdatedUser');
			END	
												
			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor;
		DEALLOCATE Json_Cursor;
		
		--IF AdmissionID Null then set to -1. It will get the default value in trigger after inserted
		--AdmissionID = Prefix + InternalEncounterID
		IF (@AdmissionID IS NULL OR @AdmissionID = '')
		BEGIN			
			SET @AdmissionID = '-1';
		END

		IF(@InternalAssigningAuthorityID IS NULL OR @InternalAssigningAuthorityID = 0)
		BEGIN
		    SET @InternalAssigningAuthorityID = (SELECT InternalAssigningAuthorityID FROM PHI.Patient WHERE InternalPatientID = @PatientID );
		END

		IF (@InternalEncounterID > 0)
		BEGIN
			UPDATE PHI.Encounter SET					
					InternalPatientID = @PatientID,
					AdmissionID = @AdmissionID,					
					AdmissionAssigningAuthorityID = @InternalAssigningAuthorityID,
					InternalFacilityID = @FacilityID,
					ConfidentialityID = @ConfidentialityID,
					EncounterClassID = @EncounterClassID,
					InsuranceStatusID = @InsuranceStatusID,
					AmbulatoryStatusID = @AmbulatoryStatusID,
					EncounterStateID = @EncounterStateID,
					ExtJson = @ExtJson
			OUTPUT INSERTED.InternalEncounterID INTO @ProcessedIDTab
			WHERE InternalEncounterID = @InternalEncounterID
		END
		ELSE
		BEGIN						
			INSERT INTO PHI.Encounter(																		
						[InternalPatientID],
						[AdmissionID],
						[AdmissionDateTime],
						[ExtJson],
						[AdmissionAssigningAuthorityID],						
						[InternalFacilityID],
						[ConfidentialityID],
						[EncounterClassID],
						[InsuranceStatusID],			
						[AmbulatoryStatusID],						
						[EncounterStateID]						
						)
				OUTPUT INSERTED.InternalEncounterID INTO @ProcessedIDTab
				VALUES (
						@PatientID,
						@AdmissionID,
						@AddmissionDateTime, 
						@ExtJson,
						@InternalAssigningAuthorityID,
						@FacilityID,
						@ConfidentialityID,
						@EncounterClassID,
						@InsuranceStatusID,
						@AmbulatoryStatusID,						
						@EncounterStateID
						);
		END
		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);	
END
