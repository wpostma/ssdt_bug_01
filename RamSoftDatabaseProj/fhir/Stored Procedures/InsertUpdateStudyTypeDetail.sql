﻿CREATE procedure [fhir].[InsertUpdateStudyTypeDetail]
(
	@InternalStudyTypeID bigint,
	@JsonInput nvarchar(max)
)
AS 
BEGIN
	SET NOCOUNT ON;	    
	    
	BEGIN				
		IF (@JsonInput IS NULL) OR (@JsonInput = '')
		BEGIN
			DELETE from dbo.StudyTypeDetail Where InternalStudyTypeID = @InternalStudyTypeID
			Return
		END

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @InternalProcedureCodeID int;
		DECLARE @ProcedureCode nvarchar(16);
		DECLARE @Modifier nvarchar(8);
		DECLARE @Quantity int;
		DECLARE @ExtJson nvarchar (max) = NULL;

		--Declare a temp table it will be use to store all the internalProcedureCodeIDs in the new update model from client
		DECLARE @StudyTypeDetailMapTemp TABLE (
			internalProcedureCodeID int
		);

		--Declare Json Cursor for loop through each row in the model Json
		DECLARE Json_Cursor CURSOR LOCAL FOR SELECT * FROM OPENJSON(@JsonInput, '$');

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
			IF (@CurValue <> '')
			BEGIN
				DECLARE @ProKey NVARCHAR(512);
 				DECLARE	@ProValue NVARCHAR(MAX);
				DECLARE	@ProType int;

				SET @InternalProcedureCodeID = NULL;
				SET @ProcedureCode = NULL;
				SET @Modifier = NULL;
				SET @Quantity = NULL;
					
				--Declare Property_Cursor for looping throught each property and get value
				DECLARE Property_Cursor CURSOR LOCAL FOR SELECT * FROM OPENJSON(@CurValue)	

				OPEN Property_Cursor;
				FETCH NEXT FROM Property_Cursor INTO @ProKey, @ProValue, @ProType;
				WHILE (@@FETCH_STATUS = 0) 
				BEGIN
					IF (@ProKey = 'procedureCode') SET @ProcedureCode = @ProValue;
					IF (@ProKey = 'modifiers') SET @Modifier = @ProValue;
					IF (@ProKey = 'quantity') SET @Quantity = @ProValue;

					FETCH NEXT FROM Property_Cursor INTO @ProKey, @ProValue, @ProType;
				END	
				CLOSE Property_Cursor;
				DEALLOCATE Property_Cursor; 
			END	
				
			IF @ProcedureCode IS NOT NULL 
			BEGIN
				SET @InternalProcedureCodeID = (SELECT InternalProcedureCodeID FROM code.ProcedureCode WHERE ProcedureCode = @ProcedureCode);
			END		

			--Check if the the current StudyTypeDetail is exist in database 
			IF EXISTS (SELECT InternalStudyTypeDetailID FROM dbo.StudyTypeDetail WHERE InternalStudyTypeID = @InternalStudyTypeID AND InternalProcedureCodeID = @InternalProcedureCodeID)					
			BEGIN
				--IF exist then update the current
				UPDATE dbo.StudyTypeDetail SET
						CombinedModifier = @Modifier,
						Quantity = @Quantity
				WHERE InternalStudyTypeID = @InternalStudyTypeID AND InternalProcedureCodeID = @InternalProcedureCodeID;
			END
			ELSE
			BEGIN
				--IF not exists then insert the new one
				INSERT INTO dbo.StudyTypeDetail(
					InternalStudyTypeID, 
					InternalProcedureCodeID, 
					CombinedModifier, 
					Quantity)
				VALUES(
					@InternalStudyTypeID,
					@InternalProcedureCodeID,
					@Modifier,
					@Quantity
					);
			END
	
			--Store all the Update UserFacility into table. This will be use to compare and remove Userfacility that don't belong in here.
			INSERT INTO @StudyTypeDetailMapTemp Values(@InternalProcedureCodeID);

		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor;
		DEALLOCATE Json_Cursor;

		--Check and Delete if there are any result that dont exist in the new update model
		--Delete all that dont appear in the new Model
		DELETE FROM dbo.StudyTypeDetail WHERE InternalStudyTypeID = @InternalStudyTypeID AND InternalProcedureCodeID NOT IN (select InternalProcedureCodeID from @StudyTypeDetailMapTemp)			
	END
END
