﻿CREATE PROCEDURE [fhir].[ProblemCode_View_InsertUpdate]
		@InternalProblemCodeID bigint,
		@JsonText nvarchar(max),
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @IsActive bit = 1;  --default: true
		DECLARE @ProblemCode nvarchar(16);
		DECLARE @Description nvarchar(64);
		DECLARE @IsCritical bit = 0;
		DECLARE @ExtJson nvarchar (max) = NULL;

		DECLARE Json_Cursor CURSOR LOCAL FOR
			  SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
    		
			IF (@CurKey = 'isActive') SET @IsActive = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'isCritical') SET @IsCritical = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor; 
		DEALLOCATE Json_Cursor;

		SET @ProblemCode = JSON_VALUE(@JsonText, '$.code.coding[0].code');
		SET @Description = JSON_VALUE(@JsonText, '$.code.coding[0].display');
		IF (@Description IS NULL)
		BEGIN
		   SET @Description = JSON_VALUE(@JsonText, '$.code.text');
		END

		IF (@InternalProblemCodeID > 0)
		BEGIN
           SET @ExtJson = (SELECT ExtJson FROM code.Problem WHERE InternalProblemCodeID = @InternalProblemCodeID);
		END

		IF @ExtJson IS NULL
		BEGIN
		   SET @ExtJson = '{ }';
		END

		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.alertMessage', JSON_VALUE(@JsonText, '$.alertMessage'));

		IF (@InternalProblemCodeID > 0)
		BEGIN
			UPDATE code.Problem SET
					  ProblemCode = @ProblemCode,
					  [Description] = @Description,					  
					  IsActive = @IsActive,
					  IsCritical = @IsCritical,					  
					  ExtJson = @ExtJson					  
					OUTPUT inserted.InternalProblemCodeID INTO @ProcessedIDTab
				WHERE InternalProblemCodeID = @InternalProblemCodeID;		
		END
		ELSE
		BEGIN
			insert into code.Problem(
					  ProblemCode,
					  [Description],
					  IsActive,
					  IsCritical,
					  ExtJson)
					OUTPUT inserted.InternalProblemCodeID INTO @ProcessedIDTab
			   values (
					  @ProblemCode,
					  @Description,
					  @IsActive,
					  @IsCritical,
					  @ExtJson);			
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
	END

