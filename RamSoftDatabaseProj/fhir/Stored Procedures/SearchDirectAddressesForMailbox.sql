﻿CREATE PROCEDURE [fhir].[SearchDirectAddressesForMailbox]
		@DirectAddress nvarchar(128),
		@MailBoxJSON nvarchar(max)
	AS
	BEGIN
	    -- Meta-Search direct address for mailbox, which should belong to the facilities of the users which already in the same mailbox
		-- Direct address only belong to one mailbox

		SET NOCOUNT ON;

		DECLARE @DirectAddressArrayJSON nvarchar(max);
		DECLARE @UserArrayJSON nvarchar(max);
		DECLARE @SQL nvarchar(max);
		DECLARE @ArrayNum int = 0;
		DECLARE @FacilityIDs nvarchar(max) = null;
		DECLARE @FacilityID bigint;
		DECLARE @DirectAddresses nvarchar(max) = null;
		DECLARE @DirectAddressID bigint;
		DECLARE @MAIBOXID bigint = -1;

		DECLARE @ReturnCount int = 100;

		--Declare a temp table it will be used to store all the InternalUserID which already binding with mailbox
		DECLARE @UserMapTemp Table (
		    internalUserID bigint
		);
		
		IF (ISJSON(@MailBoxJSON) > 0)
		BEGIN
		    SET @MAIBOXID = CAST(JSON_VALUE(@MailBoxJSON, '$.internalMailboxID') as bigint);

			SET @UserArrayJSON = JSON_QUERY(@MailBoxJSON, '$.users');

			IF (ISJSON(@UserArrayJSON) > 0)
			BEGIN
			    INSERT INTO @UserMapTemp
					SELECT CAST(JSON_VALUE([value], '$.id') as bigint) from openjson(@UserArrayJSON);

				IF (EXISTS (SELECT 1 FROM @UserMapTemp))
				BEGIN
					DECLARE Facility_Cursor CURSOR LOCAL FOR
						SELECT DISTINCT InternalFacilityID
						FROM dbo.UserFacilityMap m
						JOIN @UserMapTemp t ON t.internalUserID = m.InternalUserID;

                    OPEN Facility_Cursor;
					FETCH NEXT FROM Facility_Cursor INTO @FacilityID;
					WHILE (@@FETCH_STATUS = 0) 
					BEGIN
					   IF (@FacilityID > 0)
					   BEGIN
					       IF (@FacilityIDs IS NOT NULL)
						   BEGIN
						      SET @FacilityIDs = @FacilityIDs + ', ' + CAST(@FacilityID as nvarchar);
						   END
						   ELSE
						   BEGIN
						      SET @FacilityIDs = CAST(@FacilityID as nvarchar);
						   END;
					   END

					   FETCH NEXT FROM Facility_Cursor INTO @FacilityID;
					END
					CLOSE Facility_Cursor;
					DEALLOCATE Facility_Cursor; 
				END				 
			END

			SET @DirectAddressArrayJSON = JSON_QUERY(@MailBoxJSON, '$.directAddresses');
            IF (ISJSON(@DirectAddressArrayJSON) > 0)
			BEGIN
			    DECLARE DirectAddress_Cursor CURSOR LOCAL FOR
					SELECT DISTINCT CAST(JSON_VALUE([value], '$.id') as bigint) from openjson(@DirectAddressArrayJSON); 

				OPEN DirectAddress_Cursor;
				FETCH NEXT FROM DirectAddress_Cursor INTO @DirectAddressID;
				WHILE (@@FETCH_STATUS = 0) 
				BEGIN
					IF (@DirectAddressID > 0)
					BEGIN
					    IF (@DirectAddresses IS NOT NULL)
						BEGIN
						    SET @DirectAddresses = @DirectAddresses + ', ' + CAST(@DirectAddressID as nvarchar);
						END
						ELSE
						BEGIN
						    SET @DirectAddresses = CAST(@DirectAddressID as nvarchar);
						END;
					END

					FETCH NEXT FROM DirectAddress_Cursor INTO @DirectAddressID;
				END
				CLOSE DirectAddress_Cursor;
				DEALLOCATE DirectAddress_Cursor; 
			END
		END

		SET @SQL = 'SELECT TOP ' + CAST(@ReturnCount AS nvarchar) + ' d.* FROM dbo.DirectRegistered_View d WHERE (d.IsActive = 1)  ';

		IF (@MAIBOXID > 0)
		BEGIN
		   SET @SQL = @SQL + ' AND ((d.ExtJson IS NULL) OR (JSON_VALUE(d.ExtJson, ''$.internalMailboxID'') IS NULL) OR (JSON_VALUE(d.ExtJson, ''$.internalMailboxID'') = ' + @MAIBOXID + ')) '; 
		END
		ELSE
		BEGIN
		   SET @SQL = @SQL + ' AND ((d.ExtJson IS NULL) OR (JSON_VALUE(d.ExtJson, ''$.internalMailboxID'') IS NULL )) ';
		END

		IF (@DirectAddress > '')
		BEGIN
		   SET @SQL = @SQL + ' AND (d.DirectAddress like ''' + @DirectAddress + '%'') ';
		END

		IF (@FacilityIDs IS NOT NULL)
		BEGIN
		   SET @SQL = @SQL + ' AND (d.InternalFacilityID in (' + @FacilityIDs + ')) ';
		END

		IF (@DirectAddresses IS NOT NULL)
		BEGIN
		   SET @SQL = @SQL + ' AND (d.InternalDirectAddressID not in (' + @DirectAddresses + ')) ';
		END

		EXEC (@SQL);
		
	END

