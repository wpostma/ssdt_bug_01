﻿
CREATE procedure [fhir].[sp_InsertUpdateDiagnosisByStudy]
(
	@internalStudyID bigint,
	@JsonInput nvarchar(max)
)
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	   
	    
	BEGIN		
		BEGIN TRY
		BEGIN TRANSACTION

			IF @JsonInput = ''
			BEGIN				
				DELETE FROM StudyDiagnosisMap WHERE InternalStudyID = @internalStudyID				
				RETURN
			END

			DECLARE @CurKey NVARCHAR(512);
 			DECLARE	@CurValue NVARCHAR(MAX);
			DECLARE	@CurType INT;

			--Declare a temp table it will be use to store all the procedureIDs in the new update model from client
			DECLARE @StudyDiagnosisMapTab TABLE (
			   ID BIGINT
			   );			   									

			--Declare Json Cursor for loop through each row in the model Json
			DECLARE Json_Cursor CURSOR LOCAL FOR
				  SELECT * FROM OPENJSON(@JsonInput);

			OPEN Json_Cursor;
			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

			WHILE (@@FETCH_STATUS = 0) 
			BEGIN				

				IF (@CurValue <> '')
				BEGIN
					DECLARE @ProKey NVARCHAR(512);
 					DECLARE	@ProValue NVARCHAR(MAX);
					DECLARE	@ProType INT;

					DECLARE @newDiagnosisCodeID BIGINT = NULL;
					DECLARE @newDiagnosisCode NVARCHAR(20) = NULL;
					DECLARE @ID BIGINT = NULL;

					--Declare Property_Cursor for looping throught each property and get value
					DECLARE Property_Cursor CURSOR LOCAL FOR
						SELECT * FROM OPENJSON(@CurValue)	

					OPEN Property_Cursor;
					FETCH NEXT FROM Property_Cursor INTO @ProKey, @ProValue, @ProType;
					WHILE (@@FETCH_STATUS = 0) 
					BEGIN					
						IF (@ProKey = 'internalDiagnosisCodeID') SET @newDiagnosisCodeID = @ProValue;									
						IF (@ProKey = 'id') SET @ID = @ProValue;									
						IF (@ProKey = 'diagnosisCode') SET @newDiagnosisCode = @ProValue;	
						FETCH NEXT FROM Property_Cursor INTO @ProKey, @ProValue, @ProType;
					END	
					CLOSE Property_Cursor;
					DEALLOCATE Property_Cursor; 			
				END
						
			--Check if the the current DiagnosisCodeID is exist in database 
			SELECT @newDiagnosisCodeID = InternalDiagnosisCodeID FROM code.Diagnosis WHERE DiagnosisCode = @newDiagnosisCode

			IF @newDiagnosisCodeID is Null Or @newDiagnosisCodeID = ''
			BEGIN				
					DECLARE @ErrorMsg NVARCHAR(2048) = 
					   FORMATMESSAGE(N'This DiagnosisCode (%s) is invalid . Please enter a valid DiagnosisCode.', 
					   convert(NVARCHAR, @newDiagnosisCode));   
					THROW 51000, @ErrorMsg, 1;
			END
			
			IF EXISTS (SELECT EntryID FROM dbo.StudyDiagnosisMap WHERE InternalStudyID = @internalStudyID AND EntryID = @ID)					
			BEGIN
			--IF exist then Update the current			
				UPDATE dbo.StudyDiagnosisMap SET
						InternalDiagnosisCodeID = @newDiagnosisCodeID						
				WHERE EntryID = @ID
										
			END
			ELSE
			BEGIN
				--IF not exists then insert the new one
				IF NOT EXISTS (SELECT EntryID FROM dbo.StudyDiagnosisMap WHERE InternalStudyID = @internalStudyID AND EntryID = @ID)					
				BEGIN
					INSERT INTO dbo.StudyDiagnosisMap(InternalStudyID, InternalDiagnosisCodeID )				
					VALUES(@internalStudyID, @newDiagnosisCodeID);
										
					--Store the newly inserted StudyDiagnosisMap
					SET @ID = (SELECT SCOPE_IDENTITY());					
				END
			END
						
			--Store all the Update Diagnosis into table. This will be use to compare and remove Diagnosis that don't belong in here.
			INSERT INTO @StudyDiagnosisMapTab Values(@ID);
			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
			END
			CLOSE Json_Cursor;
			DEALLOCATE Json_Cursor; 	

			--Check and Delete if there are any result that dont exist in the new update model
			--Delete all the dont appear in the new Model
			DELETE FROM dbo.StudyDiagnosisMap WHERE EntryID in 
				(SELECT EntryID FROM dbo.StudyDiagnosisMap WHERE InternalStudyID = @internalStudyID     
				AND EntryID NOT IN (SELECT ID FROM @StudyDiagnosisMapTab))	
			COMMIT TRANSACTION			
		END TRY
		BEGIN CATCH			
			DECLARE @ErrorNumber   INT;
			DECLARE @ErrorMessage  NVARCHAR(max);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState    INT;

			SELECT @ErrorNumber   = ERROR_NUMBER(),
				   @ErrorMessage  = ERROR_MESSAGE(),
				   @ErrorSeverity = ERROR_SEVERITY(),
				   @ErrorState    = ERROR_STATE();
			insert into fhir.LogTable(logitem, logdesc, logtime) values('ERROR MESSAGE ', @ErrorMessage , DEFAULT);		
			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);			
			ROLLBACK TRANSACTION
		END CATCH
	END
END
