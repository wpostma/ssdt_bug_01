﻿CREATE PROCEDURE [fhir].[Employer_View_InsertUpdate]
		@InternalEmployerID bigint,
		@JsonText nvarchar(max),
		@Result bigint output
	AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		DECLARE @CurKey NVARCHAR(512);
 		DECLARE	@CurValue NVARCHAR(MAX);
		DECLARE	@CurType int;

		DECLARE @IsActive bit = 1;  --default: true
		DECLARE @EmployerName nvarchar(64) = NULL;
		DECLARE @ExtJson nvarchar (max) = NULL;
		DECLARE @Address nvarchar(64) = NULL;
					
		DECLARE Json_Cursor CURSOR LOCAL FOR
			  SELECT * FROM OPENJSON(@JsonText);

		OPEN Json_Cursor;
		FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN    		
			IF (@CurKey = 'active') SET @IsActive = (CASE @CurValue WHEN 'false' THEN 0 ELSE 1 END);
			IF (@CurKey = 'name') SET @EmployerName = @CurValue;

			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
		END
		CLOSE Json_Cursor; 
		DEALLOCATE Json_Cursor;

		SET @Address = JSON_Value(@JsonText, '$.address[0].line');

		SET @ExtJson = '{ }';
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.contact', JSON_query(@JsonText, '$.contact'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.address', JSON_query(@JsonText, '$.address'));
		SET @ExtJson = JSON_MODIFY(@ExtJson, '$.telecom', JSON_query(@JsonText, '$.telecom'));
     	SET @ExtJson = JSON_MODIFY(@ExtJson, '$.notes', JSON_VALUE(@JsonText, '$.notes'));
		
		IF (@InternalEmployerID > 0)
		BEGIN
			UPDATE dbo.Employer SET
					  EmployerName = @EmployerName,
					  Address = @Address,
					  IsActive = @IsActive,
					  ExtJson = @ExtJson,
					  Timestamp = CURRENT_TIMESTAMP
					OUTPUT inserted.InternalEmployerID INTO @ProcessedIDTab
				WHERE InternalEmployerID = @InternalEmployerID;		
		END
		ELSE
		BEGIN
			insert into dbo.Employer (
					  EmployerName,
					  Address,
					  IsActive,
					  ExtJson,
					  Timestamp)
					OUTPUT inserted.InternalEmployerID INTO @ProcessedIDTab
			   values (
					  @EmployerName,
					  @Address,					 
					  @IsActive,
					  @ExtJson,
					  CURRENT_TIMESTAMP);			
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);
		insert into fhir.LogTable(logitem, logdesc, logint) values('Employer_View_InsertUpdate End', 'OK', @Result);
	END


