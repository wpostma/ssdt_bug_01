﻿CREATE PROCEDURE [fhir].[Practitioner_View_InsertUpdate] 
	(
		@InternalUserID BIGINT,
		@HashingPassword NVARCHAR(MAX),		
		@JsonText NVARCHAR(MAX),
		@Result BIGINT OUTPUT
	)
	AS
	BEGIN	
			-- SET NOCOUNT ON added to prevent extra result sets from		
			SET NOCOUNT ON;
			DECLARE @ProcessedIDTab TABLE (id BIGINT);

			DECLARE @CurKey NVARCHAR(512);
 			DECLARE	@CurValue NVARCHAR(MAX);
			DECLARE	@CurType INT;
			
			DECLARE @InternalGroupID BIGINT;
			DECLARE @InternalRoleID BIGINT;
			DECLARE @UserName NVARCHAR(64);
			DECLARE @FullName NVARCHAR(64);			
			DECLARE @SexID INT;
			DECLARE @BirthDate DATETIME;
			DECLARE @Specialty BIGINT;
			DECLARE @IsActive BIT = 1;
			DECLARE @UserID NVARCHAR(16);
			DECLARE @Identifier NVARCHAR(512);
			DECLARE @LastUpdatedUserID BIGINT;
			DECLARE @LastUpdatedUTC DATETIME2;
			DECLARE @ExtJson NVARCHAR(MAX);
			DECLARE @IsUseExternalAuthenticate BIT;
					
			IF (@InternalUserID > 0)  -- update
			BEGIN
				SELECT @ExtJson = ExtJson FROM dbo.[User] WHERE InternalUserID = @InternalUserID; --Get the current one 
			END
			IF (@ExtJson IS NULL) OR (@ExtJson = '')
			BEGIN
				SET @ExtJson = '{ }'; -- Initialize as Json text
			END

			DECLARE Json_Cursor CURSOR LOCAL FOR SELECT * FROM OPENJSON(@JsonText);
			OPEN Json_Cursor;
			FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;

			WHILE (@@FETCH_STATUS = 0) 
			BEGIN				
				IF (@CurKey = 'identifier') SET @Identifier = @CurValue;
				IF (@CurKey = 'active') SET @IsActive = CASE @CurValue WHEN 'true' THEN 1 ELSE 0 END;
				IF (@CurKey = 'isUseExternalAuthenticate') SET @IsUseExternalAuthenticate = CASE @CurValue WHEN 'true' THEN 1 ELSE 0 END;				
				IF (@CurKey = 'group') SET @InternalGroupID = JSON_VALUE(@CurValue, '$.id');
				IF (@CurKey = 'role') SET @InternalRoleID = JSON_VALUE(@CurValue, '$.id');						
				IF (@CurKey = 'userName') SET @UserName = @CurValue;
				IF (@CurKey = 'fullName') SET @FullName = @CurValue;
				IF (@CurKey = 'userID') SET @UserID = @CurValue;				
				IF (@CurKey = 'sexCode') SET @SexID = (SELECT SexID FROM code.Sex WHERE SexCode = @CurValue)
				IF (@CurKey = 'birthDate') SET @BirthDate = @CurValue;
				IF (@CurKey = 'specialty') SET @Specialty = JSON_VALUE(@CurValue, '$.id');
				IF (@CurKey = 'meta')
				BEGIN							
					SET @LastUpdatedUserID = JSON_VALUE(@CurValue, '$.internalUserID');
				END
				IF (@CurKey = 'telecom') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.contactPoint', JSON_QUERY(@CurValue));
				IF (@CurKey = 'address') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.address', JSON_QUERY(@CurValue));
				IF (@CurKey = 'directAddress') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.directAddress', @CurValue);
				IF (@CurKey = 'notes') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.notes', @CurValue);
				IF (@CurKey = 'reportingDetails') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.reportingDetails', JSON_QUERY(@CurValue));
				IF (@CurKey = 'integrations') SET @ExtJson = JSON_MODIFY(@ExtJson, '$.integrations', JSON_QUERY(@CurValue));
				IF (@CurKey = 'license')
				BEGIN
					IF (JSON_QUERY(@ExtJson, '$.license') IS NULL)
					BEGIN						
						SET @ExtJson = (SELECT JSON_MODIFY(@ExtJson, '$.license', JSON_QUERY('{}')));
					END									
					SET @ExtJson = JSON_MODIFY(@ExtJson, '$.license.dea', JSON_VALUE(@CurValue, '$.dea'));
					SET @ExtJson = JSON_MODIFY(@ExtJson, '$.license.licenseNumber', JSON_VALUE(@CurValue, '$.licenseNumber'));
					SET @ExtJson = JSON_MODIFY(@ExtJson, '$.license.countryCode', JSON_VALUE(@CurValue, '$.countryCode'));
					SET @ExtJson = JSON_MODIFY(@ExtJson, '$.license.stateCode', JSON_VALUE(@CurValue, '$.stateCode'));
				END
				 

				FETCH NEXT FROM Json_Cursor INTO @CurKey, @CurValue, @CurType;
			END
			CLOSE Json_Cursor; 
			DEALLOCATE Json_Cursor;					

			SET @LastUpdatedUTC = SYSUTCDATETIME();

			IF (ISJSON(@Identifier) > 0)
 				SET @ExtJson = JSON_MODIFY(@ExtJson, '$.identifier', JSON_QUERY(@Identifier));
			ELSE
 				SET @ExtJson = JSON_MODIFY(@ExtJson, '$.identifier', @Identifier);							
															
			IF (@InternalUserID > 0)
			BEGIN				
				UPDATE dbo.[User] SET
						UserName = @UserName,
						Name = @FullName,
						InternalGroupID = @InternalGroupID,
						InternalRoleID = @InternalRoleID,
						ID = @UserID,
						IsActive = @IsActive,
						Password = @HashingPassword,
						SexID = @SexID,
						BirthDate = @BirthDate,
						PhysicianSpecialtyID = @Specialty,
						LastupdateUTC = @LastUpdatedUTC,
						LastUpdateUserID = @LastUpdatedUserID,
						ExtJson = @ExtJson
				OUTPUT INSERTED.InternalUserID INTO @ProcessedIDTab
				WHERE InternalUserID = @InternalUserID;
			END
			ELSE
			BEGIN					
				INSERT INTO dbo.[User](
							InternalGroupID,
							InternalRoleID,
							UserName,
							Name,
							ID,
							IsActive,
							ExtJson,
							Password,
							SexID,
							BirthDate,
							PhysicianSpecialtyID,
							LastUpdateUTC,
							LastupdateUserID
							)
					OUTPUT INSERTED.InternalUserID INTO @ProcessedIDTab
					VALUES (
							@InternalGroupID,
							@InternalRoleID,
							@UserName,
							@FullName,
							@UserID,
							@IsActive,
							@ExtJson,
							@HashingPassword,
							@SexID,
							@BirthDate,
							@Specialty,
							@LastUpdatedUTC,
							@LastUpdatedUserID
							);
			END
			SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);			
	END	
	


