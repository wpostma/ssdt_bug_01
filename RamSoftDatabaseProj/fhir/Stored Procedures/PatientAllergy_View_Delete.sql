﻿CREATE PROCEDURE [fhir].[PatientAllergy_View_Delete]
		@InternalPatientID bigint,
		@AllergyName nvarchar(64),
		@Result bigint output
	AS 
	BEGIN
		SET NOCOUNT ON;
		DECLARE @ProcessedIDTab TABLE (id bigint);

		insert into fhir.LogTable(logitem, logdesc, logint) values('PatientAllergy_View_Delete - Start - @AllergyName=', @AllergyName, @InternalPatientID);

		IF (@AllergyName IS NULL) OR (@AllergyName = '')
		BEGIN
				UPDATE PHI.Patient SET ExtJson = JSON_MODIFY(ExtJson, '$.allergies', NULL) 
					OUTPUT inserted.InternalPatientID INTO @ProcessedIDTab
					WHERE InternalPatientID = @InternalPatientID;
		END
		ELSE
		BEGIN
			DECLARE @AllergiesJson nvarchar(max) = (Select JSON_VALUE(ExtJson, '$.allergies') from PHI.Patient where InternalPatientID = @InternalPatientID);
			insert into fhir.LogTable(logitem, logdesc, logtime) values('PatientAllergy_View_Delete - OLD @AllergiesJson', @AllergiesJson, default);

			DECLARE @AllergiesTab TABLE (
			   onset NVARCHAR(64),
			   substance NVARCHAR(64), 
			   [status] NVARCHAR(64),
			   criticality NVARCHAR(64), 
			   [type] NVARCHAR(64), 
			   category NVARCHAR(64), 
			   note NVARCHAR(1000));
	
			INSERT INTO @AllergiesTab 
			   SELECT JSON_VALUE([VALUE], '$.onset'), JSON_VALUE([VALUE], '$.substance'), JSON_VALUE([VALUE], '$.status'), JSON_VALUE([VALUE], '$.criticality'), 
					  JSON_VALUE([VALUE], '$.type'), JSON_VALUE([VALUE], '$.category'), JSON_VALUE([VALUE], '$.note') 
			   FROM OPENJSON(@AllergiesJson);
   
			IF (EXISTS (SELECT 1 FROM @AllergiesTab WHERE substance = @AllergyName))
			BEGIN
				DELETE FROM @AllergiesTab WHERE substance = @AllergyName;   

	  			SET @AllergiesJson =(SELECT * FROM @AllergiesTab FOR JSON PATH);
				insert into fhir.LogTable(logitem, logdesc, logtime) values('PatientAllergy_View_Delete - NEW @AllergiesJson', @AllergiesJson, default);

				UPDATE PHI.Patient SET ExtJson = JSON_MODIFY(ExtJson, '$.allergies', @AllergiesJson) 
					OUTPUT inserted.InternalPatientID INTO @ProcessedIDTab
					WHERE InternalPatientID = @InternalPatientID;
			END
		END

		SET @Result = (SELECT TOP 1 id FROM @ProcessedIDTab);

		insert into fhir.LogTable(logitem, logdesc, logint) values('trPatientView_Delete - End', 'OK', @Result);
	END
