﻿CREATE TYPE [fhir].[SearchCriteriaType] AS TABLE (
    [Key]   NVARCHAR (512) NULL,
    [Value] NVARCHAR (512) NULL);

