﻿
CREATE FUNCTION [fhir].[GetDiagnosisJson] 
(
   @internalStudyID int = 0
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @jsonstr nvarchar(max) = null;

	set @jsonstr = (select sm.EntryID as id, d.CodingScheme as codingScheme, d.DiagnosisCode as diagnosisCode, d.InternalDiagnosisCodeID as internalDiagnosisCodeID, d.Description as description, InternalStudyID  as internalStudyID
							from dbo.StudyDiagnosisMap sm join  
								 code.Diagnosis d on d.InternalDiagnosisCodeID = sm.InternalDiagnosisCodeID
						    Where InternalStudyID = @internalStudyID FOR JSON PATH)   

	RETURN @jsonstr
END

