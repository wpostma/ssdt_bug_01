﻿CREATE Function [fhir].[GetPrimaryEncounter]
(
	@InternalStudyID BIGINT
)
RETURNS BIGINT
WITH SCHEMABINDING
As
BEGIN

DECLARE @primaryEncounterID BIGINT;
SELECT @PrimaryEncounterID = PrimaryEncounterID FROM phi.Study WHERE InternalStudyID = @internalStudyID;

IF (@PrimaryEncounterID IS NULL)
BEGIN

SET @PrimaryEncounterID  = (SELECT TOP 1 InternalEncounterID FROM PHI.Encounter e WHERE InternalEncounterID in
	(SELECT Do.InternalEncounterID FROM phi.Study s Cross apply OPENJSON(JSON_QUERY(s.ExtJson,'$.internalDiagnosticOrderID')) Dids
	JOIN PHI.DiagnosticOrder Do ON Do.InternalDiagnosticOrderID = Dids.Value WHERE s.InternalStudyID = @internalStudyID))

End
RETURN @primaryEncounterID
End

