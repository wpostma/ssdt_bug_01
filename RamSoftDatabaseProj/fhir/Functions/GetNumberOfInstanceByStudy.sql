﻿CREATE FUNCTION [fhir].[GetNumberOfInstanceByStudy](@InternalStudyID BIGINT)
RETURNS BIGINT
BEGIN	
	RETURN (SELECT COUNT(*) FROM phi.Instance WHERE InternalSeriesID in (SELECT InternalSeriesID FROM phi.Series WHERE InternalStudyID = @InternalStudyID))
END

