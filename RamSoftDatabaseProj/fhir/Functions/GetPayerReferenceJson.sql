﻿CREATE FUNCTION [fhir].[GetPayerReferenceJson] 
	(@InternalPayerID BIGINT = 0)
	RETURNS nvarchar(max)
	AS
	BEGIN
		-- Declare the return variable here
		DECLARE @jsonstr nvarchar(max) = null;

		set @jsonstr = (SELECT  							
						 (SELECT 
							CONCAT('fhir/payer/', CAST(py.InternalPayerID AS NVARCHAR(20))) AS reference,
							py.InternalPayerID AS id,
							CONCAT(py.CarrierID, ' - ', py.CompanyName) AS value,
							py.CompanyName AS display,
							(SELECT JSON_VALUE(value,'$.value') FROM OPENJSON(py.ExtJson, '$.telecom') WHERE JSON_VALUE(value,'$.system') = 'other') AS website
							FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS payer		
						FROM dbo.Payer py
						WHERE py.InternalPayerID = @InternalPayerID)

		-- Return the result of the function
		RETURN @jsonstr
	END
