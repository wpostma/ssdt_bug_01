﻿Create Function [fhir].[GetNumberOfInstanceBySeries](@seriesID bigint)
returns bigint
Begin	
	return (select count(*) from phi.Instance where InternalSeriesID = @seriesID )
End