﻿
CREATE FUNCTION fhir.ParseHumanNameJson 
(
	@Input_Json nvarchar(max)
)
RETURNS nvarchar(64)
--WITH SCHEMABINDING
AS
BEGIN
    DECLARE @Family nvarchar(64) = '';
	DECLARE	@Given nvarchar(64) = '';
	DECLARE	@Middle nvarchar(64) = '';
	DECLARE	@Prefix nvarchar(64) = '';
	DECLARE	@Suffix nvarchar(64) = '';
	DECLARE	@DicomName nvarchar(64) = NULL;

    SET @Input_Json = dbo.TRIM(@Input_Json);
    IF (CHARINDEX('[', @Input_Json) = 1)
	BEGIN
	   SET @Input_Json = (SELECT [value] from openjson(@Input_Json) where [key] = 0); --get the first array
	END

    SET @DicomName = UPPER(JSON_VALUE(@Input_Json, '$.text'));
	IF (@DicomName is null) OR (dbo.TRIM(@DicomName) = '')
	BEGIN
	    SET @DicomName = UPPER(JSON_VALUE(@Input_Json, '$.Text'));
	END

	IF (@DicomName is null) OR (dbo.TRIM(@DicomName) = '')
	BEGIN
		IF ((select count(*) from openjson(@Input_Json, '$.family')) > 0) 
		BEGIN
		   SET @Family = (SELECT [value] from openjson(@Input_Json, '$.family') where [key] = 0);
		END
		ELSE
		BEGIN
		   SET @Family = JSON_Value(@Input_Json, '$.family');
		END

		IF ((select count(*) from openjson(@Input_Json, '$.given')) > 0) 
		BEGIN
		   SELECT @Given = [value] from openjson(@Input_Json, '$.given') where [key] = 0;
		   IF ((select count(*) from openjson(@Input_Json, '$.given')) > 1)
		   BEGIN 
			   SET @Middle = (SELECT [value] from openjson(@Input_Json, '$.given') where [key] = 1);
		   END
		END
		ELSE
		BEGIN
		   SET @Given = JSON_Value(@Input_Json, '$.given');
		END

		IF ((select count(*) from openjson(@Input_Json, '$.prefix')) > 0) 
		BEGIN
		   SET @Prefix = (SELECT [value] from openjson(@Input_Json, '$.prefix') where [key] = 0);
		END
		ELSE
		BEGIN
		   SET @Prefix = JSON_Value(@Input_Json, '$.prefix');
		END

		IF ((select count(*) from openjson(@Input_Json, '$.suffix')) > 0) 
		BEGIN
		   SET @Suffix = (SELECT [value] from openjson(@Input_Json, '$.suffix') where [key] = 0);
		END
		ELSE
		BEGIN
		   SET @Suffix = JSON_Value(@Input_Json, '$.suffix');
		END

		--NEED TO REMOVE ARRAY WRAPPER
		IF (CHARINDEX('[', dbo.TRIM(@Family)) = 1)
		BEGIN
   			SELECT @Family = [value] from OPENJSON(@Family) where [key] = 0; 
		END
		IF (CHARINDEX('[', dbo.TRIM(@Given)) = 1)
		BEGIN
			DECLARE @GivenOrg nvarchar (1000) = @Given;
   			SELECT @Given = [value] from OPENJSON(@GivenOrg) where [key] = 0; 
   			SELECT @Middle = [value] from OPENJSON(@GivenOrg) where [key] = 1; 
		END
		IF (CHARINDEX('[', dbo.TRIM(@Middle)) = 1)
		BEGIN
   			SELECT @Middle = [value] from OPENJSON(@Middle) where [key] = 0; 
		END
		IF (CHARINDEX('[', dbo.TRIM(@Prefix)) = 1)
		BEGIN
   			SELECT @Prefix = [value] from OPENJSON(@Prefix) where [key] = 0; 
		END
		IF (CHARINDEX('[', dbo.TRIM(@Suffix)) = 1)
		BEGIN
   			SELECT @Suffix = [value] from OPENJSON(@Suffix) where [key] = 0; 
		END

		SET	@DicomName = @Family;
		if (@Given != '') and (@Given is not null) SET @DicomName = @DicomName + '^' + @Given;
		if (@Middle != '') and (@Middle is not null) SET @DicomName = @DicomName + '^' + @Middle;
		if (@Prefix != '') and (@Prefix is not null) SET @DicomName = @DicomName + '^' + @Prefix;
		if (@Suffix != '') and (@Suffix is not null) SET @DicomName = @DicomName + '^' + @Suffix;

	END

	RETURN  @DicomName;
END
