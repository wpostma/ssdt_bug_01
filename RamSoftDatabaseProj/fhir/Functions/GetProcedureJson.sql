﻿CREATE FUNCTION [fhir].[GetProcedureJson]
(
   @internalStudyID int = 0
)
RETURNS nvarchar(max)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @jsonstr nvarchar(max) = null;
	DECLARE @display nvarchar(max) = null;
	Set @display = 'ProcedureCode';

	   	set @jsonstr = (select 'fhir/procedure/'+ cast(p.InternalProcedureCodeID as nvarchar(20)) as reference, p.InternalProcedureCodeID as id, p.ProcedureCode as display
							   from StudyProcedureMap sp join code.ProcedureCode p on sp.InternalProcedureCodeID = p.InternalProcedureCodeID 
							   where InternalStudyID = @InternalStudyID
							   FOR JSON PATH) 						

	-- Return the result of the function
	RETURN @jsonstr
END




