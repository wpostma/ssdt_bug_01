﻿-- =============================================
-- Author:		Jasmine
-- Create date: 4/7/2016
-- Description:	Convert full name to FHIR HumanName Json
-- =============================================
--IF OBJECT_ID ('fhir.GetMaritalStatusIDByJson', 'FUNCTION') IS NOT NULL
--	DROP function fhir.GetMaritalStatusIDByJson ;
--GO

--CREATE FUNCTION fhir.GetMaritalStatusIDByJson 
CREATE FUNCTION fhir.GetMaritalStatusIDByJson 
(@Input_Json nvarchar(max))
RETURNS int
AS
BEGIN
    DECLARE @Code nvarchar(64) = null;
    DECLARE @Display nvarchar(64) = null;
    DECLARE @CodingJson nvarchar(1000) = null;
    DECLARE @MaritalStatusID int = null;

	SET @CodingJson = (select JSON_QUERY(@Input_Json,'$.coding[0]'));
	IF (@CodingJson IS NULL)
	BEGIN
    	SET @CodingJson = (select JSON_QUERY(@Input_Json,'$.coding'));
	END 

    IF (@CodingJson IS NOT NULL) AND (IsJson(@CodingJson) > 0) 
	BEGIN
	   SET @Code = (select Json_Value(@CodingJson, '$.code'));
	   SET @Display = (select Json_Value(@CodingJson, '$.display'));
	END

  	SELECT @MaritalStatusID = MaritalStatusID FROM code.MaritalStatus where MaritalStatus = @Display;
	if (@MaritalStatusID is null) 
	BEGIN
		SELECT @MaritalStatusID = MaritalStatusID FROM code.MaritalStatus where MaritalStatusCode = @Code;
	END

	RETURN @MaritalStatusID;
END
