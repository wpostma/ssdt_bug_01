﻿CREATE FUNCTION [fhir].[GetModalitiyListJson]
(
	@internalStudyID bigint
)
RETURNS nvarchar(max)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @jsonstr nvarchar(max) = null;
	

	   set @jsonstr = (select c.ModalityCode as code, c.[description] as display
							   from code.Modality c Join phi.Series s ON c.ModalityCode = s.ModalityCode
						Where InternalStudyID = @internalStudyID FOR JSON PATH)		
	-- Return the result of the function
	RETURN @jsonstr
END


