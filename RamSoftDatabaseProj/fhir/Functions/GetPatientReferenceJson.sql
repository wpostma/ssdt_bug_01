﻿
	CREATE FUNCTION [fhir].[GetPatientReferenceJson] 
	(@InternalPatientID BIGINT = 0)
	RETURNS nvarchar(max)
	AS
	BEGIN
		-- Declare the return variable here
		DECLARE @jsonstr nvarchar(max) = null;

		set @jsonstr = (SELECT 'fhir/Patient/' + cast(@InternalPatientID as nvarchar(20)) as reference,
						   cast(p.InternalPatientID as nvarchar(16)) AS id, 
						   CONCAT(p.PatientID, ' - ', p.PatientName) as [value],
						   fhir.GetPatientDisplayStr(p.InternalPatientID) as [display],
						   --JSON_QUERY(fhir.GetHumanNameJson(p.PatientName)) as name, 
						   CONCAT('assigningAuthority/', p.InternalAssigningAuthorityID) as 'managingOrganization.reference',
						   p.InternalAssigningAuthorityID as 'managingOrganization.id',
						   aa.Name as 'managingOrganization.display',						  
						   p.PatientID as patientID, 
						   sx.Sex as gender,
						   p.BirthDate as birthDate, 
						   p.SSN as ssn, 
						   ms.MaritalStatusCode as 'maritalStatus.coding.code', 
						   ms.MaritalStatus as 'maritalStatus.coding.display', 
						   JSON_QUERY(p.ExtJson, '$.address') as [address],
						   JSON_QUERY(p.ExtJson, '$.contactPoint') as telecom, 
						   JSON_VALUE(p.ExtJson, '$.employmentStatus') as employmentStatus,
						   e.InternalEmployerID as 'employer.id',
						   e.EmployerName as 'employer.name',
						  (e.EmployerName + ' - ' + e.Address) as 'employer.display',
						   0 as 'updatePatientInfo'
						FROM phi.Patient p
						   LEFT JOIN dbo.AssigningAuthority aa on aa.InternalAssigningAuthorityID = p.InternalAssigningAuthorityID
						   LEFT JOIN dbo.Employer e on e.InternalEmployerID = p.InternalEmployerID
						   LEFT JOIN code.Sex sx ON sx.SexID = p.SexID
						   LEFT JOIN code.MaritalStatus ms on ms.MaritalStatusID = p.MaritalStatusID
						WHERE P.InternalPatientID = @InternalPatientID
						FOR JSON PATH, WITHOUT_ARRAY_WRAPPER);

		-- Return the result of the function
		RETURN @jsonstr
	END
