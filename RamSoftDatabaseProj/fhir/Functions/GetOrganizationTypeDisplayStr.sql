﻿CREATE FUNCTION [fhir].[GetOrganizationTypeDisplayStr] 
		( @IsImaging bit = 0,
		  @IsReferring bit = 0,
		  @IsMammoTracking bit = 0 )
	RETURNS nvarchar(max)
	AS
	BEGIN
		DECLARE @Separator nvarchar(3) = ' | ';
		DECLARE @Result nvarchar(max);

		SET @Result = CONCAT((select case when @IsImaging = 1 then concat(@Separator, 'Imaging') else null end),
					(select case when @IsReferring = 1 then concat(@Separator, 'Referring') else null end),
					(select case when @IsMammoTracking = 1 then concat(@Separator, 'Stana') else null end));

		-- remove the first separator
		SET @Result = STUFF(@Result, CHARINDEX(@Separator, @Result), LEN(@Separator), '');

		RETURN @Result;
	END
