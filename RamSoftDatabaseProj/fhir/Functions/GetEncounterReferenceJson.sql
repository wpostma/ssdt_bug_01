﻿
	CREATE FUNCTION [fhir].[GetEncounterReferenceJson] 
	(@InternalEncounterID BIGINT = 0)
	RETURNS nvarchar(max)
	AS
	BEGIN
		-- Declare the return variable here
		DECLARE @jsonstr nvarchar(max) = null;

		set @jsonstr = (SELECT 'fhir/Encounter/' + cast(@InternalEncounterID as nvarchar(20)) as reference,
						   cast(e.InternalEncounterID as nvarchar(16)) AS id, 
	 					    e.AdmissionID AS [value],
						   CONCAT('VISITNUMBER: ', e.AdmissionID, ' - PATIENT: ', fhir.GetPatientDisplayStr(e.InternalPatientID)) AS display, 
						    est.EncounterStateName AS [status],
                            ec.EncounterClassName AS encounterClass, 
                            i.InsuranceStatusName AS insuranceStatus, 
							a.AmbulatoryStatusName AS ambulatoryStatus, 
							JSON_VALUE(e.ExtJson, '$.patientLocation') AS patientLocation,
							JSON_VALUE(e.ExtJson, '$.description') AS [description]
						FROM phi.Encounter e
						     LEFT JOIN code.EncounterClass ec on ec.EncounterClassID = e.EncounterClassID
	                         LEFT JOIN code.AmbulatoryStatus a ON a.AmbulatoryStatusID = e.AmbulatoryStatusID 
	                         LEFT JOIN code.InsuranceStatus i ON i.InsuranceStatusID = e.InsuranceStatusID 
	                         LEFT JOIN code.EncounterState est ON est.EncounterStateID = e.EncounterStateID
						WHERE e.InternalEncounterID = @InternalEncounterID
						FOR JSON PATH, WITHOUT_ARRAY_WRAPPER);
		-- Return the result of the function
		RETURN @jsonstr
	END
