﻿CREATE FUNCTION fhir.ParseContactPointJson 
(
  @Input_Json nvarchar(max)
)
RETURNS nvarchar(max)
AS 
BEGIN
   DECLARE @HomePhone nvarchar(64) = null;
   DECLARE @WorkPhone nvarchar(64) = null;
   DECLARE @MobilePhone nvarchar(64) = null;
   DECLARE @FaxNumber nvarchar(64) = null;
   DECLARE @Email nvarchar(64) = null;

   DECLARE @CurSystem nvarchar(64) = '';
   DECLARE @CurValue nvarchar(64) = '';
   DECLARE @CurUse nvarchar(64) = '';
	
   DECLARE @CurRowData nvarchar(512) = '';
   DECLARE @RowCount int = 0, @rownum int = 0;

   select @RowCount = count(*) from openjson(@Input_Json);

   DECLARE @contacts TABLE (
	   [system] nvarchar(64),
	   [value] nvarchar(64),
	   [use] nvarchar(64));

   DECLARE contact_cursor CURSOR LOCAL FOR 
     select [VALUE] from openjson(@Input_Json);

   OPEN contact_cursor;
   fetch next from contact_cursor into @CurRowData;

	while (@@FETCH_STATUS = 0) 
	begin
	   SET @CurSystem = JSON_VALUE(@CurRowData, '$.system');
	   SET @CurValue = JSON_VALUE(@CurRowData, '$.value');
	   SET @CurUse = JSON_VALUE(@CurRowData, '$.use');

	   if (@CurValue != '') and (@CurValue is not null)
	   begin
		   if (@CurSystem IS NULL)
		   begin
		      if (PATINDEX('%@%', @CurValue) = 0) set @CurSystem = 'phone';
			  else if(PATINDEX('%@%', @CurValue) > 0) set @CurSystem = 'email';
			  else SET @CurSystem = 'other';
		   end 

		   INSERT INTO @contacts VALUES (@CurSystem, @CurValue, @CurUse);  
	   end

	   set @rownum = @rownum + 1;
       fetch next from contact_cursor into @CurRowData;
	end

	CLOSE contact_cursor;

	DECLARE @JsonStr nvarchar(max) = (select *  from @contacts FOR JSON PATH);
	
	-- Return the result of the function
	RETURN  @JsonStr

END
