﻿CREATE FUNCTION [fhir].[GetSeriesJson] 
(
   @InternalStudyID int = 0
)
RETURNS nvarchar(max)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @jsonstr nvarchar(max) = null;	

	   set @jsonstr = (select (select s.InternalSeriesID as 'number', 
	                             (select c.ModalityCode as code, c.[description] as display from code.Modality c where c.ModalityCode = s.ModalityCode FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) as modality,
								 s.SeriesUID as [uid], fhir.GetNumberOfInstanceBySeries(s.InternalSeriesID) as numberOfInstances,
								 (select c.InternalBodyPartID as code, c.BodyPartExamined as display from code.BodyPart c where c.InternalBodyPartID = s.InternalBodyPartID FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) as bodySite,
								 (select i.InternalInstanceID as number, i.SOPInstanceUID as [uid] , i.InternalSOPClassID as sopClass from phi.Instance i where i.InternalSeriesID = s.InternalSeriesID FOR JSON PATH)  as 'instance'
							  From Phi.Series s Where s.InternalStudyID = @InternalStudyID
                       FOR JSON PATH));

	-- Return the result of the function
	RETURN @jsonstr
END
