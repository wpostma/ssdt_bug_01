﻿CREATE FUNCTION [fhir].[GetPatientJson] 
(
   @InternalPatientID int = 0
)
RETURNS nvarchar(max)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @jsonstr nvarchar(max) = null;
	DECLARE @Name nvarchar(64);
	DECLARE @value nvarchar(200);

	select @Name = p.PatientName, @value = CONCAT(p.PatientID,' - ',p.PatientName) from PHI.Patient p
        where InternalPatientID = @InternalPatientID;

    if (@Name is not null) and (@Name != '')
	begin
	   set @jsonstr = (select 'fhir/Patient/' + cast(@InternalPatientID as nvarchar(20)) as reference, 
						@InternalPatientID as id,
						@value as value,
						@Name as display
                       FOR JSON PATH, WITHOUT_ARRAY_WRAPPER);
	end

	-- Return the result of the function
	RETURN @jsonstr
END
