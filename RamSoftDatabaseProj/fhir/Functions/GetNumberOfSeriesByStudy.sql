﻿Create Function fhir.GetNumberOfSeriesByStudy(@studyID bigint)
returns bigint
Begin
	return (select count(*) from phi.Series s where s.InternalStudyID = @studyID)
End
