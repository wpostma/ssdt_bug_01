﻿CREATE FUNCTION [fhir].[GetPatientDisplayStr] 
	( @InternalPatientID int = 0 )
	RETURNS nvarchar(max)
	AS
	BEGIN
		DECLARE @Separator nvarchar(3) = ' | ';
		DECLARE @Result nvarchar(max) = null;
		DECLARE @Name nvarchar(64);
		DECLARE @PatientID nvarchar(200);
		DECLARE @Issuer nvarchar(200);
		DECLARE @BirthDate nvarchar(200);
		DECLARE @CellPhone nvarchar(200);
		DECLARE @Sex nvarchar(200);

		select @Name = PatientName, @PatientID = PatientID, @Issuer = AssigningAuthorityName,
		       @BirthDate = BirthDate, @CellPhone = CELLPHONE, @Sex = Sex
			from dbo.Patient_View
			where InternalPatientID = @InternalPatientID;


		set @Result = CONCAT(@Name, 
			(select case when @PatientID is not null then concat(@Separator, @PatientID) else null end),
			(select case when @Issuer is not null then concat(@Separator, @Issuer) else null end),
			(select case when @BirthDate is not null then concat(@Separator, @BirthDate) else null end),
			(select case when @CellPhone is not null then concat(@Separator, @CellPhone) else null end),
			(select case when @Sex is not null then concat(@Separator, @Sex) else null end));

		RETURN @Result
	END
