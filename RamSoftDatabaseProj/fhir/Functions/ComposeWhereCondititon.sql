﻿
CREATE FUNCTION [fhir].[ComposeWhereCondititon] 
(
   @SearchName nvarchar(512) = null,
   @SearchValueList nvarchar(512) = null,
   @FieldName nvarchar(64) = null,
   @ValueType nvarchar(64) = null 
)
RETURNS nvarchar(1024)
AS
BEGIN
    declare @Operator nvarchar(64) = ' = ';
    declare @LikeOperator nvarchar(64) = null;
	DECLARE @Result nvarchar(1024) = null;
    DECLARE @NumOP char(2);
    DECLARE @SearchValue nvarchar(512);
	DECLARE @CurResult nvarchar(1024) = null;

    DECLARE Value_cursor cursor LOCAL for
	   select value from string_split(@SearchValueList, ',');  --can be multiples for OR condition

	OPEN Value_cursor;
	FETCH NEXT from Value_cursor into @SearchValue;

	while (@@FETCH_STATUS = 0) 
	begin
	    SET @CurResult = NULL;

		IF (@ValueType = 'bool')
		BEGIN
		   IF (UPPER(@SearchValue) = 'TRUE') SET @CurResult = @FieldName + ' = 1' 
		   IF (UPPER(@SearchValue) = 'FALSE') SET @CurResult = @FieldName + ' = 0' 
		END
		ELSE IF (@ValueType = 'number')
		BEGIN
		   SET @NumOP = 
		   CASE UPPER(LEFT(@SearchValue, 2))
			  WHEN 'NE' THEN '!='
			  WHEN 'GE' THEN '>='
			  WHEN 'GT' THEN '>'
			  WHEN 'LE' THEN '<='
			  WHEN 'LT' THEN '<'
			  ELSE '='
		   END
	   
		   IF (@NumOP = '=')
		   BEGIN
			  SET @CurResult = @FieldName + '=' + @SearchValue;
		   END
		   ELSE
		   BEGIN
			  SET @CurResult = @FieldName + @NumOP + RIGHT(@SearchValue, LEN(@SearchValue)-2);  --EXCLUDE operand char
		   END
		END
		ELSE IF (@ValueType = 'date')
		BEGIN
		   SET @NumOP = 
		   CASE UPPER(LEFT(@SearchValue, 2))
			  WHEN 'NE' THEN '!='
			  WHEN 'GE' THEN '>='
			  WHEN 'GT' THEN '>'
			  WHEN 'LE' THEN '<='
			  WHEN 'LT' THEN '<'
			  ELSE '='
		   END
	   
		   IF (@NumOP = '=')
		   BEGIN
			  SET @CurResult = @FieldName + '=''' + @SearchValue + '''';
		   END
		   ELSE
		   BEGIN
			  SET @CurResult = @FieldName + @NumOP + '''' + RIGHT(@SearchValue, LEN(@SearchValue)-2) + '''';  --EXCLUDE operand char
		   END
		END
		ELSE
		BEGIN
			DECLARE @SepIndex int = CHARINDEX(':', @SearchName);
			IF (@SepIndex > 0)
			BEGIN
				DECLARE @TempName nvarchar(256) =  LEFT(@SearchName, @SepIndex - 1); 
				DECLARE @TempOper nvarchar(256) =  Right(@SearchName, LEN(@SearchName) - @SepIndex); 
				SET @SearchName = @TempName;
				SET @LikeOperator = @TempOper;
			END 

			IF (@FieldName IS NOT NULL) AND (@FieldName != '')
			BEGIN
			   SET @CurResult = 
	   			  CASE UPPER(@LikeOperator)
					WHEN 'CONTAINS' THEN @FieldName + ' LIKE ''%' + @SearchValue + '%''' -- like '%abc%'
					WHEN 'EXACT' THEN FORMATMESSAGE('%s = ''%s''', @FieldName, @SearchValue)  -- = 'abc'
					ELSE @FieldName + ' LIKE ''' + @SearchValue + '%'''  -- Starts With, liek 'abc%' 
				END
			END
		END

		IF (@CurResult IS NOT NULL) AND (@CurResult != '')
		BEGIN
		   IF (@Result IS NULL) SET @Result = @CurResult
		   ELSE SET @Result = @Result + ' OR ' + @CurResult;
		END

   	    FETCH NEXT from Value_cursor into @SearchValue;
	END
	
	-- Return the result of the function
	IF (@Result != '') SET @Result = '(' + @Result + ')';
	
	RETURN @Result;
END
