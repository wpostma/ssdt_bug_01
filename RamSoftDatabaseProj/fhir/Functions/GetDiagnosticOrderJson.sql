﻿CREATE FUNCTION [fhir].[GetDiagnosticOrderJson] 
(
   @internalStudyID int = 0
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @jsonstr NVARCHAR(MAX);

	SET @jsonstr = (SELECT 'fhir/DiagnosticOrder/' + CAST(do.InternalDiagnosticOrderID AS NVARCHAR(20)) AS reference, s.AccessionNumber AS display 
					FROM phi.study s CROSS APPLY OPENJSON(JSON_QUERY(s.ExtJson, '$.internalDiagnosticOrderID')) DIDs
					JOIN PHI.DiagnosticOrder DO ON DO.InternalDiagnosticOrderID = DIDs.VALUE               
					WHERE s.InternalStudyID = @internalStudyID FOR JSON PATH)   

	RETURN @jsonstr
END

