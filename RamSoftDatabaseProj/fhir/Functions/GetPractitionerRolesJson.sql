﻿
CREATE FUNCTION [fhir].[GetPractitionerRolesJson] 
(
   @internalUserID int = 0
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @jsonstr nvarchar(max) = null;

	set @jsonstr = ( Select 'assigningAuthority/' + cast(Facility.InternalAssigningAuthorityID as nvarchar(30)) as 'managingOrganization.reference', 
							Facility.InternalAssigningAuthorityID as 'managingOrganization.id', dbo.AssigningAuthority.Name as 'managingOrganization.display',
							StudyPlayerType.StudyPlayerType as 'role.coding', StudyPlayerType.Description as 'role.text',
							code.PhysicianSpecialty.PhysicianSpecialtyID as 'specialty.coding', code.PhysicianSpecialty.PhysicianSpecialtyName as 'specialty.text'
						 From dbo.StudyPlayer LEFT JOIN
							  dbo.Facility ON Facility.InternalFacilityID = StudyPlayer.InternalFacilityID	LEFT JOIN
							  code.StudyPlayerType ON StudyPlayerType.StudyPlayerType = StudyPlayer.StudyPlayerType LEFT JOIN
							  dbo.[User] ON dbo.[User].InternalUserID = StudyPlayer.InternalUserID LEFT JOIN
							  code.PhysicianSpecialty ON PhysicianSpecialty.PhysicianSpecialtyID = dbo.[User].PhysicianSpecialtyID LEFT JOIN
							  dbo.AssigningAuthority ON dbo.AssigningAuthority.InternalAssigningAuthorityID = dbo.Facility.InternalAssigningAuthorityID
					Where dbo.StudyPlayer.InternalUserID = @internalUserID FOR JSON PATH)   

	RETURN @jsonstr
END


