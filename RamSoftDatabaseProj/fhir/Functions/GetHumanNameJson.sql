﻿
CREATE FUNCTION fhir.GetHumanNameJson 
(
	@Input_FullName nvarchar(64)
)
RETURNS nvarchar(max)
AS
BEGIN
    DECLARE @json_string VARCHAR(1000) = null;

	if (@Input_FullName is not null) and (dbo.TRIM(@Input_FullName) <> '')
	begin

		DECLARE @rownum int = 0;
		DECLARE @curname nvarchar(64);

		DECLARE	@nametable TABLE (
			Family nvarchar(64),
			Given nvarchar(64),
			Middle nvarchar(64),
			Prefix nvarchar(64),
			Suffix nvarchar(64));

		DECLARE	@FamilyTab TABLE (name nvarchar(64));
		DECLARE	@GivenTab TABLE (name nvarchar(64));
		DECLARE	@PrefixTab TABLE (name nvarchar(64));
		DECLARE	@SuffixTab TABLE (name nvarchar(64));

		declare name_cursor cursor LOCAL for
			select value from string_split(@Input_FullName, '^');
    
		OPEN name_cursor;
		fetch next from name_cursor into @curname;

		while (@@FETCH_STATUS = 0) and (@rownum < 5)
		begin
		   if @rownum = 0 INSERT INTO @FamilyTab (name) VALUES (@curname);
		   else if @rownum = 1 INSERT INTO @GivenTab (name) VALUES (@curname);
		   else if @rownum = 2 INSERT INTO @GivenTab (name) VALUES (@curname);
		   else if @rownum = 3 INSERT INTO @PrefixTab (name) VALUES (@curname);
		   else if @rownum = 4 INSERT INTO @SuffixTab (name) VALUES (@curname);

		   set @rownum = @rownum + 1;
		   fetch next from name_cursor into @curname;
		end
		CLOSE name_cursor;


		SET @json_string = (SELECT (SELECT @Input_FullName) AS text,
								dbo.ufnToRawJsonArray((SELECT name FROM @FamilyTab FOR JSON PATH), 'name') AS family,
								dbo.ufnToRawJsonArray((SELECT name FROM @GivenTab FOR JSON PATH), 'name') AS given,
								dbo.ufnToRawJsonArray((SELECT name FROM @PrefixTab FOR JSON PATH), 'name') AS prefix,
								dbo.ufnToRawJsonArray((SELECT name FROM @SuffixTab FOR JSON PATH), 'name') AS suffix
							 FOR JSON PATH); 
    end
	 
	RETURN @json_string;
END
