﻿CREATE TABLE [PHI].[Encounter] (
    [InternalEncounterID]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalPatientID]             BIGINT         NOT NULL,
    [AdmissionID]                   NVARCHAR (64)  NOT NULL,
    [AdmissionDateTime]             DATETIME2 (7)  NULL,
    [ExtJson]                       NVARCHAR (MAX) NULL,
    [AdmissionAssigningAuthorityID] BIGINT         NULL,
    [InternalFacilityID]            BIGINT         NULL,
    [ConfidentialityID]             INT            NULL,
    [EncounterClassID]              INT            NULL,
    [InsuranceStatusID]             INT            NULL,
    [AmbulatoryStatusID]            INT            NULL,
    [EncounterStateID]              INT            NULL,
    [HL7Json]                       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Encounter] PRIMARY KEY CLUSTERED ([InternalEncounterID] ASC),
    CONSTRAINT [CK_Encounter_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [CK_Encounter_HL7Json_Valid] CHECK (isjson([HL7Json])>(0)),
    CONSTRAINT [FK_Encounter_AmbulatoryStatus] FOREIGN KEY ([AmbulatoryStatusID]) REFERENCES [code].[AmbulatoryStatus] ([AmbulatoryStatusID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Encounter_AssigningAuthority] FOREIGN KEY ([AdmissionAssigningAuthorityID]) REFERENCES [dbo].[AssigningAuthority] ([InternalAssigningAuthorityID]),
    CONSTRAINT [FK_Encounter_Confidentiality] FOREIGN KEY ([ConfidentialityID]) REFERENCES [code].[Confidentiality] ([ConfidentialityID]),
    CONSTRAINT [FK_Encounter_EncounterClass] FOREIGN KEY ([EncounterClassID]) REFERENCES [code].[EncounterClass] ([EncounterClassID]),
    CONSTRAINT [FK_Encounter_EncounterState] FOREIGN KEY ([EncounterStateID]) REFERENCES [code].[EncounterState] ([EncounterStateID]),
    CONSTRAINT [FK_Encounter_Facility] FOREIGN KEY ([InternalFacilityID]) REFERENCES [dbo].[Organization] ([InternalOrganizationID]),
    CONSTRAINT [FK_Encounter_InsuranceStatus] FOREIGN KEY ([InsuranceStatusID]) REFERENCES [code].[InsuranceStatus] ([InsuranceStatusID]),
    CONSTRAINT [FK_Encounter_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [U_Encounter_PatientAndAdmissionID] UNIQUE NONCLUSTERED ([InternalPatientID] ASC, [AdmissionID] ASC)
);


GO
CREATE TRIGGER [PHI].[Encounter_AIU_SetDefault]
	   ON  [PHI].[Encounter]
	   FOR INSERT, UPDATE 
	AS 
	BEGIN
		IF (EXISTS (SELECT 1 FROM inserted i WHERE i.AdmissionID = '-1' OR i.AdmissionID is NULL 
				OR i.EncounterClassID is NULL OR i.EncounterStateID is NULL OR i.AmbulatoryStatusID IS NULL OR  i.ConfidentialityID is NULL))
		BEGIN
			DECLARE @Prefix nvarchar(64) = dbo.GetSystemConfigStr('Ramsoft ID Prefix');
			DECLARE @AdmissionID nvarchar(64) = (SELECT CASE WHEN Inserted.AdmissionID = '-1' THEN CONCAT(@Prefix, Inserted.InternalEncounterID) ELSE Inserted.AdmissionID END FROM Inserted)
			DECLARE @DefaultEncounterClassID int = (SELECT EncounterClassid FROM code.EncounterClass WHERE EncounterClassCode = 'outpatient');
			DECLARE @DefaultEncounterStateID int = (SELECT EncounterStateID FROM code.EncounterState WHERE EncounterStateCode = 'planned');
			DECLARE @DefaultAlambulatoryID int = (SELECT AmbulatoryStatusID FROM code.AmbulatoryStatus WHERE AmbulatoryStatusCode = 'A0'); -- NO FUNCTIONAL LIMITATIONS
			DECLARE @DefaultConfidentialityID int = (SELECT ConfidentialityID FROM code.Confidentiality WHERE ConfidentialityCode = 'N'); --normal	

			UPDATE PHI.Encounter SET
					AdmissionID = (SELECT CASE WHEN i.AdmissionID = '-1' OR i.AdmissionID is NULL then CONCAT(@Prefix, i.InternalEncounterID) ELSE i.AdmissionID END),
					EncounterClassID = COALESCE (i.EncounterClassID, @DefaultEncounterClassID), 
					EncounterStateID = COALESCE (i.EncounterStateID, @DefaultEncounterStateID),
					AmbulatoryStatusID = COALESCE (i.AmbulatoryStatusID, @DefaultAlambulatoryID),
					ConfidentialityID = COALESCE (i.ConfidentialityID, @DefaultConfidentialityID)
				FROM inserted i
				WHERE PHI.Encounter.InternalEncounterID = i.InternalEncounterID	
				  AND (i.AdmissionID = '-1' OR i.AdmissionID is NULL OR i.EncounterClassID is NULL OR i.EncounterStateID is NULL OR i.AmbulatoryStatusID IS NULL OR  i.ConfidentialityID is NULL);
		END
	END

GO
CREATE TRIGGER [PHI].[Encounter_AI_Coverage]
	   ON  [PHI].[Encounter]
	   AFTER INSERT
	AS 
	BEGIN
		-- =============================================
		-- Author:		Jasmine
		-- Create date: June 9, 2016
		-- Description:	Copy Patient Coverage to Encounter Coverage
		-- =============================================

		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Copy PatientCoverage to EncounterCoverage for initial loading
		INSERT INTO PHI.EncounterCoverage (
				InternalEncounterID, 
				CoverageLevelID, 
				InternalPayerID, 
				InsuredID, 
				GroupID, 
				StartDate, 
				EndDate, 
				IsActive, 
				InsuredInternalPatientID, 
				InsuredRelationshipID)
			SELECT 
				i.InternalEncounterID, 
				pc.CoverageLevelID, 
				pc.InternalPayerID, 
				pc.InsuredID, 
				pc.GroupID, 
				pc.StartDate, 
				pc.EndDate, 
				pc.IsActive, 
				(CASE WHEN pc.InsuredInternalPatientID IS NULL THEN i.InternalPatientID ELSE pc.InsuredInternalPatientID END) as InsuredInternalPatientID,  
				(CASE WHEN (pc.InsuredInternalPatientID IS NULL) OR (pc.InsuredInternalPatientID = i.InternalPatientID) 
						THEN (SELECT RelationshipID FROM code.Relationship WHERE RelationshipName = 'SELF')
						ELSE pc.InsuredRelationshipID END) as InsuredRelationshipID
			FROM inserted i
			JOIN PHI.PatientCoverage pc ON pc.InternalPatientID = i.InternalPatientID;
	END
