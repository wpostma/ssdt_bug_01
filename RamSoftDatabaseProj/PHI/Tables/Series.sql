﻿CREATE TABLE [PHI].[Series] (
    [InternalSeriesID]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [LastUpdateUTC]      DATETIME2 (7)  CONSTRAINT [DF_Series_LastUpdateUTC] DEFAULT (sysutcdatetime()) NULL,
    [InternalStudyID]    BIGINT         NOT NULL,
    [ModalityCode]       NVARCHAR (8)   NOT NULL,
    [LateralityCode]     CHAR (1)       NULL,
    [InternalBodyPartID] BIGINT         NULL,
    [SeriesUID]          NVARCHAR (64)  NOT NULL,
    [SeriesNumber]       INT            NULL,
    [ExtJson]            NVARCHAR (MAX) NULL,
    [IsActive]           BIT            CONSTRAINT [DF_Series_IsActive] DEFAULT ((1)) NULL,
    [LastUpdateUserID]   BIGINT         NULL,
    CONSTRAINT [PK_Series] PRIMARY KEY CLUSTERED ([InternalSeriesID] ASC),
    CONSTRAINT [CK_Series_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_Series_BodyPart] FOREIGN KEY ([InternalBodyPartID]) REFERENCES [code].[BodyPart] ([InternalBodyPartID]) NOT FOR REPLICATION,
    CONSTRAINT [FK_Series_InternalStudyID] FOREIGN KEY ([InternalStudyID]) REFERENCES [PHI].[Study] ([InternalStudyID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Series_LastUpdateUserID] FOREIGN KEY ([LastUpdateUserID]) REFERENCES [dbo].[User] ([InternalUserID]),
    CONSTRAINT [FK_Series_Laterality] FOREIGN KEY ([LateralityCode]) REFERENCES [code].[Laterality] ([LateralityCode]) NOT FOR REPLICATION,
    CONSTRAINT [FK_Series_Modality] FOREIGN KEY ([ModalityCode]) REFERENCES [code].[Modality] ([ModalityCode])
);


GO
ALTER TABLE [PHI].[Series] NOCHECK CONSTRAINT [FK_Series_BodyPart];


GO
ALTER TABLE [PHI].[Series] NOCHECK CONSTRAINT [FK_Series_Laterality];


GO
CREATE UNIQUE NONCLUSTERED INDEX [U_Series_UID]
    ON [PHI].[Series]([SeriesUID] ASC);

