﻿CREATE TABLE [PHI].[PatientCoverage] (
    [InternalPatientID]        BIGINT         NOT NULL,
    [CoverageLevelID]          SMALLINT       CONSTRAINT [DF_PatientCoverage_CoverageLevel] DEFAULT ((1)) NOT NULL,
    [InternalPayerID]          BIGINT         NOT NULL,
    [InsuredID]                NVARCHAR (64)  NULL,
    [GroupID]                  NVARCHAR (64)  NULL,
    [StartDate]                DATE           NULL,
    [EndDate]                  DATE           NULL,
    [IsActive]                 BIT            CONSTRAINT [DF_PatientCoverage_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]                  NVARCHAR (MAX) NULL,
    [FinancialClassID]         INT            NULL,
    [InsuredInternalPatientID] BIGINT         NULL,
    [InsuredRelationshipID]    INT            NULL,
    [InternalCoverageID]       BIGINT         IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_PatientCoverage] PRIMARY KEY CLUSTERED ([InternalCoverageID] ASC),
    CONSTRAINT [PatientCoverage_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_PatientCoverage_CoverageLevel] FOREIGN KEY ([CoverageLevelID]) REFERENCES [code].[CoverageLevel] ([CoverageLevelID]),
    CONSTRAINT [FK_PatientCoverage_InsuredPatient] FOREIGN KEY ([InsuredInternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]),
    CONSTRAINT [FK_PatientCoverage_InsuredRelationship] FOREIGN KEY ([InsuredRelationshipID]) REFERENCES [code].[BeneficiaryRelationship] ([BeneficiaryRelationshipID]),
    CONSTRAINT [FK_PatientCoverage_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_PatientCoverage_Payer] FOREIGN KEY ([InternalPayerID]) REFERENCES [dbo].[Payer] ([InternalPayerID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [INSURANCECOVERAGE_I_INSURANCECOVERAGE_INSUREDID]
    ON [PHI].[PatientCoverage]([InsuredID] ASC);


GO
CREATE NONCLUSTERED INDEX [INSURANCECOVERAGE_I_INSURANCECOVERAGE_POLICYGROUP]
    ON [PHI].[PatientCoverage]([GroupID] ASC);


GO
CREATE NONCLUSTERED INDEX [INSURANCECOVERAGE_I_INSURANCECOVERAGE_ENDDATE]
    ON [PHI].[PatientCoverage]([EndDate] ASC);


GO
CREATE NONCLUSTERED INDEX [INSURANCECOVERAGE_I_INSURNACECOVERAGE_STATUS]
    ON [PHI].[PatientCoverage]([IsActive] ASC);


GO
CREATE TRIGGER [PHI].[PatientCoverage_AIU_SetPatientFinancialClass]
	   ON  [PHI].[PatientCoverage]
	   FOR INSERT, UPDATE 
	AS 
	BEGIN
		DECLARE @InternalPatientID bigint;

		DECLARE Inserted_Cursor CURSOR LOCAL FOR
		   SELECT InternalPatientID FROM inserted;

		OPEN Inserted_Cursor;
		FETCH NEXT FROM Inserted_Cursor INTO @InternalPatientID;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN
			-- To sync Patient's Financial Class with Primary Insurance Company's Financial Class
			EXEC [dbo].[SP_UpdatePatientFinancialClass]	@InternalPatientID = @InternalPatientID;

			FETCH NEXT FROM Inserted_Cursor INTO @InternalPatientID;
		END
		CLOSE Inserted_Cursor;
	END
