﻿CREATE TABLE [PHI].[Patient] (
    [InternalPatientID]            BIGINT           IDENTITY (1, 1) NOT NULL,
    [InternalAssigningAuthorityID] BIGINT           NOT NULL,
    [PatientGUID]                  UNIQUEIDENTIFIER CONSTRAINT [DF_Patient_PatientGUID] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [IsActive]                     BIT              CONSTRAINT [DF_Patient_Active] DEFAULT ((1)) NOT NULL,
    [LastupdateUserID]             BIGINT           CONSTRAINT [DF_Patient_LastUpdateUserID] DEFAULT ((-1)) NOT NULL,
    [LastUpdateUTC]                DATETIME2 (7)    CONSTRAINT [DF_Patient_LastUpdateUTC] DEFAULT (sysutcdatetime()) NOT NULL,
    [PatientID]                    NVARCHAR (64)    NULL,
    [PatientName]                  NVARCHAR (64)    NULL,
    [Accountnumber]                NVARCHAR (64)    NULL,
    [Ssn]                          NVARCHAR (64)    NULL,
    [Hl7updated]                   BIT              CONSTRAINT [DF_Patient_HL7Updated] DEFAULT ((0)) NOT NULL,
    [PatientBalance]               SMALLMONEY       CONSTRAINT [DF_Patient_PatientBalance] DEFAULT ((0)) NOT NULL,
    [ExtJson]                      NVARCHAR (MAX)   NULL,
    [IsDeceased]                   BIT              CONSTRAINT [DF_Patient_IsDeceased] DEFAULT ((0)) NOT NULL,
    [SexID]                        INT              NULL,
    [StateID]                      INT              NULL,
    [CountryID]                    INT              NULL,
    [FinancialClassID]             INT              NULL,
    [MaritalStatusID]              INT              NULL,
    [AccountStatusID]              INT              NULL,
    [BirthDate]                    [dbo].[RSDate]   NULL,
    [DeceasedDate]                 [dbo].[RSDate]   NULL,
    [InsuranceBalance]             SMALLMONEY       CONSTRAINT [DF_Patient_InsuranceBalance] DEFAULT ((0)) NULL,
    [GuarantorInternalPatientID]   BIGINT           NULL,
    [GuarantorRelationshipID]      INT              NULL,
    [ConfidentialityID]            INT              NULL,
    [HL7Json]                      NVARCHAR (MAX)   NULL,
    [InternalEmployerID]           BIGINT           NULL,
    CONSTRAINT [IX_PatientInternaPatientlID] PRIMARY KEY CLUSTERED ([InternalPatientID] ASC),
    CONSTRAINT [CK_Patient_BirthDate_Valid] CHECK (isdate([BirthDate])>(0) OR [BirthDate] IS NULL),
    CONSTRAINT [CK_Patient_DeceasedDate_Valid] CHECK (isdate([DeceasedDate])>(0) OR [DeceasedDate] IS NULL),
    CONSTRAINT [CK_Patient_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [CK_Patient_HL7Json_Valid] CHECK (isjson([HL7Json])>(0)),
    CONSTRAINT [FK_Patient_AccountStatus] FOREIGN KEY ([AccountStatusID]) REFERENCES [code].[AccountStatus] ([AccountStatusID]),
    CONSTRAINT [FK_Patient_AssigningAuthority] FOREIGN KEY ([InternalAssigningAuthorityID]) REFERENCES [dbo].[AssigningAuthority] ([InternalAssigningAuthorityID]),
    CONSTRAINT [FK_Patient_Confidentiality] FOREIGN KEY ([ConfidentialityID]) REFERENCES [code].[Confidentiality] ([ConfidentialityID]),
    CONSTRAINT [FK_Patient_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID]),
    CONSTRAINT [FK_Patient_FinancialClass] FOREIGN KEY ([FinancialClassID]) REFERENCES [code].[FinancialClass] ([FinancialClassID]),
    CONSTRAINT [FK_Patient_GuarantorPatient] FOREIGN KEY ([GuarantorInternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]),
    CONSTRAINT [FK_Patient_GuarantorRelationship] FOREIGN KEY ([GuarantorRelationshipID]) REFERENCES [code].[Relationship] ([RelationshipID]),
    CONSTRAINT [FK_Patient_InternalEmployerID] FOREIGN KEY ([InternalEmployerID]) REFERENCES [dbo].[Employer] ([InternalEmployerID]),
    CONSTRAINT [FK_Patient_LastupdateUserID] FOREIGN KEY ([LastupdateUserID]) REFERENCES [dbo].[User] ([InternalUserID]),
    CONSTRAINT [FK_Patient_MaritalStatus] FOREIGN KEY ([MaritalStatusID]) REFERENCES [code].[MaritalStatus] ([MaritalStatusID]),
    CONSTRAINT [FK_Patient_Sex] FOREIGN KEY ([SexID]) REFERENCES [code].[Sex] ([SexID]),
    CONSTRAINT [FK_Patient_State] FOREIGN KEY ([StateID]) REFERENCES [code].[State] ([StateID]),
    CONSTRAINT [U_Patient_IssuerAndPatientID] UNIQUE NONCLUSTERED ([InternalAssigningAuthorityID] ASC, [PatientID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_PATIENT_WITH_KEYIDENT]
    ON [PHI].[Patient]([InternalPatientID] ASC)
    INCLUDE([PatientName], [BirthDate], [ExtJson]);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_InternalPatientID]
    ON [PHI].[Patient]([InternalPatientID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PatientGUID]
    ON [PHI].[Patient]([PatientGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PatientID]
    ON [PHI].[Patient]([PatientID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PatientFamilyname]
    ON [PHI].[Patient]([PatientName] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PatientBirthdate]
    ON [PHI].[Patient]([BirthDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PatientSsn]
    ON [PHI].[Patient]([Ssn] ASC);


GO

	CREATE TRIGGER [PHI].[Patient_AIU_SetDefault]
		ON  [PHI].[Patient]
		FOR INSERT, UPDATE 
	AS 
	BEGIN
		DECLARE @Prefix nvarchar(64) = dbo.GetSystemConfigStr('Ramsoft ID Prefix');	

		UPDATE [PHI].[Patient] SET
			LastupdateUTC = SYSUTCDATETIME(),
			IsDeceased = (CASE WHEN (i.IsDeceased = 1 OR i.DeceasedDate IS NOT NULL) THEN 1 ELSE 0 END),
			-- make inactive if deceased and has no balance owing (#40029)
			IsActive = (CASE WHEN (i.PatientBalance = 0 AND (i.IsDeceased = 1 OR i.DeceasedDate IS NOT NULL)) THEN 0 ELSE [PHI].[Patient].IsActive END),
			PatientID = COALESCE([PHI].[Patient].PatientID,   -- will pickup the first not null
								 JSON_VALUE(i.ExtJson, '$.identifier'), 
								 JSON_VALUE(i.ExtJson, '$.identifier.value'), 
								 JSON_VALUE(i.ExtJson, '$.identifier[0].value'), 
								 CONCAT(@Prefix, CONVERT(NVARCHAR, [PHI].[Patient].InternalPatientID)))
		FROM Inserted i
		WHERE i.InternalPatientID = [PHI].[Patient].InternalPatientID;
	END
