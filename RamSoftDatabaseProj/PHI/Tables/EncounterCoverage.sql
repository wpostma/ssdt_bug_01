﻿CREATE TABLE [PHI].[EncounterCoverage] (
    [InternalEncounterCoverageID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalEncounterID]         BIGINT         NOT NULL,
    [CoverageLevelID]             SMALLINT       NOT NULL,
    [InternalPayerID]             BIGINT         NOT NULL,
    [InsuredID]                   NVARCHAR (64)  NULL,
    [GroupID]                     NVARCHAR (64)  NULL,
    [StartDate]                   DATE           NULL,
    [EndDate]                     DATE           NULL,
    [IsActive]                    BIT            CONSTRAINT [DF_EncounterCoverage_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]                     NVARCHAR (MAX) NULL,
    [FinancialClassID]            INT            NULL,
    [InsuredInternalPatientID]    BIGINT         NULL,
    [InsuredRelationshipID]       INT            NULL,
    CONSTRAINT [PK_EncounterCoverage] PRIMARY KEY CLUSTERED ([InternalEncounterCoverageID] ASC),
    CONSTRAINT [CK_EncounterCoverage_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [FK_EncounterCoverage_CoverageLevel] FOREIGN KEY ([CoverageLevelID]) REFERENCES [code].[CoverageLevel] ([CoverageLevelID]),
    CONSTRAINT [FK_EncounterCoverage_InsuredRelationship] FOREIGN KEY ([InsuredRelationshipID]) REFERENCES [code].[BeneficiaryRelationship] ([BeneficiaryRelationshipID]),
    CONSTRAINT [FK_EncounterCoverage_Payer] FOREIGN KEY ([InternalPayerID]) REFERENCES [dbo].[Payer] ([InternalPayerID]),
    CONSTRAINT [FK_EncounterCoverage_Visit] FOREIGN KEY ([InternalEncounterID]) REFERENCES [PHI].[Encounter] ([InternalEncounterID]) ON DELETE CASCADE ON UPDATE CASCADE
);

