﻿CREATE TABLE [PHI].[Instance] (
    [InternalInstanceID] BIGINT             IDENTITY (1, 1) NOT NULL,
    [InternalSeriesID]   BIGINT             NOT NULL,
    [InternalPatientID]  BIGINT             NOT NULL,
    [InternalSOPClassID] BIGINT             NOT NULL,
    [ContentDateTime]    DATETIMEOFFSET (7) CONSTRAINT [DF_Instance_ContentDateTime] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [ObjectNumber]       INT                NOT NULL,
    [SOPInstanceUID]     NVARCHAR (64)      NOT NULL,
    [StorageVolumeID]    BIGINT             NULL,
    [LocalFileID]        NVARCHAR (256)     NOT NULL,
    [FileExists]         BIT                CONSTRAINT [DF_Instance_FileExists] DEFAULT ((0)) NOT NULL,
    [IsReport]           BIT                CONSTRAINT [DF_Instance_IsReport] DEFAULT ((0)) NULL,
    [IsActive]           BIT                CONSTRAINT [DF_Instance_IsActive] DEFAULT ((1)) NULL,
    [DicomJson]          NVARCHAR (MAX)     NULL,
    [LastUpdateUTC]      DATETIME2 (7)      CONSTRAINT [DF_Instance_LastUpdateUTC] DEFAULT (sysutcdatetime()) NULL,
    [LastUpdateUserID]   BIGINT             NULL,
    CONSTRAINT [PK_SeriesObjects] PRIMARY KEY CLUSTERED ([InternalInstanceID] ASC),
    CONSTRAINT [CK_Instance_DicomJson_Valid] CHECK (isjson([DicomJSON])>(0)),
    CONSTRAINT [FK_Instance_InternalPatientID] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]),
    CONSTRAINT [FK_Instance_InternalSOPClassID] FOREIGN KEY ([InternalSOPClassID]) REFERENCES [code].[SOPClass] ([InternalSOPClassID]),
    CONSTRAINT [FK_Instance_LastUpdateUserID] FOREIGN KEY ([LastUpdateUserID]) REFERENCES [dbo].[User] ([InternalUserID]),
    CONSTRAINT [FK_Instance_StorageVolumeID] FOREIGN KEY ([StorageVolumeID]) REFERENCES [dbo].[Volume] ([InternalVolumeID]),
    CONSTRAINT [FK_InstanceInternalSeriesID] FOREIGN KEY ([InternalSeriesID]) REFERENCES [PHI].[Series] ([InternalSeriesID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_InstanceSeriesIDObjectNumber]
    ON [PHI].[Instance]([InternalSeriesID] ASC, [ObjectNumber] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_InstanceSOPInstanceUID]
    ON [PHI].[Instance]([SOPInstanceUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_InstanceLocalFileID]
    ON [PHI].[Instance]([LocalFileID] ASC);

