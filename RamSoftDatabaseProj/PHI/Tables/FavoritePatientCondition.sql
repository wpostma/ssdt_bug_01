﻿CREATE TABLE [PHI].[FavoritePatientCondition] (
    [ID]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (64)  NOT NULL,
    [Extjson]        NVARCHAR (MAX) NULL,
    [InternalUserID] BIGINT         NULL,
    [ViewName]       NVARCHAR (64)  NULL,
    CONSTRAINT [PK_FavoritePatientCondition] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_FavoritePatientCondition]
    ON [PHI].[FavoritePatientCondition]([Name] ASC);

