﻿CREATE TABLE [PHI].[Amendment] (
    [InternalAmendmentID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalInstanceID]  BIGINT         NOT NULL,
    [RequestDateTime]     DATETIME2 (7)  CONSTRAINT [DF_Amendment_RequestDateTime] DEFAULT (sysutcdatetime()) NOT NULL,
    [RequestedBy]         NVARCHAR (64)  NOT NULL,
    [ReviewDateTime]      DATETIME2 (7)  NULL,
    [ReviewerID]          BIGINT         NULL,
    [Status]              INT            NOT NULL,
    [AmendedInstanceID]   BIGINT         NULL,
    [ExtJson]             NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Amendment] PRIMARY KEY CLUSTERED ([InternalAmendmentID] ASC),
    CONSTRAINT [FK_Amendment_AmendedInstance] FOREIGN KEY ([AmendedInstanceID]) REFERENCES [PHI].[Instance] ([InternalInstanceID]) NOT FOR REPLICATION,
    CONSTRAINT [FK_Amendment_Instance] FOREIGN KEY ([InternalInstanceID]) REFERENCES [PHI].[Instance] ([InternalInstanceID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Amendment_Reviewer] FOREIGN KEY ([ReviewerID]) REFERENCES [dbo].[User] ([InternalUserID]) ON DELETE SET NULL ON UPDATE CASCADE
);


GO
ALTER TABLE [PHI].[Amendment] NOCHECK CONSTRAINT [FK_Amendment_AmendedInstance];

