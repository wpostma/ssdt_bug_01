﻿CREATE TABLE [PHI].[Study] (
    [InternalStudyID]              BIGINT             IDENTITY (1, 1) NOT NULL,
    [InternalDepartmentID]         BIGINT             NULL,
    [InternalStudyTypeID]          BIGINT             NULL,
    [ModalityCode]                 NVARCHAR (8)       NULL,
    [LateralityCode]               CHAR (1)           NULL,
    [InternalBodyPartID]           BIGINT             NULL,
    [PriorityValue]                SMALLINT           CONSTRAINT [DF_Study_PriorityValue] DEFAULT ((0)) NOT NULL,
    [StatusValue]                  SMALLINT           CONSTRAINT [DF_Study_StatusValue] DEFAULT ((100)) NOT NULL,
    [ForeignDBStudyID]             BIGINT             NULL,
    [LastupdateUTC]                DATETIME2 (7)      CONSTRAINT [DF_Study_LastupdateUTC] DEFAULT (sysutcdatetime()) NOT NULL,
    [StudyDateTime]                DATETIMEOFFSET (7) NULL,
    [StudyDateTimeUTC]             DATETIME2 (7)      NULL,
    [StartDateTime]                DATETIMEOFFSET (7) NULL,
    [EndDateTime]                  DATETIMEOFFSET (7) NULL,
    [Description]                  NVARCHAR (64)      NULL,
    [StudyID]                      NVARCHAR (16)      NULL,
    [IsHL7Updated]                 BIT                CONSTRAINT [DF_Study_IsHL7Updated] DEFAULT ((0)) NOT NULL,
    [IsBilled]                     BIT                CONSTRAINT [DF_Study_IsBilled] DEFAULT ((0)) NOT NULL,
    [IsPosted]                     BIT                CONSTRAINT [DF_Study_IsPosted] DEFAULT ((0)) NOT NULL,
    [Source]                       INT                NULL,
    [ExtJson]                      NVARCHAR (MAX)     NULL,
    [StudyUID]                     VARCHAR (300)      NOT NULL,
    [AccessionNumber]              NVARCHAR (16)      NULL,
    [InternalPatientID]            BIGINT             NOT NULL,
    [ImagingFacilityID]            BIGINT             NULL,
    [InstanceAvailabilityID]       INT                NULL,
    [InternalRoomID]               BIGINT             NULL,
    [InternalAssigningAuthorityID] BIGINT             NULL,
    [PrimaryEncounterID]           BIGINT             NULL,
    [IsActive]                     BIT                CONSTRAINT [DF_Study_IsActive] DEFAULT ((1)) NULL,
    [LastUpdateUserID]             BIGINT             NULL,
    [HL7Json]                      NVARCHAR (MAX)     NULL,
    [FaxStatusID]                  INT                NULL,
    [FaxStatusUTC]                 DATETIME2 (7)      NULL,
    [ScheduledExamRoomID]          BIGINT             NULL,
    CONSTRAINT [IX_StudyInternalStudyID] PRIMARY KEY CLUSTERED ([InternalStudyID] ASC),
    CONSTRAINT [CK_Study_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [CK_Study_HL7Json_Valid] CHECK (isjson([HL7Json])>(0)),
    CONSTRAINT [FK_Study_AssigningAuthority] FOREIGN KEY ([InternalAssigningAuthorityID]) REFERENCES [dbo].[AssigningAuthority] ([InternalAssigningAuthorityID]),
    CONSTRAINT [FK_Study_BodyPart] FOREIGN KEY ([InternalBodyPartID]) REFERENCES [code].[BodyPart] ([InternalBodyPartID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Study_Encounter] FOREIGN KEY ([PrimaryEncounterID]) REFERENCES [PHI].[Encounter] ([InternalEncounterID]),
    CONSTRAINT [FK_Study_Facility] FOREIGN KEY ([ImagingFacilityID]) REFERENCES [dbo].[Organization] ([InternalOrganizationID]),
    CONSTRAINT [FK_Study_FaxStatus] FOREIGN KEY ([FaxStatusID]) REFERENCES [code].[CommunicationStatus] ([CommunicationStatusID]),
    CONSTRAINT [FK_Study_InstanceAvailability] FOREIGN KEY ([InstanceAvailabilityID]) REFERENCES [code].[InstanceAvailability] ([InstanceAvailabilityID]),
    CONSTRAINT [FK_Study_LastupdateUserID] FOREIGN KEY ([LastUpdateUserID]) REFERENCES [dbo].[User] ([InternalUserID]),
    CONSTRAINT [FK_Study_Laterality] FOREIGN KEY ([LateralityCode]) REFERENCES [code].[Laterality] ([LateralityCode]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Study_Modality] FOREIGN KEY ([ModalityCode]) REFERENCES [code].[Modality] ([ModalityCode]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Study_Patient] FOREIGN KEY ([InternalPatientID]) REFERENCES [PHI].[Patient] ([InternalPatientID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Study_Priority] FOREIGN KEY ([PriorityValue]) REFERENCES [code].[Priority] ([PriorityValue]),
    CONSTRAINT [FK_Study_Room] FOREIGN KEY ([InternalRoomID]) REFERENCES [dbo].[Room] ([InternalRoomID]),
    CONSTRAINT [FK_Study_ScheduledExamRoom] FOREIGN KEY ([ScheduledExamRoomID]) REFERENCES [dbo].[Room] ([InternalRoomID]),
    CONSTRAINT [FK_Study_Status] FOREIGN KEY ([StatusValue]) REFERENCES [code].[Status] ([StatusValue])
);


GO
CREATE NONCLUSTERED INDEX [IX_StudyInternalstudytypeID]
    ON [PHI].[Study]([InternalStudyTypeID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Studydatetime]
    ON [PHI].[Study]([StartDateTime] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_STUDY_STUDYUID_UNIQUE]
    ON [PHI].[Study]([StudyUID] ASC);


GO
CREATE TRIGGER [PHI].[trStudyDel] 
   ON  [PHI].[Study] 
   AFTER DELETE
AS 
BEGIN
   DELETE s FROM dbo.StudyGroup s
   INNER JOIN deleted d ON d.InternalStudyID=s.PrimaryInternalStudyID;
END

GO
Create TRIGGER [PHI].[trStudy_CheckUpdatedStatus] 
   ON  [PHI].[Study] 
   AFTER UPDATE
AS 
BEGIN
   DECLARE @studyID bigint;
   DECLARE @statusValue smallint;
   DECLARE @reservedTimeID bigint;

   Select @studyID = InternalStudyID, @statusValue = StatusValue From inserted
   Select @reservedTimeID = InternalReservedTimeID From dbo.ReservedTime where InternalStudyID = @studyID

   IF (@statusValue = 20 OR @statusValue = 80)
   Begin
		DELETE FROM dbo.ReservedTimeResource Where InternalReservedTimeID = @reservedTimeID;
		DELETE FROM dbo.ReservedTime where InternalReservedTimeID = @reservedTimeID;
   End   
END

GO
CREATE TRIGGER [PHI].[Study_AppointmentHistory] 
ON  [PHI].[Study] AFTER UPDATE
AS 
BEGIN

DECLARE	@InternalStudyID bigint;
DECLARE @ExtJson nvarchar(max)

DECLARE @LastUpdatedUserID bigint;
DECLARE @New_Status smallint;
DECLARE @Old_Status smallint;

Select @InternalStudyID = InternalStudyID, @LastUpdatedUserID = LastUpdateUserID , @New_Status = StatusValue From Inserted;
Select @Old_Status = StatusValue FROM Deleted;

/* Appointment States will be mapped to the study status and IsAppointmentStatus will be set True

StudyOrdered    	 -  20
NeedToReSchedule	 -  20
Scheduled	         -  30
ReScheduled			 -  30
Confirmed			 -  50
Arrived			     -  70
Cancelled            -  80
NoShow               -  80
ReadyForScan         -  90        */


	DECLARE @IsAppointmentStatus bit;       
	DECLARE @IsActive bit;
	SELECT @IsAppointmentStatus = IsAppointmentStatus, @IsActive = IsActive FROM code.Status WHERE StatusValue = @New_Status
	--if study status is related to appointment then it will call log.InsertAppointmentHistory and the current status should be logging				
	IF (@IsActive = 1 AND @IsAppointmentStatus = 1)
	BEGIN
		IF (@New_Status <> @Old_Status)
		BEGIN		
			--Check if the study is linked with reservedTime or not
			IF EXISTS (SELECT rt.InternalReservedTimeID FROM dbo.ReservedTime rt WHERE rt.InternalStudyID = @InternalStudyID)
			BEGIN
				SELECT
					@ExtJson = (SELECT 
						(CAST(rt.StartDate AS datetime) + CAST(rt.StartTime AS datetime)) AS startTime,
						(CAST(rt.EndDate AS datetime) + CAST(rt.EndTime AS datetime)) AS finishTime,
						(SELECT rsc.Name as name 
							FROM dbo.ReservedTimeResource rtr LEFT JOIN 								
								dbo.Resource rsc ON rtr.InternalResourceID = rsc.InternalResourceID  AND rtr.InternalReservedTimeID = rt.InternalReservedTimeID							
							FOR JSON PATH) AS resources,
						@New_Status AS appointmentState,
						(SELECT UserName FROM dbo.[User] WHERE InternalUserID = @LastUpdatedUserID) AS updatedUserName												
					FOR JSON PATH, WITHOUT_ARRAY_WRAPPER)			
				FROM dbo.ReservedTime rt
				WHERE rt.InternalStudyID = @InternalStudyID							
				
				EXEC log.InsertAppointmentHistory @InternalStudyID = @InternalStudyID, -- bigint
					@ExtJson = @ExtJson -- nvarchar(max)							
			END	
		END
	END	
END

GO
CREATE TRIGGER [PHI].[Study_AIU_StatusTransition]
	   ON  [PHI].[Study]
	   FOR INSERT, UPDATE 
	AS 
	BEGIN
	    DECLARE @InternalStudyID bigint, @NewStatusValue smallint, @OldStatusValue smallint;

		DECLARE Cur_Study_Cursor CURSOR LOCAL FOR
		   SELECT i.InternalStudyID, i.StatusValue, d.StatusValue FROM inserted i LEFT JOIN deleted d ON d.InternalStudyID = i.InternalStudyID;
	
		OPEN Cur_Study_Cursor;
		FETCH NEXT FROM Cur_Study_Cursor INTO @InternalStudyID, @NewStatusValue, @OldStatusValue;

		WHILE (@@FETCH_STATUS = 0) 
		BEGIN		
		    IF (@OldStatusValue IS NULL) OR (@OldStatusValue <= 0)
			BEGIN  -- New Study, just insert
				INSERT INTO dbo.StudyStatusTransition(InternalStudyID, StatusValue, TransitionTimeUTC)
				   SELECT @InternalStudyID, @NewStatusValue, SYSUTCDATETIME();    
			END          
			ELSE
			BEGIN
			   IF (@NewStatusValue > @OldStatusValue) 
			   BEGIN
					INSERT INTO dbo.StudyStatusTransition(InternalStudyID, StatusValue, TransitionTimeUTC)
					   SELECT @InternalStudyID, cs.StatusValue, SYSUTCDATETIME() FROM code.[Status] cs 
					       WHERE cs.StatusValue > @OldStatusValue and cs.StatusValue <= @NewStatusValue;     
			   END
			   ELSE IF (@NewStatusValue < @OldStatusValue)  
			   BEGIN
					-- remove statues >= current and insert a new one
					DELETE FROM dbo.StudyStatusTransition WHERE StatusValue >= @NewStatusValue; 

					INSERT INTO dbo.StudyStatusTransition(InternalStudyID, StatusValue, TransitionTimeUTC)
					   SELECT @InternalStudyID, @NewStatusValue, SYSUTCDATETIME();    
			   END
			END

			FETCH NEXT FROM Cur_Study_Cursor INTO @InternalStudyID, @NewStatusValue, @OldStatusValue;
		END
		CLOSE Cur_Study_Cursor;
	END

GO

	CREATE TRIGGER [PHI].[Study_AIU_SetDefault]
	   ON  [PHI].[Study]
	   FOR INSERT, UPDATE 
	AS 
	BEGIN
		DECLARE @Prefix nvarchar(64) = dbo.GetSystemConfigStr('Ramsoft Study ID Prefix');	

		UPDATE [PHI].[Study] SET
			LastupdateUTC = SYSUTCDATETIME(),
			-- Set to patient's InternalAssigningAuthorityID if it's null
			InternalAssigningAuthorityID =  (CASE WHEN i.InternalAssigningAuthorityID IS NULL THEN P.InternalAssigningAuthorityID ELSE i.InternalAssigningAuthorityID END),
			AccessionNumber = COALESCE([PHI].[Study].AccessionNumber,   -- will pickup the first not null
								 JSON_VALUE(i.ExtJson, '$.identifier'), 
								 JSON_VALUE(i.ExtJson, '$.identifier.value'), 
								 JSON_VALUE(i.ExtJson, '$.identifier[0].value'), 
								 CONCAT(@Prefix, CONVERT(NVARCHAR, [PHI].[Study].InternalStudyID))),
			StudyID = COALESCE([PHI].[Study].StudyID, [PHI].[Study].InternalStudyID) 
		FROM Inserted i
		JOIN PHI.Patient P ON P.InternalPatientID = i.InternalPatientID
		WHERE i.StudyUID = [PHI].[Study].StudyUID;
	END
