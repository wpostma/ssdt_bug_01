﻿CREATE TABLE [code].[Modality] (
    [ModalityCode] NVARCHAR (8)   NOT NULL,
    [Description]  NVARCHAR (64)  NOT NULL,
    [IsModality]   BIT            CONSTRAINT [DF_Modality_IsModality] DEFAULT ((1)) NOT NULL,
    [ExtJson]      NVARCHAR (MAX) NULL,
    [IsSchedule]   BIT            CONSTRAINT [DF_Modality_IsSchedule] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Modality] PRIMARY KEY CLUSTERED ([ModalityCode] ASC)
);

