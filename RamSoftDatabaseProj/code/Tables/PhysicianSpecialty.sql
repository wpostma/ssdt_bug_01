﻿CREATE TABLE [code].[PhysicianSpecialty] (
    [PhysicianSpecialtyID]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [PhysicianSpecialtyName] NVARCHAR (64)  NOT NULL,
    [IsActive]               BIT            CONSTRAINT [DF__PHYSICIAN__INUSE__5DEAEAF5] DEFAULT ((1)) NOT NULL,
    [ExtJSON]                NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_PhysicianSpecialty] PRIMARY KEY CLUSTERED ([PhysicianSpecialtyID] ASC),
    CONSTRAINT [CODEPHYSICIANSPECIALTY_ExtJSON_VALID] CHECK (isjson([ExtJSON])>(0))
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_PhysicianSpecialty_Name]
    ON [code].[PhysicianSpecialty]([PhysicianSpecialtyName] ASC);

