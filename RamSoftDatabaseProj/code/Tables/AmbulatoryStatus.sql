﻿CREATE TABLE [code].[AmbulatoryStatus] (
    [AmbulatoryStatusID]    INT            IDENTITY (1, 1) NOT NULL,
    [AmbulatoryStatusCode]  NVARCHAR (64)  NOT NULL,
    [AmbulatoryStatusName]  NVARCHAR (64)  NOT NULL,
    [AmbulatoryStatusLevel] INT            NULL,
    [IsActive]              BIT            CONSTRAINT [DF_AmbulatoryStatus_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]               NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_AmbulatoryStatus] PRIMARY KEY CLUSTERED ([AmbulatoryStatusID] ASC),
    CONSTRAINT [CK_AmbulatoryStatus_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_AmbulatoryStatus_Code] UNIQUE NONCLUSTERED ([AmbulatoryStatusCode] ASC)
);

