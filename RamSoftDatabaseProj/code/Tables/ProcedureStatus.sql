﻿CREATE TABLE [code].[ProcedureStatus] (
    [InternalProcedureStatusID] INT            IDENTITY (1, 1) NOT NULL,
    [Code]                      NVARCHAR (20)  NOT NULL,
    [Display]                   NVARCHAR (150) NOT NULL,
    [Description]               NVARCHAR (MAX) NULL,
    [ExtJson]                   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK__Procedur__8C95287644E70B88] PRIMARY KEY CLUSTERED ([InternalProcedureStatusID] ASC)
);

