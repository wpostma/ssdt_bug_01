﻿CREATE TABLE [code].[ShortcutAction] (
    [InternalShortcutID] BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name]               NVARCHAR (64) NOT NULL,
    [Description]        NVARCHAR (64) NOT NULL,
    [ActionName]         NVARCHAR (64) NOT NULL,
    [Category]           NVARCHAR (64) NOT NULL,
    CONSTRAINT [PK_Shortcut] PRIMARY KEY CLUSTERED ([InternalShortcutID] ASC)
);

