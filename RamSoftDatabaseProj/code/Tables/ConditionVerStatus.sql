﻿CREATE TABLE [code].[ConditionVerStatus] (
    [ConditionVerStatusID]   INT            IDENTITY (1, 1) NOT NULL,
    [ConditionVerStatusCode] NVARCHAR (64)  NOT NULL,
    [ConditionVerStatusName] NVARCHAR (64)  NOT NULL,
    [IsActive]               BIT            CONSTRAINT [DF_ConditionVerStatus_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]                NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ConditionVerStatus] PRIMARY KEY CLUSTERED ([ConditionVerStatusID] ASC),
    CONSTRAINT [CK_ConditionVerStatus_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_ConditionVerStatus_Code] UNIQUE NONCLUSTERED ([ConditionVerStatusCode] ASC)
);

