﻿CREATE TABLE [code].[Priority] (
    [PriorityValue] SMALLINT       NOT NULL,
    [Name]          NVARCHAR (16)  NOT NULL,
    [HL7Code]       NVARCHAR (16)  NOT NULL,
    [ExtJson]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Priority] PRIMARY KEY CLUSTERED ([PriorityValue] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Priority_Name]
    ON [code].[Priority]([Name] ASC);

