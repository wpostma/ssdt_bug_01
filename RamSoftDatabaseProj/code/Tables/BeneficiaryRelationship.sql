﻿CREATE TABLE [code].[BeneficiaryRelationship] (
    [BeneficiaryRelationshipID]   INT            IDENTITY (1, 1) NOT NULL,
    [BeneficiaryRelationshipCode] NVARCHAR (64)  NOT NULL,
    [BeneficiaryRelationshipName] NVARCHAR (64)  NOT NULL,
    [IsActive]                    BIT            CONSTRAINT [DF_BeneficiaryRelationship_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]                     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_BeneficiaryRelationship] PRIMARY KEY CLUSTERED ([BeneficiaryRelationshipID] ASC),
    CONSTRAINT [CK_BeneficiaryRelationship_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_BeneficiaryRelationship_Code] UNIQUE NONCLUSTERED ([BeneficiaryRelationshipCode] ASC)
);

