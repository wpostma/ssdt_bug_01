﻿CREATE TABLE [code].[TransferSyntax] (
    [InternalTransferSyntaxID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [TransferSyntaxUID]        NVARCHAR (64)  NOT NULL,
    [TransferSyntaxName]       NVARCHAR (256) NULL,
    [IsActive]                 BIT            CONSTRAINT [DF_TransferSyntax_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]                  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_TransferSyntax] PRIMARY KEY CLUSTERED ([InternalTransferSyntaxID] ASC),
    CONSTRAINT [CK_TransferSyntax_ExtJson_Valid] CHECK (isjson([ExtJson])>(0))
);

