﻿CREATE TABLE [code].[InstanceAvailability] (
    [InstanceAvailabilityID]   INT            IDENTITY (1, 1) NOT NULL,
    [InstanceAvailabilityCode] NVARCHAR (64)  NOT NULL,
    [InstanceAvailabilityName] NVARCHAR (64)  NOT NULL,
    [IsActive]                 BIT            CONSTRAINT [DF_InstanceAvailability_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]                  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_InstanceAvailability] PRIMARY KEY CLUSTERED ([InstanceAvailabilityID] ASC),
    CONSTRAINT [CK_InstanceAvailability_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_InstanceAvailability_Code] UNIQUE NONCLUSTERED ([InstanceAvailabilityCode] ASC)
);

