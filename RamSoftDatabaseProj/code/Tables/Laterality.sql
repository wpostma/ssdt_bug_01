﻿CREATE TABLE [code].[Laterality] (
    [LateralityCode] CHAR (1)      NOT NULL,
    [Description]    NVARCHAR (64) NOT NULL,
    CONSTRAINT [PK_Laterality] PRIMARY KEY CLUSTERED ([LateralityCode] ASC)
);

