﻿CREATE TABLE [code].[FinancialClass] (
    [FinancialClassID]   INT            IDENTITY (1, 1) NOT NULL,
    [FinancialClassCode] NVARCHAR (16)  NOT NULL,
    [FinancialClass]     NVARCHAR (64)  NOT NULL,
    [Description]        NVARCHAR (256) NULL,
    [IsActive]           BIT            CONSTRAINT [DF_FinancialClass_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]            NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_FinancialClass] PRIMARY KEY CLUSTERED ([FinancialClassID] ASC),
    CONSTRAINT [U_FinancialClass_Code] UNIQUE NONCLUSTERED ([FinancialClassCode] ASC)
);

