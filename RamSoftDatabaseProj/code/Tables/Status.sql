﻿CREATE TABLE [code].[Status] (
    [StatusValue]         SMALLINT       NOT NULL,
    [StatusName]          NVARCHAR (64)  NOT NULL,
    [ExtJson]             NVARCHAR (MAX) NULL,
    [Favorite]            BIT            NULL,
    [IsAppointmentStatus] BIT            CONSTRAINT [DF_Status_IsAppointmentStatus] DEFAULT ((0)) NOT NULL,
    [IsActive]            BIT            CONSTRAINT [DF_Status_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED ([StatusValue] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Status_Name]
    ON [code].[Status]([StatusName] ASC);

