﻿CREATE TABLE [code].[EncounterClass] (
    [EncounterClassID]   INT            IDENTITY (1, 1) NOT NULL,
    [EncounterClassCode] NVARCHAR (64)  NOT NULL,
    [EncounterClassName] NVARCHAR (64)  NOT NULL,
    [IsActive]           BIT            CONSTRAINT [DF_EncounterClass_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]            NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_EncounterClass] PRIMARY KEY CLUSTERED ([EncounterClassID] ASC),
    CONSTRAINT [CK_EncounterClass_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_EncounterClass_Code] UNIQUE NONCLUSTERED ([EncounterClassCode] ASC)
);

