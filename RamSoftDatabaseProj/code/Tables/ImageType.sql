﻿CREATE TABLE [code].[ImageType] (
    [EntryID]      BIGINT        IDENTITY (1, 1) NOT NULL,
    [ModalityCode] NVARCHAR (8)  NOT NULL,
    [Segment]      TINYINT       NOT NULL,
    [Name]         NVARCHAR (16) NOT NULL,
    CONSTRAINT [PK_ImageType] PRIMARY KEY CLUSTERED ([EntryID] ASC),
    CONSTRAINT [FK_ImageType_Modality1] FOREIGN KEY ([ModalityCode]) REFERENCES [code].[Modality] ([ModalityCode]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_ImageTypeName]
    ON [code].[ImageType]([ModalityCode] ASC, [Segment] ASC, [Name] ASC);

