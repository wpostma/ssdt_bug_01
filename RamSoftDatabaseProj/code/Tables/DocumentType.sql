﻿CREATE TABLE [code].[DocumentType] (
    [DocumentTypeID] INT           NOT NULL,
    [DocumentType]   NVARCHAR (64) NOT NULL,
    [IsPatientLevel] BIT           CONSTRAINT [DF__DOCUMENTT__PATIE__367C1819] DEFAULT ((0)) NOT NULL,
    [IsPrior]        BIT           CONSTRAINT [DF_DocumentType_IsPrior] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_DocumentType] PRIMARY KEY CLUSTERED ([DocumentTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DocumentTypeName]
    ON [code].[DocumentType]([DocumentType] ASC);

