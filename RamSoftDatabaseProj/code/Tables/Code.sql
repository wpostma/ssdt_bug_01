﻿CREATE TABLE [code].[Code] (
    [CodeID]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [CodingScheme] NVARCHAR (16)  NOT NULL,
    [Value]        NVARCHAR (16)  NULL,
    [Description]  NVARCHAR (256) NULL,
    [IsActive]     BIT            CONSTRAINT [DF_Code_Active] DEFAULT ((1)) NOT NULL,
    [ExtJson]      NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_CodeID] PRIMARY KEY CLUSTERED ([CodeID] ASC),
    CONSTRAINT [FK_Code_CodingScheme] FOREIGN KEY ([CodingScheme]) REFERENCES [code].[CodingScheme] ([CodingScheme]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_Code_Scheme_Description]
    ON [code].[Code]([CodingScheme] ASC, [Description] ASC);

