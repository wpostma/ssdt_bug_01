﻿CREATE TABLE [code].[Sex] (
    [SexID]       INT            IDENTITY (1, 1) NOT NULL,
    [SexCode]     NVARCHAR (16)  NOT NULL,
    [Sex]         NVARCHAR (64)  NOT NULL,
    [Description] NVARCHAR (256) NULL,
    [IsActive]    BIT            CONSTRAINT [DF_Sex_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Sex] PRIMARY KEY CLUSTERED ([SexID] ASC),
    CONSTRAINT [U_Sex_Code] UNIQUE NONCLUSTERED ([SexCode] ASC)
);

