﻿CREATE TABLE [code].[AccountStatus] (
    [AccountStatusID]   INT            IDENTITY (1, 1) NOT NULL,
    [AccountStatusCode] NVARCHAR (16)  NOT NULL,
    [AccountStatus]     NVARCHAR (64)  NOT NULL,
    [Description]       NVARCHAR (256) NULL,
    [IsActive]          BIT            CONSTRAINT [DF_AccountStatus_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_AccountStatus] PRIMARY KEY CLUSTERED ([AccountStatusID] ASC),
    CONSTRAINT [U_AccountStatus_Code] UNIQUE NONCLUSTERED ([AccountStatusCode] ASC)
);

