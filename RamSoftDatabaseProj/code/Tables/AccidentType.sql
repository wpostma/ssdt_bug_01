﻿CREATE TABLE [code].[AccidentType] (
    [AccidentTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [AccidentTypeCode] NVARCHAR (64)  NOT NULL,
    [AccidentTypeName] NVARCHAR (64)  NOT NULL,
    [IsActive]         BIT            CONSTRAINT [DF_AccidentType_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_AccidentType] PRIMARY KEY CLUSTERED ([AccidentTypeID] ASC),
    CONSTRAINT [CK_AccidentType_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_AccidentType_Code] UNIQUE NONCLUSTERED ([AccidentTypeCode] ASC)
);

