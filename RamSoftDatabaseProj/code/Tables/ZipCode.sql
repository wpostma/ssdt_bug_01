﻿CREATE TABLE [code].[ZipCode] (
    [CountryCode] CHAR (2)      NOT NULL,
    [ZipCode]     NVARCHAR (16) NOT NULL,
    [City]        NVARCHAR (64) NOT NULL,
    [State]       NVARCHAR (16) NOT NULL,
    CONSTRAINT [ZIPCODES_PK_ZIPCODES] PRIMARY KEY NONCLUSTERED ([CountryCode] ASC, [ZipCode] ASC)
);

