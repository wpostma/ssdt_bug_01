﻿CREATE TABLE [code].[Holiday] (
    [Holiday] DATE NOT NULL,
    CONSTRAINT [PK_Holiday] PRIMARY KEY CLUSTERED ([Holiday] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [HOLIDAYLIST_U_HOLIDAYLIST_HOLIDAY]
    ON [code].[Holiday]([Holiday] ASC);

