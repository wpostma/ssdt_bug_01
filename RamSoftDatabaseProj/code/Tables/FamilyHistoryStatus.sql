﻿CREATE TABLE [code].[FamilyHistoryStatus] (
    [FamilyHistoryStatusID]   INT            IDENTITY (1, 1) NOT NULL,
    [FamilyHistoryStatusCode] NVARCHAR (64)  NOT NULL,
    [FamilyHistoryStatusName] NVARCHAR (64)  NOT NULL,
    [IsActive]                BIT            CONSTRAINT [DF_FamilyHistoryStatus_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]                 NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_FamilyHistoryStatus] PRIMARY KEY CLUSTERED ([FamilyHistoryStatusID] ASC),
    CONSTRAINT [CK_FamilyHistoryStatus_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_FamilyHistoryStatus_Code] UNIQUE NONCLUSTERED ([FamilyHistoryStatusCode] ASC)
);

