﻿CREATE TABLE [code].[InsuranceStatus] (
    [InsuranceStatusID]   INT            IDENTITY (1, 1) NOT NULL,
    [InsuranceStatusCode] NVARCHAR (64)  NOT NULL,
    [InsuranceStatusName] NVARCHAR (64)  NOT NULL,
    [IsActive]            BIT            CONSTRAINT [DF_InsuranceStatus_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]             NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_InsuranceStatus] PRIMARY KEY CLUSTERED ([InsuranceStatusID] ASC),
    CONSTRAINT [CK_InsuranceStatus_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_InsuranceStatus_Code] UNIQUE NONCLUSTERED ([InsuranceStatusCode] ASC)
);

