﻿CREATE TABLE [code].[LinkageType] (
    [LinkageTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [LinkageTypeCode] NVARCHAR (64)  NOT NULL,
    [LinkageTypeName] NVARCHAR (64)  NOT NULL,
    [IsActive]        BIT            CONSTRAINT [DF_LinkageType_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]         NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_LinkageType] PRIMARY KEY CLUSTERED ([LinkageTypeID] ASC),
    CONSTRAINT [CK_LinkageType_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_LinkageType_Code] UNIQUE NONCLUSTERED ([LinkageTypeCode] ASC)
);

