﻿CREATE TABLE [code].[CommunicationRequestStatus] (
    [CommunicationRequestStatusID]   INT            IDENTITY (1, 1) NOT NULL,
    [CommunicationRequestStatusCode] NVARCHAR (64)  NOT NULL,
    [CommunicationRequestStatusName] NVARCHAR (64)  NOT NULL,
    [IsActive]                       BIT            CONSTRAINT [DF_CommunicationRequestStatus_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]                        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_CommunicationRequestStatus] PRIMARY KEY CLUSTERED ([CommunicationRequestStatusID] ASC),
    CONSTRAINT [CK_CommunicationRequestStatus_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_CommunicationRequestStatus_Code] UNIQUE NONCLUSTERED ([CommunicationRequestStatusCode] ASC)
);

