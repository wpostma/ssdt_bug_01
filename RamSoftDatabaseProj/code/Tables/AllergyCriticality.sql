﻿CREATE TABLE [code].[AllergyCriticality] (
    [AllergyCriticalityID]   INT            IDENTITY (1, 1) NOT NULL,
    [AllergyCriticalityCode] NVARCHAR (64)  NOT NULL,
    [AllergyCriticalityName] NVARCHAR (64)  NOT NULL,
    [IsActive]               BIT            CONSTRAINT [DF_AllergyCriticality_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]                NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_AllergyCriticality] PRIMARY KEY CLUSTERED ([AllergyCriticalityID] ASC),
    CONSTRAINT [CK_AllergyCriticality_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_AllergyCriticality_Code] UNIQUE NONCLUSTERED ([AllergyCriticalityCode] ASC)
);

