﻿CREATE TABLE [code].[AccessType] (
    [AccessTypeID]   INT            NOT NULL,
    [AccessTypeName] NVARCHAR (256) NOT NULL,
    [IsActive]       BIT            CONSTRAINT [DF_AccessType_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_AccessType] PRIMARY KEY CLUSTERED ([AccessTypeID] ASC),
    CONSTRAINT [CK_AccessType_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_AccessType_Name] UNIQUE NONCLUSTERED ([AccessTypeName] ASC)
);

