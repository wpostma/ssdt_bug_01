﻿CREATE TABLE [code].[Ethnicity] (
    [EthnicityID]   INT            IDENTITY (1, 1) NOT NULL,
    [EthnicityCode] NVARCHAR (64)  NOT NULL,
    [EthnicityName] NVARCHAR (64)  NOT NULL,
    [Description]   NVARCHAR (256) NULL,
    [IsActive]      BIT            CONSTRAINT [DF_Ethnicity_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Ethnicity] PRIMARY KEY CLUSTERED ([EthnicityID] ASC),
    CONSTRAINT [CK_Ethnicity_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_Ethnicity_Code] UNIQUE NONCLUSTERED ([EthnicityCode] ASC)
);

