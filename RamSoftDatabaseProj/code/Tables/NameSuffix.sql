﻿CREATE TABLE [code].[NameSuffix] (
    [Suffix] NVARCHAR (8) NOT NULL,
    CONSTRAINT [PK_NameSuffix] PRIMARY KEY CLUSTERED ([Suffix] ASC)
);

