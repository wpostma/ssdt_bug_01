﻿CREATE TABLE [code].[CodingScheme] (
    [CodingScheme]            NVARCHAR (16)  NOT NULL,
    [HL7CodingScheme]         NVARCHAR (16)  NULL,
    [CodingSchemeDescription] NVARCHAR (16)  NULL,
    [CodingSchemeUID]         NVARCHAR (64)  NULL,
    [StartDate]               DATE           NULL,
    [EndDate]                 DATE           NULL,
    [ExtJson]                 NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_CodeSystem] PRIMARY KEY CLUSTERED ([CodingScheme] ASC)
);

