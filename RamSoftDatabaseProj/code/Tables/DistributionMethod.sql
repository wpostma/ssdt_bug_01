﻿CREATE TABLE [code].[DistributionMethod] (
    [DistributionMethodID] INT            IDENTITY (1, 1) NOT NULL,
    [DistributionMethod]   NVARCHAR (64)  NOT NULL,
    [IsActive]             BIT            CONSTRAINT [DF_DistributionMethod_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]              NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_DistributionMethod] PRIMARY KEY CLUSTERED ([DistributionMethodID] ASC),
    CONSTRAINT [CK_DistributionMethod_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_DistributionMethod] UNIQUE NONCLUSTERED ([DistributionMethod] ASC)
);

