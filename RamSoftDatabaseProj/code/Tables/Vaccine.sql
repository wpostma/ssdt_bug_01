﻿CREATE TABLE [code].[Vaccine] (
    [CVXCode]     NVARCHAR (5)  NOT NULL,
    [LastUpdate]  DATE          NOT NULL,
    [Description] NVARCHAR (64) NOT NULL,
    [IsActive]    BIT           CONSTRAINT [DF_Vaccine_IsActive] DEFAULT ((1)) NOT NULL,
    [Note]        TEXT          NULL,
    CONSTRAINT [PK_Vaccine] PRIMARY KEY CLUSTERED ([CVXCode] ASC)
);

