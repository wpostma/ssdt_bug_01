﻿CREATE TABLE [code].[Language] (
    [LanguageCode] CHAR (3)      NOT NULL,
    [Language]     NVARCHAR (64) NOT NULL,
    [IsActive]     BIT           CONSTRAINT [DF_Language_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED ([LanguageCode] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Language_Name]
    ON [code].[Language]([Language] ASC);

