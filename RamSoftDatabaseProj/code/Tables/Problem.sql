﻿CREATE TABLE [code].[Problem] (
    [InternalProblemCodeID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [ProblemCode]           NVARCHAR (64)  NOT NULL,
    [Description]           NVARCHAR (256) NOT NULL,
    [IsActive]              BIT            CONSTRAINT [DF_Problem_IsActive] DEFAULT ((1)) NOT NULL,
    [IsCritical]            BIT            CONSTRAINT [DF_Problem_IsCritical] DEFAULT ((0)) NOT NULL,
    [ExtJson]               NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Problem] PRIMARY KEY CLUSTERED ([InternalProblemCodeID] ASC),
    CONSTRAINT [CK_Problem_ExtJson_Valid] CHECK (isjson([ExtJson])>(0)),
    CONSTRAINT [U_Problem_Code] UNIQUE NONCLUSTERED ([ProblemCode] ASC)
);

