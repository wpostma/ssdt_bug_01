﻿CREATE TABLE [code].[ContactMethod] (
    [ContactMethodID]   INT            IDENTITY (1, 1) NOT NULL,
    [ContactMethodCode] NVARCHAR (64)  NOT NULL,
    [ContactMethodName] NVARCHAR (64)  NOT NULL,
    [IsActive]          BIT            CONSTRAINT [DF_ContactMethod_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ContactMethod] PRIMARY KEY CLUSTERED ([ContactMethodID] ASC),
    CONSTRAINT [CK_ContactMethod_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_ContactMethod_Code] UNIQUE NONCLUSTERED ([ContactMethodCode] ASC)
);

