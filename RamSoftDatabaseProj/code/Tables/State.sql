﻿CREATE TABLE [code].[State] (
    [CountryCode] CHAR (2)      NOT NULL,
    [State]       NVARCHAR (64) NULL,
    [StateID]     INT           IDENTITY (1, 1) NOT NULL,
    [StateCode]   NVARCHAR (16) NULL,
    [CountryID]   INT           NULL,
    CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED ([StateID] ASC),
    CONSTRAINT [FK_State_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
);


GO
CREATE NONCLUSTERED INDEX [IX_State_CountryCode_State]
    ON [code].[State]([CountryCode] ASC, [State] ASC);

