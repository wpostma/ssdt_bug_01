﻿CREATE TABLE [code].[ProcedureCode] (
    [InternalProcedureCodeID]   INT            IDENTITY (1, 1) NOT NULL,
    [ProcedureCode]             NVARCHAR (16)  NOT NULL,
    [RVUTechnical]              FLOAT (53)     CONSTRAINT [DF__PROCCODES__RVUTE__DFLT] DEFAULT ((0)) NOT NULL,
    [RVUProfessional]           FLOAT (53)     CONSTRAINT [DF__PROCCODES__RVUPRO_DFLT] DEFAULT ((0)) NOT NULL,
    [Description]               NVARCHAR (64)  NOT NULL,
    [IsActive]                  BIT            CONSTRAINT [DF__PROCCODES__ISACT__DFLT] DEFAULT ((1)) NOT NULL,
    [IsCritical]                BIT            CONSTRAINT [DF__PROCCODES__ISCRI__DFLT] DEFAULT ((0)) NOT NULL,
    [IsBillable]                BIT            CONSTRAINT [DF__PROCCODES__ISBIL__DFLT] DEFAULT ((1)) NOT NULL,
    [IsOfficeVisit]             BIT            CONSTRAINT [DF__PROCCODES__ISOFF__DFLT] DEFAULT ((0)) NOT NULL,
    [IsSeenByPhysician]         BIT            CONSTRAINT [DF__PROCCODES__SEENB__DFLT] DEFAULT ((1)) NOT NULL,
    [IsMammo]                   BIT            CONSTRAINT [DF__PROCCODES__ISMAM__DFLT] DEFAULT ((0)) NOT NULL,
    [ExtJson]                   NVARCHAR (MAX) NULL,
    [InternalProcedureStatusID] INT            NOT NULL,
    CONSTRAINT [PK_ProcedureCodeIdentity] PRIMARY KEY CLUSTERED ([InternalProcedureCodeID] ASC),
    CONSTRAINT [FK_ProcedureCode_ProcedureStatus] FOREIGN KEY ([InternalProcedureStatusID]) REFERENCES [code].[ProcedureStatus] ([InternalProcedureStatusID]),
    CONSTRAINT [UN_PROCCODE] UNIQUE NONCLUSTERED ([ProcedureCode] ASC)
);

