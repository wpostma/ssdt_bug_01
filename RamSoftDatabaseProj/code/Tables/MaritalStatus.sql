﻿CREATE TABLE [code].[MaritalStatus] (
    [MaritalStatusCode] NVARCHAR (3)   NOT NULL,
    [MaritalStatus]     NVARCHAR (64)  NOT NULL,
    [Description]       NVARCHAR (256) NOT NULL,
    [IsActive]          BIT            CONSTRAINT [DF_MaritalStatus_IsActive] DEFAULT ((1)) NOT NULL,
    [MaritalStatusID]   INT            IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_MaritalStatus] PRIMARY KEY CLUSTERED ([MaritalStatusID] ASC),
    CONSTRAINT [U_MaritalStatus_Code] UNIQUE NONCLUSTERED ([MaritalStatusCode] ASC)
);

