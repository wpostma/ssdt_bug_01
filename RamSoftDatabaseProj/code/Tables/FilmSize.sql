﻿CREATE TABLE [code].[FilmSize] (
    [FilmSizeCode] NVARCHAR (16) NOT NULL,
    [Width]        REAL          NOT NULL,
    [Height]       REAL          NOT NULL,
    CONSTRAINT [PK_FilmSize] PRIMARY KEY CLUSTERED ([FilmSizeCode] ASC)
);

