﻿CREATE TABLE [code].[EncounterState] (
    [EncounterStateID]   INT            IDENTITY (1, 1) NOT NULL,
    [EncounterStateCode] NVARCHAR (64)  NOT NULL,
    [EncounterStateName] NVARCHAR (64)  NOT NULL,
    [IsActive]           BIT            CONSTRAINT [DF_EncounterState_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]            NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_EncounterState] PRIMARY KEY CLUSTERED ([EncounterStateID] ASC),
    CONSTRAINT [CK_EncounterState_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_EncounterState_Code] UNIQUE NONCLUSTERED ([EncounterStateCode] ASC)
);

