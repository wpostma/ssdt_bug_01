﻿CREATE TABLE [code].[UniversalEntityIDType] (
    [UniversalEntityIDTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [UniversalEntityIDTypeCode] NVARCHAR (64)  NOT NULL,
    [Description]               NVARCHAR (512) NOT NULL,
    [IsActive]                  BIT            CONSTRAINT [DF_UniversalEntityIDType_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]                   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_UniversalEntityIDType] PRIMARY KEY CLUSTERED ([UniversalEntityIDTypeID] ASC),
    CONSTRAINT [CK_UniversalEntityIDType_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_UniversalEntityIDType_Code] UNIQUE NONCLUSTERED ([UniversalEntityIDTypeCode] ASC)
);

