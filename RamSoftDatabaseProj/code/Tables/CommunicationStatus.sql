﻿CREATE TABLE [code].[CommunicationStatus] (
    [CommunicationStatusID]   INT            NOT NULL,
    [CommunicationStatusCode] NVARCHAR (64)  NOT NULL,
    [CommunicationStatusName] NVARCHAR (64)  NOT NULL,
    [IsActive]                BIT            CONSTRAINT [DF_CommunicationStatus_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]                 NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_CommunicationStatus] PRIMARY KEY CLUSTERED ([CommunicationStatusID] ASC),
    CONSTRAINT [CK_CommunicationStatus_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_CommunicationStatus_Code] UNIQUE NONCLUSTERED ([CommunicationStatusCode] ASC)
);

