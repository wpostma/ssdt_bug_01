﻿CREATE TABLE [code].[TimeZoneType] (
    [TimeZoneTypeID]   INT            NOT NULL,
    [TimeZoneTypeName] NVARCHAR (256) NOT NULL,
    [IsActive]         BIT            CONSTRAINT [DF_TimeZoneType_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_TimeZoneType] PRIMARY KEY CLUSTERED ([TimeZoneTypeID] ASC),
    CONSTRAINT [CK_TimeZoneType_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0))
);

