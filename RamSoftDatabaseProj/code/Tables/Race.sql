﻿CREATE TABLE [code].[Race] (
    [RaceID]      INT            IDENTITY (1, 1) NOT NULL,
    [RaceCode]    NVARCHAR (64)  NOT NULL,
    [RaceName]    NVARCHAR (64)  NOT NULL,
    [Description] NVARCHAR (256) NULL,
    [IsActive]    BIT            CONSTRAINT [DF_Race_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Race] PRIMARY KEY CLUSTERED ([RaceID] ASC),
    CONSTRAINT [CK_Race_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_Race_Code] UNIQUE NONCLUSTERED ([RaceCode] ASC)
);

