﻿CREATE TABLE [code].[Annotation] (
    [Annotation] NVARCHAR (64) NOT NULL,
    CONSTRAINT [PK_Annotation] PRIMARY KEY CLUSTERED ([Annotation] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Some text that a user (a physician) may want to commonly place onto a diagnostic image as an annotation.', @level0type = N'SCHEMA', @level0name = N'code', @level1type = N'TABLE', @level1name = N'Annotation', @level2type = N'COLUMN', @level2name = N'Annotation';

