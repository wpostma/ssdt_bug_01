﻿CREATE TABLE [code].[SOPClass] (
    [InternalSOPClassID]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [SOPClassUID]         NVARCHAR (64)  NOT NULL,
    [SOPClassName]        NVARCHAR (256) NULL,
    [ModalityCode]        NVARCHAR (8)   NULL,
    [IsImage]             BIT            CONSTRAINT [DF_SOPClass_IsImage] DEFAULT ((0)) NOT NULL,
    [IsDocument]          BIT            CONSTRAINT [DF_SOPClass_IsDocument] DEFAULT ((0)) NOT NULL,
    [IsPresentationState] BIT            CONSTRAINT [DF_SOPClass_IsPresentationState] DEFAULT ((0)) NOT NULL,
    [IsCAD]               BIT            CONSTRAINT [DF_SOPClass_IsCAD] DEFAULT ((0)) NOT NULL,
    [IsKO]                BIT            CONSTRAINT [DF_SOPClass_IsKO] DEFAULT ((0)) NOT NULL,
    [IsAudio]             BIT            CONSTRAINT [DF_SOPClass_IsAudio] DEFAULT ((0)) NOT NULL,
    [IsActive]            BIT            CONSTRAINT [DF_SOPClass_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]             NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_SOPClass] PRIMARY KEY CLUSTERED ([InternalSOPClassID] ASC),
    CONSTRAINT [FK_SOPClass_Modality] FOREIGN KEY ([ModalityCode]) REFERENCES [code].[Modality] ([ModalityCode])
);

