﻿CREATE TABLE [code].[NamePrefix] (
    [Prefix] NVARCHAR (8) NOT NULL,
    CONSTRAINT [PK_NamePrefix] PRIMARY KEY CLUSTERED ([Prefix] ASC)
);

