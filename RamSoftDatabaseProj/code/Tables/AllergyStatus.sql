﻿CREATE TABLE [code].[AllergyStatus] (
    [AllergyStatusID]    INT            IDENTITY (1, 1) NOT NULL,
    [AllergyStatusCode]  NVARCHAR (64)  NOT NULL,
    [AllergyStatusName]  NVARCHAR (64)  NOT NULL,
    [AllergyStatusLevel] INT            NULL,
    [IsActive]           BIT            CONSTRAINT [DF_AllergyStatus_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]            NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_AllergyStatus] PRIMARY KEY CLUSTERED ([AllergyStatusID] ASC),
    CONSTRAINT [CK_AllergyStatus_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_AllergyStatus_Code] UNIQUE NONCLUSTERED ([AllergyStatusCode] ASC)
);

