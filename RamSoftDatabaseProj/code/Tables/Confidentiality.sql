﻿CREATE TABLE [code].[Confidentiality] (
    [ConfidentialityID]    INT            IDENTITY (1, 1) NOT NULL,
    [ConfidentialityCode]  NVARCHAR (64)  NOT NULL,
    [ConfidentialityName]  NVARCHAR (64)  NOT NULL,
    [ConfidentialityLevel] INT            NULL,
    [IsActive]             BIT            CONSTRAINT [DF_Confidentiality_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]              NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Confidentiality] PRIMARY KEY CLUSTERED ([ConfidentialityID] ASC),
    CONSTRAINT [CK_Confidentiality_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_Confidentiality_Code] UNIQUE NONCLUSTERED ([ConfidentialityCode] ASC)
);

