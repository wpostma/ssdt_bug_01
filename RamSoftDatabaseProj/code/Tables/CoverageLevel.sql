﻿CREATE TABLE [code].[CoverageLevel] (
    [CoverageLevelID] SMALLINT       NOT NULL,
    [CoverageLevel]   NVARCHAR (64)  NOT NULL,
    [IsActive]        BIT            CONSTRAINT [DF_CoverageLevel_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]         NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_CoverageLevel] PRIMARY KEY CLUSTERED ([CoverageLevelID] ASC),
    CONSTRAINT [CK_CoverageLevel_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0))
);

