﻿CREATE TABLE [code].[Relationship] (
    [RelationshipID]   INT            IDENTITY (1, 1) NOT NULL,
    [RelationshipName] NVARCHAR (64)  NOT NULL,
    [IsActive]         BIT            CONSTRAINT [DF_Relationship_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]          NVARCHAR (MAX) NULL,
    [RelationshipCode] NVARCHAR (64)  NULL,
    CONSTRAINT [PK_Relationship] PRIMARY KEY CLUSTERED ([RelationshipID] ASC),
    CONSTRAINT [CK_Relationship_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_Relationship_Code] UNIQUE NONCLUSTERED ([RelationshipCode] ASC)
);

