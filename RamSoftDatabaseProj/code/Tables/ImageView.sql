﻿CREATE TABLE [code].[ImageView] (
    [ViewID]       BIGINT        IDENTITY (1, 1) NOT NULL,
    [ModalityCode] NVARCHAR (8)  NOT NULL,
    [ViewCode]     NVARCHAR (16) NULL,
    [ModifierCode] NVARCHAR (16) NULL,
    [ViewName]     NVARCHAR (16) NOT NULL,
    [IsActive]     BIT           CONSTRAINT [DF_MammoView_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_MammoView] PRIMARY KEY CLUSTERED ([ViewID] ASC),
    CONSTRAINT [FK_ImageView_Modality] FOREIGN KEY ([ModalityCode]) REFERENCES [code].[Modality] ([ModalityCode]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_MammoView_ViewModifier]
    ON [code].[ImageView]([ViewCode] ASC, [ModifierCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MGView_ViewName]
    ON [code].[ImageView]([ViewName] ASC);

