﻿CREATE TABLE [code].[Diagnosis] (
    [CodingScheme]            NVARCHAR (16)  CONSTRAINT [DF__DIAGCODES__INTER__0C26B6F1] DEFAULT ((1)) NOT NULL,
    [DiagnosisCode]           NVARCHAR (16)  NOT NULL,
    [Description]             NVARCHAR (256) NOT NULL,
    [IsActive]                BIT            CONSTRAINT [DF__DIAGCODES__ISACT__0A3E6E7F] DEFAULT ((1)) NOT NULL,
    [IsCritical]              BIT            CONSTRAINT [DF__DIAGCODES__ISCRI__0B3292B8] DEFAULT ((0)) NOT NULL,
    [ExtJson]                 NVARCHAR (MAX) NULL,
    [InternalDiagnosisCodeID] BIGINT         IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Diagnosis] PRIMARY KEY CLUSTERED ([InternalDiagnosisCodeID] ASC),
    CONSTRAINT [U_Diagnosis] UNIQUE NONCLUSTERED ([CodingScheme] ASC, [DiagnosisCode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Diagnosis_Description]
    ON [code].[Diagnosis]([CodingScheme] ASC, [Description] ASC);

