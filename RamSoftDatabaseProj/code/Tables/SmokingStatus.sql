﻿CREATE TABLE [code].[SmokingStatus] (
    [SmokingStatusID]   INT            IDENTITY (1, 1) NOT NULL,
    [SmokingStatusCode] NVARCHAR (64)  NOT NULL,
    [SmokingStatusName] NVARCHAR (64)  NOT NULL,
    [Description]       NVARCHAR (256) NULL,
    [IsActive]          BIT            CONSTRAINT [DF_SmokingStatus_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_SmokingStatus] PRIMARY KEY CLUSTERED ([SmokingStatusID] ASC),
    CONSTRAINT [CK_SmokingStatus_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_SmokingStatus_Code] UNIQUE NONCLUSTERED ([SmokingStatusCode] ASC)
);

