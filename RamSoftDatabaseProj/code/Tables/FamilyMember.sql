﻿CREATE TABLE [code].[FamilyMember] (
    [FamilyMemberID]   INT            IDENTITY (1, 1) NOT NULL,
    [FamilyMemberCode] NVARCHAR (64)  NOT NULL,
    [FamilyMemberName] NVARCHAR (64)  NOT NULL,
    [IsActive]         BIT            CONSTRAINT [DF_FamilyMember_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJSON]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_FamilyMember] PRIMARY KEY CLUSTERED ([FamilyMemberID] ASC),
    CONSTRAINT [CK_FamilyMember_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_FamilyMember_Code] UNIQUE NONCLUSTERED ([FamilyMemberCode] ASC)
);

