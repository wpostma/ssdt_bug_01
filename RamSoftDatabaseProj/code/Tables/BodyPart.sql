﻿CREATE TABLE [code].[BodyPart] (
    [InternalBodyPartID] BIGINT        IDENTITY (1, 1) NOT NULL,
    [CodingScheme]       NVARCHAR (16) NOT NULL,
    [Value]              NVARCHAR (16) NOT NULL,
    [Description]        NVARCHAR (64) NOT NULL,
    [BodyPartExamined]   NVARCHAR (16) NULL,
    CONSTRAINT [PK_BodyPart] PRIMARY KEY CLUSTERED ([InternalBodyPartID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BodyPart_CodeSchemeValue]
    ON [code].[BodyPart]([CodingScheme] ASC, [Value] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_BodyPart_BodyPartExamined]
    ON [code].[BodyPart]([BodyPartExamined] ASC);

