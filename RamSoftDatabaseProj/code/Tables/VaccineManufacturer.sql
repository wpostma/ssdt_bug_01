﻿CREATE TABLE [code].[VaccineManufacturer] (
    [MVXCode]          NVARCHAR (3)   NOT NULL,
    [ManufacturerName] NVARCHAR (128) NOT NULL,
    [IsActive]         BIT            CONSTRAINT [DF_VaccineManufacturer_IsActive_1] DEFAULT ((1)) NOT NULL,
    [LastUpdate]       DATE           NOT NULL,
    CONSTRAINT [PK_VaccineManufacturer_1] PRIMARY KEY CLUSTERED ([MVXCode] ASC)
);

