﻿CREATE TABLE [code].[RadlexCode] (
    [InternalCodeID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [CodeType]       NVARCHAR (64)  NOT NULL,
    [CodeID]         NVARCHAR (16)  NOT NULL,
    [Name]           NVARCHAR (256) NOT NULL,
    [IsActive]       BIT            CONSTRAINT [DF_RadlexCode_Active] DEFAULT ((1)) NOT NULL,
    [ExtJson]        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_RadlexCode_InternalCodeID] PRIMARY KEY CLUSTERED ([InternalCodeID] ASC),
    CONSTRAINT [UX_RadlexCode] UNIQUE NONCLUSTERED ([CodeType] ASC, [Name] ASC)
);

