﻿CREATE TABLE [code].[StudyPlayerType] (
    [StudyPlayerType] INT            NOT NULL,
    [Description]     NVARCHAR (256) NOT NULL,
    [IsActive]        BIT            CONSTRAINT [DF_STUDYPLAYERTYPE_ACTIVE] DEFAULT ((1)) NOT NULL,
    [ExtJson]         NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_StudyPlayerType] PRIMARY KEY CLUSTERED ([StudyPlayerType] ASC),
    CONSTRAINT [CK_StudyPlayerType_ExtJSON_Valid] CHECK (isjson([ExtJSON])>(0))
);

