﻿CREATE TABLE [code].[EmploymentStatus] (
    [EmploymentStatusID]   INT            IDENTITY (1, 1) NOT NULL,
    [EmploymentStatusCode] NVARCHAR (64)  NOT NULL,
    [EmploymentStatusName] NVARCHAR (64)  NOT NULL,
    [Description]          NVARCHAR (256) NULL,
    [IsActive]             BIT            CONSTRAINT [DF_EmploymentStatus_IsActive] DEFAULT ((1)) NOT NULL,
    [ExtJson]              NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_EmploymentStatus] PRIMARY KEY CLUSTERED ([EmploymentStatusID] ASC),
    CONSTRAINT [CK_EmploymentStatus_ExtJson_Valid] CHECK (isjson([ExtJSON])>(0)),
    CONSTRAINT [U_EmploymentStatus_Code] UNIQUE NONCLUSTERED ([EmploymentStatusCode] ASC)
);

